WORKFLOW

RuNNer
1. Copy the current input.data file of your RuNNer fits to the directory RuNNer.
2. Copy the input.nn, scaling.data, and weights.XXX.data files from two different RuNNer fits using,
   for example, different neural network architectures and initial weights, to the directories
   RuNNer/HDNNP_1 and RuNNer/HDNNP_2, respectively.

simulation
1. Copy the simulation.lammps_XXX file of choice from the directory simulation to the parent
   directory and name it simulation.lammps.

RuNNerActiveLearn.py
0. Make sure that RuNNerActiveLearn.py has execute permission (chmod u+x RuNNerActiveLearn.py).
1. Adjust the settings in RuNNerActiveLearn.py.
2. Run RuNNerActiveLearn.py in mode 1.

submit
1. Choose the submit scripts submit_mode1.sh and submit_mode2.sh from the directory submit
   according to the installed job scheduler and copy it to the parent directory.
2. According to your compute system you have to modify some general settings of the submit scripts
   as the name of the queue, number of processors, wallclock time, modules which have to be loaded,
   name of the LAMMPS executable, or the name of the scratch directory. OMP_NUM_THREADS in
   submit_mode2.sh has to be half of the number of usable cores as two RuNNer fits are run
   simultaneously.
3. If you want to run more simulations than max_len_joblist, create as many submit_mode1_X.sh
   scripts in the parent directory.
4. Adjust the number of jobs and the name of the joblist file in each submit_mode1.sh file.
5. Submit the submit_mode1.sh script(s).
6. After all simulations are finished submit the submit_mode2.sh script.


RESULTS

mode2.out contains information about the found structures and the performance of the RuNNer fits.

extrapolation_statistics_X.dat provides information of the normalised symmetry function value
extrapolations for all structures in input.data-add.

According to the data of the above mentioned files you might want to restart the selection process
with different settings of deltaE, deltaF, all_extrapolated_structures, or exceptions. These
settings can be changed in RuNNerActiveLearn.py and they do not require to perform the RuNNer
calculations in mode 2 again leading to a fast executation of RuNNerActiveLearn.py in mode 2. Other
changes, for example, different d_mins or the timestep separation values require to redo the RuNNer
calculations.

input.data-new provides all structures which will be recalculated using RuNNer and both fits.
Additionally, extrapolation information is given for extrapolated structures.

input.data-add provides the missing structures which have to be recalculated with the reference
method and added to the reference data set.
