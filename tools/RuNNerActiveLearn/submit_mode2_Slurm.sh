#!/bin/bash
#SBATCH --job-name=RuNNerActiveLearn_2
#SBATCH -p all
#SBATCH -N 1-1
#SBATCH -n 8
#SBATCH -t 24:00:00
#SBATCH -o /dev/null

export OMP_NUM_THREADS=4

ulimit -s unlimited
module load intel/18.0.2.199
cd ${SLURM_SUBMIT_DIR}

${SLURM_SUBMIT_DIR}/RuNNerActiveLearn.py 2 > ${SLURM_SUBMIT_DIR}/mode2.out
