#!/bin/bash
#PBS -N RuNNerActiveLearn_2
#PBS -q workq
#PBS -l nodes=1:ppn=8
#PBS -j oe
#PBS -o /dev/null

export OMP_NUM_THREADS=4

ulimit -s unlimited
module load intel/18.0.2.199
cd ${PBS_O_WORKDIR}

${PBS_O_WORKDIR}/RuNNerActiveLearn.py 2 > ${PBS_O_WORKDIR}/mode2.out
