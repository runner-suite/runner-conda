#!/bin/bash
#SBATCH --job-name=RuNNerActiveLearn_1
#SBATCH -p all
#SBATCH -N 1-1
#SBATCH -n 8
#SBATCH -t 01:00:00
#SBATCH -o /dev/null
#SBATCH --array=1-1000

path=$(sed -n ${SLURM_ARRAY_TASK_ID}p ${SLURM_SUBMIT_DIR}/joblist_mode1.dat)

ulimit -s unlimited
module load intel/18.0.2.199

dir=$(date '+%Y%m%d_%H%M%S_%N')
mkdir /scratch2/${dir}
rsync -a ${SLURM_SUBMIT_DIR}/mode1/${path}/* /scratch2/${dir}/
cd /scratch2/${dir}

mpirun -np ${SLURM_NTASKS} lammps.x -in input.lammps -screen none

${SLURM_SUBMIT_DIR}/RuNNerActiveLearn.py 1a

rm -r /scratch2/${dir}/RuNNer
rsync -a /scratch2/${dir}/* ${SLURM_SUBMIT_DIR}/mode1/${path}/
cd ${SLURM_SUBMIT_DIR}/mode1/${path}
if [ "${dir}" != "" ]
then
  rm -r /scratch2/${dir}
fi
