#!/bin/bash
#PBS -N RuNNerActiveLearn_1
#PBS -q workq
#PBS -l nodes=1:ppn=8
#PBS -j oe
#PBS -o /dev/null
#PBS -J 1-1000

path=$(sed -n ${PBS_ARRAY_INDEX}p ${PBS_O_WORKDIR}/joblist_mode1.dat)

ulimit -s unlimited
module load intel/18.0.2.199

dir=$(date '+%Y%m%d_%H%M%S_%N')
mkdir /scratch2/${dir}
rsync -a ${PBS_O_WORKDIR}/mode1/${path}/* /scratch2/${dir}/
cd /scratch2/${dir}

mpirun -np 8 lammps.x -in input.lammps -screen none

${PBS_O_WORKDIR}/RuNNerActiveLearn.py 1a

rm -r /scratch2/${dir}/RuNNer
rsync -a /scratch2/${dir}/* ${PBS_O_WORKDIR}/mode1/${path}/
cd ${PBS_O_WORKDIR}/mode1/${path}
if [ "${dir}" != "" ]
then
  rm -r /scratch2/${dir}
fi
