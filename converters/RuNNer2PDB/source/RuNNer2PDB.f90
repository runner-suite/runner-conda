      program RuNNer2PDB
      implicit none

      integer i1,i2,i3
      integer, dimension(:) , allocatable :: n_atoms
      integer nlattice
      integer nx,ny,nz
      integer getarg
      integer i
      integer n
      integer icount
      integer nstruct
      integer istruct

      real*8 lattice(3,3)
      real*8 latticenew(3,3)
      real*8, dimension(:,:)  , allocatable :: xyz 
      real*8, dimension(:,:)  , allocatable :: fxyz 
      real*8, dimension(:)  , allocatable :: atomcharge 
      real*8, dimension(:)  , allocatable :: atomenergy
      real*8 charge
      real*8 energy 

      character*8 :: arg
      character*8 :: dummy
      character*2, dimension(:) , allocatable :: elemsymb

      logical, dimension(:), allocatable :: lperiodic
      logical lperiodic_local

! initializations
      nlattice=0
      nstruct=0
      energy =0.0d0
      charge =0.0d0
      icount=0
      lattice(:,:)=0.0d0

! count structures
      open(20,file='input.data',form='formatted',status='old')
      rewind(20)
 98   continue
      read(20,*,END=99)dummy
      if(dummy.eq.'begin')then
        nstruct=nstruct+1
      endif
      goto 98
 99   continue
      close(20)
      write(*,*)'Number of structures in input.data ',nstruct

      allocate(n_atoms(nstruct))
      allocate(lperiodic(nstruct))
      n_atoms(:)=0
      lperiodic(:)=.false.

! count atoms/ check if we have a periodic structure
      istruct=0
      open(20,file='input.data',form='formatted',status='old')
      rewind(20)
 11   continue
      read(20,*,END=12)dummy
!      write(*,*)'read ',dummy
      if(dummy.eq.'begin')then
        istruct=istruct+1
        nlattice=0
!        write(*,*)'new structure found ',istruct
        goto 11
      elseif(dummy.eq.'lattice')then
        nlattice=nlattice+1
        backspace(20)
        read(20,*)dummy,lattice(nlattice,1),lattice(nlattice,2),&
        lattice(nlattice,3)
        goto 11
      elseif(dummy.eq.'atom')then
        n_atoms(istruct)=n_atoms(istruct)+1
!        write(*,*)'atom found ',n_atoms(istruct)
        goto 11
      elseif(dummy.eq.'charge')then
        backspace(20)
        read(20,*)dummy,charge
        goto 11
      elseif(dummy.eq.'comment')then
        goto 11
      elseif(dummy.eq.'c')then
        goto 11
      elseif(dummy.eq.'energy')then
        backspace(20)
        read(20,*)dummy,energy
        goto 11
      elseif(dummy.eq.'end')then
        if(nlattice.eq.3)then
          lperiodic(istruct)=.true.
        endif
        goto 11
      endif
 12   continue
      close(20)

      write(*,*)'Structure, number of atoms, lperiodic'
      do i1=1,nstruct
        write(*,*)i1,n_atoms(i1),lperiodic(i1)
      enddo



      open(20,file='input.data',form='formatted',status='old')
      rewind(20)
      open(21,file='input.pdb',form='formatted',status='replace')
      rewind(21)
      istruct=1

 13   continue
! allocation of arrays
      allocate(xyz(n_atoms(istruct),3))
      allocate(fxyz(n_atoms(istruct),3))
      allocate(atomcharge(n_atoms(istruct)))
      allocate(atomenergy(n_atoms(istruct)))
      allocate(elemsymb(n_atoms(istruct)))
      nlattice=0
      icount=0
 80   continue
      read(20,*,END=15)dummy
      if(dummy.eq.'lattice')then
        backspace(20)
        nlattice=nlattice+1
        read(20,*)dummy,&
        lattice(nlattice,1),lattice(nlattice,2),lattice(nlattice,3)
      endif
      if(dummy.eq.'atom')then
        backspace(20)
        icount=icount+1
        read(20,*)dummy,xyz(icount,1),xyz(icount,2),xyz(icount,3),&
        elemsymb(icount),atomcharge(icount),atomenergy(icount),&
        fxyz(icount,1),fxyz(icount,2),fxyz(icount,3)
      endif
      if(dummy.eq.'end')then
        goto 14
      endif
      goto 80
 14   continue     


      call writepdb(21,n_atoms(istruct),n_atoms(istruct),&
           lattice,xyz,atomcharge,atomenergy,elemsymb,lperiodic(istruct),.false.)

      deallocate(xyz)
      deallocate(fxyz)
      deallocate(atomcharge)
      deallocate(atomenergy)
      deallocate(elemsymb)

      icount=0
      nlattice=0
      istruct=istruct+1
      if(istruct.gt.nstruct)goto 15
      goto 13

 15   continue     
      close(20)
      close(21)

      deallocate(n_atoms)
      deallocate(lperiodic)
      end


!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!1

      subroutine writepdb(iunit,max_num_atoms,num_atoms,&
           lattice,xyzstruct,atomcharge,atomenergy,elementsymbol,lperiodic,ldebug)

      implicit none

      integer iunit                    ! in
      integer max_num_atoms            ! in
      integer num_atoms                ! in

      integer i1,i2                    ! internal

      real*8 lattice(3,3)               ! in
      real*8 xyzstruct(max_num_atoms,3) ! in
      real*8 a                          ! internal
      real*8 b                          ! internal
      real*8 c                          ! internal
      real*8 alpha                      ! internal
      real*8 beta                       ! internal
      real*8 gamm                       ! internal
      real*8 pi                         ! internal
      real*8 atomcharge(max_num_atoms)
      real*8 atomenergy(max_num_atoms)
      real*8 shift
      real*8 factor 

      character*2 elementsymbol(max_num_atoms) ! in

      logical lperiodic                ! in
      logical ldebug                   ! in
      logical lexist

      pi=3.14159265359d0
      shift=0.0d0
      factor=1.0d0

! Note: Angstrom length unit
! Caution: pdbs are fixed formatted

      inquire(file="runnerpdb.in",exist=lexist)
      if(lexist)then
        open(40,file="runnerpdb.in",form="formatted",status="old")
        rewind(40)
          read(40,*)shift
          read(40,*)factor
        close(40)
      endif

      atomenergy(:)=(atomenergy(:)+shift)*factor
      atomcharge(:)=(atomcharge(:)+shift)*factor


      if(lperiodic)then
! pdb requires a,b,c,alpha,beta,gamma instead of lattice vectors
        a=lattice(1,1)**2 + lattice(1,2)**2 + lattice(1,3)**2
        a=dsqrt(a)
!        a=a*0.529177d0
        b=lattice(2,1)**2 + lattice(2,2)**2 + lattice(2,3)**2
        b=dsqrt(b)
!        b=b*0.529177d0
        c=lattice(3,1)**2 + lattice(3,2)**2 + lattice(3,3)**2
        c=dsqrt(c)
!        c=c*0.529177d0
! calculate angles properly!!!
        alpha=abs(lattice(2,1)*lattice(3,1)+lattice(2,2)*lattice(3,2)+lattice(2,3)*lattice(3,3))/(b*c)
        alpha=acos(alpha)
        alpha=alpha*180.d0/pi
        beta =abs(lattice(1,1)*lattice(3,1)+lattice(1,2)*lattice(3,2)+lattice(1,3)*lattice(3,3))/(a*c)
        beta =acos(beta)
        beta =beta*180.d0/pi
        gamm =abs(lattice(1,1)*lattice(2,1)+lattice(1,2)*lattice(2,2)+lattice(1,3)*lattice(2,3))/(a*b)
        gamm =acos(gamm)
        gamm =gamm*180.d0/pi

        write(iunit,'(a)')'pdb generated by RuNNer'
        write(iunit,'(a6,3f9.3,3f7.2,1x,a15)')"CRYST1 ",&
        a*0.529177d0,b*0.529177d0,c*0.529177d0,alpha,beta,gamm,"P 1           1"
        write(iunit,'(a)')'MODEL 0'
        do i1=1,num_atoms
          write(iunit,201) i1,elementsymbol(i1),&
          i1,xyzstruct(i1,1)*0.529177d0,xyzstruct(i1,2)*0.529177d0,&
          xyzstruct(i1,3)*0.529177d0,atomcharge(i1),atomenergy(i1),elementsymbol(i1)
        enddo
        write(iunit,'(a)')'TER'
        write(iunit,'(a)')'ENDMDL'
      else ! lperiodic
        write(iunit,'(a)')'pdb generated by RuNNer'
        do i1=1,num_atoms
          write(iunit,201) i1,elementsymbol(i1),&
          i1,xyzstruct(i1,1)*0.529177d0,xyzstruct(i1,2)*0.529177d0,&
          xyzstruct(i1,3)*0.529177d0,atomcharge(i1),atomenergy(i1),elementsymbol(i1)
        enddo
        write(iunit,'(a)')'END'
      endif ! lperiodic


201 FORMAT ("HETATM",i5,a3,3x,"DCB",i6,4x,3f8.3,2f6.2,10x,a2)
!201 FORMAT ("HETATM",i5,a3,3x,"DCB",i6,4x,3f8.3,"  1.00  0.00")

      return
      end

