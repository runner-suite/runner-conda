Readme for RuNNer2PDB.x
-----------------------

Purpose: converts input.data to input.pdb
Also the atom charges and atom energies are transformed to the occupancy and the temperature factor in the pdb file for visualisation with VMD.

input: input.data (RuNNer format)
       runnerpdb.in (optional, contains a shifting factor and a scaling factor for the atomic charges and energies)

output: input.pdb (pbd format)

usage: ./RuNNer2PDB.x

converts the RuNNer input.data file into the pdb format, works also for trajectories
visualization of the pdb is possible with vmd: vmd input.pdb

Hint for colored atoms in VMD according to charge:
Go to -graphical representation/Draw style/Coloring Method: Occupancy

RuNNer2PDB works also for a number of structures with different numbers of atoms, but then VMD will only visualize the structures, which have the same number of atoms as the first frame.
