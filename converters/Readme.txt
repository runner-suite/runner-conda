This folder contains converters for the input.data file format of RuNNer to other common formats.
=================================================================================================

Available converters:

RuNNer2PDB: input.data -> pdb files (including periodic boundary conditions)


