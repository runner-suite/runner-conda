<!-- Project title -->
## RuNNer

<!-- Badges -->
<!-- [![pipeline status](https://gitlab.com/TheochemGoettingen/RuNner/badges/master/pipeline.svg)](https://gitlab.com/TheoChemGoettingen/RuNNer/-/commits/master) -->
[![Documentation](https://img.shields.io/badge/Documentation-latest-blue.svg?style=for-the-badge)](https://theochemgoettingen.gitlab.io/RuNNer)

<!-- Short description -->
The RuNNer Neural Network Energy Representation framework for the construction of high-dimensional neural network potentials.

WARNING: THIS FILE IS PART OF THE UNMAINTAINED CONDA PACKAGE OF RUNNER 1.3.

<!-- TABLE OF CONTENTS -->
<details open="open">
  <summary>Table of Contents</summary>
  <ol>
    <li>
      <a href="#about-the-project">About The Project</a>
      <ul>
        <li><a href="#built-with">Built With</a></li>
      </ul>
    </li>
    <li>
      <a href="#documentation-development">Documentation Development</a>
    </li>
    <li><a href="#license">License</a></li>
    <li><a href="#contact">Contact</a></li>
  </ol>
</details>

## About The Project

RuNNer is a software framework for the development of high-dimensional neural network potentials of the type which *Behler* and *Parrinello* introduced in **2007**.

RuNNer is actively developed in the group of Jörg Behler at Georg-August-Universität Göttingen in Germany.

For more information on RuNNer, [>>> visit our documentation >>>](https://theochemgoettingen.gitlab.io/RuNNer)

### Built With

RuNNer is written in pure Fortran, but relies on a few additional libraries to unfold its maximum performance:
* [BLAS - Basic Linear Algebra Subprograms](https://netlib.sandia.gov/blas/)
* [LAPACK - Linear Algebra PACKage](https://netlib.sandia.gov/lapack/)
* [Intel® MKL - Intel Math Kernel Library](https://software.intel.com/content/www/us/en/develop/tools/math-kernel-library.html)
 
## License

Distributed under the GNU General Public License. See `LICENSE` for more information.

## Contact

**Author:** Jörg Behler - [@TheochemGoettingen](joerg.behler@uni-goettingen.de)

**Project Link:** [https://gitlab.com/TheochemGoettingen/RuNNer](https://gitlab.com/TheochemGoettingen/RuNNer)
