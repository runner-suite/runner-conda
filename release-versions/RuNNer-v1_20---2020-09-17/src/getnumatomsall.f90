!######################################################################
! This routine is part of
! RuNNer - RuNNer Neural Network Energy Representation
! (c) 2008-2020 Prof. Dr. Joerg Behler 
! Georg-August-Universitaet Goettingen, Germany
!
! This program is free software: you can redistribute it and/or modify it 
! under the terms of the GNU General Public License as published by the 
! Free Software Foundation, either version 3 of the License, or 
! (at your option) any later version.
!
! This program is distributed in the hope that it will be useful, but 
! WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
! or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
! for more details.
!
! You should have received a copy of the GNU General Public License along 
! with this program. If not, see http://www.gnu.org/licenses. 
!######################################################################
!! Purpose: get array num_atoms_all, contains number of atoms in each training point

!! called by:
!! - fitting.f90
!! - fittingpair.f90 
!!
      subroutine getnumatomsall(ntrain,num_atoms_all)
!!
      use fileunits
      use nnflags
      use globaloptions
!!
      implicit none
!!     
      integer ntrain                                                ! in
      integer num_atoms_all(ntrain)                                 ! out
      integer i1,i2                                                 ! internal
      integer ndummy                                                ! internal
      integer idummy                                                ! internal
!!
      real*8 rdummy                                                 ! internal 
      character*200 :: filenametemp                                 ! internal
!!
!!
      num_atoms_all(:)=0
!!
      if(lshort)then
        open(symunit,file='function.data',form='formatted',status='old')
        rewind(symunit)
        do i1=1,ntrain
          if(nn_type_short.eq.1)then
            read(symunit,*)num_atoms_all(i1)
            do i2=1,num_atoms_all(i1)
              read(symunit,*)idummy
            enddo ! i2
          elseif(nn_type_short.eq.2)then
            read(symunit,*)num_atoms_all(i1),ndummy
            do i2=1,ndummy
              read(symunit,*)idummy
            enddo ! i2
          endif
          read(symunit,*)rdummy 
        enddo ! i1
        close(symunit)
      elseif(lelec.and.(nn_type_elec.eq.1))then  ! we also need num_atoms_all if lshort is switched off
        open(symeunit,file='functione.data',form='formatted',status='old')
        rewind(symeunit) !'
        do i1=1,ntrain
          read(symeunit,*)num_atoms_all(i1)
          do i2=1,num_atoms_all(i1)
            read(symeunit,*)idummy
          enddo ! i2
          read(symeunit,*)rdummy 
        enddo ! i1
      else
        write(ounit,*)'ERROR in getnumatomsall'
        stop
      endif
!!
      return
      end
