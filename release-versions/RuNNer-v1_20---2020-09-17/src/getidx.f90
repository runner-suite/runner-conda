!######################################################################
! This routine is part of
! RuNNer - RuNNer Neural Network Energy Representation
! (c) 2008-2020 Prof. Dr. Joerg Behler 
! Georg-August-Universitaet Goettingen, Germany
!
! This program is free software: you can redistribute it and/or modify it 
! under the terms of the GNU General Public License as published by the 
! Free Software Foundation, either version 3 of the License, or 
! (at your option) any later version.
!
! This program is distributed in the hope that it will be useful, but 
! WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
! or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
! for more details.
!
! You should have received a copy of the GNU General Public License along 
! with this program. If not, see http://www.gnu.org/licenses. 
!######################################################################
!! called by:
!! - fitting.f90
!!
      subroutine getidx(npoints,idx)
!!
      use fittingoptions
      use globaloptions
!!
      implicit none
!!
      integer npoints        ! in
      integer idx(nblock)    ! out
      integer itemp          ! internal     
      integer i1             ! internal
!!
      real*8 z(nblock)       ! internal
      real*8 ztemp           ! internal
!!
!! initialization
      do i1=1,nblock
        idx(i1)=i1
      enddo
!!
      return
      end
