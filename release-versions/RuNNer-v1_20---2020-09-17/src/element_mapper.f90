!######################################################################
!! This routine is part of
!! RuNNer - RuNNer Neural Network Energy Representation
!! (c) 2008-2018 Prof. Dr. Joerg Behler
!! Georg-August-Universitaet Goettingen, Germany
!!
!! This program is free software: you can redistribute it and/or modify it
!! under the terms of the GNU General Public License as published by the
!! Free Software Foundation, either version 3 of the License, or
!! (at your option) any later version.
!!
!! This program is distributed in the hope that it will be useful, but
!! WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
!! or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
!! for more details.
!!
!! You should have received a copy of the GNU General Public License along
!! with this program. If not, see http://www.gnu.org/licenses.
!!######################################################################
!!! Atomic masses of all elements - Added by Emir

subroutine element_mapper(element_map)

implicit none

real*8 element_map(108)

element_map(1)=1.00794
element_map(2)=4.0026
element_map(3)=6.941
element_map(4)=9.0122
element_map(5)=10.811
element_map(6)=12.0107
element_map(7)=14.0067
element_map(8)=15.9994
element_map(9)=18.9984
element_map(10)=20.1797
element_map(11)=22.9897
element_map(12)=24.305
element_map(13)=26.9815
element_map(14)=28.0855
element_map(15)=30.9738
element_map(16)=32.065
element_map(17)=35.453
element_map(18)=39.948
element_map(19)=39.0983
element_map(20)=40.078
element_map(21)=44.9559
element_map(22)=47.867
element_map(23)=50.9415
element_map(24)=51.9961
element_map(25)=54.938
element_map(26)=55.845
element_map(27)=58.9332
element_map(28)=58.6934
element_map(29)=63.546
element_map(30)=65.39
element_map(31)=69.723
element_map(32)=72.64
element_map(33)=74.9216
element_map(34)=78.96
element_map(35)=79.904
element_map(36)=83.8
element_map(37)=85.4678
element_map(38)=87.62
element_map(39)=88.9059
element_map(40)=91.224
element_map(41)=92.9064
element_map(42)=95.94
element_map(43)=98.0
element_map(44)=101.07
element_map(45)=102.9055
element_map(46)=106.42
element_map(47)=107.8682
element_map(48)=112.411
element_map(49)=114.818
element_map(50)=118.71
element_map(51)=121.76
element_map(52)=127.6
element_map(53)=126.9045
element_map(54)=131.293
element_map(55)=132.9055
element_map(56)=137.327
element_map(57)=138.9055
element_map(58)=140.116
element_map(59)=140.9077
element_map(60)=144.24
element_map(61)=145.0
element_map(62)=150.36
element_map(63)=151.964
element_map(63)=157.25
element_map(64)=158.9253
element_map(65)=162.5
element_map(66)=164.9303
element_map(67)=167.259
element_map(68)=168.9342
element_map(69)=173.04
element_map(70)=174.967
element_map(71)=178.49
element_map(72)=180.9479
element_map(73)=183.84
element_map(74)=186.207
element_map(75)=190.23
element_map(76)=192.217
element_map(77)=195.078
element_map(78)=196.9665
element_map(79)=200.59
element_map(80)=204.3833
element_map(81)=207.2
element_map(82)=208.9804
element_map(83)=209.0
element_map(84)=210.0
element_map(85)=222.0
element_map(86)=223.0
element_map(87)=226.0
element_map(88)=227.0
element_map(89)=232.0381
element_map(90)=231.0359
element_map(91)=238.0289
element_map(92)=237.0
element_map(93)=244.0
element_map(94)=243.0
element_map(95)=247.0
element_map(96)=247.0
element_map(97)=251.0
element_map(98)=252.0
element_map(99)=257.0
element_map(100)=258.0
element_map(101)=259.0
element_map(102)=262.0
element_map(103)=261.0
element_map(104)=262.0
element_map(105)=266.0
element_map(106)=264.0
element_map(107)=277.0
element_map(108)=268.0

end subroutine element_mapper
