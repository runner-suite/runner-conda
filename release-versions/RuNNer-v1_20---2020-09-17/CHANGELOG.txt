Release 1.2 (September 17, 2020)
==============================
List of most important changes with respect to version 1.1

1) new better random number generator type 6 (JF)

2) bugfix in find_contradictions (JB)

3) bugfix in derivative of symmetry function type 8 (EK and JB)

4) inclusion of symmetry function groups (EK), keyword: use_sf_groups
This results in a significant speedup in most cases, but this is still a betaversion

5) inclusion of Hessian and harmonic frequencies for mode 3 (EK,FW), keywords: 
calculate_hessian, calculate_frequencies, calculate_normalmodes
This is still a betaversion

6) Electrostatics has been disabled as the use of the present implementation is discouraged.
This part is currently under revision and an improved version will be available soon.

