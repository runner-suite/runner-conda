!######################################################################
! This routine is part of
! RuNNer - RuNNer Neural Network Energy Representation
! (c) 2008-2019 Prof. Dr. Joerg Behler 
! Georg-August-Universitaet Goettingen, Germany
!
! This program is free software: you can redistribute it and/or modify it 
! under the terms of the GNU General Public License as published by the 
! Free Software Foundation, either version 3 of the License, or 
! (at your option) any later version.
!
! This program is distributed in the hope that it will be useful, but 
! WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
! or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
! for more details.
!
! You should have received a copy of the GNU General Public License along 
! with this program. If not, see http://www.gnu.org/licenses. 
!######################################################################
!! called by:
!!
      subroutine initialweights_elec(&
        num_weights_elec,nseed,weights_elec)
!!
      use fittingoptions
      use globaloptions
!!
      implicit none
!!
      integer num_weights_elec(nelem)    ! in
      integer nseed                      ! in/out
      integer i,j,k                      ! internal
!!
      real*8 weights_elec(maxnum_weights_elec,nelem)
      real*8 ran0
      real*8 ran1
      real*8 ran2
      real*8 ran3
      real*8 ran5
!!
!! we keep here the somewhat inconsistent looping order to be backwards compatible
      if(nran.eq.1)then
        do j=1,nelem
          do i=1,num_weights_elec(j)
            weights_elec(i,j)=weightse_min+(weightse_max-weightse_min)*ran0(nseed)  !-0.5d0*(weightse_max-weightse_min)
          enddo
        enddo
      elseif(nran.eq.2)then
        do j=1,nelem
          do i=1,num_weights_elec(j)
            weights_elec(i,j)=weightse_min+(weightse_max-weightse_min)*ran1(nseed)  !-0.5d0*(weightse_max-weightse_min)
          enddo
        enddo
      elseif(nran.eq.3)then
        do j=1,nelem
          do i=1,num_weights_elec(j)
            weights_elec(i,j)=weightse_min+(weightse_max-weightse_min)*ran2(nseed)  !-0.5d0*(weightse_max-weightse_min)
          enddo
        enddo
      elseif(nran.eq.4)then
        do j=1,nelem
          do i=1,num_weights_elec(j)
            weights_elec(i,j)=weightse_min+(weightse_max-weightse_min)*ran3(nseed)  !-0.5d0*(weightse_max-weightse_min)
          enddo
        enddo
      elseif(nran.eq.5)then
        do j=1,nelem
          do i=1,num_weights_elec(j)
            weights_elec(i,j)=weightse_min+(weightse_max-weightse_min)*ran5(nseed)  !-0.5d0*(weightse_max-weightse_min)
          enddo
        enddo
      endif ! nran
!!
!!
      return
      end
!!
