!######################################################################
! This routine is part of
! RuNNer - RuNNer Neural Network Energy Representation
! (c) 2008-2019 Prof. Dr. Joerg Behler 
! Georg-August-Universitaet Goettingen, Germany
!
! This program is free software: you can redistribute it and/or modify it 
! under the terms of the GNU General Public License as published by the 
! Free Software Foundation, either version 3 of the License, or 
! (at your option) any later version.
!
! This program is distributed in the hope that it will be useful, but 
! WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
! or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
! for more details.
!
! You should have received a copy of the GNU General Public License along 
! with this program. If not, see http://www.gnu.org/licenses. 
!######################################################################!!
!! called by:
!!
      subroutine getrmse_short(nenergies,&
        nforces,rmse_short,rmse_force_s,mad_short,mad_force_s)
!!
      use fileunits
      use nnflags
      use globaloptions
!!
      implicit none
!!
      integer nenergies,nforces                 ! in
!!
      real*8 rmse_short                         ! in/out
      real*8 rmse_force_s                       ! in/out
      real*8 mad_short                          ! in/out
      real*8 mad_force_s                        ! in/out
!!
!!
!! some security checks if settings for maxenergy or maxforce are inappropriate:
      if(lshort)then
        if(nenergies.eq.0)then
          write(ounit,*)'ERROR in getrmse: nenergies is zero '
          write(ounit,*)'Probably maxenergy and/or energy_threshold is set too low'
          stop  !'
        endif
      endif
!!
!! short range
      if(lshort)then
        rmse_short          =rmse_short/dble(nenergies)
        rmse_short          =dsqrt(rmse_short)
        rmse_force_s        =rmse_force_s/dble(nforces)
        rmse_force_s        =sqrt(rmse_force_s)
        mad_short           =mad_short/dble(nenergies)
        mad_force_s         =mad_force_s/dble(nforces)
      endif
!!
      return
      end

