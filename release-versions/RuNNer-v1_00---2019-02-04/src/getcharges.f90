!######################################################################
! This routine is part of
! RuNNer - RuNNer Neural Network Energy Representation
! (c) 2008-2019 Prof. Dr. Joerg Behler 
! Georg-August-Universitaet Goettingen, Germany
!
! This program is free software: you can redistribute it and/or modify it 
! under the terms of the GNU General Public License as published by the 
! Free Software Foundation, either version 3 of the License, or 
! (at your option) any later version.
!
! This program is distributed in the hope that it will be useful, but 
! WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
! or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
! for more details.
!
! You should have received a copy of the GNU General Public License along 
! with this program. If not, see http://www.gnu.org/licenses. 
!######################################################################
!! called by:
!! - ewaldenergies_para.f90 
!! - optimize_ewald.f90
!!
!! very similar to geteshort.f90
!!
      subroutine getcharges(ndim1,npoints,&
        zelem_local,num_atoms_local,&
        symfunctione_local,nnatomcharge_local)
!!
      use fileunits
      use globaloptions
      use nnewald
!!
      implicit none
!!
      integer npoints
      integer ndim1                                                    ! in
      integer zelem_local(ndim1,max_num_atoms) 
      integer zelem(max_num_atoms)
      integer num_atoms_local(ndim1)
      integer i1
!! 
      real*8 symfunctione_local(maxnum_funcvalues_elec,max_num_atoms,ndim1)
      real*8 nnatomcharge_local(ndim1,max_num_atoms)                   ! out
      real*8 nnatomcharge(max_num_atoms)                               ! internal
!!
!!
      do i1=1,npoints
!!
!! calculate the atomic charges 
        zelem(:)=zelem_local(i1,:)
!!
        nnatomcharge(:)=0.0d0
        call calconecharge(num_atoms_local(i1),&
          zelem,symfunctione_local(1,1,i1),&
          nnatomcharge)
!!
        nnatomcharge_local(i1,:)=nnatomcharge(:)
!!
      enddo ! i1
!!
      return
      end
