!######################################################################
! This routine is part of
! RuNNer - RuNNer Neural Network Energy Representation
! (c) 2008-2019 Prof. Dr. Joerg Behler 
! Georg-August-Universitaet Goettingen, Germany
!
! This program is free software: you can redistribute it and/or modify it 
! under the terms of the GNU General Public License as published by the 
! Free Software Foundation, either version 3 of the License, or 
! (at your option) any later version.
!
! This program is distributed in the hope that it will be useful, but 
! WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
! or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
! for more details.
!
! You should have received a copy of the GNU General Public License along 
! with this program. If not, see http://www.gnu.org/licenses. 
!######################################################################
!! called by: 
!! - predict.f90
!!
      subroutine predictionelectrostatic(&
        num_atoms,zelem,&
        minvalue_elec,maxvalue_elec,avvalue_elec,&
        lattice,xyzstruct,&
        nntotalcharge,nnatomcharge,&
        chargemin,chargemax,nnelecenergy,&
        nnelecforce,nnstress_elec,sense,lperiodic)
!!
      use globaloptions
      use fileunits
      use predictionoptions
      use nnewald
      use timings
!!
      implicit none
!!
      integer zelem(max_num_atoms)                                      ! in
      integer num_atoms                                                 ! in
!!
      real*8 minvalue_elec(nelem,maxnum_funcvalues_elec)                ! in
      real*8 maxvalue_elec(nelem,maxnum_funcvalues_elec)                ! in
      real*8 avvalue_elec(nelem,maxnum_funcvalues_elec)                 ! in
      real*8 nnelecenergy                                               ! out, total electrostatic energy 
      real*8 chargemin(nelem)                                           ! in
      real*8 chargemax(nelem)                                           ! in
      real*8 nntotalcharge                                              ! out 
      real*8 nnatomcharge(max_num_atoms)                                ! out 
      real*8 nnstress_elec(3,3)                                         ! out 
      real*8 sense(nelem,maxnum_funcvalues_elec)                        ! out  
      real*8 nnelecforce(3,max_num_atoms)                               ! out 
      real*8 lattice(3,3)                                               ! in
      real*8 xyzstruct(3,max_num_atoms)                                 ! in
!! 
      logical lperiodic                                                 ! in

!!
!!======================================================================
!! Initialzations
!!======================================================================
!! initializations electrostatic part
      nntotalcharge          = 0.0d0
      nnatomcharge(:)        = 0.0d0
      nnelecenergy           = 0.0d0
      nnelecforce(:,:)       = 0.0d0
      nnstress_elec(:,:)     = 0.0d0
!! initialization of timings
      timeelec               = 0.0d0
!! initialization of sensitivities
      sense(:,:)             = 0.0d0
!!
!!======================================================================
!!======================================================================
!! Start electrostatic part
!!======================================================================
!!======================================================================
      if(lfinetime)then
        dayelec=0
        call abstime(timeelecstart,dayelec)
      endif ! lfinetime
!!
      call predictelec(&
        num_atoms,zelem,lattice,xyzstruct,&
        minvalue_elec,maxvalue_elec,avvalue_elec,chargemin,chargemax,&
        nnelecenergy,nnatomcharge,nntotalcharge,nnelecforce,&
        nnstress_elec,sense,lperiodic)
!!
      if(lfinetime)then
        call abstime(timeelecend,dayelec)
        timeelec=timeelec+timeelecend-timeelecstart
      endif ! lfinetime
!!======================================================================
!!======================================================================
!! End Electrostatic part 
!!======================================================================
!!======================================================================
      return
      end
