!######################################################################
! This routine is part of
! RuNNer - RuNNer Neural Network Energy Representation
! (c) 2008-2019 Prof. Dr. Joerg Behler 
! Georg-August-Universitaet Goettingen, Germany
!
! This program is free software: you can redistribute it and/or modify it 
! under the terms of the GNU General Public License as published by the 
! Free Software Foundation, either version 3 of the License, or 
! (at your option) any later version.
!
! This program is distributed in the hope that it will be useful, but 
! WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
! or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
! for more details.
!
! You should have received a copy of the GNU General Public License along 
! with this program. If not, see http://www.gnu.org/licenses. 
!######################################################################
!! called by:
!!
      subroutine distribute_globaloptions()
!!
      use mpi_mod
      use nnflags
      use globaloptions
!!
      implicit none
!!
      call mpi_bcast(nran,1,mpi_integer,0,mpi_comm_world,mpierror)
      call mpi_bcast(paramode,1,mpi_integer,0,mpi_comm_world,mpierror)
      call mpi_bcast(nelem,1,mpi_integer,0,mpi_comm_world,mpierror)
      call mpi_bcast(cutoff_type,1,mpi_integer,0,mpi_comm_world,mpierror)
      if(lshort.and.(nn_type_short.eq.1))then
        call mpi_bcast(maxnum_weights_short_atomic,1,mpi_integer,0,mpi_comm_world,mpierror)
      endif
      if(lshort.and.(nn_type_short.eq.2))then
        call mpi_bcast(maxnum_weights_short_pair,1,mpi_integer,0,mpi_comm_world,mpierror)
      endif
      if(lelec.and.(nn_type_elec.eq.1))then
        call mpi_bcast(maxnum_weights_elec,1,mpi_integer,0,mpi_comm_world,mpierror)
      endif

      call mpi_bcast(elempair,2*npairs,mpi_integer,0,mpi_comm_world,mpierror)
      call mpi_bcast(nucelem,nelem,mpi_integer,0,mpi_comm_world,mpierror)
      call mpi_bcast(nblock,1,mpi_integer,0,mpi_comm_world,mpierror)
      call mpi_bcast(listdim,1,mpi_integer,0,mpi_comm_world,mpierror)
      call mpi_bcast(ewaldkmax,1,mpi_integer,0,mpi_comm_world,mpierror)
      call mpi_bcast(max_num_neighbors_atomic_input,1,mpi_integer,0,mpi_comm_world,mpierror)
      call mpi_bcast(elementindex,102,mpi_integer,0,mpi_comm_world,mpierror)
      call mpi_bcast(pairindex,102*102,mpi_integer,0,mpi_comm_world,mpierror)

      call mpi_bcast(rscreen_cut,1,mpi_real8,0,mpi_comm_world,mpierror)
      call mpi_bcast(rscreen_onset,1,mpi_real8,0,mpi_comm_world,mpierror)
      call mpi_bcast(rmin,1,mpi_real8,0,mpi_comm_world,mpierror)
      call mpi_bcast(ewaldalpha,1,mpi_real8,0,mpi_comm_world,mpierror)
      call mpi_bcast(ewaldcutoff,1,mpi_real8,0,mpi_comm_world,mpierror)
      call mpi_bcast(atomrefenergies,nelem,mpi_real8,0,mpi_comm_world,mpierror)
      call mpi_bcast(fixedcharge,nelem,mpi_real8,0,mpi_comm_world,mpierror)
      call mpi_bcast(saturation_threshold,1,mpi_real8,0,mpi_comm_world,mpierror)

      call mpi_bcast(luseatomcharges,1,mpi_logical,0,mpi_comm_world,mpierror)
      call mpi_bcast(luseatomenergies,1,mpi_logical,0,mpi_comm_world,mpierror)
      call mpi_bcast(lscalesym,1,mpi_logical,0,mpi_comm_world,mpierror)
      call mpi_bcast(lcentersym,1,mpi_logical,0,mpi_comm_world,mpierror)
      call mpi_bcast(lremoveatomenergies,1,mpi_logical,0,mpi_comm_world,mpierror)
      call mpi_bcast(lfinetime,1,mpi_logical,0,mpi_comm_world,mpierror)
      call mpi_bcast(lreadunformatted,1,mpi_logical,0,mpi_comm_world,mpierror)
      call mpi_bcast(lwriteunformatted,1,mpi_logical,0,mpi_comm_world,mpierror)
      call mpi_bcast(lcheckf,1,mpi_logical,0,mpi_comm_world,mpierror)
      call mpi_bcast(lompmkl,1,mpi_logical,0,mpi_comm_world,mpierror)
      call mpi_bcast(luseforces,1,mpi_logical,0,mpi_comm_world,mpierror)
      call mpi_bcast(ldebug,1,mpi_logical,0,mpi_comm_world,mpierror)
      call mpi_bcast(ldostress,1,mpi_logical,0,mpi_comm_world,mpierror)
      call mpi_bcast(lscreen,1,mpi_logical,0,mpi_comm_world,mpierror)
      call mpi_bcast(lsilent,1,mpi_logical,0,mpi_comm_world,mpierror)
      call mpi_bcast(lnormnodes,1,mpi_logical,0,mpi_comm_world,mpierror)
      call mpi_bcast(lsens,1,mpi_logical,0,mpi_comm_world,mpierror)
      call mpi_bcast(lpearson_correlation,1,mpi_logical,0,mpi_comm_world,mpierror)
      call mpi_bcast(lenvironmentanalysis,1,mpi_logical,0,mpi_comm_world,mpierror)
      call mpi_bcast(lenforcemaxnumneighborsatomic,1,mpi_logical,0,mpi_comm_world,mpierror)
      call mpi_bcast(lmd,1,mpi_logical,0,mpi_comm_world,mpierror)
      call mpi_bcast(ldetect_saturation,1,mpi_logical,0,mpi_comm_world,mpierror)

!! check if pstring is distributed correctly!
      call mpi_bcast(pstring,1,mpi_character,0,mpi_comm_world,mpierror)
!! check if element is distributed correctly!
      call mpi_bcast(element,1,mpi_character,0,mpi_comm_world,mpierror)
      return
      end
