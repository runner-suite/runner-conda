!######################################################################
! This routine is part of
! RuNNer - RuNNer Neural Network Energy Representation
! (c) 2008-2019 Prof. Dr. Joerg Behler 
! Georg-August-Universitaet Goettingen, Germany
!
! This program is free software: you can redistribute it and/or modify it 
! under the terms of the GNU General Public License as published by the 
! Free Software Foundation, either version 3 of the License, or 
! (at your option) any later version.
!
! This program is distributed in the hope that it will be useful, but 
! WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
! or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
! for more details.
!
! You should have received a copy of the GNU General Public License along 
! with this program. If not, see http://www.gnu.org/licenses. 
!######################################################################
!! called by:
!! - optimize_ewald.f90
!!
      subroutine sortforceerror(npoints,forceerror_list,lusef)
!!
      use fileunits
      use fittingoptions
      use globaloptions
      use structures
!!
      implicit none
!!
      integer npoints
      integer i1,i2,i3
      integer icount
      integer ixyz(3*nblock*max_num_atoms)                                   ! internal 
      integer iatom(3*nblock*max_num_atoms)                                  ! internal 
      integer istruct(3*nblock*max_num_atoms)                                ! internal 
!!
      real*8 forceerror_list(3,max_num_atoms,nblock)                         ! in
      real*8 forceerror_temp(3*max_num_atoms*nblock)                         ! internal
      real*8 forceerror_copy(3*max_num_atoms*nblock)                         ! internal
      real*8 temp                                                            ! internal
      real*8 fthres                                                          ! out
      real*8 getthreshold                                                    ! internal
!!
      logical lusef(3,max_num_atoms,nblock)                                  ! out 
!!
!! initializations
      lusef(:,:,:)=.false.
!!
!! transform array
      forceerror_temp(:)=0
      icount=0
      do i1=1,npoints
        do i2=1,num_atoms_list(i1)
          do i3=1,3
            if(lupdatebyelement.and.(elemupdate.eq.zelem_list(i1,i2)))then
              icount=icount+1
              forceerror_temp(icount)=forceerror_list(i3,i2,i1)
              iatom(icount)=i2
              istruct(icount)=i1
              ixyz(icount)=i3
            elseif(.not.lupdatebyelement)then
              icount=icount+1
              forceerror_temp(icount)=forceerror_list(i3,i2,i1)
              iatom(icount)=i2
              istruct(icount)=i1
              ixyz(icount)=i3
            endif
          enddo
        enddo
      enddo
!!
!! determine the threshold for update
      temp=(1.d0-worstf)*dble(icount)
      i2=int(temp)
      forceerror_copy(:)=forceerror_temp(:)
!! caution: getthreshold changes order of forceerror_copy
      fthres=getthreshold(i2,icount,forceerror_copy)
!!
!! set luseq array
      do i1=1,icount
        if(forceerror_temp(i1).gt.fthres)then
!!          write(ounit,*)i1,forceerror_temp(i1),istruct(i1),iatom(i1),ixyz(i1)
          lusef(ixyz(i1),iatom(i1),istruct(i1))=.true.
        endif
      enddo
!!
      return
      end
