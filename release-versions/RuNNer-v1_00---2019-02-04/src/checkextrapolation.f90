!######################################################################
! This routine is part of
! RuNNer - RuNNer Neural Network Energy Representation
! (c) 2008-2019 Prof. Dr. Joerg Behler 
! Georg-August-Universitaet Goettingen, Germany
!
! This program is free software: you can redistribute it and/or modify it 
! under the terms of the GNU General Public License as published by the 
! Free Software Foundation, either version 3 of the License, or 
! (at your option) any later version.
!
! This program is distributed in the hope that it will be useful, but 
! WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
! or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
! for more details.
!
! You should have received a copy of the GNU General Public License along 
! with this program. If not, see http://www.gnu.org/licenses. 
!######################################################################
!! multipurpose subroutine

!! called by: 
!! - prediction.f90
!! - predictionpair.f90
!!
!! This subroutine checks the symmetry functions for extrapolation
!!
      subroutine checkextrapolation(natoms,atomindex,ndim,&
             maxnum_funcvalues_local,num_funcvalues_local,&
             zelem,symfunction_local,&
             minvalue_local,maxvalue_local,slabel,lextrapolation)
!!
      use fileunits
      use globaloptions
!!
      implicit none
!!
      integer maxnum_funcvalues_local            ! in
      integer num_funcvalues_local(ndim)        ! in
      integer ndim                        ! in
      integer zelem(max_num_atoms)         ! in     
      integer i1,i3                     ! internal
      integer natoms                       ! in
      integer atomindex(natoms)            ! in
!!
      real*8 symfunction_local(maxnum_funcvalues_local,natoms) ! in
      real*8 minvalue_local(ndim,maxnum_funcvalues_local)            ! in
      real*8 maxvalue_local(ndim,maxnum_funcvalues_local)            ! in
      real*8 threshold                                 ! internal
!!
      character*5 slabel                    ! in
!!
      logical lextrapolation
!!      
      threshold=1.0d-8
      lextrapolation=.false.
!!
      do i1=1,natoms
        do i3=1,num_funcvalues_local(elementindex(zelem(atomindex(i1))))
          if((symfunction_local(i3,i1)-maxvalue_local(elementindex(zelem(atomindex(i1))),i3)).gt.threshold)then
            write(ounit,'(a,a5,x,2i5,x,a,2f18.8)')'### EXTRAPOLATION WARNING ### ',slabel,&
            atomindex(i1),i3,'too large ',symfunction_local(i3,i1),&
            maxvalue_local(elementindex(zelem(atomindex(i1))),i3)
            lextrapolation=.true.
          elseif((-symfunction_local(i3,i1)+minvalue_local(elementindex(zelem(atomindex(i1))),i3)).gt.threshold)then
            write(ounit,'(a,a5,x,2i5,x,a,2f18.8)')'### EXTRAPOLATION WARNING ### ',slabel,&
            atomindex(i1),i3,'too small ',symfunction_local(i3,i1),&
            minvalue_local(elementindex(zelem(atomindex(i1))),i3)
            lextrapolation=.true.
          endif
        enddo ! i3
      enddo ! i1
!!
      return
      end
