!######################################################################
! This routine is part of
! RuNNer - RuNNer Neural Network Energy Representation
! (c) 2008-2019 Prof. Dr. Joerg Behler 
! Georg-August-Universitaet Goettingen, Germany
!
! This program is free software: you can redistribute it and/or modify it 
! under the terms of the GNU General Public License as published by the 
! Free Software Foundation, either version 3 of the License, or 
! (at your option) any later version.
!
! This program is distributed in the hope that it will be useful, but 
! WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
! or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
! for more details.
!
! You should have received a copy of the GNU General Public License along 
! with this program. If not, see http://www.gnu.org/licenses. 
!######################################################################
!! called by:
!! - main.f90
!!
      subroutine mode2(iseed)
!!
      use fileunits
      use timings
      use fittingoptions
      use nnflags
      use globaloptions
!!
      implicit none
!!
      integer iseed
!!
      call zerotime(daymode2,timemode2start,timemode2end) 
      call abstime(timemode2start,daymode2)
!!
!!
      if(lshort.and.lelec.and.(nn_type_elec.eq.1))then
        write(ounit,*)'ERROR: simultaneous fitting of short and elec does not make sense'
        stop !'
      endif
!!
!!======================================================================
!! fitting short range part
!!======================================================================
      if(lshort)then
        if(nn_type_short.eq.1)then
          if(fitmode.eq.1)then
            call fitting_short_atomic(iseed)
!! TODO:
!! fit atomic short range part and possibly charges as second output nodes
!!            call fitting_atomic(iseed)
          elseif(fitmode.eq.2)then
            write(ounit,*)'ERROR: batch fitting is not well tested and not up to date'
            stop
          endif
        elseif(nn_type_short.eq.2)then
          call fitting_short_pair(iseed)
        endif
!!
!!======================================================================
!! fitting electrostatic NN 
!!======================================================================
      elseif(lelec.and.(nn_type_elec.eq.1))then
        call fitting_electrostatic(iseed)
!!
      endif
!!
      call abstime(timemode2end,daymode2)
!!
      return
      end
