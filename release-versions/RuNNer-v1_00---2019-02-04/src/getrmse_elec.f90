!######################################################################
! This routine is part of
! RuNNer - RuNNer Neural Network Energy Representation
! (c) 2008-2019 Prof. Dr. Joerg Behler 
! Georg-August-Universitaet Goettingen, Germany
!
! This program is free software: you can redistribute it and/or modify it 
! under the terms of the GNU General Public License as published by the 
! Free Software Foundation, either version 3 of the License, or 
! (at your option) any later version.
!
! This program is distributed in the hope that it will be useful, but 
! WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
! or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
! for more details.
!
! You should have received a copy of the GNU General Public License along 
! with this program. If not, see http://www.gnu.org/licenses. 
!######################################################################!!
!! called by:
!!
      subroutine getrmse_elec(ntrain,ncharges,&
        rmse_charge,rmse_totalcharge,rmse_elec,&
        mad_charge,mad_totalcharge,mad_elec)
!!
      use fileunits
      use globaloptions
!!
      implicit none
!!
      integer ntrain,ncharges                   ! in
!!
      real*8 rmse_elec                         ! in/out
      real*8 rmse_charge                        ! in/out
      real*8 rmse_totalcharge                   ! in/out
      real*8 mad_elec                          ! in/out
      real*8 mad_charge                         ! in/out
      real*8 mad_totalcharge                    ! in/out
!!
!! electrostatic
      rmse_charge         =rmse_charge/dble(ncharges)
      rmse_charge         =dsqrt(rmse_charge)
      rmse_totalcharge    =rmse_totalcharge/dble(ntrain)
      rmse_totalcharge    =dsqrt(rmse_totalcharge)
      rmse_elec          =rmse_elec/dble(ntrain)
      rmse_elec          =dsqrt(rmse_elec)
      mad_charge          =mad_charge/dble(ncharges)
      mad_totalcharge     =mad_totalcharge/dble(ntrain)
      mad_elec           =mad_elec/dble(ntrain)
!!
      return
      end

