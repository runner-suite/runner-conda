!######################################################################
! This routine is part of
! RuNNer - RuNNer Neural Network Energy Representation
! (c) 2008-2019 Prof. Dr. Joerg Behler 
! Georg-August-Universitaet Goettingen, Germany
!
! This program is free software: you can redistribute it and/or modify it 
! under the terms of the GNU General Public License as published by the 
! Free Software Foundation, either version 3 of the License, or 
! (at your option) any later version.
!
! This program is distributed in the hope that it will be useful, but 
! WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
! or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
! for more details.
!
! You should have received a copy of the GNU General Public License along 
! with this program. If not, see http://www.gnu.org/licenses. 
!######################################################################
!! called by
!! fitting.f90
!! fitting_batch.f90
!! fittingpair.f90

!! Multipurpose subroutine

      subroutine getwshift(ndim,maxnum_weights_local,num_weights_local,&
        weights_local,weights_local_old,weights_local_veryold,&
        wshift_local,wshift_local2)
!!
      implicit none
!!
      integer ndim
      integer maxnum_weights_local
      integer num_weights_local(ndim)
      integer i1,i2
!!
      real*8 wshift_local(ndim)
      real*8 wshift_local2(ndim)
      real*8 weights_local(maxnum_weights_local,ndim)
      real*8 weights_local_old(maxnum_weights_local,ndim)
      real*8 weights_local_veryold(maxnum_weights_local,ndim)
!!
      wshift_local(:)=0.0d0
      wshift_local2(:)=0.0d0
      do i2=1,ndim
        do i1=1,num_weights_local(i2)
          wshift_local(i2)=wshift_local(i2)+abs(weights_local(i1,i2)-weights_local_old(i1,i2))
          wshift_local2(i2)=wshift_local2(i2)+abs(weights_local(i1,i2)-weights_local_veryold(i1,i2))
        enddo ! i1
        wshift_local(i2)=wshift_local(i2)/dble(num_weights_local(i2))
        wshift_local2(i2)=wshift_local2(i2)/dble(num_weights_local(i2))
      enddo ! i2
!!
      return
      end
