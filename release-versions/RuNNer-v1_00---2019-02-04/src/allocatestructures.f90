!######################################################################
! This routine is part of
! RuNNer - RuNNer Neural Network Energy Representation
! (c) 2008-2019 Prof. Dr. Joerg Behler 
! Georg-August-Universitaet Goettingen, Germany
!
! This program is free software: you can redistribute it and/or modify it 
! under the terms of the GNU General Public License as published by the 
! Free Software Foundation, either version 3 of the License, or 
! (at your option) any later version.
!
! This program is distributed in the hope that it will be useful, but 
! WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
! or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
! for more details.
!
! You should have received a copy of the GNU General Public License along 
! with this program. If not, see http://www.gnu.org/licenses. 
!######################################################################

!! called by:
!!
      subroutine allocatestructures()
     
      use globaloptions
      use structures
      use nnflags

      implicit none

      allocate (num_atoms_list(nblock))
      allocate (num_pairs_list(nblock))
      allocate (zelem_list(nblock,max_num_atoms))
      allocate (zelemp_list(2,nblock,max_num_pairs))
      allocate (lattice_list(3,3,nblock))
      allocate (xyzstruct_list(3,max_num_atoms,nblock))
      allocate (totalcharge_list(nblock))
      allocate (totalenergy_list(nblock))
      allocate (shortenergy_list(nblock))
      allocate (elecenergy_list(nblock))
      allocate (totalforce_list(3,max_num_atoms,nblock))
      allocate (totforce_list(3,max_num_atoms,nblock))
      allocate (elecforce_list(3,max_num_atoms,nblock))
      allocate (shortforce_list(3,max_num_atoms,nblock))
      allocate (atomcharge_list(nblock,max_num_atoms))
      allocate (atomenergy_list(nblock,max_num_atoms))
      allocate (lperiodic_list(nblock))
      allocate (elementsymbol_list(nblock,max_num_atoms))

      return
      end
