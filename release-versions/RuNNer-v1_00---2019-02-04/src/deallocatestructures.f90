!######################################################################
! This routine is part of
! RuNNer - RuNNer Neural Network Energy Representation
! (c) 2008-2019 Prof. Dr. Joerg Behler 
! Georg-August-Universitaet Goettingen, Germany
!
! This program is free software: you can redistribute it and/or modify it 
! under the terms of the GNU General Public License as published by the 
! Free Software Foundation, either version 3 of the License, or 
! (at your option) any later version.
!
! This program is distributed in the hope that it will be useful, but 
! WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
! or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
! for more details.
!
! You should have received a copy of the GNU General Public License along 
! with this program. If not, see http://www.gnu.org/licenses. 
!######################################################################
!! called by:
!!
      subroutine deallocatestructures()
     
      use globaloptions
      use structures 
      use nnflags
      implicit none

      deallocate (num_atoms_list)
      deallocate (num_pairs_list)
      deallocate (zelem_list)
      deallocate (zelemp_list)
      deallocate (lattice_list)
      deallocate (xyzstruct_list)
      deallocate (totalcharge_list)
      deallocate (totalenergy_list)
      deallocate (shortenergy_list)
      deallocate (elecenergy_list)
      deallocate (totalforce_list)
      deallocate (elecforce_list)
      deallocate (shortforce_list)
      deallocate (totforce_list)
      deallocate (atomcharge_list)
      deallocate (atomenergy_list)
      deallocate (lperiodic_list)
      deallocate (elementsymbol_list)

      return
      end
