!######################################################################
! This routine is part of
! RuNNer - RuNNer Neural Network Energy Representation
! (c) 2008-2019 Prof. Dr. Joerg Behler 
! Georg-August-Universitaet Goettingen, Germany
!
! This program is free software: you can redistribute it and/or modify it 
! under the terms of the GNU General Public License as published by the 
! Free Software Foundation, either version 3 of the License, or 
! (at your option) any later version.
!
! This program is distributed in the hope that it will be useful, but 
! WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
! or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
! for more details.
!
! You should have received a copy of the GNU General Public License along 
! with this program. If not, see http://www.gnu.org/licenses. 
!######################################################################
!! called by:
!!
      subroutine printdateandtime(countepoch)
!!
      use fileunits
      use timings
!!
      implicit none
!!
      integer countepoch ! in
!!
      call date_and_time(fulldate,fulltime,zone,timevalues)

      write(ounit,'(a8,i5,6x,i4,x,i2,x,i2,a7,i2,a3,i2,a4,i2,a3)')' DATE   ',&
        countepoch,&
        timevalues(1),timevalues(2),timevalues(3),&
        ' TIME: ',timevalues(5),' h ',&
        timevalues(6),' m ',timevalues(7),' s '

      end

