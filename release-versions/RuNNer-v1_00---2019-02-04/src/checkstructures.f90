!######################################################################
! This routine is part of
! RuNNer - RuNNer Neural Network Energy Representation
! (c) 2008-2019 Prof. Dr. Joerg Behler 
! Georg-August-Universitaet Goettingen, Germany
!
! This program is free software: you can redistribute it and/or modify it 
! under the terms of the GNU General Public License as published by the 
! Free Software Foundation, either version 3 of the License, or 
! (at your option) any later version.
!
! This program is distributed in the hope that it will be useful, but 
! WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
! or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
! for more details.
!
! You should have received a copy of the GNU General Public License along 
! with this program. If not, see http://www.gnu.org/licenses. 
!######################################################################
!! called by:
!! - initialization.f90
!!
      subroutine checkstructures(ielem,lelement)
!!
      use fileunits
      use globaloptions
!!
      implicit none
!!
      integer ielem                   ! out
      integer i1
!!
      logical lelement(102)           ! out
!!
!! initialization
      lelement(:)=.false.
!!
      open(dataunit,file='input.data',form='formatted')
      rewind(dataunit)
!!
!! loop over all structures in input.data
!! CAUTION: in case of mode 2 totnum_structures is not determined before, because we don't want to use input.data in mode 2
!! => ielem is always 0 after this subroutine
      do i1=1,totnum_structures
        call checkonestructure(i1,lelement)
      enddo ! i1
!!
      if(ldebug)then
!!      write(ounit,*)'-------------------------------------------------------------'
      write(ounit,*)'============================================================='
      endif !'
      close(dataunit)
!!
!! determine the number of elements
!! Caution: not all elements need to be present in each structure
      ielem=0
      do i1=1,102
        if(lelement(i1)) ielem=ielem+1
      enddo
!!
      return
      end
