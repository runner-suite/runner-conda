!######################################################################
! This routine is part of
! RuNNer - RuNNer Neural Network Energy Representation
! (c) 2008-2019 Prof. Dr. Joerg Behler 
! Georg-August-Universitaet Goettingen, Germany
!
! This program is free software: you can redistribute it and/or modify it 
! under the terms of the GNU General Public License as published by the 
! Free Software Foundation, either version 3 of the License, or 
! (at your option) any later version.
!
! This program is distributed in the hope that it will be useful, but 
! WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
! or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
! for more details.
!
! You should have received a copy of the GNU General Public License along 
! with this program. If not, see http://www.gnu.org/licenses. 
!######################################################################
!! called by:
!!
      subroutine getfilenames_elec(countepoch)
!!
      use fileunits
      use globaloptions
      use fittingoptions
!!
      implicit none
!!
      integer countepoch     ! in
      integer i1
!!
      character*20 filenametemp        ! internal
!!
!!
      if(countepoch.gt.999999)then
        write(ounit,*)'Number of epochs too large ',countepoch
        stop
      endif
!!
      do i1=1,nelem
!! charge weights
        filenametemp=filenamewe(i1)
        if(countepoch.gt.99999)then
          write(filenametemp(1:6),'(i6)')countepoch
        elseif(countepoch.gt.9999)then
          write(filenametemp(2:6),'(i5)')countepoch
        elseif(countepoch.gt.999)then
          write(filenametemp(3:6),'(i4)')countepoch
        elseif(countepoch.gt.99)then
          write(filenametemp(4:6),'(i3)')countepoch
        elseif(countepoch.gt.9)then
          write(filenametemp(5:6),'(i2)')countepoch
        else
          write(filenametemp(6:6),'(i1)')countepoch
        endif
        filenamewe(i1)=filenametemp
      enddo
!!
      return
      end
