!######################################################################
! This routine is part of
! RuNNer - RuNNer Neural Network Energy Representation
! (c) 2008-2019 Prof. Dr. Joerg Behler 
! Georg-August-Universitaet Goettingen, Germany
!
! This program is free software: you can redistribute it and/or modify it 
! under the terms of the GNU General Public License as published by the 
! Free Software Foundation, either version 3 of the License, or 
! (at your option) any later version.
!
! This program is distributed in the hope that it will be useful, but 
! WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
! or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
! for more details.
!
! You should have received a copy of the GNU General Public License along 
! with this program. If not, see http://www.gnu.org/licenses. 
!######################################################################
!! called by: - electrostatic.f90  
!!            - electrostatic_para.f90   
!!            - getcoulombforces.f90
!!            - getcoulombforcesone.f90
!!            - getcoulombforces_para.f90
!!            - splitcoulombforces.f90
!!
      subroutine getscreenfunctionforelectrostatics(rij,&
                    fscreen,fscreenderiv,drijdxyz)
!!
      use globaloptions
!!
      implicit none
!!
      real*8, intent(in)        :: rij,drijdxyz
      real*8, intent(out)       :: fscreen, fscreenderiv
      real*8, parameter         :: pi = 3.141592654d0
!!
      if(rij.gt.rscreen_cut) then      
        fscreen=1.0d0
        fscreenderiv=0.0d0
      elseif(rij.lt.rscreen_onset) then      
        fscreen=0.0d0
        fscreenderiv=0.0d0
      else
        fscreen=0.5d0*(1.0d0-dcos(pi*(rij-rscreen_onset)&
          /(rscreen_cut-rscreen_onset)))
        fscreenderiv=0.5d0*dsin(pi*(rij-rscreen_onset)/(rscreen_cut-rscreen_onset))* &
                     pi/(rscreen_cut-rscreen_onset)*drijdxyz
      endif
!!
      return
      end
!!
