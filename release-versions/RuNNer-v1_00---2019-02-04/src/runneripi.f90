!######################################################################
! This routine is part of
! RuNNer - RuNNer Neural Network Energy Representation
! (c) 2008-2019 Prof. Dr. Joerg Behler 
! Georg-August-Universitaet Goettingen, Germany
!
! This program is free software: you can redistribute it and/or modify it 
! under the terms of the GNU General Public License as published by the 
! Free Software Foundation, either version 3 of the License, or 
! (at your option) any later version.
!
! This program is distributed in the hope that it will be useful, but 
! WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
! or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
! for more details.
!
! You should have received a copy of the GNU General Public License along 
! with this program. If not, see http://www.gnu.org/licenses. 
!######################################################################
      module runneripi 
!SK: ipistring is the hostaddress: host name or IP address; if you want
!to use UNIX sockets, add 'UNIX' before the host adress
! desc_str will take care of UNIX 
!SK: ipisocket should contain the number of the port that the
!ipi-wrapper is listening to

      implicit none

      character*40 :: desc_str
      character*40 :: ipisocket
      character*40 :: ipistring 
 
      logical luseipi

      end module runneripi 

! changes SK: ipistring - charachter length; ipisocket - type character
