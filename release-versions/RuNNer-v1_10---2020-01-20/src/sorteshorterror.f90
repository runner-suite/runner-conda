!######################################################################
! This routine is part of
! RuNNer - RuNNer Neural Network Energy Representation
! (c) 2008-2020 Prof. Dr. Joerg Behler 
! Georg-August-Universitaet Goettingen, Germany
!
! This program is free software: you can redistribute it and/or modify it 
! under the terms of the GNU General Public License as published by the 
! Free Software Foundation, either version 3 of the License, or 
! (at your option) any later version.
!
! This program is distributed in the hope that it will be useful, but 
! WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
! or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
! for more details.
!
! You should have received a copy of the GNU General Public License along 
! with this program. If not, see http://www.gnu.org/licenses. 
!######################################################################
!! called by:
!! - optimize_short_combined.f90
!! - optimize_short_combinedpair.f90
!!
      subroutine sorteshorterror(npoints,energyerror_list,lusee)
!!
      use fileunits
      use fittingoptions
      use globaloptions
!!
      implicit none
!!
      integer npoints
      integer i1,i2
      integer istruct(nblock)                                                ! internal 
      real*8 getthreshold 
!!
      real*8 energyerror_list(nblock)                                        ! in
      real*8 energyerror_temp(npoints)                                       ! in
      real*8 energyerror_copy(npoints)                                       ! in
      real*8 temp                                                            ! internal
      real*8 ethres                                                          ! out
!!
      logical lusee(nblock)                                                  ! out 
!!
!! initializations
      lusee(:)=.false.
      do i1=1,npoints
        istruct(i1)=i1
        energyerror_temp(i1)=energyerror_list(i1)
      enddo
!!
!! determine the threshold for update
      temp=(1.d0-worste)*dble(npoints)
      i2=int(temp)
!!      write(ounit,*)'find point ',i2
      energyerror_copy(:)=energyerror_temp(:)
!! caution: getthreshold changes order of energyerror_copy
      ethres=getthreshold(i2,npoints,energyerror_copy)
!!
!! set lusee array of energies to be used
      do i1=1,npoints
        if(energyerror_temp(i1).gt.ethres)then
          lusee(istruct(i1))=.true.
        endif
      enddo
!!
      return
      end
