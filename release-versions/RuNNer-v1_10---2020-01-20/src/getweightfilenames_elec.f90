!######################################################################
! This routine is part of
! RuNNer - RuNNer Neural Network Energy Representation
! (c) 2008-2020 Prof. Dr. Joerg Behler 
! Georg-August-Universitaet Goettingen, Germany
!
! This program is free software: you can redistribute it and/or modify it 
! under the terms of the GNU General Public License as published by the 
! Free Software Foundation, either version 3 of the License, or 
! (at your option) any later version.
!
! This program is distributed in the hope that it will be useful, but 
! WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
! or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
! for more details.
!
! You should have received a copy of the GNU General Public License along 
! with this program. If not, see http://www.gnu.org/licenses. 
!######################################################################
!! called by:
!!
      subroutine getweightfilenames_elec()
!!
      use globaloptions
      use fittingoptions
!!
      implicit none
!!
      integer icount
      integer i1,i2
!!
      character*20 filenametemp                              ! internal
!!
      filenamewe(:)          ='000000.ewald.000.out'
      do i1=1,nelem
!! electrostatic weights
        filenametemp=filenamewe(i1)
        if(nucelem(i1).gt.99)then
          write(filenametemp(14:16),'(i3)')nucelem(i1)
        elseif(nucelem(i1).gt.9)then
          write(filenametemp(15:16),'(i2)')nucelem(i1)
        else
          write(filenametemp(16:16),'(i1)')nucelem(i1)
        endif
        filenamewe(i1)=filenametemp
      enddo
!!
      return
      end
