!######################################################################
! This routine is part of
! RuNNer - RuNNer Neural Network Energy Representation
! (c) 2008-2020 Prof. Dr. Joerg Behler 
! Georg-August-Universitaet Goettingen, Germany
!
! This program is free software: you can redistribute it and/or modify it 
! under the terms of the GNU General Public License as published by the 
! Free Software Foundation, either version 3 of the License, or 
! (at your option) any later version.
!
! This program is distributed in the hope that it will be useful, but 
! WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
! or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
! for more details.
!
! You should have received a copy of the GNU General Public License along 
! with this program. If not, see http://www.gnu.org/licenses. 
!######################################################################
!! called by: 
!! - getpointindex 
!!
!! JB this subroutine has been created for the release version of RuNNer

      subroutine sortpointindex(ntrain,z,pointindex)
!!
      implicit none
!!
      integer ntrain                ! in 
      integer pointindex(ntrain)    ! in/out
      integer indextemp
      integer i1
!!
      real*8 z(ntrain)              ! in/out
      real*8 ztemp
!!


 10     continue
        do i1=1,ntrain-1
          if(z(i1).gt.z(i1+1))then
            ztemp=z(i1)
            indextemp=pointindex(i1)
            z(i1)=z(i1+1)
            pointindex(i1)=pointindex(i1+1)
            z(i1+1)=ztemp
            pointindex(i1+1)=indextemp
            goto 10
          endif
        enddo

      return
      end
