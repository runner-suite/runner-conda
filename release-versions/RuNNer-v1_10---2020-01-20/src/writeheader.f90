!######################################################################
! This routine is part of
! RuNNer - RuNNer Neural Network Energy Representation
! (c) 2008-2020 Prof. Dr. Joerg Behler 
! Georg-August-Universitaet Goettingen, Germany
!
! This program is free software: you can redistribute it and/or modify it 
! under the terms of the GNU General Public License as published by the 
! Free Software Foundation, either version 3 of the License, or 
! (at your option) any later version.
!
! This program is distributed in the hope that it will be useful, but 
! WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
! or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
! for more details.
!
! You should have received a copy of the GNU General Public License along 
! with this program. If not, see http://www.gnu.org/licenses. 
!######################################################################
!! called by:
!! - main.f90
!!
      subroutine writeheader()
!!
      use mpi_mod
      use fileunits
!!
      implicit none
!!
      if(mpirank.eq.0)then
        if(ounit.ne.6)then
          open(ounit,file='runner.out',form='formatted',status='replace')
        endif
        open(debugunit,file='debug.out',form='formatted',status='replace')
!!
        write(ounit,*)'-------------------------------------------------------------'
        write(ounit,*)'---------------------- Welcome to the -----------------------'
        write(ounit,*)'    RuNNer Neural Network Energy Representation - RuNNer     '
        write(ounit,*)'----------     Release v1_1 (January 20, 2020)     ----------'
        write(ounit,*)'----------  (c) 2008-2020 Prof. Dr. Joerg Behler   ----------'
        write(ounit,*)'----------  Georg-August-Universitaet Goettingen   ----------'
        write(ounit,*)'----------           Theoretische Chemie           ----------'
        write(ounit,*)'----------              Tammannstr. 6              ----------'
        write(ounit,*)'----------        37077 Goettingen, Germany        ----------'
        write(ounit,*)'-------------------------------------------------------------'
        write(ounit,*)'-------------------------------------------------------------'
        write(ounit,*)' This program is free software: you can redistribute it and/or modify it' 
        write(ounit,*)' under the terms of the GNU General Public License as published by the '
        write(ounit,*)' Free Software Foundation, either version 3 of the License, or '
        write(ounit,*)' (at your option) any later version.'
        write(ounit,*)'   '
        write(ounit,*)' This program is distributed in the hope that it will be useful, but '
        write(ounit,*)' WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY' 
        write(ounit,*)' or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License '
        write(ounit,*)' for more details.'
        write(ounit,*)'     '
        write(ounit,*)' You should have received a copy of the GNU General Public License along' 
        write(ounit,*)' with this program. If not, see http://www.gnu.org/licenses. '
        write(ounit,*)'-------------------------------------------------------------'
        write(ounit,*)'-------------------------------------------------------------'
        write(ounit,*)'When using RuNNer, please cite the following papers:'
        write(ounit,*)'J. Behler, Angew. Chem. Int. Ed. 56, 12828 (2017).'
        write(ounit,*)'J. Behler, Int. J. Quant. Chem. 115, 1032 (2015).'
        write(ounit,*)'-------------------------------------------------------------'
        write(ounit,*)'Whenever using high-dimensional NNPs irrespective of the Code please cite:'
        write(ounit,*)'J. Behler and M. Parrinello, Phys. Rev. Lett. 98, 146401 (2007).'
        write(ounit,*)'-------------------------------------------------------------'
        write(ounit,*)'The reference for the atom-centered symmetry functions is:'
        write(ounit,*)'J. Behler, J. Chem. Phys. 134, 074106 (2011).'
        write(ounit,*)'-------------------------------------------------------------'
        write(ounit,*)'For high-dimensional NNs including electrostatics:'
        write(ounit,*)'N. Artrith, T. Morawietz, and J. Behler, Phys. Rev. B 83, 153101 (2011).'
        write(ounit,*)'-------------------------------------------------------------'
        write(ounit,*)'-------------------------------------------------------------'
        write(ounit,*)'RuNNer has been written by Joerg Behler'
        write(ounit,*)' '
        write(ounit,*)'*** with contributions from some friends ***'
        write(ounit,*)' '
        write(ounit,*)'Tobias Morawietz - Nguyen Widrow weights and electrostatic screening'
        write(ounit,*)'Jovan Jose Kochumannil Varghese - Pair symmetry functions and pair NNPs'
        write(ounit,*)'Michael Gastegger and Philipp Marquetand - element decoupled Kalman filter'
        write(ounit,*)'Andreas Singraber - more efficient symmetry function type 3 implementation'
        write(ounit,*)'Sinja Klees and Mariana Rossi - some infrastructure for i-Pi compatibility'
        write(ounit,*)'-------------------------------------------------------------'
        write(ounit,*)'-------------------------------------------------------------'
!!
        call generalinfo()
      endif ! mpirank.eq.0
!!
      return
      end
