!######################################################################
! This routine is part of
! RuNNer - RuNNer Neural Network Energy Representation
! (c) 2008-2020 Prof. Dr. Joerg Behler 
! Georg-August-Universitaet Goettingen, Germany
!
! This program is free software: you can redistribute it and/or modify it 
! under the terms of the GNU General Public License as published by the 
! Free Software Foundation, either version 3 of the License, or 
! (at your option) any later version.
!
! This program is distributed in the hope that it will be useful, but 
! WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
! or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
! for more details.
!
! You should have received a copy of the GNU General Public License along 
! with this program. If not, see http://www.gnu.org/licenses. 
!######################################################################

!! MG: Added: Subroutine to analyze average trace of correlation matrix
!! MG: Added: and print information to output file

!! called by:
!! - fitting_short_atomic.f90
!! 
      subroutine analyzecorrmat(maxcorrdim,corrdim,corrmatrix_list,num_weights_short_atomic)
!!
      use globaloptions
      use fileunits
!!
      implicit none
!!
      integer i1,i2
      integer num_weights_short_atomic(nelem)  ! in
      integer maxcorrdim                       ! in
      integer corrdim(nelem)                   ! in
      integer currpos                          ! internal
      real*8 trace_sum                         ! internal
      real*8 corrmatrix_list(maxcorrdim,nelem) ! in
!!
      do i1=1,nelem
        trace_sum=0d0
        currpos=1
        !! MG: Sum over the elements in the packed triangular matrix
        !! MG: corresponding to the diagonal positions.
        do i2=1,num_weights_short_atomic(i1)
          trace_sum=trace_sum+corrmatrix_list(currpos,i1)
          currpos=currpos+num_weights_short_atomic(i1)-i2+1
        enddo ! i2
        trace_sum=trace_sum/dble(num_weights_short_atomic(i1)) 
        !!write(ounit,'(a10,x,a2,x,1f20.2)') ' CORRMAT  ',element(i1),trace_sum 
!!        write(ounit,'(a10,x,a2,x,1e20.5e3)') ' CORRMAT  ',element(i1),trace_sum 
      enddo ! i1
      return
      end
