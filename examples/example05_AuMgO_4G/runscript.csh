#! /bin/csh

# convert input data to symmetry functions and split data into training and test set
cp input_1.nn.charge input.nn
./RuNNer.x | tee mode1_charge.out

# run the fit
cp input_2.nn.charge input.nn
./RuNNer.x | tee mode2_charge.out

# prepare fit for second NN
cp optweightse.008.out weightse.008.data
cp optweightse.012.out weightse.012.data
cp optweightse.013.out weightse.013.data
cp optweightse.079.out weightse.079.data
cp opthardness.008.out hardness.008.data
cp opthardness.012.out hardness.012.data
cp opthardness.013.out hardness.013.data
cp opthardness.079.out hardness.079.data

# convert input data to symmetry functions and split data into training and test set
cp input_1.nn.short input.nn
./RuNNer.x | tee mode1_short.out

# run the fit
cp input_2.nn.short input.nn
./RuNNer.x | tee mode2_short.out

# prepare fit for mode 3
cp optweights.008.out weights.008.data
cp optweights.012.out weights.012.data
cp optweights.013.out weights.013.data
cp optweights.079.out weights.079.data
# run prediction for first structure in input.data
cp input_3.nn.short input.nn
./RuNNer.x | tee mode3.out

