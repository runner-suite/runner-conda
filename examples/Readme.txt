Examples:

Please note that all examples here are unrealistically small and also the settings in the input.nn file are not representative. The purpose is to illustrate the workflow.

Example 1:
----------
Small test fit for 100 liquid water structures.

Example 3:
----------
Small test fit for 801 configurations of a graphene surface with 24 C-atoms at different temperatures ranging from 0K to 800K.
This is a reduced data set used to fit a NN-PES published in Phys. Chem. Chem. Phys. 22, 26113-26120 (2020). 
In case you need the full data set or the PES, contact the corresponding authors A. Kandratsenka and S. Wille by e-mail.
Mail to: akandra@mpibpc.mpg.de
         sebastian.wille@mpibpc.mpg.de
How the output should look like if one runs modes 1-3 is stored in output/. Therefore, just execute runscript.sh.

Example 7:
----------
mHDNNP test for 45 MnO structures with different magnetic orders.
Further information about mHDNNPs is given in M. Eckhoff and J. Behler, npj Comput. Mater. 7, 170 (2021).

Example 8:
----------
HDNNS test for 80 LiMn2O4 structures.
Further information about HDNNSs is given in M. Eckhoff, K. N. Lausch, P. E. Bloechl, and J. Behler, J. Chem. Phys. 153, 164107 (2020).
