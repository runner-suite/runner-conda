#!/usr/bin/python

####################################################################################################

####################################################################################################

'''
calc_init_weights_parameters.py
written by Marco Eckhoff (03.03.2020)

calculates the mean atomic energies and the energy range using the information of an input.data file
'''

####################################################################################################

####################################################################################################

import numpy as np

####################################################################################################

####################################################################################################

# name of the input.data file
input_name = 'input.data'
# list of the elements in the input.data file
elements = ['O', 'Mn']
# list of the values of the atom_energy keywords in the input.nn file (element order has to be the
# same as before)
atom_energies = [-75.0622724, -1162.6706313]

####################################################################################################

####################################################################################################

def read_data(input_name, elements, atom_energies):
  '''
  '''
  data = []
  energies = []
  energies_per_atom = []
  counter = {}
  for element in elements:
    counter[element] = 0
  with open(input_name) as f:
    for line in f.readlines():
      if line.startswith('atom'):
        counter[line.split()[4]] += 1
      elif line.startswith('energy'):
        energies.append(float(line.strip().split()[1]))
        counts = []
        for i in range(len(elements)):
          counts.append(counter[elements[i]])
          counter[elements[i]] = 0
          energies[-1] -= counts[-1]*atom_energies[i]
        data.append(counts)
        energies_per_atom.append(energies[-1]/np.array(counts).sum())
  data = np.array(data)
  energies = np.array(energies)
  energies_per_atom = np.array(energies_per_atom)

  return data, energies, energies_per_atom

####################################################################################################

####################################################################################################

data, energies, energies_per_atom = read_data(input_name, elements, atom_energies)

energy_range = energies_per_atom.max()-energies_per_atom.min()
energy_mean = energies_per_atom.mean()
x = np.linalg.lstsq(data, energies)[0]

print('Mean atomic energies for each element')
for i in range(len(elements)):
  print('{0}: {1} Hartree/atom'.format(elements[i], round(x[i],8)))
print('\nAlternative mean atomic energy of all atoms (in case the stoichiometry of the elements is not varied\nin the input.data file and thus the mean atomic energies for each element will yield unreasonable\nhigh or low numbers)\n{0} Hartree/atom\n'.format(round(energy_mean,8)))
print('Energy range\n{0} Hartree/atom'.format(round(energy_range,8)))
