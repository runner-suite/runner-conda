#!/bin/bash

### RuNNer mode 1 - Split the reference data to training and test data
# Check the correct setting of the mode
sed -i s/'runner_mode 2'/'runner_mode 1'/g input.nn
sed -i s/'runner_mode 3'/'runner_mode 1'/g input.nn
# Execute RuNNer
./RuNNer.x | tee mode1.out

### RuNNer mode 2 - Fit the mHDNNP on the training data
# Change the setting of the mode
sed -i s/'runner_mode 1'/'runner_mode 2'/g input.nn
# Use the modified Xavier initialization referring to M. Eckhoff, F. Schoenewald, M. Risch, C. A. Volkert, P. E. Bloechl, and J. Behler, Phys. Rev. B 102, 174102 (2020).
./init_weights.py
# Execute RuNNer
./RuNNer.x | tee mode2.out

### RuNNer mode 3 - Predict energies and forces for the structures in input.data
# Change the setting of the mode
sed -i s/'runner_mode 2'/'runner_mode 3'/g input.nn
# Use the optweights files as input for mode 3
cp optweights.008.out weights.008.data
cp optweights.025.out weights.025.data
# Execute RuNNer
./RuNNer.x | tee mode3.out
