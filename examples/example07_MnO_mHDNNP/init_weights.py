#!/usr/bin/python

####################################################################################################

####################################################################################################

'''
init_weights.py
written by Marco Eckhoff (02.03.2020)

writes initial weights files for RuNNer using a scheme similar to the Xavier initialisation whereby
the weights of the last hidden layer to the output layer are scaled by energy_range and the bias
weight of the output neuron is set to mean_atomic_energy
'''

####################################################################################################

####################################################################################################

import numpy as np

####################################################################################################

####################################################################################################

# list of the element IDs for the weights files
element_ids = ['008', '025']
# list of lists of the atomic neural network architectures for the different elements (element order
# has to be the same as before), in each list you have to specify the number of neurons in each
# layer including the input and output layer) 
atomic_NNs = [[33, 20, 15, 10, 1], [62, 20, 15, 10, 1]]
# mean atomic energies of the different elements (element order has to be the same as before),
# if the stoichiometry of the elements in the input.data file is not varied, you can also calculate
# the mean atomic energy of all atoms and use this value for all elements
mean_atomic_energies = [-0.21016184, -0.16907798]
# energy range in Hartree per atom of the structures in the input.data file
energy_range = 0.00791636
# seed of the random number generator
seed = 2

####################################################################################################

####################################################################################################

def write_weights(element_id, node, mean_atomic_energy, energy_range):
  '''
  '''
  with open('weights.'+element_id+'.data', 'w') as f:
    counter = 1
    for i in range(len(node)-1):
      if i+1==len(node)-1:
        for j in range(node[i]):
          for k in range(node[i+1]):
            weight = energy_range/np.sqrt(node[i])*np.random.random()-energy_range/2.0/np.sqrt(node[i])
            f.write('{0:>18.10f} a{1:>10}{2:>6}{3:>6}{4:>6}{5:>6}\n'.format(weight, counter, i, j+1, i+1, k+1))
            counter += 1
        for k in range(node[i+1]):
          f.write('{0:>18.10f} b{1:>10}{2:>6}{3:>6}\n'.format(mean_atomic_energy, counter, i+1, k+1))
          counter += 1
      else:
        for j in range(node[i]):
          for k in range(node[i+1]):
            weight = 2.0/np.sqrt(node[i])*np.random.random()-1.0/np.sqrt(node[i])
            f.write('{0:>18.10f} a{1:>10}{2:>6}{3:>6}{4:>6}{5:>6}\n'.format(weight, counter, i, j+1, i+1, k+1))
            counter += 1
        for k in range(node[i+1]):
          f.write('{0:>18.10f} b{1:>10}{2:>6}{3:>6}\n'.format(0.0, counter, i+1, k+1))
          counter += 1

####################################################################################################

####################################################################################################

np.random.seed(seed)

for i in range(len(element_ids)):
  write_weights(element_ids[i], atomic_NNs[i], mean_atomic_energies[i], energy_range)
