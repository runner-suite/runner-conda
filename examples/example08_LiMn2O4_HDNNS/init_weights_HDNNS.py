#!/usr/bin/python

####################################################################################################

####################################################################################################

'''
init_weights_HDNNS.py
written by Marco Eckhoff (17.03.2022)

writes initial weights files for RuNNer using a scheme similar to the Xavier initialisation whereby
the weights of the last hidden layer to the output layer are scaled by spin_range and the bias
weight of the output neuron is set to spin_mean
'''

####################################################################################################

####################################################################################################

import numpy as np

####################################################################################################

####################################################################################################

# list of the element IDs for the weights files
element_ids = ['003', '008', '025']
# list of lists of the atomic neural network architectures for the different elements (element order
# has to be the same as before), in each list you have to specify the number of neurons in each
# layer including the input and output layer) 
atomic_NNs = [[15, 4, 3, 2, 1], [15, 4, 3, 2, 1], [110, 20, 15, 10, 1]]
# list of the mean absolute atomic spins for each element in the input.data file (element order has
# to be the same as before)
spin_mean = [0.00088125, 0.01650352, 1.78709922]
# list of the absolute atomic spin ranges for each element in the input.data file (element order has
# to be the same as before)
spin_range = [0.004, 0.061, 0.578]
# seed of the random number generator
seed = 2

####################################################################################################

####################################################################################################

def write_weightse(element_id, node, spin_mean, spin_range):
  '''
  '''
  with open('weightse.'+element_id+'.data', 'w') as f:
    counter = 1
    for i in range(len(node)-1):
      if i+1==len(node)-1:
        for j in range(node[i]):
          for k in range(node[i+1]):
            weight = spin_range/np.sqrt(node[i])*np.random.random()-spin_range/2.0/np.sqrt(node[i])
            f.write('{0:>18.10f} a{1:>10}{2:>6}{3:>6}{4:>6}{5:>6}\n'.format(weight, counter, i, j+1, i+1, k+1))
            counter += 1
        for k in range(node[i+1]):
          f.write('{0:>18.10f} b{1:>10}{2:>6}{3:>6}\n'.format(spin_mean, counter, i+1, k+1))
          counter += 1
      else:
        for j in range(node[i]):
          for k in range(node[i+1]):
            weight = 2.0/np.sqrt(node[i])*np.random.random()-1.0/np.sqrt(node[i])
            f.write('{0:>18.10f} a{1:>10}{2:>6}{3:>6}{4:>6}{5:>6}\n'.format(weight, counter, i, j+1, i+1, k+1))
            counter += 1
        for k in range(node[i+1]):
          f.write('{0:>18.10f} b{1:>10}{2:>6}{3:>6}\n'.format(0.0, counter, i+1, k+1))
          counter += 1

####################################################################################################

####################################################################################################

np.random.seed(seed)

for i in range(len(element_ids)):
  write_weightse(element_ids[i], atomic_NNs[i], spin_mean[i], spin_range[i])
