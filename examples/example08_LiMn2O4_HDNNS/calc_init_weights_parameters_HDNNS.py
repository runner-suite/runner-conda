#!/usr/bin/python

####################################################################################################

####################################################################################################

'''
calc_init_weights_parameters_HDNNS.py
written by Marco Eckhoff (17.03.2022)

calculates the mean absolute atomic spins and the absolute atomic spin range using the information
of an input.data file
'''

####################################################################################################

####################################################################################################

import numpy as np

####################################################################################################

####################################################################################################

# name of the input.data file
input_name = 'input.data'
# list of the elements in the input.data file
elements = ['Li', 'O', 'Mn']

####################################################################################################

####################################################################################################

def read_data(input_name, elements):
  '''
  '''
  spins = {}
  for element in elements:
    spins[element] = []

  with open(input_name) as f:
    for line in f.readlines():
      line = line.strip()
      if line.startswith('begin'):
        try:
          line = line.split()
          elem_symbol = line.index('elem_symbol')
          atom_spin = line.index('atom_spin')
        except ValueError:
          print('ERROR: Atomic spins are not given for all structures.')
          exit()
      elif line.startswith('atom'):
        line = line.split()
        spins[line[elem_symbol]].append(float(line[atom_spin]))

  return spins

####################################################################################################

####################################################################################################

spins = read_data(input_name, elements)

spin_mean = []
spin_range = []
for element in elements:
  spins[element] = np.absolute(np.array(spins[element]))
  spin_mean.append(spins[element].mean())
  spin_range.append(spins[element].max()-spins[element].min())

print('Mean absolute atomic spins for each element')
for i in range(len(elements)):
  print('{0}: {1} hbar'.format(elements[i], round(spin_mean[i],8)))
print('\nAbsolute atomic spin range for each element')
for i in range(len(elements)):
  print('{0}: {1} hbar'.format(elements[i], round(spin_range[i],8)))
