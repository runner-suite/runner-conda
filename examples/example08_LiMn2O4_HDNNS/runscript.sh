#!/bin/bash

### RuNNer mode 1 - Split the reference data to training and test data
# Check the correct setting of the mode
sed -i s/'runner_mode 2'/'runner_mode 1'/g input.nn
sed -i s/'runner_mode 3'/'runner_mode 1'/g input.nn
# Execute RuNNer
./RuNNer.x | tee mode1.out

### RuNNer mode 2 - Fit the HDNNS on the training data
# Change the setting of the mode
sed -i s/'runner_mode 1'/'runner_mode 2'/g input.nn
# Use the modified Xavier initialization referring to M. Eckhoff, K. N. Lausch, P. E. Bloechl, and J. Behler, J. Chem. Phys. 153, 164107 (2020).
python init_weights_HDNNS.py
# Execute RuNNer
./RuNNer.x | tee mode2.out

### RuNNer mode 3 - Predict absolute atomic spins for the structures in input.data
# Change the setting of the mode
sed -i s/'runner_mode 2'/'runner_mode 3'/g input.nn
# Use the optweightse files as input for mode 3
cp optweightse.003.out weightse.003.data
cp optweightse.008.out weightse.008.data
cp optweightse.025.out weightse.025.data
# Execute RuNNer
./RuNNer.x | tee mode3.out
