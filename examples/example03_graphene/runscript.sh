#!/bin/bash

# convert input data to symmetry functions and split data into training and test set
sed -i -e "s/runner_mode.*/runner_mode 1/g" input.nn
./RuNNer.serial.x | tee mode1.out

# run the fit
sed -i -e "s/runner_mode.*/runner_mode 2/g" input.nn
./RuNNer.serial.x | tee mode2.out

# prepare fit for mode 3
cp optweights.006.out weights.006.data

# run prediction for all structures in input.data
sed -i -e "s/runner_mode.*/runner_mode 3/g" input.nn
./RuNNer.serial.x | tee mode3.out
