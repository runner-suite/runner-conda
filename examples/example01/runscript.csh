#! /bin/csh

# convert input data to symmetry functions and split data into training and test set
cp input.nn-1 input.nn
./RuNNer.serial.x | tee mode1.out

# run the fit
cp input.nn-2 input.nn
./RuNNer.serial.x | tee mode2.out

# prepare fit for mode 3
cp optweights.001.out weights.001.data
cp optweights.008.out weights.008.data

# run prediction for first structure in input.data
cp input.nn-3 input.nn
./RuNNer.serial.x | tee mode3.out

