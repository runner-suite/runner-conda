#!/bin/csh

# convert input data to symmetry functions and split data into training and test set
cp input_1.nn.charge input.nn
./RuNNer.x | tee mode1_charge.out

# run the fit
cp input_2.nn.charge input.nn
./RuNNer.x | tee mode2_charge.out

# change weight files for the short-range part
cp optweightse.001.out weightse.001.data
cp optweightse.006.out weightse.006.data


# convert input data to symmetry functions and split data into training and test set
cp input_1.nn.short input.nn
./RuNNer.x | tee mode1_short.out

# run the fit
cp input_2.nn.short input.nn
./RuNNer.x | tee mode2_short.out

# prepare fit for mode 3
cp optweights.001.out weights.001.data
cp optweights.006.out weights.006.data
# run prediction for first structure in input.data
cp input_3.nn.short input.nn
./RuNNer.x | tee mode3.out

