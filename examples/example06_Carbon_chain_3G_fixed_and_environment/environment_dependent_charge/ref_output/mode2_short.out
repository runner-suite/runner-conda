 -------------------------------------------------------------
 ---------------------- Welcome to the -----------------------
     RuNNer Neural Network Energy Representation - RuNNer     
 ----------  (c) 2008-2021 Prof. Dr. Joerg Behler   ----------
 ----------  Georg-August-Universitaet Goettingen   ----------
 ----------           Theoretische Chemie           ----------
 ----------              Tammannstr. 6              ----------
 ----------        37077 Goettingen, Germany        ----------
 -------------------------------------------------------------
 -------------------------------------------------------------
  This program is free software: you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the 
  Free Software Foundation, either version 3 of the License, or 
  (at your option) any later version.
    
  This program is distributed in the hope that it will be useful, but 
  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
  or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
  for more details.
      
  You should have received a copy of the GNU General Public License along
  with this program. If not, see http://www.gnu.org/licenses. 
 -------------------------------------------------------------
 -------------------------------------------------------------
 When using RuNNer, please cite the following papers:
 J. Behler, Angew. Chem. Int. Ed. 56, 12828 (2017).
 J. Behler, Int. J. Quant. Chem. 115, 1032 (2015).
 -------------------------------------------------------------
 Whenever using high-dimensional NNPs irrespective of the Code please cite:
 J. Behler and M. Parrinello, Phys. Rev. Lett. 98, 146401 (2007).
 -------------------------------------------------------------
 The reference for the atom-centered symmetry functions is:
 J. Behler, J. Chem. Phys. 134, 074106 (2011).
 -------------------------------------------------------------
 For high-dimensional NNs including electrostatics:
 N. Artrith, T. Morawietz, and J. Behler, Phys. Rev. B 83, 153101 (2011).
 -------------------------------------------------------------
 -------------------------------------------------------------
 RuNNer has been written by Joerg Behler
  
 *** with contributions from some friends ***
  
 Tobias Morawietz - Nguyen Widrow weights and electrostatic screening
 Jovan Jose Kochumannil Varghese - Pair symmetry functions and pair NNPs
 Michael Gastegger and Philipp Marquetand - element decoupled Kalman filter
 Andreas Singraber - more efficient symmetry function type 3 implementation
 Sinja Klees and Mariana Rossi - some infrastructure for i-Pi compatibility
 Emir Kocer - Symmetry function groups
 Fenfei Wei and Emir Kocer - Hessian, frequencies and normal modes
 Alexander Knoll - vdW corrections long ranged
 -------------------------------------------------------------
 -------------------------------------------------------------
 General job information:
 -------------------------------------------------------------
 Executing host    : tc-pc23.uni-pc.gwdg.
 User name         : kenko               
 Starting date     : 22.11.2021
 Starting time     : 15 h 53 min 
 Working directory :                                                             
 -------------------------------------------------------------
 -------------------------------------------------------------
 Serial run requested
 -------------------------------------------------------------
 Reading control parameters from input.nn
 =============================================================
 -------------------------------------------------------------
 =============================================================
 General input parameters:
 -------------------------------------------------------------
 Short range NN is on
 Electrostatic NN is off
 vdW corrections switched off
 -------------------------------------------------------------
 RuNNer nn_type_short                                   1
 RuNNer is started in mode for fitting (2)
 debugging mode is                                        F
 parallelization mode                                     1
 enable detailed time measurement                         F
 using symmetry function groups                           F
 enable detailed time measurement at epoch level          F
 silent mode                                              F
 NN force check                                           F
 number of elements                                       2
 elements (sorted):
  1 H 
  6 C 
 seed for random number generator                        10
 random number generator type                             5
 remove free atom reference energies                      T
 remove vdw dispersion energy and forces              F
 shortest allowed bond in structure                   0.400
 Cutoff_type for symmetry function is                     2
 Cutoff_alpha for inner cutoff radius is              0.000
 -------------------------------------------------------------
 Short range NN specifications:
 -------------------------------------------------------------
 global hidden layers short range NN                      2
 global nodes hidden layers short NN                10   10
 global activation functions short                      ttl
 -------------------------------------------------------------
 Electrostatic specifications:
 -------------------------------------------------------------
 electrostatic_type (nn_type_elec)                        0
 Ewald alpha                                          0.000
 Ewald cutoff                                         0.000
 Ewald kmax                                               0
 Screening electrostatics                                  4.800000      8.000000
 -------------------------------------------------------------
 General fitting parameters:
 -------------------------------------------------------------
 number of fitting epochs                                10
 print date and time for each epoch                       F
 number of data sets in memory                          500
 Fitting mode 1 (online learning) selected         
 Randomly mixing all points in training set               T
 save Kalman filter data                                  F
 restart from old Kalman filter data                      F
 rescale symmetry functions                               T
 rescale atomic charges                                  F
 min value of scaled short range symmetry functions      0.000
 max value of scaled short range symmetry functions      1.000
 remove CMS from symmetry functions                       T
 remove CMS from atomic charges                           F
 calculate symmetry function correlation                  F
 weight analysis                                          F
 environment analysis                                     F
 find contradictions                                      F
 fix some weights                                         F
 using growth mode for fitting                            F
 global fit of short and charge NN (not implemented)      F
 error unit for fitting                                  eV
 Reading formatted files 
 Writing formatted files 
 Resetting Kalman filter matrices each epoch              F
 Regularization for NN weights is used and the lambda value:  1.000000000000000E-005
 Preconditioning of weights is switched on
 -------------------------------------------------------------
 Fitting parameters short range part:
 -------------------------------------------------------------
 using forces for fitting                                 T
 using Kalman filter optimization (1) for short range energy
 using Kalman filter optimization (1) for short range forces
 short energy error threshold                    0.80000000
 short force error threshold                     0.80000000
 Kalman lambda (short)                           0.98000000
 Kalman nue (short)                              0.99870000
 use_noisematrix                                          F
 Kalman damp (short energy)                      1.00000000
 Kalman damp (short force)                       1.00000000
 restart fit with old weights (short)                     F
 scaling factor for force update (scalefactorf)  1.00000000
 grouping energies in blocks of                           1
 fraction of energies used for update                 1.000
 grouping forces in blocks of                             1
 fraction of forces used for update                   0.025
 weights_min                                         -1.000
 weights_max                                          1.000
 Using Nguyen Widrow weights for short range NN
 Using repeated energy updates after each force update
 max_energy                                       10000.000
 max force component used for fitting             10000.000 Ha/Bohr
 noise energy threshold                          0.00000000 Ha/atom
 noise force threshold                           0.00000000 Ha/Bohr
 -------------------------------------------------------------
 Fitting parameters electrostatic part:
 -------------------------------------------------------------
 charge error threshold                          0.00000000
 restart fit with old weights (charge)                    F
 fraction of charges used for update                  1.000
 using total charge constraint                            F
 noise charge threshold                          0.00000000  e
 -------------------------------------------------------------
 Fitting output options:
 -------------------------------------------------------------
 write weights in every epoch                             1
 write temporary weights each epoch                       F
 write trainpoints.out and testpoints.out                 T
 write binding energies only                              F
 write traincharges.out and testcharges.out               F
 write trainforces.out and testforces.out                 T
 =============================================================
 Element pairs:   3    , shortest distance (Bohr)
 =============================================================
 => short range NN weights type 1                 H        371
 => short range NN weights type 1                 C        491
 => electrostatic NN weights                      H        631
 => electrostatic NN weights                      C        811
 -------------------------------------------------------------
 -------------------------------------------------------------
 Atomic reference energies read from input.nn:
 H         -0.45890731
 C        -37.74811193
 -------------------------------------------------------------
 -------------------------------------------------
 Atomic short range NN for element: H 
 architecture       24   10   10    1
 -------------------------------------------------
   1   G  t  t  l
   2   G  t  t   
   3   G  t  t   
   4   G  t  t   
   5   G  t  t   
   6   G  t  t   
   7   G  t  t   
   8   G  t  t   
   9   G  t  t   
  10   G  t  t   
  11   G
  12   G
  13   G
  14   G
  15   G
  16   G
  17   G
  18   G
  19   G
  20   G
  21   G
  22   G
  23   G
  24   G
 -------------------------------------------------
 Atomic short range NN for element: C 
 architecture       36   10   10    1
 -------------------------------------------------
   1   G  t  t  l
   2   G  t  t   
   3   G  t  t   
   4   G  t  t   
   5   G  t  t   
   6   G  t  t   
   7   G  t  t   
   8   G  t  t   
   9   G  t  t   
  10   G  t  t   
  11   G
  12   G
  13   G
  14   G
  15   G
  16   G
  17   G
  18   G
  19   G
  20   G
  21   G
  22   G
  23   G
  24   G
  25   G
  26   G
  27   G
  28   G
  29   G
  30   G
  31   G
  32   G
  33   G
  34   G
  35   G
  36   G
 ---------------------------------------------------
 Electrostatic NN for element: H 
 architecture       24   15   15    1
 ---------------------------------------------------
   1   G  t  t  l
   2   G  t  t   
   3   G  t  t   
   4   G  t  t   
   5   G  t  t   
   6   G  t  t   
   7   G  t  t   
   8   G  t  t   
   9   G  t  t   
  10   G  t  t   
  11   G  t  t   
  12   G  t  t   
  13   G  t  t   
  14   G  t  t   
  15   G  t  t   
  16   G
  17   G
  18   G
  19   G
  20   G
  21   G
  22   G
  23   G
  24   G
 ---------------------------------------------------
 Electrostatic NN for element: C 
 architecture       36   15   15    1
 ---------------------------------------------------
   1   G  t  t  l
   2   G  t  t   
   3   G  t  t   
   4   G  t  t   
   5   G  t  t   
   6   G  t  t   
   7   G  t  t   
   8   G  t  t   
   9   G  t  t   
  10   G  t  t   
  11   G  t  t   
  12   G  t  t   
  13   G  t  t   
  14   G  t  t   
  15   G  t  t   
  16   G
  17   G
  18   G
  19   G
  20   G
  21   G
  22   G
  23   G
  24   G
  25   G
  26   G
  27   G
  28   G
  29   G
  30   G
  31   G
  32   G
  33   G
  34   G
  35   G
  36   G
 -------------------------------------------------------------
 -------------------------------------------------------------
  short range atomic symmetry functions element H  :
 -------------------------------------------------------------
    1 H   2  H                  0.000000   0.000000   8.000000
    2 H   2  C                  0.000000   0.000000   8.000000
    3 H   2  H                  0.006000   0.000000   8.000000
    4 H   2  H                  0.011000   0.000000   8.000000
    5 H   2  C                  0.013000   0.000000   8.000000
    6 H   2  H                  0.018000   0.000000   8.000000
    7 H   2  H                  0.026000   0.000000   8.000000
    8 H   2  C                  0.029000   0.000000   8.000000
    9 H   2  H                  0.035000   0.000000   8.000000
   10 H   2  C                  0.054000   0.000000   8.000000
   11 H   2  C                  0.093000   0.000000   8.000000
   12 H   2  C                  0.161000   0.000000   8.000000
   13 H   3  H  C    0.000000  -1.000000   1.000000   8.000000
   14 H   3  C  C    0.000000  -1.000000   1.000000   8.000000
   15 H   3  H  C    0.000000   1.000000   1.000000   8.000000
   16 H   3  C  C    0.000000   1.000000   1.000000   8.000000
   17 H   3  H  C    0.000000  -1.000000   2.000000   8.000000
   18 H   3  C  C    0.000000  -1.000000   2.000000   8.000000
   19 H   3  H  C    0.000000   1.000000   2.000000   8.000000
   20 H   3  C  C    0.000000   1.000000   2.000000   8.000000
   21 H   3  H  C    0.000000   1.000000   4.000000   8.000000
   22 H   3  C  C    0.000000   1.000000   4.000000   8.000000
   23 H   3  H  C    0.000000   1.000000   8.000000   8.000000
   24 H   3  C  C    0.000000   1.000000   8.000000   8.000000
 -------------------------------------------------------------
  short range atomic symmetry functions element C  :
 -------------------------------------------------------------
    1 C   2  H                  0.000000   0.000000   8.000000
    2 C   2  C                  0.000000   0.000000   8.000000
    3 C   2  C                  0.010000   0.000000   8.000000
    4 C   2  H                  0.013000   0.000000   8.000000
    5 C   2  C                  0.023000   0.000000   8.000000
    6 C   2  H                  0.029000   0.000000   8.000000
    7 C   2  C                  0.041000   0.000000   8.000000
    8 C   2  H                  0.054000   0.000000   8.000000
    9 C   2  C                  0.065000   0.000000   8.000000
   10 C   2  H                  0.093000   0.000000   8.000000
   11 C   2  C                  0.103000   0.000000   8.000000
   12 C   2  H                  0.161000   0.000000   8.000000
   13 C   3  H  H    0.000000  -1.000000   1.000000   8.000000
   14 C   3  H  C    0.000000  -1.000000   1.000000   8.000000
   15 C   3  C  C    0.000000  -1.000000   1.000000   8.000000
   16 C   3  H  H    0.000000   1.000000   1.000000   8.000000
   17 C   3  H  C    0.000000   1.000000   1.000000   8.000000
   18 C   3  C  C    0.000000   1.000000   1.000000   8.000000
   19 C   3  H  H    0.000000  -1.000000   2.000000   8.000000
   20 C   3  H  C    0.000000  -1.000000   2.000000   8.000000
   21 C   3  C  C    0.000000  -1.000000   2.000000   8.000000
   22 C   3  H  H    0.000000   1.000000   2.000000   8.000000
   23 C   3  H  C    0.000000   1.000000   2.000000   8.000000
   24 C   3  C  C    0.000000   1.000000   2.000000   8.000000
   25 C   3  H  H    0.000000  -1.000000   4.000000   8.000000
   26 C   3  H  C    0.000000  -1.000000   4.000000   8.000000
   27 C   3  C  C    0.000000  -1.000000   4.000000   8.000000
   28 C   3  H  H    0.000000   1.000000   4.000000   8.000000
   29 C   3  H  C    0.000000   1.000000   4.000000   8.000000
   30 C   3  C  C    0.000000   1.000000   4.000000   8.000000
   31 C   3  H  H    0.000000  -1.000000   8.000000   8.000000
   32 C   3  H  C    0.000000  -1.000000   8.000000   8.000000
   33 C   3  C  C    0.000000  -1.000000   8.000000   8.000000
   34 C   3  H  H    0.000000   1.000000   8.000000   8.000000
   35 C   3  H  C    0.000000   1.000000   8.000000   8.000000
   36 C   3  C  C    0.000000   1.000000   8.000000   8.000000
 -------------------------------------------------------------
 =============================================================
 Short range symmetry function values for element H 
 Training set:  min           max       average         range        stddev      range/stddev
   1     0.00000000    0.16847433    0.05143660    0.16847433    0.06334690    2.65955116
   2     0.30405858    0.41710478    0.34591559    0.11304620    0.01643125    6.87995294
   3     0.00000000    0.15946523    0.04769326    0.15946523    0.05879566    2.71219396
   4     0.00000000    0.15232677    0.04478431    0.15232677    0.05525866    2.75661361
   5     0.27036724    0.37728930    0.31224289    0.10692206    0.01454235    7.35246217
   6     0.00000000    0.14286655    0.04100958    0.14286655    0.05066825    2.81964613
   7     0.00000000    0.13277213    0.03708637    0.13277213    0.04589591    2.89289678
   8     0.23624129    0.33657312    0.27800313    0.10033183    0.01280424    7.83582605
   9     0.00000000    0.12226589    0.03312272    0.12226589    0.04107209    2.97686051
  10     0.19404378    0.28607654    0.23585771    0.09203276    0.01098072    8.38130586
  11     0.14641396    0.23097961    0.18800041    0.08456565    0.00947137    8.92855709
  12     0.09432878    0.16744424    0.13330093    0.07311545    0.00849210    8.60982137
  13     0.00000000    0.00394060    0.00076628    0.00394060    0.00097043    4.06066544
  14     0.00000000    0.00217985    0.00039524    0.00217985    0.00044880    4.85706706
  15     0.00000000    0.02261515    0.00668807    0.02261515    0.00829353    2.72684372
  16     0.00679906    0.01478606    0.00985450    0.00798700    0.00104202    7.66495462
  17     0.00000000    0.00075421    0.00011847    0.00075421    0.00015279    4.93621268
  18     0.00000000    0.00030711    0.00003118    0.00030711    0.00004399    6.98180528
  19     0.00000000    0.02017145    0.00604026    0.02017145    0.00748597    2.69456646
  20     0.00644757    0.01299704    0.00949044    0.00654947    0.00076036    8.61360043
  21     0.00000000    0.01674922    0.00500244    0.01674922    0.00619885    2.70198978
  22     0.00579909    0.01111309    0.00884495    0.00531400    0.00069550    7.64050111
  23     0.00000000    0.01266231    0.00356524    0.01266231    0.00443353    2.85603753
  24     0.00441798    0.01101955    0.00782196    0.00660157    0.00148076    4.45824119
 =============================================================
 Short range symmetry function values for element C 
 Training set:  min           max       average         range        stddev      range/stddev
   1     0.00000000    0.52808592    0.08649996    0.52808592    0.13251672    3.98505129
   2     0.24355100    0.57639765    0.47819195    0.33284665    0.10298774    3.23190566
   3     0.22048695    0.52746937    0.43763560    0.30698242    0.09380913    3.27241495
   4     0.00000000    0.50353455    0.07807973    0.50353455    0.12481494    4.03424909
   5     0.19509715    0.47330565    0.39251070    0.27820850    0.08369382    3.32412228
   6     0.00000000    0.47487956    0.06951771    0.47487956    0.11632495    4.08235342
   7     0.16625451    0.41178373    0.34093485    0.24552922    0.07224420    3.39860113
   8     0.00000000    0.43333727    0.05897879    0.43333727    0.10464227    4.14113043
   9     0.13622094    0.34796466    0.28640072    0.21174372    0.06024683    3.51460357
  10     0.00000000    0.37567165    0.04701155    0.37567165    0.08908489    4.21700771
  11     0.10158128    0.27309231    0.22197774    0.17151103    0.04619920    3.71242414
  12     0.00000000    0.29287798    0.03333335    0.29287798    0.06733629    4.34948230
  13     0.00000000    0.01526781    0.00060792    0.01526781    0.00254860    5.99067399
  14     0.00000000    0.02036733    0.00232310    0.02036733    0.00460202    4.42573931
  15     0.00000000    0.00665803    0.00399047    0.00665803    0.00202107    3.29431344
  16     0.00000000    0.00928448    0.00032410    0.00928448    0.00105917    8.76579919
  17     0.00000000    0.02498584    0.00280302    0.02498584    0.00524908    4.76004503
  18     0.00352236    0.01252347    0.00802997    0.00900111    0.00254598    3.53542642
  19     0.00000000    0.01192185    0.00042997    0.01192185    0.00186759    6.38354077
  20     0.00000000    0.01555842    0.00202393    0.01555842    0.00397396    3.91509773
  21     0.00000000    0.00651859    0.00394164    0.00651859    0.00200157    3.25673650
  22     0.00000000    0.00382338    0.00014615    0.00382338    0.00045756    8.35597453
  23     0.00000000    0.02344629    0.00250385    0.02344629    0.00502098    4.66966850
  24     0.00351299    0.01243332    0.00798114    0.00892033    0.00252989    3.52597994
  25     0.00000000    0.00867873    0.00022911    0.00867873    0.00102247    8.48799514
  26     0.00000000    0.01071420    0.00169575    0.01071420    0.00328936    3.25722829
  27     0.00000000    0.00638443    0.00387839    0.00638443    0.00197112    3.23897845
  28     0.00000000    0.00175837    0.00006004    0.00175837    0.00024004    7.32536585
  29     0.00000000    0.02109576    0.00231908    0.02109576    0.00468952    4.49849254
  30     0.00350470    0.01233977    0.00794786    0.00883507    0.00251890    3.50751841
  31     0.00000000    0.00469998    0.00006919    0.00469998    0.00032987   14.24778901
  32     0.00000000    0.01070123    0.00135740    0.01070123    0.00284851    3.75678345
  33     0.00000000    0.00633887    0.00375782    0.00633887    0.00191928    3.30272623
  34     0.00000000    0.00094265    0.00002301    0.00094265    0.00010039    9.39000803
  35     0.00000000    0.01713776    0.00211168    0.01713776    0.00413377    4.14579672
  36     0.00347687    0.01215547    0.00788315    0.00867860    0.00249830    3.47379949
 -------------------------------------------------------------
 Energies in training set (Ha/atom):
                   Emin          Emax          average        stddev          range
 Eshort         -0.269240      -0.236219      -0.253331       0.014428       0.033021
 Eelec          -0.000125       0.001871       0.000595       0.000520       0.001995
 Etot           -0.269130      -0.236279      -0.252736       0.014911       0.032851
 -------------------------------------------------------------
 Energies in training set (eV/atom):
                   Emin          Emax          average        stddev          range
 Eshort         -7.326299      -6.427767      -6.893387       0.392595       0.898532
 Eelec          -0.003389       0.050906       0.016199       0.014159       0.054294
 Etot           -7.323284      -6.429377      -6.877187       0.405743       0.893906
 -------------------------------------------------------------
 Force vectors in training set (Ha/Bohr):
               Fmin          Fmax          average        stddev          range
 H          0.000000       0.214894       0.024801       0.022295       0.214894
 C          0.000000       0.289672       0.043770       0.030282       0.289672
 -------------------------------------------------------------
 Force vectors in training set (eV/Bohr):
               Fmin          Fmax          average        stddev          range
 H          0.000000       5.847473       0.674871       0.606678       5.847473
 C          0.000000       7.882272       1.191032       0.823996       7.882272
 -------------------------------------------------------------
 Force components in training set (Ha/Bohr):
               Fmin          Fmax          range
 H          0.000000       0.214894       0.214894
 C          0.000000       0.289672       0.289672
 -------------------------------------------------------------
 Force components in training set (eV/Bohr):
               Fmin          Fmax          range
 H          0.000000       5.847473       5.847473
 C          0.000000       7.882272       7.882272
 -------------------------------------------------------------
 number of training points         9035
 number of training atoms        112943
 number of training forces       338829
 number of testing points           984
 number of testing atoms          12296
 number of testing forces         36888
 -------------------------------------------------------------
 Number of atoms for each element:   
            training:    testing:   
  1  H         22593        2456
  2  C         90350        9840
 =============================================================
 Weight Preconditioner:
 Warning: Forces are not used for preconditioning
 ----------------------
 -------------------------------------------------------------
 Final preconditioning of the output values:
 --------------------------------------------
 Minimum NN Eshort         -0.041654 Ha/atom
 Minimum Ref Eshort        -0.269240 Ha/atom
 Maximum NN Eshort          0.057473 Ha/atom
 Maximum Ref Eshort        -0.236219 Ha/atom
 Average NN Eshort          0.004303 Ha/atom
 Average Ref Eshort        -0.253331 Ha/atom
 Stddev NN Eshort           0.024166 Ha/atom
 Stddev Ref Eshort          0.014428 Ha/atom
 Factor for connecting short range weights:       0.597034
 =============================================================
 -------------------------------------------------------------
 initialization time (min):    0.08
 -------------------------------------------------------------
 Did you check your output file for warnings? ;-)             
 -------------------------------------------------------------
 Short range energies below      10000.000 Ha/atom are used for fitting and Eshort RMSE!
 => Fitted energy range has width of          0.033 Ha/atom =          0.899 eV/atom
 => Number of short range training energies below max_energy:       9035
 Force components below      10000.000 Ha/Bohr are used for fitting and Fshort RMSE!
 H   => Fitted force range has width of          0.215 Ha/Bohr =          5.847 eV/Bohr
 H   => Number of short range training force components below max_force:      67779
 C   => Fitted force range has width of          0.290 Ha/Bohr =          7.882 eV/Bohr
 C   => Number of short range training force components below max_force:     271050
 -------------------------------------------------------------------------------
 RMSEs (energies: eV/atom, forces: eV/Bohr):
                      --- E_short: ---    - time -
                          /atom              min
        epoch         train         test
 ENERGY     0      0.187104     0.180505    0.23
 FORCES     0      6.410903     6.353220
 -------------------------------------------------------------------------------
 ENERGY     1      0.036364     0.034709    0.76
 FORCES     1      1.338439     1.321620
 INFORMATION USED FOR UPDATE (E,F)     1       235       120
 -------------------------------------------------------------------------------
 ENERGY     2      0.013572     0.013725    0.81
 FORCES     2      0.615405     0.611736
 INFORMATION USED FOR UPDATE (E,F)     2       935       709
 -------------------------------------------------------------------------------
 ENERGY     3      0.004947     0.005004    0.89
 FORCES     3      0.431621     0.431103
 INFORMATION USED FOR UPDATE (E,F)     3      2237      1560
 -------------------------------------------------------------------------------
 ENERGY     4      0.004138     0.004198    0.95
 FORCES     4      0.412449     0.414792
 INFORMATION USED FOR UPDATE (E,F)     4      4909      1987
 -------------------------------------------------------------------------------
 ENERGY     5      0.003891     0.003899    0.99
 FORCES     5      0.401562     0.404823
 INFORMATION USED FOR UPDATE (E,F)     5      5314      2047
 -------------------------------------------------------------------------------
 ENERGY     6      0.003790     0.003861    0.98
 FORCES     6      0.394816     0.398622
 INFORMATION USED FOR UPDATE (E,F)     6      5340      2000
 -------------------------------------------------------------------------------
 ENERGY     7      0.003672     0.003694    0.95
 FORCES     7      0.389013     0.393132
 INFORMATION USED FOR UPDATE (E,F)     7      5485      2003
 -------------------------------------------------------------------------------
 ENERGY     8      0.003635     0.003681    0.97
 FORCES     8      0.386418     0.391001
 INFORMATION USED FOR UPDATE (E,F)     8      5555      2010
 -------------------------------------------------------------------------------
 ENERGY     9      0.003638     0.003673    0.94
 FORCES     9      0.383581     0.388634
 INFORMATION USED FOR UPDATE (E,F)     9      5494      1945
 -------------------------------------------------------------------------------
 ENERGY    10      0.003621     0.003645    0.96
 FORCES    10      0.381233     0.386734
 INFORMATION USED FOR UPDATE (E,F)    10      5564      2035
 =============================================================
 Best short range fit has been obtained in epoch    10
                    --- E_short: ---          --- F_short: ---
                   train         test        train         test
 OPTSHORT        0.003621     0.003645     0.381233     0.386734
 -------------------------------------------------------------
 max Eshort error in last epoch (train set):       0.014119  eV/atom (structure     9007 )
 max Eshort error in last epoch (test set) :       0.013245  eV/atom (structure      105 )
 -------------------------------------------------------------
 Total runtime (s)  :        571.149
 Total runtime (min):          9.519
 Total runtime (h)  :          0.159
 Normal termination of RuNNer
 -------------------------------------------------------------
