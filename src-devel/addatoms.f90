!######################################################################
! This routine is part of
! RuNNer - RuNNer Neural Network Energy Representation
! (c) 2008-2024 Prof. Dr. Joerg Behler 
! Ruhr-Universitaet Bochum, Germany
!
! This program is free software: you can redistribute it and/or modify it 
! under the terms of the GNU General Public License as published by the 
! Free Software Foundation, either version 3 of the License, or 
! (at your option) any later version.
!
! This program is distributed in the hope that it will be useful, but 
! WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
! or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
! for more details.
!
! You should have received a copy of the GNU General Public License along 
! with this program. If not, see http://www.gnu.org/licenses.
! WARNING: THIS FILE IS PART OF THE UNMAINTAINED CONDA PACKAGE OF RUNNER 1.3. 
!######################################################################
!!
!! called by:
!! - prediction.f90
!! - predictionpair.f90
!!
      subroutine addatoms(num_atoms,&
             zelem,num_atoms_element,&
             totalenergy,atomenergy)
!!
      use globaloptions
!!
      implicit none
!!
      integer, intent(in) :: num_atoms
      integer, intent(in) :: num_atoms_element(nelem)
      integer, intent(in) :: zelem(max_num_atoms)
      integer i1                                   ! internal
!!
      real*8, intent(out) :: totalenergy
      real*8, intent(inout) :: atomenergy(max_num_atoms)
!!
!!
!! add atomic energies to total energy
      totalenergy = 0.d0
      do i1=1,nelem
        totalenergy=totalenergy&
         +dble(num_atoms_element(i1))*atomrefenergies(i1)
      enddo
!!
!!
!! add atomic energies to atomic energy contributions
      do i1=1,num_atoms ! loop over all atoms
        atomenergy(i1)=atomenergy(i1)&
          +atomrefenergies(elementindex(zelem(i1)))
      enddo
!!
      return
      end
