

subroutine optimizeEem(countepoch_local,npoints, pointe, idx, &
    numWeightsEwaldFree, rmse, &
    wconstraintidxe, lam, fullKalDim, kalP, seed,&
    nCharges)

    use globaloptions
    use structures
    use nnewald
    use fileunits
    use fittingoptions
    use nnflags
    use kalman_eem
    use eem
    use ewaldSummation

    implicit none
    ! in / out
    integer, intent(in) :: npoints ! number of systems
    integer, intent(inout) :: pointe  ! todo: identify purpose
    integer, intent(in) :: idx(npoints)     ! todo: identify purpose
    integer, intent(in) :: numWeightsEwaldFree(nelem)
    integer, intent(in) :: wconstraintidxe(maxnum_weights_elec,nelem)
    integer, intent(in) :: countepoch_local
    real*8, intent(out) :: rmse
    integer*8, intent(inout) :: seed
    
    
    ! internal
    integer :: i1, i2, i3, i4
    integer :: num_atoms
    integer :: ielement
    real*8 :: hardness(max_num_atoms)
    real*8 :: gausswidth(max_num_atoms)
    real*8 :: nnatomchi(max_num_atoms)
    real*8 :: totalcharge
    real*8 :: AMatrix(max_num_atoms, max_num_atoms)
    real*8 :: nnatomcharge(max_num_atoms)
    real*8 :: nodes_values(maxnum_layers_elec, maxnodes_elec) ! apparently just dummy 
    real*8 :: nodes_sum(maxnum_layers_elec, maxnodes_elec) ! just dummy too
    real*8 :: xyz(3, max_num_atoms)
    real*8, allocatable :: dQdChi(:,:)
    real*8 :: dChidwT(maxnum_weights_elec, max_num_atoms) ! transposed
    integer :: nElemTmp ! number of elements in current point
    integer :: atomsPerElement(nElem) ! number of atoms of each element in this point
    integer :: elementIndices(nElem, max_num_atoms) ! contains the atom indices for each element 
    integer :: atIndex
    real*8 :: dQdw(maxnum_weights_elec)
    real*8, allocatable :: dQdJ(:,:)
    real*8 :: weights(maxnum_weights_elec)
    real*8 :: error
    integer :: nCharges ! to count the number of charges

    ! writes the reference and nn charges to a file called charges.txt
    logical, parameter :: debugOutput = .false.

    integer, intent(in) :: fullKalDim
    real*8, intent(inout) :: kalP(fullKalDim, fullKalDim)
    real*8 :: lam
    real*8, allocatable :: pMatrix(:,:)
    real*8 :: dErrdParas(maxnum_weights_elec*nElem + max_num_atoms)
    real*8 :: paras(maxnum_weights_elec*nElem + max_num_atoms)
    integer :: iPara, nParas
    real*8 :: t0, t1
    real*8 :: err0
    integer :: itmp

    integer :: mixlist(npoints), i0

    ! count number of parameters
    iPara = 0
    do i1=1,nElem
      iPara = iPara + numweightsewaldfree(i1) + 1
    enddo
    ! making this matrix with the perfect size will prevent a copy when passing it to the Kalman filter
    allocate(pMatrix(iPara, iPara))
    pMatrix(:,:) = kalP(:iPara, :iPara)


    rmse = 0.d0
    nCharges = 0
    
    if (debugOutput) open(137, file='charges.txt', status='replace') !! KK: for debug 

    do i1=1,npoints
        mixlist(i1) = i1
    enddo
    call shuffle(npoints, mixlist, seed)

    ! loop over all points
    do i0=1,npoints
        i1 = mixlist(i0)
        pointe = pointe + 1
        num_atoms = num_atoms_list(idx(i1))

        if (allocated(dQdChi)) deallocate(dQdChi)
        allocate(dQdChi(num_atoms,num_atoms))
        if (allocated(dQdJ)) deallocate(dQdJ)
        allocate(dQdJ(num_atoms,num_atoms))

        ! count all elements and make lists of atoms for each element
        atomsPerElement(:) = 0
        elementIndices(:,:) = 0
        do i2=1,num_atoms
            ielement = elementindex(zelem_list(idx(i1), i2))
            atomsPerElement(ielement) = atomsPerElement(ielement) + 1
            elementIndices(ielement, atomsPerElement(ielement)) = i2
        enddo

        
        ! important: this dChidw has to be zeroed!
        dChidwT = 0.d0
        hardness(:)=0.0d0
        gausswidth(:)=0.0d0
        do i2=1,num_atoms
            ! reset 
            ielement = elementindex(zelem_list(idx(i1), i2))
            hardness(i2) = fixedhardness(ielement) 
            gausswidth(i2)= fixedgausswidth(ielement)
            nodes_values(:,:) = 0.d0
            nodes_sum(:,:) = 0.d0
            ! NN forward pass
            call calconenn(1,maxnum_funcvalues_elec,maxnodes_elec,&
              maxnum_layers_elec,num_layers_elec(ielement),&
              maxnum_weights_elec,nodes_elec(0,ielement),&
              symfunction_elec_list(1,i2,idx(i1)),&
              weights_elec(1,ielement),&
              nodes_values,nodes_sum,&
              nnatomchi(i2),actfunc_elec(1,1,ielement))
            ! NN backward pass
            call getonededw(1,&
                maxnum_funcvalues_elec,maxnum_weights_elec,&
                maxnodes_elec,maxnum_layers_elec,&
                num_layers_elec(ielement),&
                windex_elec(1,ielement),&
                nodes_elec(0,ielement),&
                symfunction_elec_list(1,i2,idx(i1)),&
                weights_elec(1,ielement),&
                dChidwT(:,i2),actfunc_elec(1,1,ielement))
        enddo
        totalcharge = totalcharge_list(idx(i1))
        !print*, 'Q', totalcharge, sum(atomcharge_list(idx(i1), :num_atoms))
        !totalcharge = sum(atomcharge_list(idx(i1),:num_atoms))
        xyz(:,:) = xyzstruct_list(:,:,idx(i1))
        if (lperiodic_list(idx(i1))) then
            call eemMatrixEwald(num_atoms, xyz, lattice_list(:,:,idx(i1)), gausswidth, hardness, Amatrix)
        else
            call eemMatrix(num_atoms,xyz,gausswidth,hardness,Amatrix)
        endif
        call eemCharges(num_atoms, AMatrix, nnatomchi, totalcharge, nnatomcharge)
        call eemdQdChi(num_atoms, AMatrix, dQdChi(:num_atoms, :num_atoms))
        call eemdQdHardness(num_atoms, AMatrix, nnatomcharge, dQdJ(:num_atoms, :num_atoms))

        ! fitting ...
        error = sum((atomcharge_list(idx(i1),:num_atoms) - nnatomcharge(:num_atoms))**2) / num_atoms 
        if (debugOutput) then
          do i2=1,num_atoms
            write(137, *) &
              elementindex(zelem_list(idx(i1), i2)),&
              atomcharge_list(idx(i1),i2),&
              nnatomcharge(i2)
          enddo
        endif 

        rmse = rmse + error * num_atoms
        nCharges = nCharges + num_atoms

        iPara = 0 
        dErrdParas(:) = 0.d0

        do iElement=1,nElem ! loop over all elements to calculate the dEdw for the corresponding NN
          nParas = numweightsewaldfree(ielement)

          do i2=1,num_atoms ! over all atoms 
            dQdw(:) = 0.d0
            ! dQdw = sum_i dQ/dX_i dX_i/dw  - dX_i/dw is only nonzero if of the same element
            do i3=1,atomsPerElement(ielement) ! over all atoms of element iElement 
              atIndex = elementIndices(ielement, i3)

              dQdw(:nParas) = dQdw(:nParas) &
                + dQdChi(i2, atIndex) * dChidwT(:nParas,atIndex)
            enddo 
            ! dEdw = sum dEdQ dQdw
            dErrdParas(iPara+1:iPara+nParas) = dErrdParas(iPara+1:iPara+nParas) &
                + 1.d0 / num_atoms * 2.d0 * (nnatomcharge(i2) - atomcharge_list(idx(i1),i2)) & !dEdQ_i
                * dQdw(:nParas) !dQ_i/dw
          enddo

          do i3=1,nParas
             paras(iPara+i3) = weights_elec(wconstraintidxe(i3,ielement),ielement)
          enddo
          iPara = iPara + nParas

          do i2=1,num_atoms
            do i3=1,atomsPerElement(ielement) ! over all atoms of element iElement 
              atIndex = elementIndices(ielement, i3)
              dErrdParas(iPara+1) = dErrdParas(iPara+1) &
                + 1.d0 / num_atoms * 2.d0 * (nnatomcharge(i2) - atomcharge_list(idx(i1),i2)) & !dEdQ_i
                * dQdJ(i2, atIndex) & ! dQ/dJ
                * 2.d0 * sqrt(fixedhardness(ielement)) ! dJ/dh
                ! optimize h -> J = h**2 ... dE/dh = dE/dJ dJ/dh

            enddo
          enddo
          paras(iPara+1) = sqrt(fixedhardness(ielement)) ! h = sqrt(J)
          !paras(iPara+1) = fixedhardness(ielement)
          iPara = iPara + 1

        enddo

    !    write(*,*) iPara
    !    call random_number(err0)
    !    itmp = int(err0 * numweightsewaldfree(1) - 1) + 1
    !    weights_elec(wconstraintidxe(itmp,1),1) = weights_elec(wconstraintidxe(itmp,1),1) + 0.0000001
    !    err0 = error


    !    ! important: this dChidw has to be zeroed!
    !    dChidwT = 0.d0
    !    hardness(:)=0.0d0
    !    gausswidth(:)=0.0d0
    !    do i2=1,num_atoms
    !        ! reset
    !        ielement = elementindex(zelem_list(idx(i1), i2))
    !        hardness(i2) = fixedhardness(ielement)
    !        gausswidth(i2)= fixedgausswidth(ielement)
    !        nodes_values(:,:) = 0.d0
    !        nodes_sum(:,:) = 0.d0
    !        ! NN forward pass
    !        call calconenn(1,maxnum_funcvalues_elec,maxnodes_elec,&
    !                maxnum_layers_elec,num_layers_elec(ielement),&
    !                maxnum_weights_elec,nodes_elec(0,ielement),&
    !                symfunction_elec_list(1,i2,idx(i1)),&
    !                weights_elec(1,ielement),&
    !                nodes_values,nodes_sum,&
    !                nnatomchi(i2),actfunc_elec(1,1,ielement))
    !        ! NN backward pass
    !        call getonededw(1,&
    !                maxnum_funcvalues_elec,maxnum_weights_elec,&
    !                maxnodes_elec,maxnum_layers_elec,&
    !                num_layers_elec(ielement),&
    !                windex_elec(1,ielement),&
    !                nodes_elec(0,ielement),&
    !                symfunction_elec_list(1,i2,idx(i1)),&
    !                weights_elec(1,ielement),&
    !                dChidwT(:,i2),actfunc_elec(1,1,ielement))
    !    enddo
    !    totalcharge = totalcharge_list(idx(i1))
    !    !totalcharge = sum(atomcharge_list(idx(i1),:num_atoms))
    !    xyz(:,:) = xyzstruct_list(:,:,idx(i1))
    !    if (lperiodic_list(idx(i1))) then
    !        call eemMatrixEwald(num_atoms, xyz, lattice_list(:,:,idx(i1)), gausswidth, hardness, Amatrix)
    !    else
    !        call eemMatrix(num_atoms,xyz,gausswidth,hardness,Amatrix)
    !    endif
    !    call eemCharges(num_atoms, AMatrix, nnatomchi, totalcharge, nnatomcharge)
    !    call eemdQdChi(num_atoms, AMatrix, dQdChi(:num_atoms, :num_atoms))
    !    call eemdQdHardness(num_atoms, AMatrix, nnatomcharge, dQdJ(:num_atoms, :num_atoms))

    !    ! fitting ...
    !    error = sum((atomcharge_list(idx(i1),:num_atoms) - nnatomcharge(:num_atoms))**2) / num_atoms

    !    write(*,*) 'nat', num_atoms, atomsPerElement(:)
    !    write(*,*) 'i', itmp
    !    write(*,*) 'err', err0, error
    !    write(*,*) 'J', dErrdParas(itmp), (error - err0) / 0.0000001
    !    write(*,*) 'Chi', nnatomchi(:num_atoms)
    !    if (dErrdParas(itmp) > 1.e-8 .and. abs(dErrdParas(itmp) / (error - err0) * 0.0000001 - 1.d0) > 0.3) stop





        ! call cpu_time(t0)
        ! write(*,*) 'trest', t0-t1
        if (luseregularize) then
            error = error + regularize_fit_param * sum(paras(:iPara)**2) / iPara
            dErrdParas(:iPara) = dErrdParas(:iPara) + regularize_fit_param * 2._dp * paras(:iPara) / iPara
        endif
        call updatekalman_eem(kalmannuee, lam, error, iPara, paras(:iPara), dErrdParas(:iPara), pMatrix)
        !write(*,*) minval(paras(:iPara)), maxval(paras(:iPara))
        ! call cpu_time(t1)
        ! write(*,*) 'tkal', t1-t0

        iPara = 0 
        do iElement=1,nElem ! loop over all elements to calculate the dEdw for the corresponding NN
          nParas = numweightsewaldfree(ielement)
          do i3=1,nParas     
             weights_elec(wconstraintidxe(i3,ielement),ielement) = paras(iPara+i3)
          enddo
          iPara = iPara + nParas
          fixedhardness(ielement) = paras(iPara+1)**2
          iPara = iPara + 1
        enddo

    enddo ! loop over all points


    if (debugOutput) close(137)  !! KK: for debug

    iPara = 0
    do i1=1,nElem
      iPara = iPara + numweightsewaldfree(i1) + 1
    enddo
    kalP(:iPara, :iPara) = pMatrix(:,:)

    rmse = sqrt(rmse / nCharges)
    if (debugOutput) write(*,*) 'RMSE', rmse  !! KK: for debug 
end subroutine
