!######################################################################
! This routine is part of
! RuNNer - RuNNer Neural Network Energy Representation
! (c) 2008-2024 Prof. Dr. Joerg Behler 
! Ruhr-Universitaet Bochum, Germany
!
! This program is free software: you can redistribute it and/or modify it 
! under the terms of the GNU General Public License as published by the 
! Free Software Foundation, either version 3 of the License, or 
! (at your option) any later version.
!
! This program is distributed in the hope that it will be useful, but 
! WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
! or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
! for more details.
!
! You should have received a copy of the GNU General Public License along 
! with this program. If not, see http://www.gnu.org/licenses.
! WARNING: THIS FILE IS PART OF THE UNMAINTAINED CONDA PACKAGE OF RUNNER 1.3. 
!######################################################################
!! called by: - predictionpair.f90
!!            - getallshortforcespair.f90
!!
      subroutine scaledsfuncpair(num_pairs,&
        minvalue_short_pair,maxvalue_short_pair,&
        zelemp,dsfuncdxyz_pair,strs_pair)
!!
      use fileunits
      use globaloptions
      use nnshort_pair
!! 
      implicit none
!!
      integer zelemp(2,max_num_pairs) 
      integer i2,i3                                                  ! internal
      integer num_pairs
!!
      real*8 dsfuncdxyz_pair(maxnum_funcvalues_short_pair,max_num_pairs,max_num_atoms,3)
      real*8 minvalue_short_pair(npairs,maxnum_funcvalues_short_pair)     ! in
      real*8 maxvalue_short_pair(npairs,maxnum_funcvalues_short_pair)     ! in
      real*8 strs_pair(3,3,maxnum_funcvalues_short_pair,max_num_pairs)
!!
!!
      do i2= 1,num_pairs 
        do i3=1,num_funcvalues_short_pair(pairindex(zelemp(1,i2),zelemp(2,i2)))
!! scale each symmetry function derivative value for the respective element
!!           dsfuncdxyz_pair(i3,i2,:,:)=dsfuncdxyz_pair(i3,i2,:,:)/&
!!             (maxvalue_short_pair(pairindex(zelemp(1,i2),zelemp(2,i2)),i3)-minvalue_short_pair(pairindex(zelemp(1,i2),zelemp(2,i2)),i3))
!! new for variable scaling range
          dsfuncdxyz_pair(i3,i2,:,:)=dsfuncdxyz_pair(i3,i2,:,:)/&
            (maxvalue_short_pair(pairindex(zelemp(1,i2),zelemp(2,i2)),i3)&
            -minvalue_short_pair(pairindex(zelemp(1,i2),zelemp(2,i2)),i3))&
            *(scmax_short_pair-scmin_short_pair)
!!
!! scale stress components, CHECK IF THIS IS RIGHT!!!
!!          strs_pair(:,:,i3,i2)=strs_pair(:,:,i3,i2)/&
!!             (maxvalue_short_pair(pairindex(zelemp(1,i2),zelemp(2,i2)),i3)-minvalue_short_pair(pairindex(zelemp(1,i2),zelemp(2,i2)),i3))
!! new for variable scaling range
          strs_pair(:,:,i3,i2)=strs_pair(:,:,i3,i2)/&
            (maxvalue_short_pair(pairindex(zelemp(1,i2),zelemp(2,i2)),i3)&
            -minvalue_short_pair(pairindex(zelemp(1,i2),zelemp(2,i2)),i3))&
            *(scmax_short_pair-scmin_short_pair)
        enddo
      enddo
!!
      return
      end
