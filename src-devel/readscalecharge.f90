!######################################################################
! This routine is part of
! RuNNer - RuNNer Neural Network Energy Representation
! (c) 2008-2024 Prof. Dr. Joerg Behler 
! Ruhr-Universitaet Bochum, Germany
!
! This program is free software: you can redistribute it and/or modify it 
! under the terms of the GNU General Public License as published by the 
! Free Software Foundation, either version 3 of the License, or 
! (at your option) any later version.
!
! This program is distributed in the hope that it will be useful, but 
! WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
! or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
! for more details.
!
! You should have received a copy of the GNU General Public License along 
! with this program. If not, see http://www.gnu.org/licenses.
! WARNING: THIS FILE IS PART OF THE UNMAINTAINED CONDA PACKAGE OF RUNNER 1.3. 
!######################################################################
!! Multipurpose subroutine

!! called by:
!! - predict.f90
!! - fitting.f90
!! - fittingpair.f90
!! - getpairsymfunctions.f90
!! - getsymmetryfunctions.f90
!!
      subroutine readscalecharge(ndim,&
           minvalue_local,maxvalue_local,avvalue_local)
!!
      use fileunits
      use globaloptions
!!
      implicit none
!!
      integer ndim                               ! in
      integer i1,i2,dummy                           ! internal
!!
      real*8 avvalue_local(ndim)     ! out 
      real*8 maxvalue_local(ndim)    ! out 
      real*8 minvalue_local(ndim)    ! out 
      real*8 thres                               ! internal
!!
      character*19 filename                      ! internal
!!
      logical lexist                             ! internal
!!
      thres=0.00001d0
!!
      filename='scaling_charge.data'
!!
      inquire(file=filename,exist=lexist)
      if(.not.lexist) then
        write(ounit,*)'Error: could not find ',filename
        stop
      endif
!!
      open(scalechargeunit,file=filename,form='formatted',status='old')
      rewind(scalechargeunit)
      do i1=1,ndim
          read(scalechargeunit,*)dummy,minvalue_local(i1),&
            maxvalue_local(i1),avvalue_local(i1)
      enddo ! i1
      close(scalechargeunit)
!!
      do i1=1,ndim
        write(ounit,*)'============================================================='
        write(ounit,*)'Partial charges values for element ',element(i1)
        write(ounit,*)'Training set:  min           max       average         range '
!!        write(ounit,*)'-------------------------------------------------------------'
!! check if default min and max values are still there => element or pair is not present, then don't write it
        if(minvalue_local(i1).gt.maxvalue_local(i1))then
          write(ounit,*)'No range of partial charge for this type have been present in training set'
        else
          write(ounit,'(i4,x,4f14.8)')i1,minvalue_local(i1),maxvalue_local(i1),&
            avvalue_local(i1),abs(maxvalue_local(i1)-minvalue_local(i1))
          if(abs(minvalue_local(i1)-maxvalue_local(i1)).lt.thres)then
            write(ounit,*)'### WARNING ###: minvalue=maxvalue ',i1,nucelem(i1)
            if(lscalesym)then
              write(ounit,*)'scaling partial charges cannot be used with minvalue=maxvalue'
              stop
            endif
          endif
        endif
      enddo ! i1
!!
      return
      end
