!###############################################################################
! This routine is part of
! RuNNer - RuNNer Neural Network Energy Representation
! (c) 2008-2024 Prof. Dr. Joerg Behler
! Ruhr-Universitaet Bochum, Germany
!
! This program is free software: you can redistribute it and/or modify it
! under the terms of the GNU General Public License as published by the
! Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! This program is distributed in the hope that it will be useful, but
! WITHout ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
! or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
! for more details.
!
! You should have received a copy of the GNU General Public License along
! with this program. If not, see http://www.gnu.org/licenses.
! WARNING: THIS FILE IS PART OF THE UNMAINTAINED CONDA PACKAGE OF RUNNER 1.3.
!###############################################################################

subroutine calc_grimme_d2(                                                     &
  n_start, n_end,                                                              &
  lsta, lstc, lste, lstb,                                                      &
  lperiodic, xyzstruct, zelem,                                                 &
  atomvdw, vdwenergy_block, vdwforces, vdwstress                               &
)

!----------Module imports-------------------------------------------------------
  use mpi_mod
  use fileunits
  use nnflags
  use globaloptions
  use predictionoptions
  
  implicit none
!-------------------------------------------------------------------------------
! Description:
!   This routine calculates the Grimme D2 dispersion energy, the
!   force components arising from this potential and the contribution to the 
!   stress vector for a chunk/block of atoms in the structure.
!
! Method:
!   - A detailed explanation of the implementation of the aforementioned 
!     equations in this routine is provived in the RuNNer manual.
!   - A derivation of the mathematical expressions used in this module 
!     by T. Buĉko, et al. can be found in their paper
!     DOI: 10.1103/PhysRevB.87.064110
!   - The original paper by Grimme:
!     DOI: 10.1002/jcc.20495
!
! Current Code Maintainer: Alexander Knoll
!
! Last revision: 22.09.2020
!
! Code placement in the RuNNer suite:
!   - Called by: distribute_vdw_calculation.f90
!-------------------------------------------------------------------------------

! Subroutine arguments.
  ! IDs of first and last atom in the block of atoms to be processed.
  integer, intent(in   )                                 :: n_start, n_end
  ! First and last index of each atom's neighbor. Map to lstc, lste and lstb. 
  integer, intent(in   ), dimension(2, max_num_atoms)    :: lsta
  ! Atom ID (lstc) and nucleus charge (lste) of neighbors.
  integer, intent(in   ), dimension(listdim)             :: lstc, lste
  ! Neighbor xyz and distance.
  real*8,  intent(in   ), dimension(listdim, 4)          :: lstb
  ! Whether the structure is periodic or not.
  logical, intent(in   )                                 :: lperiodic
  ! Cartesian coordinates of all atoms in the structure.
  real*8,  intent(in   ), dimension(3, max_num_atoms)    :: xyzstruct
  ! Nucleus charge of all atoms in the structure.
  integer, intent(in   ), dimension(max_num_atoms)       :: zelem

  ! Total dispersion energy of the structure.
  real*8,  intent(inout)                                 :: vdwenergy_block
  ! Total vdw energy of each atom in the structure.
  real*8,  intent(inout), dimension(max_num_atoms)       :: atomvdw
  ! Total vdw force components acting on each atom in the structure.
  real*8,  intent(inout), dimension(3, max_num_atoms)    :: vdwforces  
  ! Total vdw stress components.
  real*8,  intent(inout), dimension(3, 3)                :: vdwstress
                
! Internal variables.
  ! Loop running integers.
  integer  :: i1, i2, i3
  ! Atom id of the two elements in a pair.
  integer  :: idelem1, idelem2
  ! Value of the screening function for the current pair of atoms.
  real*8   :: screening_function
  ! Value of the screening function derivative for the current pair of atoms.
  real*8   :: grad_screening_function
  ! Total vdw radius of a pair of atoms is the sum of their individual radii.
  real*8   :: vdw_radius_combined
  ! The combined C6 coefficient for unequal elements i and j.
  real*8   :: vdw_coefficient_combined
  ! Derivative of the pairwise distance with respect to the cartesian axes.
  real*8, dimension(3)    :: grad_r_wrt_cart
  ! Derivative of the pairwise distance with respect to the cell vectors.
  real*8, dimension(3, 3) :: grad_r_wrt_cell
  
  ! Common variables in the Grimme D2 equations.
  real*8 :: d, sr, s6
  ! Common expressions in the Grimme D2 equations.
  real*8 :: c6_fraction, exp_denominator, exp_factor, grad_energy
  real*8, dimension(3) :: distance_components
  
!----------Initializations------------------------------------------------------

  ! Reset all counters.
  i1      = 0
  i2      = 0
  i3      = 0
  idelem1 = 0
  idelem2 = 0

  ! Set all internal variables to 0.
  screening_function       = 0.0d0
  grad_screening_function  = 0.0d0
  vdw_radius_combined      = 0.0d0
  vdw_coefficient_combined = 0.0d0
  c6_fraction              = 0.0d0
  exp_denominator          = 0.0d0
  exp_factor               = 0.0d0
  distance_components(:)   = 0.0d0
  grad_r_wrt_cart(:)       = 0.0d0
  
  grad_energy              = 0.0d0

  ! Assign RuNNer keywords to their common notation in the equations.
  s6 = vdw_screening(1)
  d  = vdw_screening(2)
  sr = vdw_screening(3)

!----------Main dispersion loop-------------------------------------------------
! Calculate the dispersion interaction between an atom and all its neighbors 
! for each atom in the block.
!-------------------------------------------------------------------------------
  loop_block_of_atoms: do i1 = n_start, n_end

    loop_neighbors: do i2 = lsta(1, i1), lsta(2, i1)

      ! Find the element IDs of the current elements.
      loop_elements: do i3 = 1, nelem
        
        ! For the center atom, the nucleus charge is stored in zelem.
        if (zelem(i1) == nucelem(i3)) then
          idelem1 = i3
        endif
        ! For each neighbor, the nucleus charge is already stored in lste.
        if (lste(i2) == nucelem(i3)) then
          idelem2 = i3
        endif
        
      enddo loop_elements

!----------Common expression calculations---------------------------------------
! Calculate all expression which appear frequently in the following formula
! in order to improve speed and readability.
    
      ! Calculate the combined vdw radius of the two elements.
      vdw_radius_combined = vdw_radius(idelem1) + vdw_radius(idelem2)
      
      ! Calculate the combined C6 coefficient.
      vdw_coefficient_combined                                                 &
        = sqrt(vdw_coefficients(idelem1, idelem1)                              &
               *vdw_coefficients(idelem2, idelem2))
 
      ! Calculate common expressions in the dispersion interaction terms.
      c6_fraction     = vdw_coefficient_combined / ((lstb(i2, 4))**6)
      exp_denominator = sr * vdw_radius_combined
      exp_factor      = dexp(-d * (lstb(i2, 4) / exp_denominator - 1.0d0))
      
      ! Calculate the value of the screening function.
      screening_function  = s6 / (1.0d0 + exp_factor)
      
      ! Calculate the distance between the pair for all three cartesian axes. 
      ! lstb contains the position of the current neighbor (i2) on the first 
      ! three positions. 
      distance_components(:) = xyzstruct(:, i1) - lstb(i2, :3)

!----------Energy calculation---------------------------------------------------

      if (lstc(i2) == i1) then 
        atomvdw(i1) = atomvdw(i1) - 0.5 * (c6_fraction * screening_function)
      else
        atomvdw(i1) = atomvdw(i1) -       (c6_fraction * screening_function)
      endif 

!----------Force component calculation------------------------------------------

      ! Calculate the derivate of the screening function.
      grad_screening_function                                                  &
        = screening_function**2 / s6 * exp_factor * (d / exp_denominator)

      ! Calculate the derivative of the distance vector for the three different
      ! cartesian axes {x, y, z}. 
      grad_r_wrt_cart(:) = distance_components / lstb(i2, 4)

      ! Calculate the full gradient of the energy.
      grad_energy =                                                            &
        + (6 * (c6_fraction / lstb(i2, 4)) * screening_function)               &
        - (c6_fraction * grad_screening_function)

      if (ldoforces) then  
        ! Add pairwise forces to total atomic forces.
        if (lstc(i2) /= i1) then 
          vdwforces(:, i1)                                                     &
            = vdwforces(:, i1)       + grad_energy * grad_r_wrt_cart(:)
          vdwforces(:, lstc(i2))                                               &
            = vdwforces(:, lstc(i2)) - grad_energy * grad_r_wrt_cart(:)
        endif 
      endif !ldoforces

!----------Stress tensor calculation--------------------------------------------
      if (ldostress.and.lperiodic) then

        ! Calculate the derivative of the distance vector wrt the cell vectors,
        ! which essentially comes down to calculating the outer product of the 
        ! distance vector with itself. BLAS DGER does that.
        grad_r_wrt_cell(:, :)   = 0.0d0
        call dger(                                                             &
          ! Dimension of the input vectors and scaling factor.
          3, 3, 1.0d0,                                                         &
          ! The input vectors and their entry increment.
          distance_components, 1,                                              &
          distance_components, 1,                                              &
          ! The target matrix and its first dimension.
          grad_r_wrt_cell(:, :),                                               &
          3                                                                    &
        )

        ! Normalize by the full distance.
        grad_r_wrt_cell(:, :) = grad_r_wrt_cell(:, :) / lstb(i2, 4)
      
        ! Add pairwise stress contribution to total stress.
        if (lstc(i2) == i1) then 
          vdwstress(:, :)                                                      &
            = vdwstress(:, :) - 0.5 * grad_energy * grad_r_wrt_cell(:, :)
        else
          vdwstress(:, :)                                                      &
            = vdwstress(:, :) -       grad_energy * grad_r_wrt_cell(:, :) 
        endif 
        
      endif !ldostress
      
    enddo loop_neighbors

    !! Add atomic vdw energy contribution to total vdw energy of the block.
    vdwenergy_block = vdwenergy_block + atomvdw(i1)

  enddo loop_block_of_atoms

  return
end subroutine calc_grimme_d2
