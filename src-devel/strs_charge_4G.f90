!######################################################################
! This routine is part of
! RuNNer - RuNNer Neural Network Energy Representation
! (c) 2008-2024 Prof. Dr. Joerg Behler 
! Ruhr-Universitaet Bochum, Germany
!
! This program is free software: you can redistribute it and/or modify it 
! under the terms of the GNU General Public License as published by the 
! Free Software Foundation, either version 3 of the License, or 
! (at your option) any later version.
!
! This program is distributed in the hope that it will be useful, but 
! WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
! or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
! for more details.
!
! You should have received a copy of the GNU General Public License along 
! with this program. If not, see http://www.gnu.org/licenses.
! WARNING: THIS FILE IS PART OF THE UNMAINTAINED CONDA PACKAGE OF RUNNER 1.3. 
!######################################################################

!! called by:
!! - calconefunction_para.f90
!!
      subroutine strs_charge_4G(natoms,lsta,lstb,&
        xyzstruct,dchargedxyz,strs_temp)
!!
      use fileunits
      use nnconstants
      use symfunctions
      use globaloptions
!!
      implicit none
!!
      integer i1,i2,i3
      integer natoms
      integer,intent(in):: lsta(2,max_num_atoms)
!!
      real*8,intent(in):: dchargedxyz(natoms,3,natoms)
      real*8,intent(inout):: strs_temp(3,3,natoms)
      real*8 rij
      real*8,intent(in):: xyzstruct(3,natoms)
      real*8 deltaxj,deltayj,deltazj
      real*8 lstb(listdim,4)
!!
!!
      do i2=1,natoms
!!
        do i3=lsta(1,i2),lsta(2,i2) ! loop over neighbors
!!
            rij=lstb(i3,4)
            if(rij.le.maxcutoff_short_atomic)then


              !!
              deltaxj=-1.d0*(xyzstruct(1,i2)-lstb(i3,1))
              deltayj=-1.d0*(xyzstruct(2,i2)-lstb(i3,2))
              deltazj=-1.d0*(xyzstruct(3,i2)-lstb(i3,3))
              !!
!! Calculation of derivatives for stress
!! dcharge/dx
              strs_temp(1,1,:natoms)=strs_temp(1,1,:natoms)&
                +deltaxj*dchargedxyz(i2,1,:)
              strs_temp(2,1,:natoms)=strs_temp(2,1,:natoms)&
                +deltayj*dchargedxyz(i2,1,:)
              strs_temp(3,1,:natoms)=strs_temp(3,1,:natoms)&
                +deltazj*dchargedxyz(i2,1,:)
!! dcharge/dy
              strs_temp(1,2,:natoms)=strs_temp(1,2,:natoms)&
                +deltaxj*dchargedxyz(i2,2,:)
              strs_temp(2,2,:natoms)=strs_temp(2,2,:natoms)&
                +deltayj*dchargedxyz(i2,2,:)
              strs_temp(3,2,:natoms)=strs_temp(3,2,:natoms)&
                +deltazj*dchargedxyz(i2,2,:)
!! dcharge/dz
              strs_temp(1,3,:natoms)=strs_temp(1,3,:natoms)&
                +deltaxj*dchargedxyz(i2,3,:)
              strs_temp(2,3,:natoms)=strs_temp(2,3,:natoms)&
                +deltayj*dchargedxyz(i2,3,:)
              strs_temp(3,3,:natoms)=strs_temp(3,3,:natoms)&
                +deltazj*dchargedxyz(i2,3,:)
!!

            endif
        enddo
      enddo
!!  
      return
      end
