! Created by  on 3/2/20.

subroutine electrostatics_type5(nat,zelem,&
        Amatrix,dAdxyzQ,dAdlatQ,nnatomcharge,xyzstruct,dChidx,dChidlat,&
        nnelecenergy,nnelecforce,lat, lperiodic, dEdlat)

    use fileunits
    use globaloptions
    use nnflags
    use eem
    use ewaldSummation
    use linalg

    implicit none

    integer, intent(in) :: nat
    integer, intent(in) :: zelem(max_num_atoms)
    real*8, intent(in) :: nnatomcharge(nat)
    real*8, intent(in) :: xyzstruct(3,nat)
    real*8, intent(in) :: dChidx(nat,3,nat) ! in general indices are as in the variable name dChidx(i,j,k) = dChi_i / dx_jk (xyz j of atom k)
    real*8, intent(in) :: dChidLat(nat,3,3)
    real*8, intent(in) :: Amatrix(nat,nat)
    real*8, intent(in) :: dAdxyzQ(nat,3,nat)
    real*8, intent(in) :: dAdlatQ(nat,3,3)
    real*8, intent(inout) :: nnelecenergy
    real*8, intent(inout) :: nnelecforce(3,nat)
    real*8, intent(in) :: lat(3,3)
    logical, intent(in) :: lperiodic
    real*8, intent(inout) :: dEdlat(3,3)

    integer :: i1, i2, i3, i
    real*8 :: dEdQ(nat)
    real*8 :: fscreen_list(nat,nat)
    real*8 :: gausswidth(nat)
    real*8 :: hardness(nat)
    real*8 :: ctforce(3,nat)
    real*8 :: zeros(nat), minusones(nat)
    real*8 :: Bmatrix(nat, nat)
    real*8 :: escreen, fscreen(3,nat), ctdEdlat(3,3), screendEdLat(3,3)



    zeros = 0.d0
    minusones = -1.d0
    nnelecenergy = 0.0d0
    nnelecforce(:,:) = 0.0d0
    fscreen_list(:,:) = 0.0d0
    ! standard coulomb force and energy
    do i1=1,nat
       gausswidth(i1) = fixedgausswidth(elementindex(zelem(i1)))
       hardness(i1)   = fixedhardness(elementindex(zelem(i1)))
    end do

    ! To calculate the charges we use en energy expression in the eem which includes hardness and electronegativity (E = 1/2 q^t A q + chi^t q)
    ! In the electrostatics here we do not want these term, we only want the coulomb term
    ! We therefore use a matrix B, that has hardness and electronegativity of zero.

    ! calculate the B Matrix (=AMatrix but no hardness, such that it relates directly to e_elec)
    !if (lperiodic) then
    !    call eemMatrixEwald(nat, xyzstruct, lat, gausswidth, zeros, Bmatrix)
    !else
    !    call eemMatrix(nat, xyzstruct, gausswidth, zeros, Bmatrix)
    !end if
    ! Saving time by just substracting the hardness terms
    Bmatrix = Amatrix
    do i=1,nat
        Bmatrix(i,i) = Bmatrix(i,i) - hardness(i)
    end do

    ! because E = 1/2 q^t A q
    ! we use dE/dx = 1/2 q^t dA/dx q
    ! no need for another ewald summation
    ! call ewaldEnergy(nat, xyzstruct, lat, nnatomcharge, gausswidth, nnelecenergy, nnelecenergy)
    ! calculate elec. energy. Since we have A anyways, we dont need to call ewald summation again.
    nnelecenergy = 0.5d0 * sum(nnatomcharge(:) * matmul(Bmatrix, nnatomcharge(:)))

    ! E = 1/2 q^t A q
    ! therefore f = -dE/dx = -1/2 q^t dA/dx q + ...
    ! this is only the direct electrostatic (Coulomb) force the ... part is the one due to dQ/dx, it is calculated further down
    dEdLat = 0._dp
    do i1=1,nat
        nnelecforce(:,:) = nnelecforce(:,:) - 0.5d0 * dAdxyzQ(i1,:,:) * nnatomcharge(i1)
        ! stress is very similar to the forces in this subroutine
        if (ldostress) then
            dEdLat(:,:) = dEdlat(:,:) + 0.5_dp * dAdlatQ(i1,:,:) * nnatomcharge(i1) ! ewald stress (again no need to call ewald since we have the dAdlatQ)
        end if
    end do

    ! Setting dEdQ to zero here as it is output of screening. Make sure its zero without screening
    dEdQ(:) = 0.d0
    screendEdlat = 0.d0

    if (lscreen) then
!        stop 'no screening with Gaussian charges yet (not tested)' ! JF: I assume it has been tested in the meantime ;)
        ! here we add the energy and forces due to screening. They are short ranged and dont require Ewald treatment
        if (lperiodic) then
            ! the screendEdlat is always calculated. It's very cheap
            call getScreeningPeriodic(nat, lat, xyzstruct, nnatomcharge, gausswidth, escreen, fscreen, dEdQ, screendEdlat)
        else
            call getScreeningFree(nat, xyzstruct, nnatomcharge, gausswidth, escreen, fscreen, dEdQ)
        end if
        nnelecforce(:,:) = nnelecforce(:,:) + fscreen(:,:)
        nnelecenergy = nnelecenergy + escreen
    end if

    ! dE/dQ = dE_{screen}/dQ + B q
    dEdQ(:) = dEdQ(:) + matmul(Bmatrix, nnatomcharge(:))

    call calcPartialForcesEEM(nat, Amatrix, dAdxyzQ, dEdQ, dChidx, nnatomcharge, ctforce) ! using force trick.
    nnelecforce(:,:) = nnelecforce(:,:) + ctforce(:,:)

    ! use force trick for stress, equivalent to the force part above
    if (lperiodic .and. ldostress) then
        call calcPartialLatticeDerivativesEEM(nat, Amatrix, dAdlatQ, dEdQ, dChidLat, nnatomcharge, ctdEdLat)
        dEdlat = dEdlat + ctdEdlat + screendEdLat
    end if

end subroutine electrostatics_type5



