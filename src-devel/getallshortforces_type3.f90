!######################################################################
! This routine is part of
! RuNNer - RuNNer Neural Network Energy Representation
! (c) 2008-2024 Prof. Dr. Joerg Behler 
! Ruhr-Universitaet Bochum, Germany
!
! This program is free software: you can redistribute it and/or modify it 
! under the terms of the GNU General Public License as published by the 
! Free Software Foundation, either version 3 of the License, or 
! (at your option) any later version.
!
! This program is distributed in the hope that it will be useful, but 
! WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
! or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
! for more details.
!
! You should have received a copy of the GNU General Public License along 
! with this program. If not, see http://www.gnu.org/licenses.
! WARNING: THIS FILE IS PART OF THE UNMAINTAINED CONDA PACKAGE OF RUNNER 1.3. 
!######################################################################
!! Purpose: calculate nnshortforce for npoints structures 

!! called by: 
!! - getshortenergies_para.f90
!! - optimize_short_combined.f90 
!! - optimize_atomic.f90   
!!
      subroutine getallshortforces_type3(ndim,npoints,&
       num_atoms_local,zelem_local,&
       symfunction_local,nnshortforce_local,&
       lattice_local,xyzstruct_local,atomspin_local,&
       minvalue_short_atomic,maxvalue_short_atomic,&
       minvalue_q,maxvalue_q,& 
       atomcharge_local,scalecharge_local,lperiodic_local)
!!
      use mpi_mod
      use fileunits
      use fittingoptions
      use globaloptions
      use symfunctions
      use nnshort_atomic
      use nnewald
      use timings
      use eem
      use ewaldSummation

!!
      implicit none
!!
      integer ndim                                                      ! in
      integer npoints                                                   ! in
      integer num_atoms                                                 ! internal
      integer num_atoms_local(ndim)                                     ! in
      integer zelem(max_num_atoms)                                      ! internal
      integer zelem_local(ndim,max_num_atoms)                           ! in
      integer i1,i2,i3,i4                                               ! internal
      integer, allocatable :: lsta(:,:)                                 ! internal, numbers of neighbors
      integer, allocatable :: lstc(:)                                   ! internal, identification of atom
      integer, allocatable :: lste(:)                                   ! internal, nuclear charge of atom
      integer, allocatable :: num_neighbors_short_atomic(:)             ! internal         
      integer max_num_neighbors_short_atomic                            ! internal
      integer, allocatable :: neighboridx_short_atomic(:,:)             ! internal
      integer, allocatable :: invneighboridx_short_atomic(:,:)          ! internal
      integer, allocatable :: atomindex_dummy(:)                        ! internal
      integer maxinputnode                                              ! internal
!!
      real*8, allocatable,dimension(:,:) :: Amatrix
      real*8, allocatable,dimension(:) :: atomcharge                    ! internal
      real*8 atomcharge_local(ndim,max_num_atoms)                       ! internal
      real*8 scalecharge_local(ndim,max_num_atoms)
      real*8, allocatable,dimension(:) :: scalecharge
      real*8 nnshortforce(3,max_num_atoms)                              ! internal 
      real*8 nnshortforce_local(3,max_num_atoms,ndim)                   ! out
      real*8 deshortdcharge(max_num_atoms)                              ! internal
      real*8, allocatable,dimension(:,:,:) :: dchargedxyz               ! internal
      real*8  dChidsfunc(max_num_atoms,maxnum_funcvalues_short_atomic)  ! internal
      real*8, allocatable,dimension(:,:,:) :: dChidxyz                  ! internal
      real*8, allocatable,dimension(:,:) ::  deshortdsfunc              ! dummy here
      real*8 symfunctiondummy(maxnum_funcvalues_short_atomic,max_num_atoms)       ! internal
      real*8 symfunction_local(maxnum_funcvalues_short_atomic,max_num_atoms,ndim) ! in
      real*8 symfunction(maxnum_funcvalues_short_atomic,max_num_atoms)
      real*8, allocatable,dimension(:) :: atomhardness                  ! internal
      real*8, allocatable,dimension(:) :: gausswidth                    ! internal
      real*8 lattice_local(3,3,ndim)                                    ! in
      real*8 xyzstruct_local(3,max_num_atoms,ndim)                      ! in
      real*8 xyzstruct(3,max_num_atoms)
      real*8 atomspin_local(ndim,max_num_atoms)                         ! in
!      real*8 shortforce(3,max_num_atoms)                                ! internal
      real*8 strs(3,3,maxnum_funcvalues_short_atomic,max_num_atoms)     ! internal dummy
      real*8 minvalue_short_atomic(nelem,maxnum_funcvalues_short_atomic)! in
      real*8 maxvalue_short_atomic(nelem,maxnum_funcvalues_short_atomic)! in
      real*8 minvalue_q(nelem)! in
      real*8 maxvalue_q(nelem)! in
      real*8, allocatable :: lstb(:,:)                                  ! internal, xyz and r_ij 
      real*8, allocatable :: dsfuncdxyz(:,:,:,:)                        ! internal
      real*8  rdummy(3,3,max_num_atoms)
!!
      logical lperiodic_local(ndim)                                     ! in
      logical lperiodic                                                 ! internal
      real*8, allocatable :: dAdxyzQ(:,:,:)
      logical ldummy
!!
!      shortforce(:,:)=0.0d0 ! just dummy here
      nnshortforce_local(:,:,:)=0.0d0 
      symfunction(:,:)=0.0d0
      rdummy(:,:,:)=0.0d0
!!
      do i1=1,npoints
        num_atoms        =num_atoms_local(i1)
        zelem(:)         =zelem_local(i1,:)
        lperiodic        =lperiodic_local(i1)
        xyzstruct(:,:)   = xyzstruct_local(:,:,i1)
!        atomcharge(:)    = atomcharge_local(i1,:)
        symfunction(:,:) = symfunction_local(:,:,i1)
        allocate(dAdxyzQ(num_atoms,3,num_atoms))
!!
!! allocate the arrays for charge
        allocate(dchargedxyz(num_atoms,3,num_atoms))
        dchargedxyz(:,:,:)=0.0d0
        allocate(dChidxyz(num_atoms,3,num_atoms))
        dChidxyz(:,:,:)  =0.0d0
        allocate(Amatrix(num_atoms,num_atoms))
        Amatrix(:,:)=0.0d0
        allocate(atomcharge(num_atoms))
        atomcharge(:) = atomcharge_local(i1,:num_atoms)
        allocate(scalecharge(num_atoms))
        scalecharge(:) = scalecharge_local(i1,:num_atoms)

!!     
!! in principle dsfuncdxyz could be precalculated, but needs too much storage on disk
!!
        allocate(lsta(2,max_num_atoms))
        allocate(lstc(listdim))
        allocate(lste(listdim))
        allocate(lstb(listdim,4))
        allocate(num_neighbors_short_atomic(num_atoms))
!!
!! we must use the serial version here because we have already parallelized in a higher level routine
        call getneighborsatomic(num_atoms,&
          num_neighbors_short_atomic,zelem,max_num_neighbors_short_atomic,&
          lsta,lstc,lste,&
          maxcutoff_short_atomic,lattice_local(1,1,i1),xyzstruct_local(1,1,i1),&
          lstb,lperiodic)
!!
        allocate(dsfuncdxyz(maxnum_funcvalues_short_atomic,max_num_atoms,0:max_num_neighbors_short_atomic,3))
!! allocation of index arrays must be over num_atoms here because we cannot parallelize over atoms in geterror
        allocate(neighboridx_short_atomic(num_atoms,0:max_num_neighbors_short_atomic))  
        allocate(invneighboridx_short_atomic(num_atoms,max_num_atoms))  
        call getneighboridxatomic(num_atoms,listdim,&
          max_num_atoms,max_num_neighbors_short_atomic,&
          lsta,lstc,neighboridx_short_atomic,invneighboridx_short_atomic)
!!
!! get dsfuncdxyz:
!! Caution: symmetry functions cannot be used because here they are not scaled
        call abstime(timecalconefunctionstart,daycalconefunction)
        allocate(atomindex_dummy(num_atoms))
        do i2=1,num_atoms
          atomindex_dummy(i2)=i2
        enddo
        call calconefunction_atomic(cutoff_type,cutoff_alpha,max_num_neighbors_short_atomic,&
           max_num_atoms,1,num_atoms,atomindex_dummy,max_num_atoms,elementindex,&
           maxnum_funcvalues_short_atomic,num_funcvalues_short_atomic,&
           nelem,zelem,listdim,&
           lsta,lstc,lste,invneighboridx_short_atomic,&
           function_type_short_atomic,symelement_short_atomic,&
           xyzstruct_local(1,1,i1),atomspin_local(i1,:),symfunctiondummy,0.d0,&
           funccutoff_short_atomic,eta_short_atomic,rshift_short_atomic,&
           lambda_short_atomic,zeta_short_atomic,dsfuncdxyz,strs,lstb,&
           lperiodic,.true.,.false.,.false.,ldummy)
        deallocate(atomindex_dummy)
        call abstime(timecalconefunctionend,daycalconefunction)
        timecalconefunction=timecalconefunction+timecalconefunctionend-timecalconefunctionstart 
!!
        deallocate(lsta)
        deallocate(lstc)
        deallocate(lste)
        deallocate(lstb)
!!
!! scale dsfuncdxyz
        if(lscalesym)then
          call scaledsfunc(max_num_neighbors_short_atomic,&
            maxnum_funcvalues_short_atomic,num_funcvalues_short_atomic,&
            nelem,num_atoms,minvalue_short_atomic,maxvalue_short_atomic,&
            scmin_short_atomic,scmax_short_atomic,&
            zelem,dsfuncdxyz,strs)
        endif
!!
!! getdChidsfunc using getdchargedsfunc
        allocate(atomhardness(num_atoms))
        allocate(gausswidth(num_atoms))
        dChidsfunc(:,:)=0.0d0
        atomhardness(:)=0.0d0
        gausswidth(:)=0.0d0
        call getdchargedsfunc(num_atoms,&
            zelem,symfunction_local(:,:,i1),dChidsfunc)
        do i2=1,num_atoms
!! get dChidxyz         
          atomhardness(i2)=fixedhardness(elementindex(zelem(i2)))
          gausswidth(i2)=fixedgausswidth(elementindex(zelem(i2)))
          do i3=1,num_atoms
            if (invneighboridx_short_atomic(i2,i3) /= -1) then ! they are neighbors
              do i4=1,num_funcvalues_short_atomic(elementindex(zelem(i2))) ! loop over all SF
                ! dChi_i/dx = sum_j dChi_i/dSF_j * dSF_j/dx   | j goes over
                ! neighbours, otherwise deriv. is 0
                dChidxyz(i2,:,i3) = dChidxyz(i2,:,i3) &
                    + dChidsfunc(i2, i4)*dsfuncdxyz(i4,i2,invneighboridx_short_atomic(i2,i3),:)
              enddo
            endif
          enddo
        enddo
        if (lperiodic) then
            call eemMatrixEwald(num_atoms, xyzstruct, lattice_local(1,1,i1), gausswidth, atomhardness, Amatrix)
            call eemdAdxyzTimesQEwald(num_atoms, xyzstruct, lattice_local(1,1,i1), gausswidth, atomcharge, dAdxyzQ)
        else
            call eemMatrix(num_atoms,xyzstruct,gausswidth,atomhardness,Amatrix)
            call eemdAdxyzTimesQ(num_atoms, xyzstruct, atomcharge, gausswidth, dAdxyzQ)
        endif
        call eemdQdxyz(num_atoms, atomcharge,gausswidth, Amatrix, dAdxyzQ, dChidxyz, dchargedxyz)
        deallocate(atomhardness)
        deallocate(gausswidth)
        if(lscalecharge)then
          call scaledQdxyz(num_atoms,minvalue_q,maxvalue_q,&
               scmin_short_atomic,scmax_short_atomic,zelem,&
               dchargedxyz,rdummy(:,:,:num_atoms))
        endif
!!
        maxinputnode= maxnum_funcvalues_short_atomic+1
        allocate(deshortdsfunc(max_num_atoms,maxinputnode))
!!
        call abstime(timegetshortforcesstart,daygetshortforces)
        call getshortforces_type3(max_num_neighbors_short_atomic,maxinputnode,num_atoms,&
          num_neighbors_short_atomic,neighboridx_short_atomic,invneighboridx_short_atomic,zelem,&
          symfunction,scalecharge,dsfuncdxyz,deshortdsfunc,dchargedxyz,nnshortforce)
        call abstime(timegetshortforcesend,daygetshortforces)
        timegetshortforces=timegetshortforces+timegetshortforcesend-timegetshortforcesstart 
!!       
        nnshortforce_local(:,:,i1)=nnshortforce(:,:)

        deallocate(dsfuncdxyz)
        deallocate(neighboridx_short_atomic)  
        deallocate(invneighboridx_short_atomic)  
        deallocate(num_neighbors_short_atomic)
        deallocate(dChidxyz)
        deallocate(dchargedxyz)
        deallocate(Amatrix)
        deallocate(deshortdsfunc)
        deallocate(atomcharge)
        deallocate(scalecharge)
        deallocate(dAdxyzQ)
!!
      enddo ! i1
!!
      return
      end
