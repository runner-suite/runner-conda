module energyAndForces4G
    use precision
    use linalg
    implicit none
    contains

subroutine get_energies_and_forces(num_atoms,zelem,lattice,&
               xyzstruct,atomspin,totalcharge,lperiodic,minvalue,maxvalue,&
               avvalue,nnatomcharge,nntotalenergy,nntotalforces,elecenergy,elecforce,atomicenergy,stress)
    use globaloptions 
    use symfunctions
    use nnewald
    use nnshort_atomic
    use fileunits
    use nnflags
    use eem
    use ewaldSummation

    implicit none
 
    integer,intent(in) :: num_atoms
    integer :: num_neighbors(num_atoms)
    integer :: max_num_neighbors 
    integer lsta(2,max_num_atoms)                          ! internal, numbers of neighbors
    integer lstc(listdim)                                  ! internal, identification of atom
    integer lste(listdim)                                  ! internal, nuclear charge of atom
    integer :: i1,i2,i3
    integer,allocatable,dimension(:) :: atomindex_dummy
    integer,allocatable,dimension(:,:) :: neighboridx
    integer,allocatable,dimension(:,:) :: invneighboridx
    integer :: ielem
    integer :: zelem(max_num_atoms)
    integer :: maxinputnode
    integer :: num_atoms_element(nelem)

    real*8,intent(in) :: atomspin(num_atoms)
    real*8,intent(in) :: xyzstruct(3,num_atoms)
    real*8,intent(out) :: nntotalenergy
    real*8,intent(out) :: nntotalforces(3,num_atoms)
    real*8,intent(in) :: lattice(3,3)
    real*8,intent(in) :: totalcharge
    real*8,intent(in) :: minvalue(nelem,maxnum_funcvalues_short_atomic)
    real*8,intent(in) :: maxvalue(nelem,maxnum_funcvalues_short_atomic)
    real*8,intent(in) :: avvalue(nelem,maxnum_funcvalues_short_atomic)
    real*8, intent(out), optional :: elecenergy, elecforce(3,num_atoms), atomicenergy(num_atoms), stress(3,3)
    real*8 :: nnelecenergy
    real*8 :: nnelecforces(3,num_atoms)
    real*8 :: nnshortenergy
    real*8 :: nnatomenergy(num_atoms)
    real*8 :: nnshortforces(3,num_atoms)
    real*8 :: nnatomcharge(num_atoms)
    real*8 :: atomhardness(num_atoms)
    real*8 :: gausswidth(num_atoms)
    real*8 :: nnatomchi(num_atoms)
    real*8 :: symfunction(maxnum_funcvalues_short_atomic,max_num_atoms)
    real*8 :: rdummy
    real*8 lstb(listdim,4)
    real*8 :: dChidsfunc(max_num_atoms,maxnum_funcvalues_elec)
    real*8 :: Amatrix(num_atoms,num_atoms)
    real*8 :: dChidxyz(num_atoms,3,num_atoms)
    real*8 :: dchargedxyz(num_atoms,3,num_atoms)
    real*8,allocatable,dimension(:,:,:,:) :: dsfuncdxyz
    real*8 strs(3,3,maxnum_funcvalues_short_atomic,max_num_atoms)       ! internal
    real*8 strs_temp(3,3,max_num_atoms) ! internal
    real*8 :: threshold
    real*8,allocatable,dimension(:,:) :: deshortdsfunc
    real*8 :: nodes_values(maxnum_layers_elec, maxnodes_elec) ! apparently just dummy 
    real*8 :: nodes_sum(maxnum_layers_elec, maxnodes_elec) ! just dummy too
    real*8 :: atomenergysum
    real*8 :: dAdxyzQ(num_atoms,3, num_atoms)
    real*8 :: nnstress_short(3,3)
    real*8 :: nnstress_elec(3,3)
    real*8 :: dEdLat(3,3), dChidLat(num_atoms, 3, 3)
    integer :: iat, inei, isf, i, c
    real(dp) :: relat(3), invlat(3,3), rnei(3), dAdlatQ(num_atoms, 3, 3), dQdlat(num_atoms, 3, 3), datdlat(3,3,3)
    real(dp) :: dEshortdLat(num_atoms, 3,3), dEelecdLat(3,3), dEdQ(num_atoms)
    real(dp) :: trickforce(3,num_atoms)

    logical, intent(in) :: lperiodic
    logical lrmin 
    logical lextrapolation

    !! initialization
    lrmin=.true.
    Amatrix(:,:)=0.0d0
    dChidxyz(:,:,:)=0.0d0
    dchargedxyz(:,:,:)=0.0d0
    atomhardness(:)=0.0d0
    threshold =0.0001d0
    dEdLat(:,:)=0.0d0
    !!
    call getneighborsatomic(num_atoms,num_neighbors,&
         zelem, max_num_neighbors,&
         lsta,lstc,lste,maxcutoff_short_atomic,&
         lattice,xyzstruct,lstb,lperiodic)
    allocate(dsfuncdxyz(maxnum_funcvalues_short_atomic,max_num_atoms,0:max_num_neighbors,3))
    allocate(neighboridx(num_atoms,0:max_num_neighbors))
    allocate(invneighboridx(num_atoms,max_num_atoms))
    call getneighboridxatomic(num_atoms,listdim,&
         max_num_atoms,max_num_neighbors,&
         lsta,lstc,neighboridx,invneighboridx)
    allocate(atomindex_dummy(max_num_atoms))

    do i2 =1,num_atoms
      ielem=elementindex(zelem(i2))
      atomindex_dummy(i2)=i2
      atomhardness(i2)=fixedhardness(ielem)
      gausswidth(i2)=fixedgausswidth(ielem)
    enddo
    if(.not.lshort)then
        !! KK: Here we want to take care the case of the prediction of the electrostatic part,
        !!     but this subroutine only consider parameters of short-range symmetry functions
        !!     Therefore, here we put all parameters obtained from electrostatic part to short
        !!     -range part
        function_type_short_atomic(:,:)=function_type_elec(:,:) 
        symelement_short_atomic(:,:,:)=symelement_elec(:,:,:) 
        funccutoff_short_atomic(:,:)=funccutoff_elec(:,:)
        eta_short_atomic(:,:)=eta_elec(:,:) 
        zeta_short_atomic(:,:)=zeta_elec(:,:) 
        lambda_short_atomic(:,:)=lambda_elec(:,:) 
        rshift_short_atomic(:,:)=rshift_elec(:,:)     
    endif
    call calconefunction_atomic(cutoff_type,cutoff_alpha,&
         max_num_neighbors,max_num_atoms,1,num_atoms,&
         atomindex_dummy,max_num_atoms,elementindex,&
         maxnum_funcvalues_elec,num_funcvalues_elec,nelem,&
         zelem,listdim,lsta,lstc,lste,invneighboridx,&
         function_type_short_atomic,symelement_short_atomic,&
         xyzstruct,atomspin,symfunction,rmin,funccutoff_short_atomic,&
         eta_short_atomic,rshift_short_atomic,lambda_short_atomic,&
         zeta_short_atomic,dsfuncdxyz,strs,lstb,lperiodic,.true.,present(stress),.false.,&
         lrmin)
    !!==============================================================================
    !! Do the scaling or centering for the SFs
    !!==============================================================================
    do i2 =1,num_atoms
        ielem=elementindex(zelem(i2))
      do i3 =1,num_funcvalues_short_atomic(ielem)
        lextrapolation=.false.
        if((symfunction(i3,i2)-maxvalue(ielem,i3)).gt.threshold)then
          write(ounit,'(a,a5,x,2i5,x,a,2f18.8)')&
           '### EXTRAPOLATION WARNING ### ','short',&
           i3,i2,'too large',symfunction(i3,i2),maxvalue(ielem,i3)
           lextrapolation=.true.
        elseif((-symfunction(i3,i2)+minvalue(ielem,i3)).gt.threshold)then
          write(ounit,'(a,a5,x,2i5,x,a,2f18.8)')&
           '### EXTRAPOLATION WARNING ### ','short',&
           i3,i2,'too small',symfunction(i3,i2),minvalue(ielem,i3)
          lextrapolation=.true.
        endif
        if(lcentersym.and..not.lscalesym)then
        !! For each symmetry function remove the CMS of the respective element 
          symfunction(i3,i2)=symfunction(i3,i2)-avvalue(ielem,i3)
        elseif(lscalesym.and..not.lcentersym)then
        !! Scale each symmetry function value for the respective element
          symfunction(i3,i2)=(symfunction(i3,i2)&
         -minvalue(ielem,i3))/ &
         (maxvalue(ielem,i3)-minvalue(ielem,i3))&
          *(scmax_short_atomic-scmin_short_atomic) + scmin_short_atomic
        elseif(lscalesym.and.lcentersym)then
          symfunction(i3,i2)=(symfunction(i3,i2)&
          -avvalue(ielem,i3))/ &
          (maxvalue(ielem,i3)-minvalue(ielem,i3))
        else
        endif
      enddo
    enddo
    !!==============================================================================
    !! Calculate atomic electronegativities
    !!==============================================================================
    do i2 =1,num_atoms
      ielem=elementindex(zelem(i2))
      nodes_values(:,:) = 0.d0
      nodes_sum(:,:) = 0.d0
      call calconenn(1,maxnum_funcvalues_elec,maxnodes_elec,&
        maxnum_layers_elec,num_layers_elec(ielem),&
        maxnum_weights_elec,nodes_elec(0,ielem),&
        symfunction(1,i2),&
        weights_elec(1,ielem),&
        nodes_values,nodes_sum,&
        nnatomchi(i2),actfunc_elec(1,1,ielem))
    enddo
    !!==============================================================================
    !! Do the scaling for dsfuncdxyz
    !!==============================================================================
    if(lscalesym)then
        call scaledsfunc(max_num_neighbors,&
            maxnum_funcvalues_short_atomic,num_funcvalues_short_atomic,&
            nelem,num_atoms,minvalue,maxvalue,&
            scmin_short_atomic, scmax_short_atomic,&
            zelem,dsfuncdxyz,strs)
    endif
    !!==============================================================================
    !! Calculating the EEM charges and electriostatic energies and forces
    !!==============================================================================
    if (lperiodic) then
        call eemMatrixEwald(num_atoms, xyzstruct, lattice, gausswidth, atomhardness, Amatrix)
        call eemCharges(num_atoms,Amatrix,nnatomchi,totalcharge,nnatomcharge)
        call eemdAdxyzTimesQEwald(num_atoms, xyzstruct, lattice, gausswidth, nnatomcharge, dAdxyzQ)
        if (ldostress) then
            call eemdAdlatTimesQEwald(num_atoms, xyzstruct, lattice, gausswidth, nnatomcharge, dAdlatQ)
        end if
    else
        call eemMatrix(num_atoms,xyzstruct,gausswidth,atomhardness,Amatrix)
        call eemCharges(num_atoms,Amatrix,nnatomchi,totalcharge,nnatomcharge)
        call eemdAdxyzTimesQ(num_atoms, xyzstruct, nnatomcharge, gausswidth, dAdxyzQ)
    endif
    ! this routine is from the old electrostatics. It therefore calculates the derivative of the nn output wrt. s. func.
    ! the name is therefore a bit misleading here, as it is now calculating der. of Chi wrt. s. func.
    call getdchargedsfunc(num_atoms,zelem,symfunction,dChidsfunc)
    dchidxyz = 0._dp
    dChidLat(:,:,:) = 0._dp
    do i1=1,num_atoms
        !c = 0
        do i2=1,num_atoms
            ! because neighbourlist: use atomindex(i2)
            ! invneighboridx_elec(i2, i1) = neighbour index of atom

            !  o----------------------------------------------------------------------o
            !  | TODO: Here neighbourlist should be used to avoid loop over all atoms |
            !  o----------------------------------------------------------------------o
            ! JF: the reason the neighborlist is not used here, is because in the neighborlist atoms
            ! are listed multiple times when they occur in neighbouring cells.
            ! But, in dsfuncdxyz these multiple occurences are already accumulated!
            if (invneighboridx(i1,i2) /= -1) then ! they are neighbors
                !c = c + 1
                !if( i2 /= neighboridx(i1, invneighboridx(i1,i2))) stop 'EEEE'
                do i3=1,num_funcvalues_short_atomic(elementindex(zelem(i1))) ! loop over all SF
                    ! dChi_i/dx = sum_j dChi_i/dSF_j * dSF_j/dx   | j goes over
                    ! neighbours, otherwise deriv. is 0
                    dChidxyz(i1,:,i2) = dChidxyz(i1,:,i2) &
                            + dChidsfunc(i1, i3) * dsfuncdxyz(i3,i1,invneighboridx(i1,i2),:)
                enddo
            endif
        enddo
        !print*, 'JFX', c, num_neighbors(i1) ! JF: here c /= num_neighbours(i1). num_neighbours(i1) >= c!
        do i3=1,num_funcvalues_short_atomic(elementindex(zelem(i1))) ! loop over all SF
            dChidLat(i1,:,:) = dChidLat(i1,:,:) + dChidsfunc(i1, i3) * strs(:,:,i3,i1)
        end do
    enddo
    ! JF: Here we transfort from the 'stress' to the actual lattice derivates for better understanding
    call inv3DM(transpose(lattice), invlat) ! invlat is transposed (wrt RuNNer convention) which allows for the use of matmul below
    do iat=1,num_atoms
        dChidLat(iat,:,:) = matmul(invlat, transpose(dChidLat(iat, :,:)))
    end do
    call electrostatics_type5(num_atoms,zelem,&
            Amatrix,dAdxyzQ,dAdlatQ,nnatomcharge,xyzstruct,dChidxyz,dChidlat,&
            nnelecenergy,nnelecforces,lattice, lperiodic,dEelecdLat)

    dEdLat = dEdLat +  dEelecdLat
    !!==============================================================================
    !! Calculating the short range energies and forces
    !!==============================================================================
    if(lshort)then
        call calconeshort_type3(num_atoms,zelem,&
              symfunction,nnatomcharge,nnshortenergy,nnatomenergy)
        maxinputnode = maxnum_funcvalues_short_atomic+1



        ! call eemdQdlat(num_atoms, Amatrix, dAdlatQ, dChidlat, dQdlat)

        ! dChidLat and dQdlat have been tested against finite differences (not sure if it should be transposed though)

        allocate(deshortdsfunc(max_num_atoms,maxinputnode))
        ! todo: replace this and use the force trick!!!  ! JF: done
        ! JF: old version -----------------------------
        ! call eemdQdxyz(num_atoms, nnatomcharge,gausswidth, Amatrix, dAdxyzQ, dChidxyz, dchargedxyz)
        ! call getshortforces_type3(max_num_neighbors,maxinputnode,&
        !     num_atoms,num_neighbors,neighboridx,invneighboridx,zelem,&
        !     symfunction,nnatomcharge,dsfuncdxyz,deshortdsfunc,&
        !     dchargedxyz,nnshortforces)

        ! JF: new version -----------------------------
        call getshortforces_type3_forceTrick(max_num_neighbors,maxinputnode,&
                num_atoms,num_neighbors,neighboridx,invneighboridx,zelem,&
                symfunction,nnatomcharge,dsfuncdxyz,deshortdsfunc,&
                dEdQ,nnshortforces)
        call calcPartialForcesEEM(num_atoms, Amatrix, dAdxyzQ, dEdQ, dChidxyz, nnatomcharge, trickforce) ! using force trick.
        nnshortforces = nnshortforces + trickforce


        ! JF: STRESS
        if (ldostress) then
            dEshortdLat(:,:,:) = 0._dp
            do i1=1,num_atoms
                do i3=1,num_funcvalues_short_atomic(elementindex(zelem(i1)))
                    dEshortdLat(i1,:,:) = dEshortdLat(i1,:,:) + deshortdsfunc(i1, i3) * strs(:,:,i3,i1)
                end do
            enddo
            call inv3DM(transpose(lattice), invlat) ! invlat is transposed (wrt RuNNer convention)
            ! convert strs stuff to actual lattice derivatives
            do iat=1,num_atoms
                dEshortdLat(iat,:,:) = matmul(invlat, transpose(dEshortdLat(iat, :,:)))
            end do

            ! add the part that comes from the dQdlat
            ! no force trick (leaving this here as a reference and for Kenko ;) )
            ! do iat=1,num_atoms
            !     dEshortdLat(iat,:,:) = dEshortdLat(iat,:,:) + dQdlat(iat,:,:) * dEdQ(iat)
            ! end do
            ! dEdLat = sum(dEshortdLat, 1) ! tested with finited differences
            ! dEdLat = dEdLat + dEelecdLat

            ! with force trick
            call calcPartialLatticeDerivativesEEM(num_atoms, Amatrix, dAdlatQ, dEdQ, dChidLat, nnatomcharge, dEdLat)
            dEdLat = dEdLat + sum(dEshortdLat, 1) 


        end if

!!=================================================================================
!! Calculating the total energies and forces
!!=================================================================================   
        if(lremoveatomenergies.and.lshort)then
            num_atoms_element(:) = 0
            do i1=1,num_atoms ! loop over all atoms
                num_atoms_element(elementindex(zelem(i1))) = num_atoms_element(elementindex(zelem(i1))) + 1
            enddo
          call addatoms(num_atoms,&
               zelem,num_atoms_element,&
               atomenergysum,nnatomenergy)
         nnshortenergy=nnshortenergy+atomenergysum
        endif ! lremoveatomenergies
    endif !! KK: if short-range is used
    nntotalenergy = nnshortenergy + nnelecenergy
    !write(*,*) 'Eelec', nnelecenergy
    !write(*,*) 'Eshort', nnshortenergy
    !write(*,*) 'Etotal', nntotalenergy
    nntotalforces(:,:num_atoms) = nnelecforces(:,:num_atoms)+nnshortforces(:,:num_atoms)
    !max_num_atoms = max_num_atoms_old
    if (present(elecenergy)) then
        elecenergy = nnelecenergy
    endif
    if (present(elecforce)) then
        elecforce = nnelecforces(:,:num_atoms)
    endif
    if(present(atomicenergy)) then
        atomicenergy = nnatomenergy(:num_atoms)
    endif


    if (present(stress)) then
        ! JF: I compared the stress to finite differences.
        ! also compared with the stress of 2G. The ordering (transpose) of the elements should be correct.
        call dEdLat2Stress(lattice, dEdLat, stress)
    end if
end subroutine

    subroutine dEdLat2Stress(lat, dEdlat, stress)
        real(dp), intent(in) :: lat(3,3)
        real(dp), intent(out) :: stress(3,3)
        real(dp), intent(in) :: dEdlat(3,3)

        stress = -1._dp / det3D(lat) * matmul(transpose(dEdlat), lat)

    end subroutine

    subroutine stress2dEdLat(lat, stress, dEdlat)
        real(dp), intent(in) :: lat(3,3)
        real(dp), intent(in) :: stress(3,3)
        real(dp), intent(out) :: dEdlat(3,3)
        real(dp) :: invlat(3,3)

        call inv3DM(transpose(lat), invlat)
        dEdlat = -1._dp * det3D(lat) * transpose(matmul(stress, transpose(invlat)))

    end subroutine


end module energyAndForces4G
