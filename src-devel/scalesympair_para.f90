!######################################################################
! This routine is part of
! RuNNer - RuNNer Neural Network Energy Representation
! (c) 2008-2024 Prof. Dr. Joerg Behler 
! Ruhr-Universitaet Bochum, Germany
!
! This program is free software: you can redistribute it and/or modify it 
! under the terms of the GNU General Public License as published by the 
! Free Software Foundation, either version 3 of the License, or 
! (at your option) any later version.
!
! This program is distributed in the hope that it will be useful, but 
! WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
! or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
! for more details.
!
! You should have received a copy of the GNU General Public License along 
! with this program. If not, see http://www.gnu.org/licenses.
! WARNING: THIS FILE IS PART OF THE UNMAINTAINED CONDA PACKAGE OF RUNNER 1.3. 
!######################################################################
!! called by:
!! - predictionpair.f90
!!
      subroutine scalesympair_para(num_pairs,&
         pindex,zelemp_list,symfunctionp,&
         minvalue_short_pair,maxvalue_short_pair,avvalue_short_pair)
!!
      use fileunits
      use globaloptions
      use nnshort_pair
!!
      implicit none
!!
      integer num_pairs
      integer zelemp_list(2,max_num_pairs)
      integer i2,i3
      integer pindex(max_num_pairs)
!!
!!      real*8 symfunctionp(maxnum_funcvaluesp,num_pairs_para)
      real*8 symfunctionp(maxnum_funcvalues_short_pair,max_num_pairs)
      real*8 minvalue_short_pair(npairs,maxnum_funcvalues_short_pair)
      real*8 maxvalue_short_pair(npairs,maxnum_funcvalues_short_pair)
      real*8 avvalue_short_pair(npairs,maxnum_funcvalues_short_pair)
!!
        do i2=1,num_pairs
          do i3=1,num_funcvalues_short_pair(pairindex(zelemp_list(1,pindex(i2)),zelemp_list(2,pindex(i2)))) 
            if(lcentersym.and..not.lscalesym)then
!! For each symmetry function remove the CMS of the respective element 
              symfunctionp(i3,pindex(i2))=symfunctionp(i3,pindex(i2))&
                  - avvalue_short_pair(pairindex(zelemp_list(1,pindex(i2)),zelemp_list(2,pindex(i2))),i3) 
            elseif(lscalesym.and..not.lcentersym)then
!! Scale each symmetry function value for the respective element
              symfunctionp(i3,pindex(i2))=&
             (symfunctionp(i3,pindex(i2)) &
             -minvalue_short_pair(pairindex(zelemp_list(1,pindex(i2)),zelemp_list(2,pindex(i2))),i3))/ &
             (maxvalue_short_pair(pairindex(zelemp_list(1,pindex(i2)),zelemp_list(2,pindex(i2))),i3)-&
              minvalue_short_pair(pairindex(zelemp_list(1,pindex(i2)),zelemp_list(2,pindex(i2))),i3))&
              *(scmax_short_pair-scmin_short_pair) + scmin_short_pair
            elseif(lscalesym.and.lcentersym)then
              symfunctionp(i3,pindex(i2))=&
             (symfunctionp(i3,pindex(i2))-avvalue_short_pair(pairindex(zelemp_list(1,pindex(i2)),zelemp_list(2,pindex(i2))),i3))&
             / &
             (maxvalue_short_pair(pairindex(zelemp_list(1,pindex(i2)),zelemp_list(2,pindex(i2))),i3)- &
              minvalue_short_pair(pairindex(zelemp_list(1,pindex(i2)),zelemp_list(2,pindex(i2))),i3))
            else
            endif
          enddo ! i3
        enddo ! i2
!!
      return
      end
