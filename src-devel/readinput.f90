!######################################################################
! This routine is part of
! RuNNer - RuNNer Neural Network Energy Representation
! (c) 2008-2024 Prof. Dr. Joerg Behler
! Ruhr-Universitaet Bochum, Germany
!
! This program is free software: you can redistribute it and/or modify it 
! under the terms of the GNU General Public License as published by the 
! Free Software Foundation, either version 3 of the License, or 
! (at your option) any later version.
!
! This program is distributed in the hope that it will be useful, but 
! WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
! or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
! for more details.
!
! You should have received a copy of the GNU General Public License along 
! with this program. If not, see http://www.gnu.org/licenses.
! WARNING: THIS FILE IS PART OF THE UNMAINTAINED CONDA PACKAGE OF RUNNER 1.3. 
!######################################################################
!! called by:
!! - main.f90
!!
      subroutine readinput(ielem,iseed,lelement)
!!
      use mpi_mod
      use fileunits
      use fittingoptions
      use nnflags
      use globaloptions
      use mode1options
      use predictionoptions
      use symfunctions
      use nnshort_atomic
      use nnewald
      use nnshort_pair
      use inputnncounters
!!    
      implicit none
!!
      integer icount                                                    ! internal
      integer jcount                                                    ! internal
      integer nodes_short_atomic_temp(0:maxnum_layers_short_atomic)     ! internal
      integer nodes_elec_temp(0:maxnum_layers_elec)                     ! internal
      integer nodes_short_pair_temp(0:maxnum_layers_short_pair)         ! internal
      integer*8 iseed                                                   ! out, seed for weight initialization
      integer ielem                                                     ! in (read also before from input.nn) 
      integer wcount                                                    ! internal 
      integer i,j,k                                                     
      integer i0,i1,i2,i3                                               ! internal
      integer ztemp                                                     ! internal
      integer ztemp1                                                    ! internal
      integer ztemp2                                                    ! internal
      integer itemp                                                     ! internal
      integer layer                                                     ! internal
      integer node                                                      ! internal
      integer sym_short_atomic_count(nelem)                             ! internal
      integer sym_elec_count(nelem)                                     ! internal
      integer sym_short_pair_count(npairs)                              ! internal
!!                                                                      
      real*8 kalmanlambda_local                                         ! internal
      real*8 kalmanlambdae_local                                        ! internal
      real*8 chargetemp                                                 ! internal
      real*8 hardnesstemp                                               ! internal
      real*8 gausswidthtemp                                             ! internal
!!                                                                      
      character*40 dummy                                                ! internal
      character*40 keyword                                              ! internal
      character*1 actfunc_short_atomic_dummy(maxnum_layers_short_atomic) ! internal
      character*1 actfunc_elec_dummy(maxnum_layers_elec)                 ! internal
      character*1 actfunc_short_pair_dummy(maxnum_layers_short_pair)     ! internal
      character*1 actfunc                                               ! internal
      character*2 elementtemp                                           ! internal
      character*2 elementtemp1                                          ! internal
      character*2 elementtemp2                                          ! internal
!!                                                                      
      logical lelement(102)                                             ! internal
!!
!! all inputs must be initialized here, except for the ones read before in getdimensions.f90
      write(ounit,*)'Reading control parameters from input.nn'
      write(ounit,*)'============================================================='
!!'
!! initialization of counters
      call initializecounters()
!! initializations of input quantities
      if(lshort.and.((nn_type_short.eq.1).or.(nn_type_short.eq.3)))then
        nodes_short_atomic_temp(:)   =0
        actfunc_short_atomic_dummy(:)=' '
      endif
      if(lshort.and.(nn_type_short.eq.2))then
        nodes_short_pair_temp(:)     =0
        actfunc_short_pair_dummy(:)  =' '
      endif
      if((lelec.and.(nn_type_elec.eq.1)).or.(lelec.and.(nn_type_elec.eq.5)))then
        nodes_elec_temp(:)           =0
        actfunc_elec_dummy(:)        =' '
      endif
      if(lelec.and.lshort)then !! KK: for second NN in 3G and 4G cases
        nodes_elec_temp(:)           =0
        actfunc_elec_dummy(:)        =' '
      endif
      kalmanlambda_local =0.98000d0
      kalmanlambdae_local=0.98000d0
      iseed=200
!! read all other defaults
      call inputnndefaults()
!!
!! initializations of derived quantities
      if((lshort.and.(nn_type_short.eq.1)).or.(lshort.and.(nn_type_short.eq.3)))then
        windex_short_atomic(:,:)    =0
        num_weights_short_atomic(:) =0 
        maxnum_weights_short_atomic =0
      endif
      if(lshort.and.(nn_type_short.eq.2))then
        windex_short_pair(:,:)      =0
        num_weights_short_pair(:)   =0 
        maxnum_weights_short_pair   =0
      endif
      if((lelec.and.(nn_type_elec.eq.1)).or.(lelec.and.(nn_type_elec.eq.5)))then
        windex_elec(:,:)            =0
        num_weights_elec(:)         =0 
        maxnum_weights_elec         =0
      endif
      if(lelec.and.lshort)then !! KK: for second NN in 3G and 4G cases
        windex_elec(:,:)            =0
        num_weights_elec(:)         =0
        maxnum_weights_elec         =0
      endif
!!
!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!! start regular reading of input.nn '
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!
      call readkeywords(iseed,&
        nodes_short_atomic_temp,nodes_elec_temp,nodes_short_pair_temp,&
        kalmanlambda_local,kalmanlambdae_local)
!!
!! TODO: move this to a better place:
!! set output nodes
      if((lshort.and.(nn_type_short.eq.1)).or.(lshort.and.(nn_type_short.eq.3)))then
        do i1=1,nelem
          nodes_short_atomic(maxnum_layers_short_atomic,i1)=1 ! initialize as one output node
!! add second output node for atomic charge
          if(lelec.and.(nn_type_elec.eq.2))then
            nodes_short_atomic(maxnum_layers_short_atomic,i1)&
              =nodes_short_atomic(maxnum_layers_short_atomic,i1)+1
          endif
        enddo
      endif
      if(lshort.and.(nn_type_short.eq.2))then
        do i1=1,npairs
          nodes_short_pair(maxnum_layers_short_pair,i1)=1
        enddo
      endif
      if((lelec.and.(nn_type_elec.eq.1)).or.(lelec.and.(nn_type_elec.eq.5)))then
        do i1=1,nelem
          nodes_elec(maxnum_layers_elec,i1)=1
        enddo
      endif
      if(lshort.and.lelec)then  !! KK: for second NN in 3G and 4G cases
        do i1=1,nelem
            nodes_elec(maxnum_layers_elec,i1)=1
        enddo
      endif
!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!! Read global activation functions 
!! this must be done after reading the global NN architecture
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      open(nnunit,file='input.nn',form='formatted',status='old')
      rewind(nnunit)
 50   continue
      read(nnunit,*,END=51) keyword 
!!
      if((keyword.eq.'global_activation_short').and.lshort.and.(nn_type_short.eq.1))then
        if(count_global_activation_short_atomic.gt.0)then
          write(ounit,*)'Error: global_activation_short specified twice'
          stop
        endif
        call setglobalactivation(nelem,count_global_activation_short_atomic,&
          maxnum_layers_short_atomic,maxnodes_short_atomic,nodes_short_atomic,actfunc_short_atomic,&
          actfunc_short_atomic_dummy,keyword)
!!
      elseif((keyword.eq.'global_activation_electrostatic').and.lelec.and.(nn_type_elec.eq.1))then
        if(count_global_activation_elec.gt.0)then
          write(ounit,*)'Error: global_activation_electrostatic specified twice'
          stop !'
        endif
        call setglobalactivation(nelem,count_global_activation_elec,&
          maxnum_layers_elec,maxnodes_elec,nodes_elec,actfunc_elec,&
          actfunc_elec_dummy,keyword)
!!
      elseif((keyword.eq.'global_activation_electrostatic').and.lelec.and.(nn_type_elec.eq.5))then
        if(count_global_activation_elec.gt.0)then
          write(ounit,*)'Error: global_activation_electrostatic specified twice'
          stop !'
        endif
        call setglobalactivation(nelem,count_global_activation_elec,&
          maxnum_layers_elec,maxnodes_elec,nodes_elec,actfunc_elec,&
          actfunc_elec_dummy,keyword)
!!
      elseif((keyword.eq.'global_activation_electrostatic'.and.lelec.and.lshort))then !! for second NN in 3G and 4G cases
        if(count_global_activation_elec.gt.0)then
            write(ounit,*)'Error: global_activation_electrostatic specified twice'
            stop !'
        endif
        call setglobalactivation(nelem,count_global_activation_elec,&
          maxnum_layers_elec,maxnodes_elec,nodes_elec,actfunc_elec,&
          actfunc_elec_dummy,keyword)
!!
      elseif((keyword.eq.'global_activation_pair').and.lshort.and.(nn_type_short.eq.2))then
        if(count_global_activation_short_pair.gt.0)then
          write(ounit,*)'Error: global_activation_pair specified twice'
          stop
        endif
        call setglobalactivation(npairs,count_global_activation_short_pair,&
          maxnum_layers_short_pair,maxnodes_short_pair,nodes_short_pair,actfunc_short_pair,&
          actfunc_short_pair_dummy,keyword)
!!
      elseif((keyword.eq.'global_activation_short').and.lshort.and.(nn_type_short.eq.3))then
        if(count_global_activation_short_atomic.gt.0)then
          write(ounit,*)'Error: global_activation_short specified twice'
          stop
        endif
        call setglobalactivation(nelem,count_global_activation_short_atomic,&
          maxnum_layers_short_atomic,maxnodes_short_atomic,nodes_short_atomic,actfunc_short_atomic,&
          actfunc_short_atomic_dummy,keyword)
      endif
      goto 50
 51   continue
      close(nnunit)
!!
!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!! get nuclear charges and sort arrays element and nucelem according to nuclear charge
!! this must be done before reading any element-specific input 
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!
      do i=1,nelem
        call nuccharge(element(i),nucelem(i))
      enddo
      call sortelements()
!! we have to check here if the elements specified in input.nn are the same as the ones
!! occuring in the data set. Every element in input.data must be in input.nn
!! WARNING: The following check will not work for mode 2, because in mode 2 the array
!! lelement is not determined and always false
      do i=1,102
        if(lelement(i)) then ! element with nuclear charge i is present in input.data
          do j=1,nelem
            if(i.eq.nucelem(j)) then
              goto 11
            endif
          enddo
          write(ounit,*)'Error: element ',i,' in input.data is not in input.nn'
          stop
        endif !'
 11     continue
      enddo
!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!! Read fixed charges if requested 
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      if(lelec.and.((nn_type_elec.eq.3).or.lusefixedcharge))then
        open(nnunit,file='input.nn',form='formatted',status='old')
        rewind(nnunit)
 52     continue
        read(nnunit,*,END=53) keyword 
        if(keyword.eq.'fixed_charge')then
          backspace(nnunit)
          read(nnunit,*,ERR=99)dummy,elementtemp,chargetemp
          call nuccharge(elementtemp,ztemp)
          fixedcharge(elementindex(ztemp))=chargetemp
        endif
        goto 52
 53     continue
        close(nnunit)
!! final check if all elements have been specified
        do i1=1,nelem
          if(fixedcharge(i1).gt.10.0d0)then
            write(ounit,*)'ERROR: No fixed charge specified for element ',element(i1)
            stop !'
          endif
        enddo
      endif 
!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!! Read fixed atomhardness if requested
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      if((lelec.and.(nn_type_elec.eq.5)).or.(lshort.and.(nn_type_short.eq.3)))then
        open(nnunit,file='input.nn',form='formatted',status='old')
        rewind(nnunit)
 54     continue
        read(nnunit,*,END=55) keyword
        if(keyword.eq.'initial_hardness')then
          backspace(nnunit)
          read(nnunit,*,ERR=99)dummy,elementtemp,hardnesstemp
          call nuccharge(elementtemp,ztemp)
          fixedhardness(elementindex(ztemp))=hardnesstemp
        endif
        goto 54
 55     continue
        close(nnunit)
!! final check if all elements have been specified
        do i1=1,nelem
          if(fixedhardness(i1).gt.50.0d0)then
            write(ounit,*)'ERROR: hardness is not defined or value is too high ',element(i1)
            stop !'
          endif
        enddo
      endif
!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!! Read fixed gaussian width of element if requested
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      if((lelec.and.(nn_type_elec.eq.5)).or.(lshort.and.(nn_type_short.eq.3)))then
        open(nnunit,file='input.nn',form='formatted',status='old')
        rewind(nnunit)
 56     continue
        read(nnunit,*,END=57) keyword
        if(keyword.eq.'fixed_gausswidth')then
          backspace(nnunit)
          read(nnunit,*,ERR=99)dummy,elementtemp,gausswidthtemp
          call nuccharge(elementtemp,ztemp)
          fixedgausswidth(elementindex(ztemp))=gausswidthtemp
        endif
        goto 56
 57     continue
        close(nnunit)
!! final check if all elements have been specified
        do i1=1,nelem
          if(fixedgausswidth(i1).gt.50.0d0)then
            write(ounit,*)'ERROR: fixed_gausswidth is not defined or value is too high ',element(i1)
            stop !'
          endif
        enddo
      endif
!!

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!! Get the unique element pairs for nn_type_short=2 
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      icount=0
      do i=1,nelem
        do j=i,nelem
          icount=icount+1
          elempair(icount,1)=nucelem(i)
          elempair(icount,2)=nucelem(j)
        enddo
      enddo
!!'
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!! Read element-specific numbers of layers 
!! this must be done after reading the global NN architecture and activation functions 
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!
!! read element-specific numbers of layers, at this point we already need to know all chemical species 
      open(nnunit,file='input.nn',form='formatted',status='old')
      rewind(nnunit)
 40   continue
      read(nnunit,*,END=42) keyword 
!!
      if(keyword.eq.'element_hidden_layers_short')then
        call readelementlayersatomic(maxnodes_short_atomic,&
          maxnum_layers_short_atomic,num_layers_short_atomic,nodes_short_atomic,actfunc_short_atomic)
!!
      elseif(keyword.eq.'element_hidden_layers_electrostatic')then
        call readelementlayersatomic(maxnodes_elec,&
          maxnum_layers_elec,num_layers_elec,nodes_elec,actfunc_elec)
!!
      elseif(keyword.eq.'element_hidden_layers_pair')then
        call readelementlayerspair(maxnodes_short_pair,&
          maxnum_layers_short_pair,num_layers_short_pair,nodes_short_pair,actfunc_short_pair)
!!
      endif
      goto 40
 42   continue
      close(nnunit)
!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!! Read element-specific numbers of nodes 
!! this must be done after reading the global NN architecture and activation functions 
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!
      open(nnunit,file='input.nn',form='formatted',status='old')
      rewind(nnunit)
 43   continue
      read(nnunit,*,END=44) keyword 
!!
      if(keyword.eq.'element_nodes_short')then
        backspace(nnunit)
        read(nnunit,*,ERR=99)dummy,elementtemp
        call checkelement(elementtemp)
        call nuccharge(elementtemp,ztemp)
        backspace(nnunit)
        read(nnunit,*,ERR=99)dummy,elementtemp,layer,node
        if(layer.eq.num_layers_short_atomic(elementindex(ztemp)))then
          write(ounit,*)'Error: do not modifiy the number of output nodes'
          stop !'
        endif
        if(node.gt.maxnodes_short_atomic)then
          write(ounit,*)'Error: too many nodes requested for element ',element(elementindex(ztemp))
          stop 
        endif
        nodes_short_atomic(layer,elementindex(ztemp))=node
!! delete all other activation functions
        do i1=nodes_short_atomic(layer,elementindex(ztemp))+1,maxnodes_short_atomic
          actfunc_short_atomic(i1,layer,elementindex(ztemp))=' '
        enddo
!!
      elseif(keyword.eq.'element_nodes_electrostatic')then
        backspace(nnunit)
        read(nnunit,*,ERR=99)dummy,elementtemp
        call checkelement(elementtemp)
        call nuccharge(elementtemp,ztemp)
        backspace(nnunit)
        read(nnunit,*,ERR=99)dummy,elementtemp,layer,node
        if(layer.eq.num_layers_elec(elementindex(ztemp)))then
          write(ounit,*)'Error: do not modifiy the number of output nodes'
          stop !'
        endif
        if(node.gt.maxnodes_elec)then
          write(ounit,*)'Error: too many nodes requested for element ',element(elementindex(ztemp))
          stop
        endif
        nodes_elec(layer,elementindex(ztemp))=node
!! delete all other activation functions
        do i1=nodes_elec(layer,elementindex(ztemp))+1,maxnodes_elec
          actfunc_elec(i1,layer,elementindex(ztemp))=' '
        enddo
!!
      elseif(keyword.eq.'element_nodes_pair')then
        backspace(nnunit)
        read(nnunit,*,ERR=99)dummy,elementtemp1,elementtemp2
        call checkelement(elementtemp1)
        call checkelement(elementtemp2)
        call nuccharge(elementtemp1,ztemp1)
        call nuccharge(elementtemp2,ztemp2)
        backspace(nnunit)
        read(nnunit,*,ERR=99)dummy,elementtemp1,elementtemp2,layer,node
        icount=0
        jcount=0
        do i1=1,nelem
          do i2=i1,nelem
            jcount=jcount+1
            if((ztemp1.eq.elempair(jcount,1)).and.(ztemp2.eq.elempair(jcount,2)))then
              icount=jcount
            elseif((ztemp2.eq.elempair(jcount,1)).and.(ztemp1.eq.elempair(jcount,2)))then
              icount=jcount
            endif
          enddo
        enddo
        if(layer.eq.num_layers_short_pair(icount))then
          write(ounit,*)'Error: do not modifiy the number of output nodes'
          stop !'
        endif
        if(node.gt.maxnodes_short_pair)then
          write(ounit,*)'Error: too many nodes requested for element pair ',&
            element(elementindex(elempair(icount,1))),element(elementindex(elempair(icount,2)))
          stop !'
        endif
        nodes_short_pair(layer,icount)=node
!! delete all other activation functions
        do i1=nodes_short_pair(layer,icount)+1,maxnodes_short_pair
          actfunc_short_pair(i1,layer,icount)=' '
        enddo
!!
      endif
      goto 43
 44   continue
      close(nnunit)
!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!! Read element-specific activation functions 
!! this must be done after reading the NN architecture  
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!
      open(nnunit,file='input.nn',form='formatted',status='old')
      rewind(nnunit)
 45   continue
      read(nnunit,*,END=46) keyword 
!!
      if(keyword.eq.'element_activation_short')then
        backspace(nnunit)
        read(nnunit,*,ERR=99)dummy,elementtemp
        call checkelement(elementtemp)
        call nuccharge(elementtemp,ztemp)
        backspace(nnunit)
        read(nnunit,*,ERR=99)dummy,elementtemp,layer,node,actfunc
        if(layer.gt.num_layers_short_atomic(elementindex(ztemp)))then
          write(ounit,*)'Error: layer is too large in ',keyword
          stop
        endif
        if(node.gt.nodes_short_atomic(layer,elementindex(ztemp)))then
          write(ounit,*)'Error: node is too large in ',keyword
          stop
        endif
        actfunc_short_atomic(node,layer,elementindex(ztemp))=actfunc 
!!
      elseif(keyword.eq.'element_activation_electrostatic')then
        backspace(nnunit)
        read(nnunit,*,ERR=99)dummy,elementtemp
        call checkelement(elementtemp)
        call nuccharge(elementtemp,ztemp)
        backspace(nnunit)
        read(nnunit,*,ERR=99)dummy,elementtemp,layer,node,actfunc
        if(layer.gt.num_layers_elec(elementindex(ztemp)))then
          write(ounit,*)'Error: layer is too large in ',keyword
          stop
        endif
        if(node.gt.nodes_elec(layer,elementindex(ztemp)))then
          write(ounit,*)'Error: node is too large in ',keyword
          stop
        endif
        actfunc_elec(node,layer,elementindex(ztemp))=actfunc
!!
      elseif(keyword.eq.'element_activation_pair')then
        backspace(nnunit)
        read(nnunit,*,ERR=99)dummy,elementtemp1,elementtemp2
        call checkelement(elementtemp1)
        call checkelement(elementtemp2)
        call nuccharge(elementtemp1,ztemp1)
        call nuccharge(elementtemp2,ztemp2)
        backspace(nnunit)
        read(nnunit,*,ERR=99)dummy,elementtemp1,elementtemp2,&
          layer,node,actfunc
        icount=0
        jcount=0
        do i1=1,nelem
          do i2=i1,nelem
            jcount=jcount+1
            if((ztemp1.eq.elempair(jcount,1)).and.(ztemp2.eq.elempair(jcount,2)))then
              icount=jcount
            elseif((ztemp2.eq.elempair(jcount,1)).and.(ztemp1.eq.elempair(jcount,2)))then
              icount=jcount
            endif
          enddo ! i2
        enddo ! i1
        if(layer.gt.num_layers_short_pair(icount))then
          write(ounit,*)'Error: layer is too large in ',keyword
          stop
        endif
        if(node.gt.nodes_short_pair(layer,icount))then
          write(ounit,*)'Error: node is too large in ',keyword
          stop
        endif
        actfunc_short_pair(node,layer,icount)=actfunc
!!
      endif
      goto 45
 46   continue
      close(nnunit)
!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!! Read element-specific symmetry functions 
!! this must be done after the determination of elementindex  
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!
      if(lshort.and.((nn_type_short.eq.1).or.(nn_type_short.eq.3)))then
        sym_short_atomic_count(:)=0
        num_funcvalues_short_atomic(:)=0
      endif
      if(lshort.and.(nn_type_short.eq.2))then
        sym_short_pair_count(:)=0
        num_funcvalues_short_pair(:)=0
      endif
      if((lelec.and.(nn_type_elec.eq.1)).or.(lelec.and.(nn_type_elec.eq.5)))then
        sym_elec_count(:)=0
        num_funcvalues_elec(:)=0
      endif
      if(lelec.and.lshort)then ! KK: for the first NN in 3G and 4G cases
        sym_elec_count(:)=0
        num_funcvalues_elec(:)=0
      endif
      open(nnunit,file='input.nn',form='formatted',status='old')
      rewind(nnunit)
 47   continue
      read(nnunit,*,END=48) keyword
!!
      if(keyword.eq.'symfunction_short')then
        if(lshort.and.((nn_type_short.eq.1).or.(nn_type_short.eq.3)))then
          call readsymfunctionatomic(keyword,&
            maxnum_funcvalues_short_atomic,sym_short_atomic_count,function_type_short_atomic,symelement_short_atomic,&
            funccutoff_short_atomic,eta_short_atomic,zeta_short_atomic,rshift_short_atomic,lambda_short_atomic)
        endif
      elseif(keyword.eq.'symfunction')then !!KK: for 3G and 4G cases
        if(lshort.and.lelec)then !! KK: read symfunctions for both NN in 3G and 4G case
          call readsymfunctionatomic(keyword,&
            maxnum_funcvalues_short_atomic,sym_short_atomic_count,function_type_short_atomic,symelement_short_atomic,&
            funccutoff_short_atomic,eta_short_atomic,zeta_short_atomic,rshift_short_atomic,lambda_short_atomic)
          call readsymfunctionatomic(keyword,&
            maxnum_funcvalues_elec,sym_elec_count,function_type_elec,symelement_elec,&
            funccutoff_elec,eta_elec,zeta_elec,rshift_elec,lambda_elec) 
        elseif((lelec.and.(.not.lusefixedcharge)).or.(lelec.and.(.not.luseatomcharges)))then
          call readsymfunctionatomic(keyword,&
            maxnum_funcvalues_elec,sym_elec_count,function_type_elec,symelement_elec,&
            funccutoff_elec,eta_elec,zeta_elec,rshift_elec,lambda_elec)  
        endif
            
!!
      elseif(keyword.eq.'element_symfunction_short')then
        if(lshort.and.((nn_type_short.eq.1).or.(nn_type_short.eq.3)))then
          call readsymfunctionelementatomic(keyword,&
            maxnum_funcvalues_short_atomic,sym_short_atomic_count,function_type_short_atomic,symelement_short_atomic,&
            funccutoff_short_atomic,eta_short_atomic,zeta_short_atomic,rshift_short_atomic,lambda_short_atomic)
        endif
!!
      elseif((keyword.eq.'global_symfunction_short').or.&
             (keyword.eq.'global_symfunction_short_atomic'))then
        if(lshort.and.(nn_type_short.eq.1).or.(nn_type_short.eq.3))then
          call readsymfunctionglobalatomic(keyword,&
            maxnum_funcvalues_short_atomic,sym_short_atomic_count,function_type_short_atomic,symelement_short_atomic,&
            funccutoff_short_atomic,eta_short_atomic,zeta_short_atomic,rshift_short_atomic,lambda_short_atomic)
        endif
!!
      elseif(keyword.eq.'symfunction_electrostatic')then
        if((lelec.and.(nn_type_elec.eq.1)).or.(lelec.and.(nn_type_elec.eq.5)))then
          call readsymfunctionatomic(keyword,&
            maxnum_funcvalues_elec,sym_elec_count,function_type_elec,symelement_elec,&
            funccutoff_elec,eta_elec,zeta_elec,rshift_elec,lambda_elec)
        endif
!!
      elseif(keyword.eq.'element_symfunction_electrostatic')then
        if((lelec.and.(nn_type_elec.eq.1)).or.(lelec.and.(nn_type_elec.eq.5)))then
          call readsymfunctionelementatomic(keyword,&
            maxnum_funcvalues_elec,sym_elec_count,function_type_elec,symelement_elec,&
            funccutoff_elec,eta_elec,zeta_elec,rshift_elec,lambda_elec)
        endif
!!
      elseif(keyword.eq.'global_symfunction_electrostatic')then
        if((lelec.and.(nn_type_elec.eq.1)).or.(lelec.and.(nn_type_elec.eq.5)))then
          call readsymfunctionglobalatomic(keyword,&
            maxnum_funcvalues_elec,sym_elec_count,function_type_elec,symelement_elec,&
            funccutoff_elec,eta_elec,zeta_elec,rshift_elec,lambda_elec)
        endif
!!
      elseif(keyword.eq.'global_pairsymfunction_short')then
        if(lshort.and.(nn_type_short.eq.2))then
          call readsymfunctionglobalpair(keyword,&
            maxnum_funcvalues_short_pair,sym_short_pair_count,function_type_short_pair,symelement_short_pair,&
            funccutoff_short_pair,eta_short_pair,zeta_short_pair,rshift_short_pair,lambda_short_pair)
        endif
!!
      elseif(keyword.eq.'element_pairsymfunction_short')then
        if(lshort.and.(nn_type_short.eq.2))then
          call readsymfunctionelementpair(keyword,&
            maxnum_funcvalues_short_pair,sym_short_pair_count,function_type_short_pair,symelement_short_pair,&
            funccutoff_short_pair,eta_short_pair,zeta_short_pair,rshift_short_pair,lambda_short_pair)
        endif
!!
      elseif(keyword.eq.'pairsymfunction_short')then
        if(lshort.and.(nn_type_short.eq.2))then
          call readsymfunctionpair(keyword,&
            maxnum_funcvalues_short_pair,sym_short_pair_count,function_type_short_pair,symelement_short_pair,&
            funccutoff_short_pair,eta_short_pair,zeta_short_pair,rshift_short_pair,lambda_short_pair)
        endif
!!
      endif ! keyword
      goto 47
 48   continue
      close(nnunit)

!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!! determination of the number of symmetry functions for each element and elementalpair
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      do i1=1,nelem
        if(lshort)then
          num_funcvalues_short_atomic(i1)=sym_short_atomic_count(i1)
          if(nn_type_short.eq.3)then
            nodes_short_atomic(0,i1)=num_funcvalues_short_atomic(i1)+1
          else
            nodes_short_atomic(0,i1)=num_funcvalues_short_atomic(i1)
          endif
        endif
        if((lelec.and.(nn_type_elec.eq.1)).or.(lelec.and.(nn_type_elec.eq.5)))then
          num_funcvalues_elec(i1)=sym_elec_count(i1)
          nodes_elec(0,i1)=num_funcvalues_elec(i1)
        endif
        if(lelec.and.lshort)then ! KK: for 3G and 4G cases
          num_funcvalues_elec(i1)=sym_elec_count(i1)
          nodes_elec(0,i1)=num_funcvalues_elec(i1)
        endif
      enddo
      do i1=1,npairs
        if(lshort.and.(nn_type_short.eq.2))then
          num_funcvalues_short_pair(i1)=sym_short_pair_count(i1)
          nodes_short_pair(0,i1)=num_funcvalues_short_pair(i1)
        endif
      enddo
!! do some checks
      if(lshort.and.((nn_type_short.eq.1).or.(nn_type_short.eq.3)))then
        do i1=1,nelem
          if(num_funcvalues_short_atomic(i1).eq.0)then
            write(ounit,*)'ERROR: No short range symfunctions specified for ',element(i1)
            stop !'
          endif
        enddo
      endif
      if((lelec.and.(nn_type_elec.eq.1)).or.(lelec.and.(nn_type_elec.eq.5)))then
        do i1=1,nelem
          if(num_funcvalues_elec(i1).eq.0)then
            write(ounit,*)'ERROR: No electrostatic symfunctions specified for ',element(i1)
            stop !'
          endif
        enddo
      endif
      if(lshort.and.(nn_type_short.eq.2))then
        icount=0
        do i1=1,nelem
          do i2=i1,nelem
            icount=icount+1
            if(num_funcvalues_short_pair(icount).eq.0)then
              write(ounit,*)'WARNING: No short range pair symfunctions specified for ',element(i1),element(i2)
!!              stop !'
            endif
          enddo
        enddo
      endif
!!
!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!! check for inconsistencies in the input.nn file
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!
      call checkinputnn()
!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!! write settings of input.nn/defaults to runner.out:
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!
      call printinputnn(iseed,ielem,&
       nodes_short_atomic_temp,nodes_elec_temp,nodes_short_pair_temp,&
       kalmanlambda_local,kalmanlambdae_local,& 
       actfunc_short_atomic_dummy,actfunc_elec_dummy,actfunc_short_pair_dummy)
!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!! Write the unique element pairs 
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      write(ounit,'(a15,i4,a30)')' Element pairs: ',npairs,' , shortest distance (Bohr)'
      icount=0 !'
      do i=1,nelem
        do j=i,nelem
          icount=icount+1
          if(dmin_element(icount).lt.9999.d0)then ! write the distance only if the pair has been found, JB 2019/07/22
            write(ounit,'(a6,i4,2a3,1x,f10.3)')' pair ',&
              icount,element(i),element(j),dmin_element(icount)
          endif
        enddo
      enddo
      write(ounit,*)'============================================================='
!!'
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!! calculate derived quantities 
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!
!! calculate number of weight parameters in short range NN type 1 or 3
      if(lshort.and.((nn_type_short.eq.1).or.(nn_type_short.eq.3)))then
        do i1=1,nelem
          wcount=0
          do i=1,num_layers_short_atomic(i1) ! loop over all hidden and output layers
            wcount=wcount+1
            windex_short_atomic(wcount,i1)=num_weights_short_atomic(i1)+1
            num_weights_short_atomic(i1)=num_weights_short_atomic(i1)&
              +nodes_short_atomic(i-1,i1)*nodes_short_atomic(i,i1)
            wcount=wcount+1
            windex_short_atomic(wcount,i1)=num_weights_short_atomic(i1)+1
            num_weights_short_atomic(i1)=num_weights_short_atomic(i1)&
              +nodes_short_atomic(i,i1) ! bias weights
          enddo
!! JB 2019/05/08: output changed for modes 1,2,3 
!            if(nn_type_short.eq.3)then ! KK: for 4G case
!              num_weights_elec(i1)=num_weights_short_atomic(i1)-nodes_short_atomic(1,i1)
!              num_layers_elec(i1)=num_layers_short_atomic(i1)
!              nodes_elec(:,i1)=nodes_short_atomic(:,i1)
!              nodes_elec(0,i1)=nodes_short_atomic(0,i1)-1
!              actfunc_elec(:,:,i1)=actfunc_short_atomic(:,:,i1)
!              windex_elec(:,i1)=windex_short_atomic(:,i1)-nodes_short_atomic(1,i1)
!              maxnodes_elec = maxnodes_short_atomic
!              maxnum_layers_elec=maxnum_layers_short_atomic
!              num_funcvalues_elec(i1)= num_funcvalues_short_atomic(i1)
!            endif
          if((mode.eq.2).or.(mode.eq.3))then
            if(nn_type_short.eq.3)then
              write(ounit,'(a,a3,i10)')' => short range NN weights type 3                ',&
                element(i1),num_weights_short_atomic(i1)
            else
              write(ounit,'(a,a3,i10)')' => short range NN weights type 1                ',&
                element(i1),num_weights_short_atomic(i1)
            endif
!            if(nn_type_short.eq.3)then
!              num_weights_elec(i1)=num_weights_short_atomic(i1)-nodes_short_atomic(1,i1)
!              num_layers_elec(i1)=num_layers_short_atomic(i1)
!              nodes_elec(:,i1)=nodes_short_atomic(:,i1)
!              nodes_elec(0,i1)=nodes_short_atomic(0,i1)-1
!              actfunc_elec(:,:,i1)=actfunc_short_atomic(:,:,i1)
!              windex_elec(:,i1)=windex_short_atomic(:,i1)-nodes_short_atomic(1,i1)
!              maxnodes_elec = maxnodes_short_atomic
!              maxnum_layers_elec=maxnum_layers_short_atomic
!              num_funcvalues_elec(i1)= num_funcvalues_short_atomic(i1)
            !endif
          endif
          maxnum_weights_short_atomic=max(maxnum_weights_short_atomic,num_weights_short_atomic(i1))
!          if(nn_type_short.eq.3)then
!            maxnum_weights_elec=maxnum_weights_short_atomic-nodes_short_atomic(1,i1)
!          endif
        enddo ! i1'
      endif ! lshort
!!
!! calculate number of weight parameters in electrostatic NN
      if((lelec.and.(nn_type_elec.eq.1)).or.(lelec.and.(nn_type_elec.eq.5)))then
        do i1=1,nelem
          wcount=0
          do i=1,num_layers_elec(i1)
            wcount=wcount+1
            windex_elec(wcount,i1)=num_weights_elec(i1)+1
            num_weights_elec(i1)=num_weights_elec(i1)+nodes_elec(i-1,i1)*nodes_elec(i,i1)
            wcount=wcount+1
            windex_elec(wcount,i1)=num_weights_elec(i1)+1
            num_weights_elec(i1)=num_weights_elec(i1)+nodes_elec(i,i1) ! bias weights
          enddo
          write(ounit,'(a,a3,i10)')' => electrostatic NN weights                     ',element(i1),num_weights_elec(i1)
          maxnum_weights_elec=max(maxnum_weights_elec,num_weights_elec(i1))
        enddo ! i1 '
        if(mode.eq.3)then ! KK: to make use of get_energies_and_forces.f90 
            actfunc_short_atomic(:,:,:)=actfunc_elec(:,:,:)
            maxnum_layers_short_atomic=maxnum_layers_elec
            num_funcvalues_short_atomic(:)= num_funcvalues_elec(:)
            maxnum_funcvalues_short_atomic = maxnum_funcvalues_elec
        endif
      endif ! 
!! 
!! calculate number of weight parameters in electrostatic NN in 3G and 4G
      if(lelec.and.lshort)then
        do i1=1,nelem
          wcount=0
          do i=1,num_layers_elec(i1)
            wcount=wcount+1
            windex_elec(wcount,i1)=num_weights_elec(i1)+1
            num_weights_elec(i1)=num_weights_elec(i1)+nodes_elec(i-1,i1)*nodes_elec(i,i1)
            wcount=wcount+1
            windex_elec(wcount,i1)=num_weights_elec(i1)+1
            num_weights_elec(i1)=num_weights_elec(i1)+nodes_elec(i,i1) ! bias weights
          enddo
          write(ounit,'(a,a3,i10)')' => electrostatic NN weights                     ',element(i1),num_weights_elec(i1)
          maxnum_weights_elec=max(maxnum_weights_elec,num_weights_elec(i1))
        enddo ! i1 '
      endif
!!
!! calculate number of weight parameters in short range NN type 2
      if(lshort.and.(nn_type_short.eq.2))then
        do i1=1,npairs
          wcount=0
          do i=1,num_layers_short_pair(i1) ! loop over all hidden and output layers
            wcount=wcount+1
            windex_short_pair(wcount,i1)=num_weights_short_pair(i1)+1
            num_weights_short_pair(i1)=num_weights_short_pair(i1)+nodes_short_pair(i-1,i1)*nodes_short_pair(i,i1)
            wcount=wcount+1
            windex_short_pair(wcount,i1)=num_weights_short_pair(i1)+1
            num_weights_short_pair(i1)=num_weights_short_pair(i1)+nodes_short_pair(i,i1) ! bias weights
          enddo
!! JB 2019/05/08: output changed for modes 1,2,3
          if((mode.eq.2).or.(mode.eq.3))then
            write(ounit,'(a,2a3,i10)')' => short range NN weights type 2                ',&
              element(elementindex(elempair(i1,1))),element(elementindex(elempair(i1,2))),num_weights_short_pair(i1)
          endif !'
          maxnum_weights_short_pair=max(maxnum_weights_short_pair,num_weights_short_pair(i1))
        enddo ! i1
      endif ! lshort
!!
      write(ounit,*)'-------------------------------------------------------------'
!!'
!! set dummy dimensions for unused arrays
      if((nn_type_short.eq.1).or.(nn_type_short.eq.3))then
        maxnum_weights_short_pair=1
      endif
      if(nn_type_short.eq.2)then
        maxnum_weights_short_atomic=1
      endif
      if(.not.lelec)then
          maxnum_weights_elec=1
      endif
!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!! read atom reference energies
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      if(lremoveatomenergies)then
        call readatomenergies() 
      endif

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!! read atom masses for frequency calculation (Emir)
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      if(ldohessian)then
        call readatommasses()
      endif

!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!! read vdw coefficients
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      if((lvdw).or.(lremovevdwenergies))then
        if(nn_type_vdw.eq.1)then
          call read_vdw_coefficients_type_1()
          call read_vdw_radius_type_1()
        elseif(nn_type_vdw.eq.2)then
          call read_vdw_coefficients_type_2()
          call read_vdw_radius_type_1()
        else
          write(ounit,*)'ERROR: unknown nn_type_vdw in readinput ',nn_type_vdw
          stop
        endif
      endif
!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!! read activation functions of individual nodes 
!! WHY IS THIS COMMENTED???
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!        open(nnunit,file='input.nn',form='formatted',status='old')
!!        rewind(nnunit)
!!        icount=0
!! 33     continue       
!!          read(nnunit,*,END=34)keyword
!!          if(keyword.eq.'node_activation_short')then
!!            backspace(nnunit)
!!            read(nnunit,*,ERR=99)dummy,elementtemp,layer,node
!!            call nuccharge(elementtemp,ztemp)
!!            if(layer.gt.num_layers_short_atomic(elementindex(ztemp)))then
!!              write(ounit,*)'Error: layer is too large in ',keyword
!!              stop
!!            endif
!!            if(node.gt.nodes_short_atomic(layer,elementindex(ztemp)))then
!!              write(ounit,*)'Error: node is too large in ',keyword
!!              stop
!!            endif
!!            backspace(nnunit)
!!            read(nnunit,*,ERR=99)dummy,element,layer,node,actfunc_short(node,layer,elementindex(ztemp))
!!          elseif(keyword.eq.'node_activation_electrostatic')then
!!            backspace(nnunit)
!!            read(nnunit,*,ERR=99)dummy,element,layer,node
!!            call nuccharge(elementtemp,ztemp)
!!            if(layer.gt.num_layersewald(elementindex(ztemp)))then
!!              write(ounit,*)'Error: layer is too large in ',keyword
!!              stop
!!            endif
!!            if(node.gt.nodes_ewald(layer,elementindex(ztemp)))then
!!              write(ounit,*)'Error: node is too large in ',keyword
!!              stop
!!            endif
!!            backspace(nnunit)
!!            read(nnunit,*,ERR=99)dummy,element,layer,node,actfunc_ewald(node,layer,elementindex(ztemp))
!!         endif
!!          goto 33
!! 34     continue      
!!        close(nnunit)
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!! print NN architectures 
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        if(lshort.and.((nn_type_short.eq.1).or.(nn_type_short.eq.3)).and.(mode.ne.1))then
          do i3=1,nelem
            write(ounit,*)'-------------------------------------------------'
            write(ounit,*)'Atomic short range NN for element: ',element(i3)
            write(ounit,'(a,10i5)')' architecture    ',(nodes_short_atomic(i1,i3),i1=0,num_layers_short_atomic(i3))
            write(ounit,*)'-------------------------------------------------'
            itemp=0
            do i1=0,num_layers_short_atomic(i3)
              itemp=max(itemp,nodes_short_atomic(i1,i3))
            enddo ! i1
            do i1=1,itemp ! loop over all lines with hidden nodes
              if(i1.le.nodes_short_atomic(0,i3))then ! still input node to be printed
                if(i1.le.maxnodes_short_atomic)then ! still hidden nodes present
                  write(ounit,'(i4,x,9a3)')i1,'  G',(actfunc_short_atomic(i1,i2,i3),i2=1,num_layers_short_atomic(i3))
                else
                  write(ounit,'(i4,x,a3)')i1,'  G'
                endif
              else ! no input node in front of hidden nodes
                write(ounit,'(i4,4x,8a3)')i1,(actfunc_short_atomic(i1,i2,i3),i2=1,num_layers_short_atomic(i3))
              endif
            enddo
          enddo ! i3
        endif
!!
        if(lshort.and.(nn_type_short.eq.2).and.(mode.ne.1))then
          do i3=1,npairs
            write(ounit,*)'-------------------------------------------------'
            write(ounit,'(a33,2a3)')' Pair short range NN for element: ',&
              element(elementindex(elempair(i3,1))),element(elementindex(elempair(i3,2)))
            write(ounit,'(a,10i5)')' architecture    ',(nodes_short_pair(i1,i3),i1=0,num_layers_short_pair(i3))
            write(ounit,*)'-------------------------------------------------'
            itemp=0
            do i1=0,num_layers_short_pair(i3)
              itemp=max(itemp,nodes_short_pair(i1,i3))
            enddo ! i1
            do i1=1,itemp ! loop over all lines with hidden nodes
              if(i1.le.nodes_short_pair(0,i3))then ! still input node to be printed
                if(i1.le.maxnodes_short_pair)then ! still hidden nodes present
                  write(ounit,'(i4,x,9a3)')i1,'  G',(actfunc_short_pair(i1,i2,i3),i2=1,num_layers_short_pair(i3))
                else
                  write(ounit,'(i4,x,a3)')i1,'  G'
                endif
              else ! no input node in front of hidden nodes
                write(ounit,'(i4,4x,8a3)')i1,(actfunc_short_pair(i1,i2,i3),i2=1,num_layers_short_pair(i3))
              endif
            enddo
          enddo ! i3
        endif
!!
        if((lelec.and.(nn_type_elec.eq.1).and.(mode.ne.1)).or.(lelec.and.(nn_type_elec.eq.5).and.(mode.ne.1)))then
          do i3=1,nelem
            write(ounit,*)'---------------------------------------------------'
            write(ounit,*)'Electrostatic NN for element: ',element(i3)
            write(ounit,'(a,10i5)')' architecture    ',(nodes_elec(i1,i3),i1=0,num_layers_elec(i3))
            write(ounit,*)'---------------------------------------------------'
            itemp=0
            do i1=0,num_layers_elec(i3)
              itemp=max(itemp,nodes_elec(i1,i3))
            enddo ! i1
            do i1=1,itemp ! loop over all lines with hidden nodes
              if(i1.le.nodes_elec(0,i3))then ! still input node to be printed
                if(i1.le.maxnodes_elec)then ! still hidden nodes present
                  write(ounit,'(i4,x,9a3)')i1,'  G',(actfunc_elec(i1,i2,i3),i2=1,num_layers_elec(i3))
                else
                  write(ounit,'(i4,x,a3)')i1,'  G'
                endif
              else ! no input node in front of hidden nodes
                write(ounit,'(i4,4x,8a3)')i1,(actfunc_elec(i1,i2,i3),i2=1,num_layers_elec(i3))
              endif
            enddo
          enddo ! i3
        endif
        if(lelec.and.lshort.and.(mode.ne.1))then
          do i3=1,nelem
            write(ounit,*)'---------------------------------------------------'
            write(ounit,*)'Electrostatic NN for element: ',element(i3)
            write(ounit,'(a,10i5)')' architecture    ',(nodes_elec(i1,i3),i1=0,num_layers_elec(i3))
            write(ounit,*)'---------------------------------------------------'
            itemp=0
            do i1=0,num_layers_elec(i3)
              itemp=max(itemp,nodes_elec(i1,i3))
            enddo ! i1
            do i1=1,itemp ! loop over all lines with hidden nodes
              if(i1.le.nodes_elec(0,i3))then ! still input node to be printed
                if(i1.le.maxnodes_elec)then ! still hidden nodes present
                  write(ounit,'(i4,x,9a3)')i1,'  G',(actfunc_elec(i1,i2,i3),i2=1,num_layers_elec(i3))
                else
                  write(ounit,'(i4,x,a3)')i1,'  G'
                endif
              else ! no input node in front of hidden nodes
                write(ounit,'(i4,4x,8a3)')i1,(actfunc_elec(i1,i2,i3),i2=1,num_layers_elec(i3))
              endif
            enddo
          enddo ! i3
        endif
      write(ounit,*)'-------------------------------------------------------------'
!!'
!!
!! write debugging print options
      write(debugunit,*)pstring
      if(pstring(1:1).eq.'1')then
        write(debugunit,*)'printing all short range weights'
      endif
      if(pstring(2:2).eq.'1')then
        write(debugunit,*)'printing all electrostatic weights'
      endif
      if(pstring(3:3).eq.'1')then
        write(debugunit,*)'printing all deshortdw derivatives'
      endif
      if(pstring(4:4).eq.'1')then
        write(debugunit,*)'printing all dfshortdw derivatives'
      endif

!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!! sort and analyze symmetry functions  
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!! sort short range atomic symmetry functions
      if(((nn_type_short.eq.1).or.(nn_type_short.eq.3)).and.lshort)then
        call sortsymfunctions(&
          maxnum_funcvalues_short_atomic,num_funcvalues_short_atomic,&
          function_type_short_atomic,symelement_short_atomic,&
          eta_short_atomic,zeta_short_atomic,rshift_short_atomic,&
          lambda_short_atomic,funccutoff_short_atomic)
      endif
!!
!! sort electrostatic symmetry functions
      if((lelec.and.(nn_type_elec.eq.1)).or.(lelec.and.(nn_type_elec.eq.5)))then
        call sortsymfunctions(&
          maxnum_funcvalues_elec,num_funcvalues_elec,&
          function_type_elec,symelement_elec,&
          eta_elec,zeta_elec,rshift_elec,lambda_elec,funccutoff_elec)
      endif
!!
!! sort short range pair symmetry functions
      if((nn_type_short.eq.2).and.lshort)then
        call sortpairsymfunctions(npairs,&
          maxnum_funcvalues_short_pair,num_funcvalues_short_pair,&
          function_type_short_pair,symelement_short_pair,&
          eta_short_pair,zeta_short_pair,rshift_short_pair,lambda_short_pair,funccutoff_short_pair,&
          elempair)
      endif
!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!! write symmetry functions to output 
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      if(lshort.and.((nn_type_short.eq.1).or.(nn_type_short.eq.3)))then
        do i1=1,nelem
          write(ounit,*)'-------------------------------------------------------------'
          write(ounit,*)' short range atomic symmetry &
                          &functions element ',element(i1),' :'
          write(ounit,*)'-------------------------------------------------------------'
          do i2=1,num_funcvalues_short_atomic(i1)
            if(function_type_short_atomic(i2,i1).eq.1)then
              write(ounit,'(i5,a3,i3,x,a3,3x,27x,f11.6)')&
                i2,element(i1),function_type_short_atomic(i2,i1),&
                element(elementindex(symelement_short_atomic(i2,1,i1))),&
                funccutoff_short_atomic(i2,i1)
            elseif(function_type_short_atomic(i2,i1).eq.2)then
              write(ounit,'(i5,a3,i3,x,a3,3x,11x,3f11.6)')&
                i2,element(i1),function_type_short_atomic(i2,i1),&
                element(elementindex(symelement_short_atomic(i2,1,i1))),&
                eta_short_atomic(i2,i1),rshift_short_atomic(i2,i1),funccutoff_short_atomic(i2,i1)
            elseif(function_type_short_atomic(i2,i1).eq.3)then
              write(ounit,'(i5,a3,i3,x,2a3,4f11.6)')&
                i2,element(i1),function_type_short_atomic(i2,i1),&
                element(elementindex(symelement_short_atomic(i2,1,i1))),&
                element(elementindex(symelement_short_atomic(i2,2,i1))),&
                eta_short_atomic(i2,i1),lambda_short_atomic(i2,i1),&
                zeta_short_atomic(i2,i1),funccutoff_short_atomic(i2,i1)
            elseif(function_type_short_atomic(i2,i1).eq.4)then
              write(ounit,'(i5,a3,i3,x,a3,3x,16x,2f11.6)')&
                i2,element(i1),function_type_short_atomic(i2,i1),&
                element(elementindex(symelement_short_atomic(i2,1,i1))),&
                eta_short_atomic(i2,i1),funccutoff_short_atomic(i2,i1)
            elseif(function_type_short_atomic(i2,i1).eq.5)then
              write(ounit,'(i5,a3,i3,4x,27x,f11.6)')&
                i2,element(i1),function_type_short_atomic(i2,i1),eta_short_atomic(i2,i1)
            elseif(function_type_short_atomic(i2,i1).eq.6)then
              write(ounit,'(i5,a3,i3,x,a3,3x,24x,f11.6)')&
                i2,element(i1),function_type_short_atomic(i2,i1),&
                element(elementindex(symelement_short_atomic(i2,1,i1))),&
                funccutoff_short_atomic(i2,i1)
            elseif(function_type_short_atomic(i2,i1).eq.8)then
              write(ounit,'(i5,a3,i3,x,2a3,4f11.6)')&
                i2,element(i1),function_type_short_atomic(i2,i1),&
                element(elementindex(symelement_short_atomic(i2,1,i1))),&
                element(elementindex(symelement_short_atomic(i2,2,i1))),&
                eta_short_atomic(i2,i1),rshift_short_atomic(i2,i1),&
                funccutoff_short_atomic(i2,i1)
            elseif(function_type_short_atomic(i2,i1).eq.9)then
              write(ounit,'(i5,a3,i3,x,2a3,4f11.6)')&
                i2,element(i1),function_type_short_atomic(i2,i1),&
                element(elementindex(symelement_short_atomic(i2,1,i1))),&
                element(elementindex(symelement_short_atomic(i2,2,i1))),&
                eta_short_atomic(i2,i1),lambda_short_atomic(i2,i1),&
                zeta_short_atomic(i2,i1),funccutoff_short_atomic(i2,i1)
            elseif(function_type_short_atomic(i2,i1).eq.21)then
              write(ounit,'(i5,a3,i3,x,a3,3x,11x,3f11.6)')&
                i2,element(i1),function_type_short_atomic(i2,i1),&
                element(elementindex(symelement_short_atomic(i2,1,i1))),&
                eta_short_atomic(i2,i1),rshift_short_atomic(i2,i1),funccutoff_short_atomic(i2,i1)
            elseif(function_type_short_atomic(i2,i1).eq.22)then
              write(ounit,'(i5,a3,i3,x,a3,3x,11x,3f11.6)')&
                i2,element(i1),function_type_short_atomic(i2,i1),&
                element(elementindex(symelement_short_atomic(i2,1,i1))),&
                eta_short_atomic(i2,i1),rshift_short_atomic(i2,i1),funccutoff_short_atomic(i2,i1)
            elseif(function_type_short_atomic(i2,i1).eq.23)then
              write(ounit,'(i5,a3,i3,x,a3,3x,11x,3f11.6)')&
                i2,element(i1),function_type_short_atomic(i2,i1),&
                element(elementindex(symelement_short_atomic(i2,1,i1))),&
                eta_short_atomic(i2,i1),rshift_short_atomic(i2,i1),funccutoff_short_atomic(i2,i1)
            elseif(function_type_short_atomic(i2,i1).eq.24)then
              write(ounit,'(i5,a3,i3,x,2a3,4f11.6)')&
                i2,element(i1),function_type_short_atomic(i2,i1),&
                element(elementindex(symelement_short_atomic(i2,1,i1))),&
                element(elementindex(symelement_short_atomic(i2,2,i1))),&
                eta_short_atomic(i2,i1),lambda_short_atomic(i2,i1),&
                zeta_short_atomic(i2,i1),funccutoff_short_atomic(i2,i1)
            elseif(function_type_short_atomic(i2,i1).eq.25)then
              write(ounit,'(i5,a3,i3,x,2a3,4f11.6)')&
                i2,element(i1),function_type_short_atomic(i2,i1),&
                element(elementindex(symelement_short_atomic(i2,1,i1))),&
                element(elementindex(symelement_short_atomic(i2,2,i1))),&
                eta_short_atomic(i2,i1),lambda_short_atomic(i2,i1),&
                zeta_short_atomic(i2,i1),funccutoff_short_atomic(i2,i1)
            elseif(function_type_short_atomic(i2,i1).eq.26)then
              write(ounit,'(i5,a3,i3,x,2a3,4f11.6)')&
                i2,element(i1),function_type_short_atomic(i2,i1),&
                element(elementindex(symelement_short_atomic(i2,1,i1))),&
                element(elementindex(symelement_short_atomic(i2,2,i1))),&
                eta_short_atomic(i2,i1),lambda_short_atomic(i2,i1),&
                zeta_short_atomic(i2,i1),funccutoff_short_atomic(i2,i1)
            elseif(function_type_short_atomic(i2,i1).eq.27)then
              write(ounit,'(i5,a3,i3,x,2a3,4f11.6)')&
                i2,element(i1),function_type_short_atomic(i2,i1),&
                element(elementindex(symelement_short_atomic(i2,1,i1))),&
                element(elementindex(symelement_short_atomic(i2,2,i1))),&
                eta_short_atomic(i2,i1),lambda_short_atomic(i2,i1),&
                zeta_short_atomic(i2,i1),funccutoff_short_atomic(i2,i1)
            elseif(function_type_short_atomic(i2,i1).eq.28)then
              write(ounit,'(i5,a3,i3,x,2a3,4f11.6)')&
                i2,element(i1),function_type_short_atomic(i2,i1),&
                element(elementindex(symelement_short_atomic(i2,1,i1))),&
                element(elementindex(symelement_short_atomic(i2,2,i1))),&
                eta_short_atomic(i2,i1),lambda_short_atomic(i2,i1),&
                zeta_short_atomic(i2,i1),funccutoff_short_atomic(i2,i1)
            elseif(function_type_short_atomic(i2,i1).eq.29)then
              write(ounit,'(i5,a3,i3,x,2a3,4f11.6)')&
                i2,element(i1),function_type_short_atomic(i2,i1),&
                element(elementindex(symelement_short_atomic(i2,1,i1))),&
                element(elementindex(symelement_short_atomic(i2,2,i1))),&
                eta_short_atomic(i2,i1),lambda_short_atomic(i2,i1),&
                zeta_short_atomic(i2,i1),funccutoff_short_atomic(i2,i1)
            elseif(function_type_short_atomic(i2,i1).eq.30)then
              write(ounit,'(i5,a3,i3,x,2a3,4f11.6)')&
                i2,element(i1),function_type_short_atomic(i2,i1),&
                element(elementindex(symelement_short_atomic(i2,1,i1))),&
                element(elementindex(symelement_short_atomic(i2,2,i1))),&
                eta_short_atomic(i2,i1),lambda_short_atomic(i2,i1),&
                zeta_short_atomic(i2,i1),funccutoff_short_atomic(i2,i1)
            elseif(function_type_short_atomic(i2,i1).eq.31)then
              write(ounit,'(i5,a3,i3,x,2a3,4f11.6)')&
                i2,element(i1),function_type_short_atomic(i2,i1),&
                element(elementindex(symelement_short_atomic(i2,1,i1))),&
                element(elementindex(symelement_short_atomic(i2,2,i1))),&
                eta_short_atomic(i2,i1),lambda_short_atomic(i2,i1),&
                zeta_short_atomic(i2,i1),funccutoff_short_atomic(i2,i1)
            else
              write(ounit,*)'Error: printing unknown symfunction in readinput '
              stop !'
            endif
          enddo ! i2
        enddo ! i1=1,nelem
      endif ! lshort
!!
      if(lshort.and.(nn_type_short.eq.2))then
        do i1=1,npairs
          write(ounit,*)'-------------------------------------------------------------'
          write(ounit,'(a47,2a3,a2)')' short range pair symmetry functions elements ',&
            element(elementindex(elempair(i1,1))),&
            &element(elementindex(elempair(i1,2))),' :'
          write(ounit,*)'-------------------------------------------------------------'
          do i2=1,num_funcvalues_short_pair(i1) !'
            if(function_type_short_pair(i2,i1).eq.1)then
              write(ounit,'(i5,2a3,i3,x,a3,3x,24x,f11.6)')&
                i2,element(elementindex(elempair(i1,1))),&
                element(elementindex(elempair(i1,2))),&
                function_type_short_pair(i2,i1),&
                element(elementindex(symelement_short_pair(i2,1,i1))),&
                funccutoff_short_pair(i2,i1)
            elseif(function_type_short_pair(i2,i1).eq.2)then
              write(ounit,'(i5,2a3,i3,x,6x,24x,f11.6)')&
                i2,element(elementindex(elempair(i1,1))),&
                element(elementindex(elempair(i1,2))),&
                function_type_short_pair(i2,i1),&
                funccutoff_short_pair(i2,i1)
            elseif(function_type_short_pair(i2,i1).eq.3)then  ! introduce by Jovan
              write(ounit,'(i5,2a3,i3,x,a3,f11.6,3x,f11.6,17x,f11.6)')&
                i2,element(elementindex(elempair(i1,1))),&
                element(elementindex(elempair(i1,2))),&
                function_type_short_pair(i2,i1),&
                element(elementindex(symelement_short_pair(i2,1,i1))),&
                eta_short_pair(i2,i1),rshift_short_pair(i2,i1),funccutoff_short_pair(i2,i1)
            elseif(function_type_short_pair(i2,i1).eq.4)then  ! introduce by Jovan
              write(ounit,'(i5,2a3,i3,x,a3,f11.6,3x,f11.6,4x,f11.6,8x,f11.6)')&
                i2,element(elementindex(elempair(i1,1))),& !'
                element(elementindex(elempair(i1,2))),&
                function_type_short_pair(i2,i1),&
                element(elementindex(symelement_short_pair(i2,1,i1))),&
                eta_short_pair(i2,i1),lambda_short_pair(i2,i1),&
                zeta_short_pair(i2,i1),funccutoff_short_pair(i2,i1)
            elseif(function_type_short_pair(i2,i1).eq.5)then  ! introduced by Jovan
              write(ounit,'(i5,2a3,i3,x,6x,24x,f11.6)')&
                i2,element(elementindex(elempair(i1,1))),&
                element(elementindex(elempair(i1,2))),&
                function_type_short_pair(i2,i1),&
                funccutoff_short_pair(i2,i1)
            elseif(function_type_short_pair(i2,i1).eq.6)then  ! introduced by Jovan
              write(ounit,'(i5,2a3,i3,4x,f11.6,3x,f11.6,17x,f11.6)')&
                i2,element(elementindex(elempair(i1,1))),&
                element(elementindex(elempair(i1,2))),&
                function_type_short_pair(i2,i1),eta_short_pair(i2,i1),rshift_short_pair(i2,i1),&
                funccutoff_short_pair(i2,i1)
            endif
          enddo ! i2
        enddo ! i1=1,npairs
      endif ! lshort
!!
      if((lelec.and.(nn_type_elec.eq.1)).or.(lelec.and.(nn_type_elec.eq.5)))then
        do i1=1,nelem
          write(ounit,*)'-------------------------------------------------------------'
          write(ounit,*)' electrostatic symmetry functions element ',element(i1),' :'
          write(ounit,*)'-------------------------------------------------------------'
          do i2=1,num_funcvalues_elec(i1)
            if(function_type_elec(i2,i1).eq.1)then
              write(ounit,'(i5,a3,i3,x,a3,3x,24x,f11.6)')&
                i2,element(i1),function_type_elec(i2,i1),&
                element(elementindex(symelement_elec(i2,1,i1))),&
                funccutoff_elec(i2,i1)
            elseif(function_type_elec(i2,i1).eq.2)then
              write(ounit,'(i5,a3,i3,x,a3,3x,8x,3f11.6)')&
                i2,element(i1),function_type_elec(i2,i1),&
                element(elementindex(symelement_elec(i2,1,i1))),&
                eta_elec(i2,i1),rshift_elec(i2,i1),funccutoff_elec(i2,i1)
            elseif(function_type_elec(i2,i1).eq.3)then
              write(ounit,'(i5,a3,i3,x,2a3,4f11.6)')&
                i2,element(i1),function_type_elec(i2,i1),&
                element(elementindex(symelement_elec(i2,1,i1))),&
                element(elementindex(symelement_elec(i2,2,i1))),&
                eta_elec(i2,i1),lambda_elec(i2,i1),&
                zeta_elec(i2,i1),funccutoff_elec(i2,i1)
            elseif(function_type_elec(i2,i1).eq.4)then
              write(ounit,'(i5,a3,i3,x,a3,3x,16x,2f11.6)')&
                i2,element(i1),function_type_elec(i2,i1),&
                element(elementindex(symelement_elec(i2,1,i1))),&
                eta_elec(i2,i1),funccutoff_elec(i2,i1)
            elseif(function_type_elec(i2,i1).eq.5)then
              write(ounit,'(i5,a3,i3,4x,27x,f11.6)')&
                i2,element(i1),function_type_elec(i2,i1),eta_elec(i2,i1)
            elseif(function_type_elec(i2,i1).eq.6)then
              write(ounit,'(i5,a3,i3,x,a3,3x,24x,f11.6)')&
                i2,element(i1),function_type_elec(i2,i1),&
                element(elementindex(symelement_elec(i2,1,i1))),&
                funccutoff_elec(i2,i1)
            elseif(function_type_elec(i2,i1).eq.8)then
              write(ounit,'(i5,a3,i3,x,2a3,4f11.6)')&
                i2,element(i1),function_type_elec(i2,i1),&
                element(elementindex(symelement_elec(i2,1,i1))),&
                element(elementindex(symelement_elec(i2,2,i1))),&
                eta_elec(i2,i1),rshift_elec(i2,i1),&
                funccutoff_elec(i2,i1)
            elseif(function_type_elec(i2,i1).eq.9)then
              write(ounit,'(i5,a3,i3,x,2a3,4f11.6)')&
                i2,element(i1),function_type_elec(i2,i1),&
                element(elementindex(symelement_elec(i2,1,i1))),&
                element(elementindex(symelement_elec(i2,2,i1))),&
                eta_elec(i2,i1),lambda_elec(i2,i1),&
                zeta_elec(i2,i1),funccutoff_elec(i2,i1)
            elseif(function_type_elec(i2,i1).eq.21)then
              write(ounit,'(i5,a3,i3,x,a3,3x,8x,3f11.6)')&
                i2,element(i1),function_type_elec(i2,i1),&
                element(elementindex(symelement_elec(i2,1,i1))),&
                eta_elec(i2,i1),rshift_elec(i2,i1),funccutoff_elec(i2,i1)
            elseif(function_type_elec(i2,i1).eq.22)then
              write(ounit,'(i5,a3,i3,x,a3,3x,8x,3f11.6)')&
                i2,element(i1),function_type_elec(i2,i1),&
                element(elementindex(symelement_elec(i2,1,i1))),&
                eta_elec(i2,i1),rshift_elec(i2,i1),funccutoff_elec(i2,i1)
            elseif(function_type_elec(i2,i1).eq.23)then
              write(ounit,'(i5,a3,i3,x,a3,3x,8x,3f11.6)')&
                i2,element(i1),function_type_elec(i2,i1),&
                element(elementindex(symelement_elec(i2,1,i1))),&
                eta_elec(i2,i1),rshift_elec(i2,i1),funccutoff_elec(i2,i1)
            elseif(function_type_elec(i2,i1).eq.24)then
              write(ounit,'(i5,a3,i3,x,2a3,4f11.6)')&
                i2,element(i1),function_type_elec(i2,i1),&
                element(elementindex(symelement_elec(i2,1,i1))),&
                element(elementindex(symelement_elec(i2,2,i1))),&
                eta_elec(i2,i1),lambda_elec(i2,i1),&
                zeta_elec(i2,i1),funccutoff_elec(i2,i1)
            elseif(function_type_elec(i2,i1).eq.25)then
              write(ounit,'(i5,a3,i3,x,2a3,4f11.6)')&
                i2,element(i1),function_type_elec(i2,i1),&
                element(elementindex(symelement_elec(i2,1,i1))),&
                element(elementindex(symelement_elec(i2,2,i1))),&
                eta_elec(i2,i1),lambda_elec(i2,i1),&
                zeta_elec(i2,i1),funccutoff_elec(i2,i1)
            elseif(function_type_elec(i2,i1).eq.26)then
              write(ounit,'(i5,a3,i3,x,2a3,4f11.6)')&
                i2,element(i1),function_type_elec(i2,i1),&
                element(elementindex(symelement_elec(i2,1,i1))),&
                element(elementindex(symelement_elec(i2,2,i1))),&
                eta_elec(i2,i1),lambda_elec(i2,i1),&
                zeta_elec(i2,i1),funccutoff_elec(i2,i1)
            elseif(function_type_elec(i2,i1).eq.27)then
              write(ounit,'(i5,a3,i3,x,2a3,4f11.6)')&
                i2,element(i1),function_type_elec(i2,i1),&
                element(elementindex(symelement_elec(i2,1,i1))),&
                element(elementindex(symelement_elec(i2,2,i1))),&
                eta_elec(i2,i1),lambda_elec(i2,i1),&
                zeta_elec(i2,i1),funccutoff_elec(i2,i1)
            elseif(function_type_elec(i2,i1).eq.28)then
              write(ounit,'(i5,a3,i3,x,2a3,4f11.6)')&
                i2,element(i1),function_type_elec(i2,i1),&
                element(elementindex(symelement_elec(i2,1,i1))),&
                element(elementindex(symelement_elec(i2,2,i1))),&
                eta_elec(i2,i1),lambda_elec(i2,i1),&
                zeta_elec(i2,i1),funccutoff_elec(i2,i1)
            elseif(function_type_elec(i2,i1).eq.29)then
              write(ounit,'(i5,a3,i3,x,2a3,4f11.6)')&
                i2,element(i1),function_type_elec(i2,i1),&
                element(elementindex(symelement_elec(i2,1,i1))),&
                element(elementindex(symelement_elec(i2,2,i1))),&
                eta_elec(i2,i1),lambda_elec(i2,i1),&
                zeta_elec(i2,i1),funccutoff_elec(i2,i1)
            elseif(function_type_elec(i2,i1).eq.30)then
              write(ounit,'(i5,a3,i3,x,2a3,4f11.6)')&
                i2,element(i1),function_type_elec(i2,i1),&
                element(elementindex(symelement_elec(i2,1,i1))),&
                element(elementindex(symelement_elec(i2,2,i1))),&
                eta_elec(i2,i1),lambda_elec(i2,i1),&
                zeta_elec(i2,i1),funccutoff_elec(i2,i1)
            elseif(function_type_elec(i2,i1).eq.31)then
              write(ounit,'(i5,a3,i3,x,2a3,4f11.6)')&
                i2,element(i1),function_type_elec(i2,i1),&
                element(elementindex(symelement_elec(i2,1,i1))),&
                element(elementindex(symelement_elec(i2,2,i1))),&
                eta_elec(i2,i1),lambda_elec(i2,i1),&
                zeta_elec(i2,i1),funccutoff_elec(i2,i1)
            else
              write(ounit,*)'Error: printing unknown symfunctione in readinput '
              stop !'
            endif
          enddo ! i2
        enddo ! i1=1,nelem
      endif ! lelec
      write(ounit,*)'-------------------------------------------------------------'
!!'
      return
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!
 99   continue
      write(ounit,*)'Error: keyword ',keyword
      write(ounit,*)'is missing arguments '
      stop

      end
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!
