!######################################################################
! This routine is part of
! RuNNer - RuNNer Neural Network Energy Representation
! (c) 2008-2024 Prof. Dr. Joerg Behler
! Ruhr-Universitaet Bochum, Germany
!
! This program is free software: you can redistribute it and/or modify it 
! under the terms of the GNU General Public License as published by the 
! Free Software Foundation, either version 3 of the License, or 
! (at your option) any later version.
!
! This program is distributed in the hope that it will be useful, but 
! WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
! or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
! for more details.
!
! You should have received a copy of the GNU General Public License along 
! with this program. If not, see http://www.gnu.org/licenses.
! WARNING: THIS FILE IS PART OF THE UNMAINTAINED CONDA PACKAGE OF RUNNER 1.3. 
!######################################################################
!! called by: 
!! - main.f90
!!
      subroutine predict()
!!
      use mpi_mod
      use fileunits
      use predictionoptions 
      use nnflags 
      use globaloptions
      use symfunctions 
      use timings
      use nnewald
      use nnshort_atomic
      use nnshort_pair
      use structures
      use energyAndForces3G
      use energyAndForces4G
!!
      implicit none
!!
      integer zelem(max_num_atoms)                                      ! internal
      integer num_atoms                                                 ! internal
      integer num_pairs                                                 ! internal
      integer num_atoms_element(nelem)                                  ! internal 
      integer i1,i2,i3,i4,i5,i6,i7                                      ! internal
      integer counter                                                   ! internal
      integer pairs_charge(2,max_num_pairs)                             ! internal
!!                                                                      
      real*8 lattice(3,3)                                               ! internal
      real*8 xyzstruct(3,max_num_atoms)                                 ! internal
      real*8 atomspin(max_num_atoms)                                    ! internal
      real*8 minvalue_short_atomic(nelem,maxnum_funcvalues_short_atomic) ! internal
      real*8 maxvalue_short_atomic(nelem,maxnum_funcvalues_short_atomic) ! internal
      real*8 avvalue_short_atomic(nelem,maxnum_funcvalues_short_atomic)  ! internal
      real*8 minvalue_elec(nelem,maxnum_funcvalues_elec)                ! internal
      real*8 maxvalue_elec(nelem,maxnum_funcvalues_elec)                ! internal
      real*8 avvalue_elec(nelem,maxnum_funcvalues_elec)                 ! internal
      real*8 minvalue_short_pair(npairs,maxnum_funcvalues_short_pair)   ! internal
      real*8 maxvalue_short_pair(npairs,maxnum_funcvalues_short_pair)   ! internal
      real*8 avvalue_short_pair(npairs,maxnum_funcvalues_short_pair)    ! internal
      real*8 minvalue_charge(nelem)                                     ! internal
      real*8 maxvalue_charge(nelem)                                     ! internal
      real*8 avvalue_charge(nelem)                                      ! internal
      real*8, dimension(:,:)  , allocatable :: sens                     ! internal
      real*8, dimension(:,:)  , allocatable :: sense                    ! internal
      real*8 eshort                                                     ! internal
      real*8 volume                                                     ! internal
!! DFT data (not necessarily provided in prediction mode)               
      real*8 totalcharge                                                ! internal
      real*8 totalenergy                                                ! internal
      real*8 totalforce(3,max_num_atoms)                                ! internal
      real*8 atomcharge(max_num_atoms)                                  ! internal
      real*8 atomenergy(max_num_atoms)                                  ! internal
!! NN data                                                              
      real*8 nntotalenergy                                              ! internal
      real*8 nnshortenergy                                              ! internal
      real*8 nntotalcharge                                              ! internal
      real*8 nnshortforce(3,max_num_atoms)                              ! internal
      real*8 nntotalforce(3,max_num_atoms)                              ! internal
      real*8 nnatomcharge(max_num_atoms)                                ! internal
      real*8 nnatomenergy(max_num_atoms)                                ! internal
      real*8 atomhardness(max_num_atoms)                                ! internal
      real*8 nnelecforce(3,max_num_atoms)                               ! internal
      real*8 nnpairenergy(max_num_pairs)                                ! internal
      real*8 nnstress(3,3)                                              ! internal
      real*8 nnstress_short(3,3)                                        ! internal 
      real*8 nnstress_elec(3,3)                                         ! internal
      real*8 nnelecenergy                                               ! internal
      real*8 atomenergysum                                              ! internal
      real*8 eshortmin                                                  ! internal
      real*8 eshortmax                                                  ! internal
                                                                        
      real*8 chargemin(nelem)                                           ! internal
      real*8 chargemax(nelem)                                           ! internal
      real*8 pressure                                                   ! internal
      real*8 forcesum(3)                                                ! internal
      real*8 au2gpa                                                     ! internal
!! vdw data                                                             
      real*8, dimension(:   ), allocatable :: atomvdw                   ! in
      real*8, dimension(:, :), allocatable :: vdwforces                 
      real*8, dimension(:, :), allocatable :: vdwstress                 
      real*8 vdwenergy                                                  ! in
!!Niko
      real*8, dimension(output_max_prop, max_num_atoms) :: output_arr
      character*12, dimension(output_max_prop) :: output_beginline
!!Niko
      ! testing
      ! Set this to true to print finite differences to test if force calculation is correct
      logical, parameter :: testFD = .true.
      real*8 :: xyz1(3,max_num_atoms), xyz2(3,max_num_atoms), dx(3,max_num_atoms), e2, e1, ee1, ee2
      real*8 :: f1(3,max_num_atoms), f2(3,max_num_atoms), fe1(3,max_num_atoms), fe2(3,max_num_atoms)

!!
      character*2 elementsymbol(max_num_atoms)                          ! internal
!!
      logical lperiodic                                                 ! internal
      logical lspin                                                     ! internal
!!
      call zerotime(daymode3,timemode3start,timemode3end) 
      call abstime(timemode3start,daymode3)
!!
!!=====================================================================
!! initializations 
!!=====================================================================
      if((mpirank.eq.0).and.(.not.lmd))then
        write(ounit,*)'Prediction Mode for all configurations'
      endif
      timeio                     = 0.0d0
      timeoutput                 = 0.0d0
      au2gpa=29419.844d0 ! 1 Ha/Bohr3 = 29419.844 GPa

!! we want to do the for loop from 1 to totnum_structures
      do i4 = 1,totnum_structures
        eshort            = 0.0d0
        nnshortenergy     = 0.0d0
        nntotalenergy     = 0.0d0
        nnelecenergy      = 0.0d0
        vdwenergy         = 0.0d0
        nnatomenergy(:)   = 0.0d0
        nntotalforce(:,:) = 0.0d0
        nnshortforce(:,:) = 0.0d0
        nnelecforce(:,:)  = 0.0d0
        nntotalcharge     = 0.0d0
        atomenergysum     = 0.0d0
        nnstress_short(:,:)=0.0d0
        nnstress_elec(:,:) =0.0d0
        nnatomcharge(:)    = 0.0d0
        totalcharge        = 0.0d0
        output_arr         = 0.0d0 !Niko
        output_beginline   = '   ' !Niko

        ! extrapolation counter for each structure
        count_extrapolation_warnings_energy  = 0
        count_extrapolation_warnings_symfunc = 0
        count_extrapolation_warnings_charges = 0
!!
        if((lshort.and.(nn_type_short.eq.1)).or.(lshort.and.(nn_type_short.eq.3)))then
          allocate(sens(nelem,maxnum_funcvalues_short_atomic))
        elseif(lshort.and.(nn_type_short.eq.2))then
          allocate(sens(npairs,maxnum_funcvalues_short_pair))
        endif
!! we need to allocate sense also for nn_type_elec 3 and 4 because of subroutine call below
        if(lelec.and.(nn_type_elec.eq.1).or.(nn_type_elec.eq.3).or.(nn_type_elec.eq.4).or.(nn_type_elec.eq.5))then
          allocate(sense(nelem,maxnum_funcvalues_elec))
        endif
        if(lvdw.and.((nn_type_vdw.eq.1).or.nn_type_vdw.eq.2))then
            allocate(atomvdw(max_num_atoms))
            allocate(vdwforces(3, max_num_atoms))
            allocate(vdwstress(3, 3))
            atomvdw(:)        = 0.0d0
            vdwforces(:, :)   = 0.0d0
            vdwstress(:, :)   = 0.0d0
        endif !lvdw
!!
        if(ldebug.and.(mpisize.gt.1).and.(.not.lmd))then
          ldebug=.false.
          write(ounit,*)'### WARNING ### ldebug is switched off for parallel case'
        endif !'
!!
!!=====================================================================
!! timining initialization of mode 3 
!!=====================================================================
        if(lfinetime)then
          dayio=0
          call abstime(timeiostart,dayio)
        endif ! lfinetime
!!
!!=====================================================================
!! read structure and derive all structure related data, also mpi distribution 
!!=====================================================================
       call getstructure_mode3(i4,num_atoms,num_pairs,zelem,&
          num_atoms_element,lattice,xyzstruct,atomspin,&
          totalenergy,totalcharge,totalforce,atomenergy,atomcharge,&
          elementsymbol,lperiodic,lspin)
!!
!!=====================================================================
!! read and distribute scaling data and weights, determine num_pairs if needed 
!!=====================================================================
        !IMPORTANT: this has to be called in every iteration or things break.:
        call initmode3(i4,&
          minvalue_short_atomic,maxvalue_short_atomic,avvalue_short_atomic,&
          minvalue_short_pair,maxvalue_short_pair,avvalue_short_pair,&
          minvalue_elec,maxvalue_elec,avvalue_elec,&
          minvalue_charge,maxvalue_charge,avvalue_charge,&
          eshortmin,eshortmax,chargemin,chargemax)

        if(ldohessian)then
            allocate(nnhessian(3*num_atoms,3*num_atoms))
            allocate(nnfrequencies(3*num_atoms))
            allocate(nnnormalmodes(3*num_atoms,3*num_atoms))
            nnhessian(:,:) = 0.d0
            nnfrequencies(:) = 0.d0
            nnnormalmodes(:,:) = 0.d0
        end if

!!
!!=====================================================================
!! timining initialization of mode 3 
!!=====================================================================
        if(lfinetime)then
          call abstime(timeioend,dayio)
          timeio=timeioend-timeiostart
        endif ! lfinetime
!!
!!=====================================================================
!!=====================================================================
!! all input reading is done now
!! now predict the energies and forces etc.
!! GOAL: These subroutines should be without any IO, so that they can be
!! coupled with MD codes
!!=====================================================================
!!=====================================================================
        if(lelec)then
            if(lshort) then
                select case (nn_type_short)
                    case (1) ! 3GHDNNP
!                        if (testFD) then
!                            xyz1 = xyzstruct
!                            call random_number(dx)
!                            dx = dx - 0.5d0
!                            dx = dx * 1.d-5
!                            xyz2 = xyz1 + dx
!                            call get_energies_and_forces_for_3G( &
!                                    num_atoms, zelem, lattice, &
!                                    xyz1, atomspin, totalcharge, lperiodic, &
!                                    minvalue_short_atomic, maxvalue_short_atomic, avvalue_short_atomic, &
!                                    nnatomcharge, e1, f1 , enforcetotcharge, totalcharge, ldoforces,&
!                                    ee1,fe1)
!                            call get_energies_and_forces_for_3G( &
!                                    num_atoms, zelem, lattice, &
!                                    xyz2, atomspin, totalcharge, lperiodic, &
!                                    minvalue_short_atomic, maxvalue_short_atomic, avvalue_short_atomic, &
!                                    nnatomcharge, e2, f2 , enforcetotcharge, totalcharge, ldoforces,&
!                                    ee2, fe2)
!                            print*, 'FD nat', num_atoms
!                            print*, 'FD t', e2-e1, -1.d0 * sum((f1(:,:num_atoms)+f2(:,:num_atoms)) / 2.d0 * dx(:,:num_atoms)), &
!                               1.d0 - (e2-e1) / (-1.d0 * sum((f1(:,:num_atoms)+f2(:,:num_atoms)) / 2.d0 * dx(:,:num_atoms)))
!                            print*, 'FD e', ee2-ee1, -1.d0 * sum((fe1(:,:num_atoms)+fe2(:,:num_atoms)) / 2.d0 * dx(:,:num_atoms)), &
!                               1.d0 - (ee2-ee1) / (-1.d0 * sum((fe1(:,:num_atoms)+fe2(:,:num_atoms)) / 2.d0 * dx(:,:num_atoms)))
!                            e1 = e1 - ee1
!                            e2 = e2 - ee2
!                            f1 = f1 - fe1
!                            f2 = f2 - fe2
!                            print*, 'FD s', e2-e1, -1.d0 * sum((f1(:,:num_atoms)+f2(:,:num_atoms)) / 2.d0 * dx(:,:num_atoms)), &
!                               1.d0 - (e2-e1) / (-1.d0 * sum((f1(:,:num_atoms)+f2(:,:num_atoms)) / 2.d0 * dx(:,:num_atoms)))
!                        endif
                        call get_energies_and_forces_for_3G( &
                            num_atoms, zelem, lattice, &
                            xyzstruct, atomspin, totalcharge, lperiodic, &
                            minvalue_short_atomic, maxvalue_short_atomic, avvalue_short_atomic, &
                            nnatomcharge, nntotalenergy, nntotalforce , enforcetotcharge, totalcharge, ldoforces,&
                            nnelecenergy, nnelecforce, nnatomenergy)
                        nnshortenergy = nntotalenergy - nnelecenergy
                        nnshortforce = nntotalforce - nnelecforce
                        !print*, 'Q', nnatomcharge
                    case (3) ! 4GHDNNP
                        call get_energies_and_forces( &
                            num_atoms, zelem, lattice, &
                            xyzstruct, atomspin, totalcharge, lperiodic, &
                            minvalue_short_atomic, maxvalue_short_atomic, avvalue_short_atomic, &
                            nnatomcharge, nntotalenergy, nntotalforce, &
                            nnelecenergy, nnelecforce, nnatomenergy, nnstress_short)
                        if (ldostress) then
                            nnstress_elec = 0._dp ! JF: why split it up into 2? If anyone is interested in the electrostatic stress, its available in get_energies_and_forces.
                            call getvolume(lattice, volume)
                            nnstress_short = nnstress_short * volume ! because stress here is not stress!?! see division by volume below
                        end if
                        nnshortenergy = nntotalenergy - nnelecenergy
                        nnshortforce = nntotalforce - nnelecforce
                end select
            elseif(nn_type_elec.le.5)then !! KK: nn_type_elec < 5 will all go to get_energies_and_forces_for_3G
                ! todo: do these things work?
                if(nn_type_elec.eq.5)then
                  call get_energies_and_forces( &
                      num_atoms, zelem, lattice, &
                      xyzstruct, atomspin, totalcharge, lperiodic, &
                      minvalue_elec, maxvalue_elec, avvalue_elec, &
                      nnatomcharge, nntotalenergy, nntotalforce, &
                      nnelecenergy, nnelecforce, nnatomenergy, nnstress_short)
                      if (ldostress) then
                          nnstress_elec = 0._dp ! JF: why split it up into 2? If anyone is interested in the electrostatic stress, its available in get_energies_and_forces.
                          call getvolume(lattice, volume)
                          nnstress_short = nnstress_short * volume ! because stress here is not stress!?! see division by volume below
                      end if
                      nnshortenergy = 0.0d0 
                      nnshortforce(:,:)=0.0d0
                else
                  !! KK: for the prediction mode of the electrostatic part and HDNNS
                  call get_energies_and_forces_for_3G( &
                    num_atoms, zelem, lattice, &
                    xyzstruct, atomspin, totalcharge, lperiodic, &
                    minvalue_elec, maxvalue_elec, avvalue_elec, &
                    nnatomcharge, nntotalenergy, nntotalforce , enforcetotcharge, totalcharge, ldoforces,&
                    nnelecenergy, nnelecforce, nnatomenergy)
                endif 
                ! todo: can we have lelec but not lshort?
            else
                write(ounit,*)'ERROR: unknown nn_type_elec in predict.f90', nn_type_elec
                stop !'
            endif
            nntotalcharge = sum(nnatomcharge(:num_atoms))
        else
            if (lshort) then
                nntotalcharge = 0.d0
                select case (nn_type_short)
                    case (1) ! 2G
                        call predictionshortatomic(&
                            num_atoms,num_atoms_element,zelem,&
                            lattice,xyzstruct,atomspin,&
                            minvalue_short_atomic,maxvalue_short_atomic,avvalue_short_atomic,&
                            eshortmin,eshortmax,&
                            nntotalenergy,nnshortforce,&
                            nnatomenergy,nnshortenergy,nnstress_short,&
                            atomenergysum,sens,lperiodic)
                    case (2)
                        call predictionshortpair(&
                            num_atoms,num_atoms_element,zelem,&
                            lattice,xyzstruct,&
                            minvalue_short_pair,maxvalue_short_pair,avvalue_short_pair,&
                            eshortmin,eshortmax,&
                            nntotalenergy,nnshortforce,&
                            nnatomenergy,nnpairenergy,nnshortenergy,&
                            nnstress_short,pairs_charge,&
                            atomenergysum,sens,lperiodic)
                    !case(3)
                        !call predictionshortatomic_type3(&
                        !  num_atoms,num_atoms_element,zelem,&
                        !  lattice,xyzstruct,atomspin,atomcharge,&
                        !  minvalue_short_atomic,maxvalue_short_atomic,avvalue_short_atomic,&
                        !  minvalue_charge,maxvalue_charge,avvalue_charge,&
                        !  eshortmin,eshortmax,&
                        !  nntotalenergy,nnshortforce,&
                        !  nnatomenergy,nnshortenergy,nnstress_short,&
                        !  atomenergysum,sens,lperiodic)
                    case default
                    ! todo: error message
                    print*, 'ERROR: illegal combination of nntype short / elec etc...'
                    stop !'

                end select
            else
                ! todo: print proper error message or rewrite all other messages to also rhyme
                print*, ''
                print*, ' no lelec'
                print*, ' no lshort'
                print*, ' i guess we better'
                print*, ' just quickly abort'
                stop
            endif
        endif


          !call predictionelectrostatic_type5(&
          !  num_atoms,zelem,minvalue_elec,&
          !  maxvalue_elec,avvalue_elec,&
          !  lattice,xyzstruct,atomspin,nnatomcharge,&
          !  totalcharge,chargemin,&
          !  chargemax,nnelecenergy,&
          !  nnelecforce,nnstress_elec,sense,lperiodic)
!! vdW part
        if(lvdw.and.((nn_type_vdw.eq.1).or.nn_type_vdw.eq.2))then
          call predictionvdw(&
            num_atoms,zelem,&
            lattice,xyzstruct,lperiodic,&
            vdwenergy, atomvdw, vdwforces, vdwstress)
        endif ! lvdw
!!
!!======================================================================
!! combine the short range, electrostatic energies and vdw dispersion energies
!!======================================================================
        ! todo: we should also return the individual energy components for the 3G and 4G
        !if(.not.lelec)then
        !    nntotalenergy=nnshortenergy+nnelecenergy
        !endif
        !!
        !!======================================================================
        !! combination of short-range, electrostatic and vdw forces
        !! must be done after the separate output of short and electrostatic forces
        !!======================================================================
        if(ldoforces)then
            nntotalforce(:,:)=nnshortforce(:,:)+nnelecforce(:,:)
            if(lvdw)then
                nntotalforce(:,:)=nntotalforce(:,:)+vdwforces(:, :)
            endif !lvdw
        endif ! ldoforces
        nntotalenergy = nnshortenergy + nnelecenergy
        nntotalenergy = nntotalenergy + vdwenergy
!!
!!======================================================================
!! add energies of free atoms
!!======================================================================
        if(lremoveatomenergies.and.lshort.and.(.not.lelec))then
          call addatoms(num_atoms,&
            zelem,num_atoms_element,&
            atomenergysum,nnatomenergy)
          nntotalenergy=nntotalenergy+atomenergysum
        endif ! lremoveatomenergies
!!
!!======================================================================
!! print short range, electrostatic and vdw forces separately for debugging'
!!======================================================================
        if((mpirank.eq.0).and.(.not.lmd))then
          if(ldoforces)then
            write(ounit,*)'-------------------------------------------------------------'
            if(ldebug)then !'
              if(lshort)then
                write(ounit,'(A40,I10)')'NN short range forces for Configuration',i4
                do i2=1,num_atoms !'
                  write(ounit,'(i6,3f18.8)')i2,nnshortforce(1,i2),&
                  nnshortforce(2,i2),nnshortforce(3,i2)
                enddo
              endif
              if(lelec)then
                write(ounit,'(A42,I10)')'NN electrostatic forces for Configuration',i4
                do i2=1,num_atoms !'
                  write(ounit,'(i6,3f18.8)')i2,nnelecforce(1,i2),&
                  nnelecforce(2,i2),nnelecforce(3,i2)
                enddo !'
              endif
              if(lvdw)then
                write(ounit,'(A42,I10)')'NN vdw forces for Configuration',i4
                do i2=1,num_atoms !'
                  write(ounit,'(i6,3f18.8)')i2,vdwforces(1,i2),&
                  vdwforces(2,i2),vdwforces(3,i2)
                enddo !'
              endif
              write(ounit,*)'-------------------------------------------------------------'
            endif !'! ldebug
          endif ! ldoforces
        endif !' mpirank.eq.0
!!
!!======================================================================
!! calculate the volume, needed also for stress
!!======================================================================
        if(lperiodic)then
          volume=0.0d0
          call getvolume(lattice,volume)
          if((mpirank.eq.0).and.(.not.lmd))then
            write(ounit,*)'-------------------------------------------------------------'
            write(ounit,*)'volume ',volume,' Bohr^3 for configuration ', i4
          endif !'! mpirank.eq.0
        endif ! lperiodic
!!
!!======================================================================
!! combination of short-range, electrostatic stress and vdw stress.
!!======================================================================
        if(ldostress.and.lperiodic)then
          nnstress(:,:)=nnstress_short(:,:)+nnstress_elec(:,:)
          !! Requires extra if because vdwstress is not allocated if use_vdw=F.
          if(lvdw)then
            nnstress(:,:)=nnstress(:,:)+vdwstress(:, :)
          endif !lvdw
          nnstress(:,:)=nnstress(:,:)/volume ! Shouldnt't be called stress everywhere if it is not.
        endif ! ldostress
!!
!!======================================================================
!! now write results to files 
!!======================================================================
        if(lfinetime)then
          dayoutput=0
          call abstime(timeoutputstart,dayoutput)
        endif ! lfinetime

        if((mpirank.eq.0).and.(.not.lmd))then
          if(.not.(luseatomspins.and.(.not.lshort).and.lelec.and.(nn_type_elec.eq.1).and.(nn_type_gen.eq.2)))then
            if(i4.eq.1) then
              open(nneunit,file='energy.out',form='formatted',status='replace')
              write(nneunit,'(2A10,3A30)')'Conf.','atoms','Ref. total energy(Ha)','NN total energy(Ha)','E(Ref.) - E(NN) (Ha/atom)'
            endif
            if(lelec.and.lshort)then
                write(nneunit,'(2I10,3f30.8)') i4,num_atoms,totalenergy,nntotalenergy,(totalenergy-nntotalenergy)/float(num_atoms)
            elseif(lelec.and.(.not.lshort))then
                write(nneunit,'(2I10,3f30.8)') i4,num_atoms,totalenergy,nnelecenergy,(totalenergy-nnelecenergy)/float(num_atoms)
            else ! only for 2GHDNNP
                write(nneunit,'(2I10,3f30.8)') i4,num_atoms,totalenergy,nntotalenergy,(totalenergy-nntotalenergy)/float(num_atoms)
            endif
            if(i4.eq.totnum_structures)then
              close(nneunit)
            endif
          endif
!!
!! delete nnforces just in case an old file is present
          if(ldoforces)then
            if(i4.eq.1) then
              open(nnfunit,file='nnforces.out',form='formatted',status='unknown')
              close(nnfunit,status='delete')
              open(nnfunit,file='nnforces.out',form='formatted',status='replace')
!              write(nnfunit,*)'Configuration has',num_atoms,'atoms'
              write(nnfunit,'(2A10,6A25)')'Conf.','atom','Ref. Fx(Ha/Bohr)','Ref. Fy(Ha/Bohr)',&
                'Ref. Fz(Ha/Bohr)','NN Fx(Ha/Bohr)','NN Fy(Ha/Bohr)','NN Fz(Ha/Bohr)'
            endif
            if(lelec.and.lshort)then
                do i3=1,num_atoms !'
                  write(nnfunit,'(2I10,6f25.8)')i4,i3,&
                   (totalforce(i2,i3),i2=1,3),(nntotalforce(i2,i3),i2=1,3)
                enddo
            elseif(lelec.and.(.not.lshort))then
                do i3=1,num_atoms !'
                  write(nnfunit,'(2I10,6f25.8)')i4,i3,&
                   (totalforce(i2,i3),i2=1,3),(nnelecforce(i2,i3),i2=1,3)
                enddo
            else ! can only be for 2GHDNNP
                do i3=1,num_atoms !'
                  write(nnfunit,'(2I10,6f25.8)')i4,i3,&
                   (totalforce(i2,i3),i2=1,3),(nntotalforce(i2,i3),i2=1,3)
                enddo
            endif
            if(i4.eq.totnum_structures)then
              close(nnfunit)
            endif
          endif ! ldoforces

          !! Writing Hessian matrix for each structure
          if(ldohessian.and.lwritehessian.and.(nn_type_short.eq.1))then
            if(i4.eq.1) then
              open(nnhunit,file='nnhessian.out',form='formatted',status='unknown')
              close(nnhunit,status='delete')
              open(nnhunit,file='nnhessian.out',form='formatted',status='replace')
              write(nnhunit,'(A10)')'Conf.'
            endif
            write(nnhunit,'(I10)')i4
            do i3 = 1,3*num_atoms
                write(nnhunit,'(1000f25.8)')nnhessian(i3,:)
            enddo
            if(i4.eq.totnum_structures)then
              close(nnhunit)
            endif
          endif ! ldohessian

          !! Writing vibrational frequencies for each structure
          if(lcalculatefrequencies.and.(nn_type_short.eq.1))then
              if(i4.eq.1) then
                  open(nnfrequnit,file='nnfrequencies.out',form='formatted',status='unknown')
                  close(nnfrequnit,status='delete')
                  open(nnfrequnit,file='nnfrequencies.out',form='formatted',status='replace')
                  write(nnfrequnit,'(2A10,A25)')'Conf.','ID','Frequency(1/cm)'
              endif
              do i3 = 1,3*num_atoms
                  write(nnfrequnit,'(2I10,f25.8)')i4,i3,nnfrequencies(i3)
              enddo
              if(i4.eq.totnum_structures)then
                  close(nnfrequnit)
              endif
          endif ! ldohessian

          if(lcalculatenormalmodes.and.lcalculatefrequencies.and.(nn_type_short.eq.1))then
              if(i4.eq.1) then
                  open(nnmodesunit,file='nnnormalmodes.out',form='formatted',status='unknown')
                  close(nnmodesunit,status='delete')
                  open(nnmodesunit,file='nnnormalmodes.out',form='formatted',status='replace')
                  write(nnmodesunit,'(A10,A25)')'Conf.'
              endif !'
              write(nnmodesunit,'(I10)')i4
              write(nnmodesunit,'(A10,A25)')'Freq. ID','Eigenvector'
              do i3 = 1,3*num_atoms
                  do i5 = 1,3*num_atoms
                     write(nnmodesunit,'(I10,f25.8)')i3,nnnormalmodes(i5,i3)
                  enddo
              enddo

              if(i4.eq.totnum_structures)then
                  close(nnmodesunit)
              endif
          endif ! ldohessian

!!======================================================================
!! check sum of forces if requested 
!!======================================================================
          if(lcheckf)then
            forcesum(:)=0.0d0
            do i3=1,num_atoms
              do i2=1,3
                forcesum(i2)=forcesum(i2)+nntotalforce(i2,i3)
              enddo ! i2
            enddo ! i3
            write(ounit,'(A10,3A25)')'Conf.','Sum of Fx(Ha/Bohr)', 'Sum of Fy(Ha/Bohr)','Sum of Fz(Ha/Bohr)'
            write(ounit,'(I10,3f25.8)')i4,forcesum(1),forcesum(2),forcesum(3)
            do i2=1,3 !'
              if(abs(forcesum(i2)).gt.0.000001d0)then
                write(ounit,'(I10,A31,I10,f25.8)')i4,'Error in forces of component: ',&
                  i2,forcesum(i2)
                stop !'
              endif
            enddo ! i2
          endif ! lcheckf
!!
!! delete nnstress just in case an old file is present
          if(ldostress.and.lperiodic)then
            if(i4.eq.1)then
              open(nnsunit,file='nnstress.out',form='formatted',status='unknown')
              close(nnsunit,status='delete')
              open(nnsunit,file='nnstress.out',form='formatted',status='replace')
              write(nnsunit,'(A10,3A25)')'Conf.','NN Px(Ha/Bohr^3)','NN Py(Ha/Bohr^3)','NN Pz(Ha/Bohr^3)'
            endif !'
            do i1=1,3
              write(nnsunit,'(I10,3f25.8)')i4,(nnstress(i1,i2),i2=1,3)
            enddo ! i1
            if(i4.eq.totnum_structures)then
              close(nnsunit)
            endif
          endif ! ldostress
!!
!! delete nnatoms just in case an old file is present
          if(luseatomspins.and.(.not.lshort).and.lelec.and.(nn_type_elec.eq.1).and.(nn_type_gen.eq.2))then
            if(i4.eq.1)then
              open(nnaunit,file='nnatoms.out',form='formatted',status='unknown')
              close(nnaunit,status='delete')
              open(nnaunit,file='nnatoms.out',form='formatted',status='replace')
              write(nnaunit,'(2A10,A10,2A18)')'Conf.','atom','element','|Ref. spin|(hbar)',&
                  'NN spin(hbar)'
            endif !'
            do i3=1,num_atoms
               write(nnaunit,'(2I10,I10,2f18.8)')i4,i3,zelem(i3),&
                abs(atomspin(i3)),nnatomcharge(i3)
            enddo ! i3
            if(i4.eq.totnum_structures)then
              close(nnaunit)
            endif
          else
            if(i4.eq.1)then
              open(nnaunit,file='nnatoms.out',form='formatted',status='unknown')
              close(nnaunit,status='delete')
              open(nnaunit,file='nnatoms.out',form='formatted',status='replace')
              write(nnaunit,'(2A10,A10,2A18,2A25)')'Conf.','atom','element','Ref. charge(e)',&
                  'NN charge(e)','Ref. energy(Ha)','NN energy(Ha)'
            endif !'
            do i3=1,num_atoms
               write(nnaunit,'(2I10,I10,2f18.8,2f25.8)')i4,i3,zelem(i3),&
                atomcharge(i3),nnatomcharge(i3),atomenergy(i3),nnatomenergy(i3)
            enddo ! i3
            if(i4.eq.totnum_structures)then
              close(nnaunit)
            endif
          endif
!!
          if(nn_type_short.eq.2)then
            if(i4.eq.1)then
              open(nnpunit,file='nnpairs.out',form='formatted',status='unknown')
              close(nnpunit,status='delete')
              open(nnpunit,file='nnpairs.out',form='formatted',status='replace')
              write(nnpunit,'(4A10,2A18,2A25)')'Conf.','atom','atom1',&
                'atom2','Ref. charge(e)','NN charge(e)','Ref. energy(Ha)','NN pair energy(Ha)'
            endif !'
            do i1=1,num_pairs 
              write(nnpunit,'(2I10,2A10,2f18.8,2f25.8)')i4,i1,&
                element(elementindex(pairs_charge(1,i1))),&
                element(elementindex(pairs_charge(2,i1))),&
                atomcharge(i1),0.0d0,atomenergy(i1),nnpairenergy(i1)
            enddo ! i1
            if(i4.eq.totnum_structures)then
              close(nnpunit)
            endif
          endif
!!
!!======================================================================
!! write output data in RuNNer format and put in all results we have
!!======================================================================
          if(i4.eq.1)then
            open(outunit,file='output.data',form='formatted',status='unknown')
            close(outunit,status='delete')
            open(outunit,file='output.data',form='formatted',status='replace')
          endif
!!---------------------------------------------------------------------------
!! Assignement of keywords and properties into arrays used for write.          
!!---------------------------------------------------------------------------
          custom_order: if(lcustom_output_order.eqv..true.) then !new routine Niko
            if(output_col_indx%x_coord.gt.0)then
              output_arr(output_col_indx%x_coord, :) = xyzstruct(1,:)
              output_beginline(output_col_indx%x_coord) = 'x'
            endif !'
            if(output_col_indx%y_coord.gt.0)then  
              output_arr(output_col_indx%y_coord, :) = xyzstruct(2,:)
              output_beginline(output_col_indx%y_coord) = 'y'
            endif
            if(output_col_indx%z_coord.gt.0)then 
              output_arr(output_col_indx%z_coord, :) = xyzstruct(3,:)
              output_beginline(output_col_indx%z_coord) = 'z'
            endif
            if(luseatomspins.and.(.not.lshort).and.lelec.and.(nn_type_elec.eq.1).and.(nn_type_gen.eq.2))then
              if(output_col_indx%atom_charge.gt.0)then
                output_arr(output_col_indx%atom_charge, :) = 0.0
                output_beginline(output_col_indx%atom_charge) = 'atom_charge'
              endif !'
              if(output_col_indx%atom_spin.gt.0)then
                output_arr(output_col_indx%atom_spin, :) = nnatomcharge(:)
                output_beginline(output_col_indx%atom_spin) = 'atom_spin'
              endif !'
            else
              if(output_col_indx%atom_charge.gt.0)then
                output_arr(output_col_indx%atom_charge, :) = nnatomcharge(:)
                output_beginline(output_col_indx%atom_charge) = 'atom_charge'
              endif !'
              if(output_col_indx%atom_spin.gt.0)then
                output_arr(output_col_indx%atom_spin, :) = atomspin(:)
                output_beginline(output_col_indx%atom_spin) = 'atom_spin'
              endif !'
            endif
            if(output_col_indx%atom_energy.gt.0)then
              output_arr(output_col_indx%atom_energy, :) = nnatomenergy(:)
              output_beginline(output_col_indx%atom_energy) = 'atom_energy'
            endif !'
            if(output_col_indx%x_force.gt.0)then  
              output_arr(output_col_indx%x_force, :) = nntotalforce(1,:) 
              output_beginline(output_col_indx%x_force) = 'fx'
            endif  
            if(output_col_indx%y_force.gt.0)then  
              output_arr(output_col_indx%y_force, :) = nntotalforce(2,:) 
              output_beginline(output_col_indx%y_force) = 'fy'
            endif  
            if(output_col_indx%z_force.gt.0)then  
              output_arr(output_col_indx%z_force, :) = nntotalforce(3,:) 
              output_beginline(output_col_indx%z_force) = 'fz'
            endif  
            if(output_col_indx%elem_sym.gt.0)then
              output_beginline(output_col_indx%elem_sym) = 'elem_symbol'
            endif  
!!---------------------------------------------------------------------------
            write(outunit,'(a6)', advance='no')'begin '
            do i7=1,output_n_prop
              write(outunit, '(a,x)',advance='no')trim(output_beginline(i7))
              if(i7.eq.output_n_prop) then
                write(outunit, *)
              endif        
            enddo
            if(lperiodic)then !'
              write(outunit,'(a7,3f16.8)')'lattice  ',(lattice(1,i1),i1=1,3)
              write(outunit,'(a7,3f16.8)')'lattice  ',(lattice(2,i1),i1=1,3)
              write(outunit,'(a7,3f16.8)')'lattice  ',(lattice(3,i1),i1=1,3)
            endif
            do i1=1,num_atoms
              write(outunit, '(a5)', advance='no') 'atom '
              if(output_col_indx%elem_sym/=1) then
                do i6=1,(output_col_indx%elem_sym-1)
                  write(outunit, '(5f16.8)', advance='no')& 
                    output_arr(i6,i1)
                enddo 
                write(outunit, '(x,a2,x)', advance='no')elementsymbol(i1)
                do i6=(output_col_indx%elem_sym+1),output_n_prop
                  write(outunit, '(5f16.8)', advance='no')& 
                    output_arr(i6,i1)
                  if(i6.eq.output_n_prop) then
                    write(outunit,*)
                  endif      
                enddo  
              elseif(output_col_indx%elem_sym.eq.1) then
                write(outunit, '(a2,x)', advance='no')elementsymbol(i1)
                do i6=(output_col_indx%elem_sym+1),output_n_prop
                  write(outunit, '(5f16.8)', advance='no')& 
                    output_arr(i6,i1)
                  if(i6.eq.output_n_prop) then
                    write(outunit,*)
                  endif      
                enddo  
              endif
            enddo
            if(luseatomspins.and.(.not.lshort).and.lelec.and.(nn_type_elec.eq.1).and.(nn_type_gen.eq.2))then
              write(outunit,'(a7,f18.8)')'energy ',0.0
              write(outunit,'(a7,f18.8)')'charge ',0.0
              write(outunit,'(a3)')'end'
            elseif(lelec.and.lshort)then
              write(outunit,'(a7,f18.8)')'energy ',nntotalenergy
              write(outunit,'(a7,f18.8)')'charge ',nntotalcharge
              write(outunit,'(a3)')'end'
            elseif(lelec.and.(.not.lshort))then
              write(outunit,'(a7,f18.8)')'energy ',nnelecenergy
              write(outunit,'(a7,f18.8)')'charge ',nntotalcharge
              write(outunit,'(a3)')'end'
            else  
              write(outunit,'(a7,f18.8)')'energy ',nntotalenergy
              write(outunit,'(a7,f18.8)')'charge ',nntotalcharge
              write(outunit,'(a3)')'end'
            endif    
!!---------------------------------------------------------------------------
          else !old routine from RuNNer 1.XX
            write(outunit,'(a5)')'begin'
            if(lperiodic)then
              write(outunit,'(a7,3f20.12)')'lattice  ',(lattice(1,i1),i1=1,3)
              write(outunit,'(a7,3f20.12)')'lattice  ',(lattice(2,i1),i1=1,3)
              write(outunit,'(a7,3f20.12)')'lattice  ',(lattice(3,i1),i1=1,3)
            endif
            if(lspin)then
              if(luseatomspins.and.(.not.lshort).and.lelec.and.(nn_type_elec.eq.1).and.(nn_type_gen.eq.2))then
                  do i1=1,num_atoms
                    write(outunit,'(a5,3f20.12,x,a2,x,6f20.12)')&
                      'atom ',(xyzstruct(i2,i1),i2=1,3),&
                       elementsymbol(i1),0.0,nnatomenergy(i1),&
                       (nnelecforce(i2,i1),i2=1,3),nnatomcharge(i1)
                  enddo
                  write(outunit,'(a7,f20.12)')'energy ',0.0
                  write(outunit,'(a7,f20.12)')'charge ',0.0
                  write(outunit,'(a3)')'end'
              elseif(lelec.and.lshort)then
                  do i1=1,num_atoms
                    write(outunit,'(a5,3f20.12,x,a2,x,6f20.12)')&
                      'atom ',(xyzstruct(i2,i1),i2=1,3),&
                       elementsymbol(i1),nnatomcharge(i1),nnatomenergy(i1),&
                       (nntotalforce(i2,i1),i2=1,3),atomspin(i1)
                  enddo
                  write(outunit,'(a7,f20.12)')'energy ',nntotalenergy
                  write(outunit,'(a7,f20.12)')'charge ',nntotalcharge
                  write(outunit,'(a3)')'end'
              elseif(lelec.and.(.not.lshort))then
                  do i1=1,num_atoms
                    write(outunit,'(a5,3f20.12,x,a2,x,6f20.12)')&
                      'atom ',(xyzstruct(i2,i1),i2=1,3),&
                       elementsymbol(i1),nnatomcharge(i1),nnatomenergy(i1),&
                       (nnelecforce(i2,i1),i2=1,3),atomspin(i1)
                  enddo
                  write(outunit,'(a7,f20.12)')'energy ',nnelecenergy
                  write(outunit,'(a7,f20.12)')'charge ',nntotalcharge
                  write(outunit,'(a3)')'end'
              else
                  do i1=1,num_atoms
                    write(outunit,'(a5,3f20.12,x,a2,x,6f20.12)')&
                      'atom ',(xyzstruct(i2,i1),i2=1,3),&
                       elementsymbol(i1),nnatomcharge(i1),nnatomenergy(i1),&
                       (nntotalforce(i2,i1),i2=1,3),atomspin(i1)
                  enddo
                  write(outunit,'(a7,f20.12)')'energy ',nntotalenergy
                  write(outunit,'(a7,f20.12)')'charge ',nntotalcharge
                  write(outunit,'(a3)')'end'
              endif
            else
              if(lelec.and.lshort)then
                  do i1=1,num_atoms
                    write(outunit,'(a5,3f20.12,x,a2,x,5f20.12)')&
                      'atom ',(xyzstruct(i2,i1),i2=1,3),&
                       elementsymbol(i1),nnatomcharge(i1),nnatomenergy(i1),&
                       (nntotalforce(i2,i1),i2=1,3)
                  enddo
                  write(outunit,'(a7,f20.12)')'energy ',nntotalenergy
                  write(outunit,'(a7,f20.12)')'charge ',nntotalcharge
                  write(outunit,'(a3)')'end'
              elseif(lelec.and.(.not.lshort))then
                  do i1=1,num_atoms
                    write(outunit,'(a5,3f20.12,x,a2,x,5f20.12)')&
                      'atom ',(xyzstruct(i2,i1),i2=1,3),&
                       elementsymbol(i1),nnatomcharge(i1),nnatomenergy(i1),&
                       (nnelecforce(i2,i1),i2=1,3)
                  enddo
                  write(outunit,'(a7,f20.12)')'energy ',nnelecenergy
                  write(outunit,'(a7,f20.12)')'charge ',nntotalcharge
                  write(outunit,'(a3)')'end'
              else
                  do i1=1,num_atoms
                    write(outunit,'(a5,3f20.12,x,a2,x,5f20.12)')&
                      'atom ',(xyzstruct(i2,i1),i2=1,3),&
                       elementsymbol(i1),nnatomcharge(i1),nnatomenergy(i1),&
                       (nntotalforce(i2,i1),i2=1,3)
                  enddo
                  write(outunit,'(a7,f20.12)')'energy ',nntotalenergy
                  write(outunit,'(a7,f20.12)')'charge ',nntotalcharge
                  write(outunit,'(a3)')'end'
              endif
            endif
        endif custom_order
        if(i4.eq.totnum_structures)then
          close(outunit)
        endif
      endif ! mpirank.eq.0
!!
!!======================================================================
!! write results to standard out for process 0
!!======================================================================
        if((mpirank.eq.0).and.(.not.lmd))then
          if(.not.(luseatomspins.and.(.not.lshort).and.lelec.and.(nn_type_elec.eq.1).and.(nn_type_gen.eq.2)))then
            write(ounit,*)'-------------------------------------------------------------'
!!            if((.not.lshort).and.(lremoveatomenergies))then !'
!!            write(ounit,*)'WARNING: short range energy is just sum of atomic energies because lshort=F'
            write(ounit,"(A85, I10)")'NN sum of free atom energies, short range and electrostatic energy for configuration',&
            i4 !'
            write(ounit,'(a30,f18.8,a3)')' NN sum of free atom energies ',atomenergysum,' Ha'
            write(ounit,'(a30,f18.8,a3)')' NN short range energy        ',nntotalenergy-nnelecenergy-atomenergysum-vdwenergy,' Ha'
            write(ounit,'(a30,f18.8,a3)')' NN electrostatic energy      ',nnelecenergy,' Ha'
            write(ounit,'(a30,f18.8,a3)')' NN vdw dispersion energy     ',vdwenergy,' Ha'
            write(ounit,'(a30,f18.8,a3)')' NNenergy                     ',nntotalenergy,' Ha'
            write(ounit,*)'-------------------------------------------------------------'
            if((nn_type_short.eq.1).or.(nn_type_short.eq.3))then
                write(ounit,*)'NN atomenergies for configuration ',i4
              do i1=1,num_atoms
                write(ounit,'(a13,i6,x,a2,x,f14.6)')'NNatomenergy ',i1,elementsymbol(i1),&
                  nnatomenergy(i1)
              enddo ! i1
            elseif(nn_type_short.eq.2)then
              write(ounit,*)'NN pairenergies with configuration',i4
              do i1=1,num_pairs
                write(ounit,'(a14,i6,x,a2,x,a2,x,f14.6)')'NNpairenergy ',i1,&
                  element(elementindex(pairs_charge(1,i1))),&
                  element(elementindex(pairs_charge(2,i1))),&
                  nnpairenergy(i1)
              enddo ! i1
            endif
          endif
          write(ounit,*)'-------------------------------------------------------------'
          if(lelec)then !'
            if(luseatomspins)then
              write(ounit,*)'NNspins for configuration ',i4
              do i1=1,num_atoms
                write(ounit,'(a10,i6,f14.8)')'NNspin ',i1,nnatomcharge(i1)
              enddo
            else
              write(ounit,*)'NNcharges for configuration ',i4
              do i1=1,num_atoms
                write(ounit,'(a10,i6,f14.8)')'NNcharge ',i1,nnatomcharge(i1)
              enddo
            endif
          endif !

          if(.not.(luseatomspins.and.(.not.lshort).and.lelec.and.(nn_type_elec.eq.1).and.(nn_type_gen.eq.2)))then
            write(ounit,*)'-------------------------------------------------------------'
            write(ounit,*)'NN forces for configuration ',i4
            if(ldoforces)then !'
              do i1=1,num_atoms
                write(ounit,'(a10,i6,3f16.8,a8)')'NNforces ',i1,nntotalforce(1,i1),&
                nntotalforce(2,i1),nntotalforce(3,i1),' Ha/Bohr'
              enddo ! i1
            write(ounit,*)'-------------------------------------------------------------'
            endif !ldoforces'
            if(ldostress.and.lperiodic)then
              write(ounit,*)'NNstress for the configuration ',i4
              do i1=1,3
                write(ounit,'(a10,3f18.8,a10)')'NNstress ',nnstress(i1,1),&
                nnstress(i1,2),nnstress(i1,3),' Ha/Bohr^3'
              enddo ! i1
              write(ounit,*)'-------------------------------------------------------------'
              pressure=(nnstress(1,1)+nnstress(2,2)+nnstress(3,3))/3.0d0
              pressure=pressure*au2gpa !'
              write(ounit,'(a12,f18.8,a5)')' NNpressure ',pressure,' GPa'
              write(ounit,'(a12,f18.8,a5)')' NNpressure ',pressure*10.0d0,' kbar'
              write(ounit,*)'-------------------------------------------------------------'
            endif !ldostress'
          endif
          !! write vibrational frequencies to the standard output
          if(lcalculatefrequencies.and.(nn_type_short.eq.1))then
              write(ounit,*)'NNfrequencies for the configuration ',i4
              do i1=1,3*num_atoms
                  write(ounit,'(a15,i10,f18.8)')'NNfrequency ',i1,nnfrequencies(i1)
              enddo ! i1
              write(ounit,*)'-------------------------------------------------------------'
          endif !'
!! sensitivity:
          if(lsens.and.lshort.and.((nn_type_short.eq.1).or.(nn_type_short.eq.3)))then
            do i1=1,nelem
              do i2=1,num_funcvalues_short_atomic(i1)
                write(ounit,'(a15,x,a2,x,i5,f16.8)')' NNsensitivity ',&
!! CHANGE ANDI: Mean square average is probably a better sensitivity value
               !element(i3),i2,sens(i3,i2)/dble(num_atoms)
                  element(i1),i2,sqrt(sens(i1,i2)/dble(num_atoms_element(i1)))
!! END CHANGE
              enddo ! i2
              write(ounit,*)'-------------------------------------------------------------'
            enddo !'! i1
          endif ! lsens
          if(lsens.and.(nn_type_short.eq.2))then
            counter = 0
            do i1=1,nelem
             do i2=i1,nelem
              counter = counter + 1
              do i3=1,num_funcvalues_short_pair(counter)
                write(ounit,'(a15,x,a2,x,a2,x,i5,f16.8)')' NNsensitivity ',&
!'! CHANGE ANDI: Mean square average is probably a better sensitivity value.
!!              What is the correct normalization for nn_type_short 2 (num_atoms vs. num_atoms_element(i1))?
               !element(i1),element(i2),i3,sens(counter,i3)/dble(num_atoms)
                  element(i1),element(i2),i3,sqrt(sens(counter,i3)/dble(num_atoms))
!! END CHANGE
              enddo !! i3
              write(ounit,*)'-------------------------------------------------------------'
            enddo !'! i2
           enddo ! i1
          endif ! lsens
        endif ! mpirank.eq.0

!!======================================================================
!!
!!======================================================================

        if((mpirank.eq.0).and.(mode.eq.3))then
          write(ounit,*)'-------------------------------------------------------------'
          write(ounit,'(A45,I10)') 'extrapolation warnings for configuration   ', i4
          write(ounit,'(A45,I10)') 'extrapolation warnings (symmetry functions)', count_extrapolation_warnings_symfunc
          if(.not.(luseatomspins.and.(.not.lshort).and.lelec.and.(nn_type_elec.eq.1).and.(nn_type_gen.eq.2)))then
            write(ounit,'(A45,I10)') 'extrapolation warnings (energy)            ', count_extrapolation_warnings_energy
          endif
          if(lelec)then
            if(luseatomspins.and.(.not.lshort).and.lelec.and.(nn_type_elec.eq.1).and.(nn_type_gen.eq.2))then
              write(ounit,'(A45,I10)') 'extrapolation warnings (atomic spins)      ', count_extrapolation_warnings_charges
            else !'
              write(ounit,'(A45,I10)') 'extrapolation warnings (atomic charges)    ', count_extrapolation_warnings_charges
            endif !'
          endif
        endif

!!
!! calculate final output timing
        if(lfinetime)then
          call abstime(timeoutputend,dayoutput)
          timeoutput=timeoutputend-timeoutputstart
        endif ! lfinetime
!!
!!======================================================================
!! deallocate arrays
!!======================================================================
        if(lshort.and.((nn_type_short.eq.1).or.(nn_type_short.eq.3)))then
          deallocate(sens)
        elseif(lshort.and.(nn_type_short.eq.2))then
          deallocate(sens)
        elseif(lshort.and.(nn_type_short.eq.3))then
            deallocate(sens)
        endif
        if(lelec&
                .and.(nn_type_elec.eq.1)&
                .or.(nn_type_elec.eq.3)&
                .or.(nn_type_elec.eq.4)&
                .or.(nn_type_elec.eq.5))then
          deallocate(sense)
        endif
        if(lvdw.and.&
                ((nn_type_vdw.eq.1).or.nn_type_vdw.eq.2))then
          deallocate(atomvdw)
          deallocate(vdwforces)
          deallocate(vdwstress)
        endif

        !! Check if Hessian and frequency arrays are allocated - Emir
        if(allocated(nnhessian))deallocate(nnhessian)
        if(allocated(nnfrequencies))deallocate(nnfrequencies)
        if(allocated(nnnormalmodes))deallocate(nnnormalmodes)

        call abstime(timemode3end,daymode3)
!!
      enddo !i4
      return
      end
