!######################################################################
! This routine is part of
! RuNNer - RuNNer Neural Network Energy Representation
! (c) 2008-2024 Prof. Dr. Joerg Behler 
! Ruhr-Universitaet Bochum, Germany
!
! This program is free software: you can redistribute it and/or modify it 
! under the terms of the GNU General Public License as published by the 
! Free Software Foundation, either version 3 of the License, or 
! (at your option) any later version.
!
! This program is distributed in the hope that it will be useful, but 
! WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
! or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
! for more details.
!
! You should have received a copy of the GNU General Public License along 
! with this program. If not, see http://www.gnu.org/licenses.
! WARNING: THIS FILE IS PART OF THE UNMAINTAINED CONDA PACKAGE OF RUNNER 1.3. 
!######################################################################
!! called by:
!!
      subroutine geterror_charge(iswitch,countepoch,ntrain,&
        minvalue_elec,maxvalue_elec,avvalue_elec,&
        rmse_charge,rmse_totalcharge,mad_charge,&
        mad_totalcharge,maxerror_charge,&
        imaxerror_charge)
!!
      use mpi_mod
      use fileunits
      use fittingoptions
      use nnflags
      use globaloptions
      use symfunctions
      use nnewald
      use structures
!!
      implicit none
!!
      integer i1,i2,i3,i4                                               ! internal  
      integer iswitch                                                   ! in 0=train, 1=test
      integer ncharges                                                  ! internal
      integer ncharges_sum                                              ! internal
      integer ntrain                                                    ! in
      integer ndone                                                     ! internal
      integer ndonepara                                                 ! internal
      integer ncount                                                    ! internal
      integer npoints                                                   ! internal
      integer nstruct                                                   ! internal
      integer n_start                                                   ! internal
      integer n_end                                                     ! internal
      integer, dimension(:), allocatable :: num_atoms_mpi               ! internal
      integer, dimension(:,:), allocatable :: zelem_mpi                 ! internal
      integer icount                                                    ! internal
      integer countepoch                                                ! in
      integer tempunit                                                  ! internal
      integer pxunit                                                    ! internal
      integer qxunit                                                    ! internal
      integer num_atoms_element_list(nblock,nelem)                      ! internal 
      integer ztemp                                                     ! internal
      integer imaxerror_charge                                          ! out
      integer imaxerror_charge_temp                                     ! internal
!!
      real*8, dimension(:,:,:), allocatable :: xyzstruct_mpi            ! internal
      real*8, dimension(:,:), allocatable :: atomcharge_mpi             ! internal
      real*8, dimension(:), allocatable :: totalcharge_mpi              ! internal
      real*8, allocatable :: lattice_mpi(:,:,:)                         
      real*8, dimension(:,:,:), allocatable :: symfunctione_mpi         ! internal
      real*8, dimension(:,:), allocatable :: nnatomcharge_mpi           ! internal
      real*8, dimension(:), allocatable :: nnchargesum_mpi              ! internal
      real*8, dimension(:,:),allocatable :: nnatomchi_mpi               ! internal
      real*8 minvalue_elec(nelem,maxnum_funcvalues_elec)                ! in
      real*8 maxvalue_elec(nelem,maxnum_funcvalues_elec)                ! in
      real*8 avvalue_elec(nelem,maxnum_funcvalues_elec)                 ! in
      real*8 rmse_charge                                                ! out
      real*8 rmse_charge_temp                                           ! internal 
      real*8 rmse_totalcharge                                           ! out
      real*8 rmse_totalcharge_temp                                      ! internal
      real*8 mad_charge                                                 ! out
      real*8 mad_charge_temp                                            ! internal
      real*8 mad_totalcharge                                            ! out
      real*8 mad_totalcharge_temp                                        ! internal
      real*8 maxerror_charge                                            ! out
      real*8 maxerror_charge_temp                                       ! internal
      real*8 nnatomcharge_list(nblock,max_num_atoms)                    ! internal
      real*8 nnatomchi_list(nblock,max_num_atoms)                       ! internal
      real*8 nnchargesum_list(nblock)                                   ! internal
!!                                                                      
      character*25 filename                                             ! internal
!!                                                                      
      logical, dimension(:), allocatable :: lperiodic_mpi               ! internal
!!
!!===============================================================
!!===============================================================
!! calculate the initial training error
!!===============================================================
!!===============================================================
!! initializations
!!===============================================================
      ncharges                 = 0
      ncharges_sum             = 0
      ncount                   = ntrain
      ndone                    = 0
      nnatomcharge_list(:,:)   = 0.0d0 
      nnchargesum_list(:)      = 0.0d0
      nnatomchi_list(:,:)      = 0.0d0
      rmse_charge              = 0.0d0
      rmse_totalcharge         = 0.0d0
      mad_charge               = 0.0d0
      mad_totalcharge          = 0.0d0
      maxerror_charge          = 0.0d0
      imaxerror_charge         = 0
!!
!!===============================================================
!! open files and write headers
!!===============================================================
      if(mpirank.eq.0)then
        if(iswitch.eq.0)then ! train error
          open(trainstructunit,file='trainstruct.data',form='formatted',status='old')
          rewind(trainstructunit)
          open(symeunit,file='functione.data',form='formatted',status='old')
          rewind(symeunit) !'
          if(lwritetraincharges)then
            if(luseatomspins)then   !! if it is spin prediction
                filename='trainspins.000000.out'
                if(countepoch.gt.9999)then
                  write(ounit,*)'Error: too many epochs in geterror'
                  write(ounit,*)'switch off lwritetraincharges'
                  stop
                elseif(countepoch.gt.999)then
                  write(filename(14:17),'(i4)')countepoch
                elseif(countepoch.gt.99)then
                  write(filename(15:17),'(i3)')countepoch
                elseif(countepoch.gt.9)then
                  write(filename(16:17),'(i2)')countepoch
                else
                  write(filename(17:17),'(i1)')countepoch
                endif
                open(trainqxunit,file=filename,form='formatted',status='replace')
                rewind(trainqxunit) !'
                write(trainqxunit,'(2A10,A10,2A18)')'Conf.','atom','element','Ref. spin(hbar)','NN spin(hbar)' 
            else
                filename='traincharges.000000.out'
                if(countepoch.gt.9999)then
                  write(ounit,*)'Error: too many epochs in geterror'
                  write(ounit,*)'switch off lwritetraincharges'
                  stop
                elseif(countepoch.gt.999)then
                  write(filename(16:19),'(i4)')countepoch
                elseif(countepoch.gt.99)then
                  write(filename(17:19),'(i3)')countepoch
                elseif(countepoch.gt.9)then
                  write(filename(18:19),'(i2)')countepoch
                else
                  write(filename(19:19),'(i1)')countepoch
                endif
                open(trainqxunit,file=filename,form='formatted',status='replace')
                rewind(trainqxunit) !'
                write(trainqxunit,'(2A10,A10,2A18)')'Conf.','atom','element','Ref. charge(e)','NN charge(e)' 
            endif
          endif
!!
        elseif(iswitch.eq.1)then ! test error
          open(teststructunit,file='teststruct.data',form='formatted',status='old')
          rewind(teststructunit)
          open(tymeunit,file='testinge.data',form='formatted',status='old')
          rewind(tymeunit)
          if(lwritetraincharges)then
            if(luseatomspins)then
                filename='testspins.000000.out'
                if(countepoch.gt.999)then
                  write(filename(13:16),'(i4)')countepoch
                elseif(countepoch.gt.99)then
                  write(filename(14:16),'(i3)')countepoch
                elseif(countepoch.gt.9)then
                  write(filename(15:16),'(i2)')countepoch
                else
                  write(filename(16:16),'(i1)')countepoch
                endif
                open(testqxunit,file=filename,form='formatted',status='replace')
                rewind(testqxunit) !'
                write(testqxunit,'(2A10,A10,2A18)')'Conf.','atom','element','Ref. spin(hbar)','NN spin(hbar)'
            else
                filename='testcharges.000000.out'
                if(countepoch.gt.999)then
                  write(filename(15:18),'(i4)')countepoch
                elseif(countepoch.gt.99)then
                  write(filename(16:18),'(i3)')countepoch
                elseif(countepoch.gt.9)then
                  write(filename(17:18),'(i2)')countepoch
                else
                  write(filename(18:18),'(i1)')countepoch
                endif
                open(testqxunit,file=filename,form='formatted',status='replace')
                rewind(testqxunit) !'
                write(testqxunit,'(2A10,A10,2A18)')'Conf.','atom','element','Ref. charge(e)','NN charge(e)'
            endif ! spins or charges
          endif ! writetraincharges
        endif 
      endif ! mpirank.eq.0 
!!
!!===============================================================
!! loop over all structures
!!===============================================================
 10   continue
      if(ncount.gt.nblock)then
        npoints=nblock
        ncount=ncount-nblock
      else
        npoints=ncount
        ncount=ncount-npoints
      endif
!!
!!===============================================================
!! determine which nstruct structures of this block should be calculated
!by this process
!!===============================================================
      call mpifitdistribution(npoints,nstruct,n_start,n_end)
      call mpi_barrier(mpi_comm_world,mpierror)
!!
!!===============================================================
!! do all file reading for error determination here at one place to allow for parallelization
!!===============================================================
      if(mpirank.eq.0)then
!!===============================================================
!! read the symmetry functions for the charge prediction (train or test)
!!===============================================================
        if(iswitch.eq.0)then ! train
          tempunit=symeunit          
        elseif(iswitch.eq.1)then ! test
          tempunit=tymeunit          
        else
          write(ounit,*)'ERROR: unknown iswitch in geterror ',iswitch
          stop
        endif
        call readfunctions(1,tempunit,npoints,nelem,&
          max_num_atoms,maxnum_funcvalues_elec,num_funcvalues_elec,&
          symfunction_elec_list)
!!
!!===============================================================
!! read the structures needed for the calculation of the electrostatic energy
!! is needed for lelec (structure for electrostatics)
!! must be called after readfunctions because it needs num_atoms_list
!!===============================================================
        if(iswitch.eq.0)then ! train
          tempunit=trainstructunit          
        elseif(iswitch.eq.1)then ! test
          tempunit=teststructunit          
        else
          write(ounit,*)'ERROR: unknown iswitch in geterror ',iswitch
          stop
        endif
        call getstructures(tempunit,npoints)
!!
      endif ! mpirank.eq.0
!!
!===============================================================
!! distribute the data to all processes
!!===============================================================
      call mpi_bcast(num_atoms_list,nblock,mpi_integer,0,mpi_comm_world,mpierror)
      call mpi_bcast(zelem_list,nblock*max_num_atoms,mpi_integer,0,mpi_comm_world,mpierror)
      call mpi_bcast(totalcharge_list,nblock,mpi_real8,0,mpi_comm_world,mpierror)
      call mpi_bcast(elecenergy_list,nblock,mpi_real8,0,mpi_comm_world,mpierror)
      call mpi_bcast(symfunction_elec_list,nblock*max_num_atoms*maxnum_funcvalues_elec,&
           mpi_real8,0,mpi_comm_world,mpierror)
      call mpi_bcast(xyzstruct_list,nblock*max_num_atoms*3,mpi_real8,0,mpi_comm_world,mpierror)
      call mpi_bcast(atomcharge_list,nblock*max_num_atoms,mpi_real8,0,mpi_comm_world,mpierror)
      call mpi_bcast(lattice_list,nblock*9,mpi_real8,0,mpi_comm_world,mpierror)
      call mpi_bcast(lperiodic_list,nblock,mpi_logical,0,mpi_comm_world,mpierror)
!!
!!===============================================================
!! end of file reading for error determination
!!===============================================================
!!
!!===============================================================
!! prepare local arrays for this process
!!===============================================================
      allocate(num_atoms_mpi(nstruct))
      allocate(zelem_mpi(nstruct,max_num_atoms))
      allocate(xyzstruct_mpi(3,max_num_atoms,nstruct))
      allocate(atomcharge_mpi(nstruct,max_num_atoms))
      allocate(lperiodic_mpi(nstruct))
      allocate(totalcharge_mpi(nstruct))
      allocate(lattice_mpi(3,3,nstruct))
      allocate(symfunctione_mpi(maxnum_funcvalues_elec,max_num_atoms,nstruct))
      allocate(nnatomcharge_mpi(nstruct,max_num_atoms))
      nnatomcharge_mpi(:,:)=0.0d0
      allocate(nnchargesum_mpi(nstruct))
      nnchargesum_mpi(:)=0.0d0
      allocate(nnatomchi_mpi(nstruct,max_num_atoms))
      nnatomchi_mpi(:,:)=0.0d0
!!
!!===============================================================
!! fill local input arrays
!!===============================================================
      icount=0
      do i1=n_start,n_end
        icount=icount+1
        num_atoms_mpi(icount)       = num_atoms_list(i1)
        zelem_mpi(icount,:)         = zelem_list(i1,:)
        xyzstruct_mpi(:,:,icount)   = xyzstruct_list(:,:,i1)
        lperiodic_mpi(icount)       = lperiodic_list(i1)
        atomcharge_mpi(icount,:)    = atomcharge_list(i1,:)
        totalcharge_mpi(icount)     = sum(atomcharge_list(i1,:(num_atoms_list(i1))))
        lattice_mpi(:,:,icount)     = lattice_list(:,:,i1)
      enddo ! i1
!!
!!===============================================================
!! get the atom charges 
!!===============================================================
      ndonepara=ndone+n_start-1
      ! the name of this subroutine does not describe what it actually does
      ! be careful
      call ewaldenergies_para_type5(nstruct,&
        num_atoms_mpi,zelem_mpi,ncharges,&
        minvalue_elec,maxvalue_elec,avvalue_elec,&
        symfunction_elec_list(1,1,n_start),&
        xyzstruct_mpi,rmse_charge_temp,rmse_totalcharge_temp,&
        mad_charge_temp,mad_totalcharge_temp,atomcharge_mpi,&
        nnatomcharge_mpi,totalcharge_mpi,nnchargesum_mpi,&
        maxerror_charge_temp,imaxerror_charge_temp, lperiodic_mpi, lattice_mpi)
!!
!!===============================================================
!! find imaxerror_charge and maxerror_charge
!!===============================================================
      if (maxerror_charge_temp .gt. maxerror_charge)then
         maxerror_charge =maxerror_charge_temp
         imaxerror_charge = imaxerror_charge_temp+ndonepara   !! KK: here the serial version is considered
      endif
!!===============================================================
!! collect rmse_charge, mad_charge, rmse_totalcharge, mad_totalcharge
!!===============================================================
      rmse_charge = rmse_charge + rmse_charge_temp
      mad_charge = mad_charge + mad_charge_temp
      rmse_totalcharge = rmse_totalcharge + rmse_totalcharge_temp
      mad_totalcharge = mad_totalcharge + mad_totalcharge_temp
!!===============================================================
!! reinitialize variables
!!===============================================================
      rmse_charge_temp =0.0d0
      mad_charge_temp =0.0d0
      rmse_totalcharge_temp=0.0d0
      mad_totalcharge_temp =0.0d0 
!!===============================================================
!! copy results back on full array
!!===============================================================
      icount=0
      nnatomcharge_list(:,:)    =0.0d0
      nnchargesum_list(:)       =0.0d0
      do i1=n_start,n_end
        icount=icount+1
        nnatomcharge_list(i1,:)   = nnatomcharge_mpi(icount,:)
        nnchargesum_list(i1)      = nnchargesum_mpi(icount)
      enddo
      ncharges_sum=ncharges_sum+ncharges
      ncharges=0
!!
!!===============================================================
!! distribute results to all processes
!!===============================================================
      call mpi_allreduce(mpi_in_place,nnatomcharge_list,nblock*max_num_atoms,&
        mpi_real8,mpi_sum,mpi_comm_world,mpierror)
      call mpi_allreduce(mpi_in_place,nnchargesum_list,nblock,&
        mpi_real8,mpi_sum,mpi_comm_world,mpierror)
!!
!!===============================================================
!! prepare file units for detailed output 
!!===============================================================
      if(iswitch.eq.0)then ! train set
        pxunit=trainpxunit
        qxunit=trainqxunit
      elseif(iswitch.eq.1)then ! test set
        pxunit=testpxunit
        qxunit=testqxunit
      else
        write(ounit,*)'ERROR: unknown iswitch in geterror ',iswitch
        stop
      endif
!!
      if(mpirank.eq.0)then
!!===============================================================
!! if requested write charge details for all points to traincharges.XXXXXX.out
!!===============================================================
        if(lwritetraincharges)then
          if(lelec)then
            do i1=1,npoints
              do i2=1,num_atoms_list(i1)
                write(qxunit,'(2I10,I10,2f18.8)')i1+ndone,i2,&
                  zelem_list(i1,i2),&
                  atomcharge_list(i1,i2),nnatomcharge_list(i1,i2)
              enddo ! i2
            enddo ! i1
          endif ! lelec
        endif ! lwritetraincharges
!!
        ndone=ndone+npoints
!!
      endif ! mpirank.eq.0
!!
!!===============================================================
!! deallocate temporary arrays for this process
!!===============================================================
      deallocate(num_atoms_mpi)
      deallocate(zelem_mpi)
      deallocate(symfunctione_mpi)
      deallocate(xyzstruct_mpi)
      deallocate(atomcharge_mpi)
      deallocate(lperiodic_mpi)
      deallocate(nnatomcharge_mpi)
      deallocate(totalcharge_mpi)
      deallocate(lattice_mpi)
      deallocate(nnchargesum_mpi)
      deallocate(nnatomchi_mpi)
!!
!!===============================================================
!! if there are structures left go to next block of structures 
!!===============================================================
      if(ncount.gt.0) goto 10
!!===============================================================
!! end block of structures 
!!===============================================================
!!
!!===============================================================
!! close files
!!===============================================================
      if(mpirank.eq.0)then
        if(iswitch.eq.0)then ! training
          close(trainstructunit)
          close(symeunit)
          if(lwritetraincharges)     close(trainqxunit)
          if(lwritetrainpoints)      close(trainpxunit)
        elseif(iswitch.eq.1)then ! testing
          close(teststructunit)
          close(tymeunit)
          if(lwritetraincharges)     close(testqxunit)
          if(lwritetrainpoints)      close(testpxunit)
        else
          write(ounit,*)'ERROR: unknown iswitch in geterror ',iswitch
          stop
        endif ! iswitch
      endif ! mpirank.eq.0
!!
!!===============================================================
!! combine the partial rmse values of all processes here only to avoid double counting
!!===============================================================
      call mpi_allreduce(mpi_in_place,rmse_charge,1,&
           mpi_real8,mpi_sum,mpi_comm_world,mpierror)
      call mpi_allreduce(mpi_in_place,rmse_totalcharge,1,&
           mpi_real8,mpi_sum,mpi_comm_world,mpierror)
      call mpi_allreduce(mpi_in_place,mad_charge,1,&
           mpi_real8,mpi_sum,mpi_comm_world,mpierror)
      call mpi_allreduce(mpi_in_place,mad_totalcharge,1,&
           mpi_real8,mpi_sum,mpi_comm_world,mpierror)
      call mpi_allreduce(mpi_in_place,ncharges,1,&
           mpi_integer,mpi_sum,mpi_comm_world,mpierror)
!!
!!
!!===============================================================
!! calculate the final RMSEs 
!!===============================================================
!      write(*,*) 'JO', ntrain, ncharges_sum
      call getrmse_charge(ntrain,ncharges_sum,&
        rmse_charge,rmse_totalcharge,&
        mad_charge,mad_totalcharge,maxerror_charge)
!!
      return
      end
