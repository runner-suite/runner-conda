!######################################################################
! This routine is part of
! RuNNer - RuNNer Neural Network Energy Representation
! (c) 2008-2024 Prof. Dr. Joerg Behler
! Ruhr-Universitaet Bochum, Germany
!
! This program is free software: you can redistribute it and/or modify it
! under the terms of the GNU General Public License as published by the
! Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! This program is distributed in the hope that it will be useful, but
! WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
! or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
! for more details.
!
! You should have received a copy of the GNU General Public License along
! with this program. If not, see http://www.gnu.org/licenses.
! WARNING: THIS FILE IS PART OF THE UNMAINTAINED CONDA PACKAGE OF RUNNER 1.3.
!######################################################################
!! this is a multipurpose subroutine

!! called by:
!!
      subroutine initialweights(ndim,&
        maxnum_weights_local,num_weights_local,&
        maxnum_layers_local,num_layers_local,windex_local,nodes_local,&
        jseed,weights_local)
!!
      use fileunits
      use fittingoptions
      use nnflags
      use globaloptions
!!
      implicit none
!!
      integer ndim                       ! in
      integer maxnum_weights_local       ! in
      integer num_weights_local(ndim)    ! in
      integer maxnum_layers_local        ! in
      integer num_layers_local(ndim)     ! in
      integer*8 jseed                      ! in/out
      integer i,j,k                      ! internal
      integer windex_local(2*maxnum_layers_local,ndim) ! in
      integer nodes_local(0:maxnum_layers_local,ndim)  ! in
      integer*8 tmpseed
!!
      real*8 weights_local(maxnum_weights_local,ndim)
      real*8 ran0
      real*8 ran1
      real*8 ran2
      real*8 ran3
      real*8 ranx
!!
!!
!! we keep here the somewhat inconsistent looping order to be backwards compatible
      if(nran.eq.1)then
        tmpseed = int(jseed, kind(tmpseed))
        do j=1,ndim
          do i=1,num_weights_local(j)
            weights_local(i,j)=weights_min+(weights_max-weights_min)*ran0(tmpseed)  !-0.5d0*(weights_max-weights_min)
          enddo
        enddo
        jseed = int(tmpseed, kind(jseed))
      elseif(nran.eq.2)then
        tmpseed = int(jseed, kind(tmpseed))
        do j=1,ndim
          do i=1,num_weights_local(j)
            weights_local(i,j)=weights_min+(weights_max-weights_min)*ran1(tmpseed)  !-0.5d0*(weights_max-weights_min)
          enddo
        enddo
        jseed = int(tmpseed, kind(jseed))
      elseif(nran.eq.3)then
        tmpseed = int(jseed, kind(tmpseed))
        do j=1,ndim
          do i=1,num_weights_local(j)
            weights_local(i,j)=weights_min+(weights_max-weights_min)*ran2(tmpseed)  !-0.5d0*(weights_max-weights_min)
          enddo
        enddo
        jseed = int(tmpseed, kind(jseed))
      elseif(nran.eq.4)then
        tmpseed = int(jseed, kind(tmpseed))
        do j=1,ndim
          do i=1,num_weights_local(j)
            weights_local(i,j)=weights_min+(weights_max-weights_min)*ran3(tmpseed)  !-0.5d0*(weights_max-weights_min)
          enddo
        enddo
        jseed = int(tmpseed, kind(jseed))
      elseif(nran.gt.4)then
        do j=1,ndim
          do i=1,num_weights_local(j)
            weights_local(i,j)=weights_min+(weights_max-weights_min)*ranx(jseed)  !-0.5d0*(weights_max-weights_min)
          enddo
        enddo
      endif ! nran
!! overwrite biasweights if requested
      if(lseparatebiasini)then
        do j=1,ndim
            do i=1,num_layers_local(j)
              do k=windex_local(2*i,j),windex_local(2*i,j)+nodes_local(i,j)-1
                weights_local(k,j)=biasweights_min+(biasweights_max-biasweights_min)*ranx(jseed)
              enddo ! k
            enddo ! i
        enddo ! j
      endif ! lseparatebiasini
!!
      return
      end
!!
