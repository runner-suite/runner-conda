!######################################################################
! This routine is part of
! RuNNer - RuNNer Neural Network Energy Representation
! (c) 2008-2024 Prof. Dr. Joerg Behler 
! Ruhr-Universitaet Bochum, Germany
!
! This program is free software: you can redistribute it and/or modify it 
! under the terms of the GNU General Public License as published by the 
! Free Software Foundation, either version 3 of the License, or 
! (at your option) any later version.
!
! This program is distributed in the hope that it will be useful, but 
! WITHOUT ANY WARRANTY; without even the implied wdataarrayanty of MERCHANTABILITY 
! or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
! for more details.
!
! You should have received a copy of the GNU General Public License along 
! with this program. If not, see http://www.gnu.org/licenses.
! WARNING: THIS FILE IS PART OF THE UNMAINTAINED CONDA PACKAGE OF RUNNER 1.3. 
!######################################################################

      function getthreshold(itarget,ndim,dataarray)

      use fileunits

      implicit none

      integer itarget ! in, number of data point
      integer ndim ! in, dimension of dataarray
      integer i1

      real*8 dataarray(ndim) ! in, out
      real*8 getthreshold 
      real*8 ztemp

!      write(ounit,*)'Searching for data point ',itarget
 10   continue
      do i1=1,ndim-1
        if(dataarray(i1).gt.dataarray(i1+1))then
          ztemp=dataarray(i1)
          dataarray(i1)=dataarray(i1+1)
          dataarray(i1+1)=ztemp
          goto 10
        endif
      enddo

      getthreshold=dataarray(itarget)     
 
      return
      end











