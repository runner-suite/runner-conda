!######################################################################
! This routine is part of
! RuNNer - RuNNer Neural Network Energy Representation
! (c) 2008-2024 Prof. Dr. Joerg Behler 
! Ruhr-Universitaet Bochum, Germany
!
! This program is free software: you can redistribute it and/or modify it 
! under the terms of the GNU General Public License as published by the 
! Free Software Foundation, either version 3 of the License, or 
! (at your option) any later version.
!
! This program is distributed in the hope that it will be useful, but 
! WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
! or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
! for more details.
!
! You should have received a copy of the GNU General Public License along 
! with this program. If not, see http://www.gnu.org/licenses.
! WARNING: THIS FILE IS PART OF THE UNMAINTAINED CONDA PACKAGE OF RUNNER 1.3. 
!######################################################################
!! called by:
!! - predict.f90
!! - getstructure_mode3.f90
!!      
!! It is assumed that all inconsistencies in the input.data file
!! have been detected before by checkonestructure
!!
      subroutine readonestructure_mode3(num_atoms,&
      zelem,num_atoms_element,lattice,&
      totalcharge,totalenergy,&
      atomcharge,atomenergy,xyzstruct,atomspin,&
      totalforce,elementsymbol,lperiodic,lspin)
!!
      use fileunits
      use globaloptions
!!
      implicit none
!!
      integer nlattice                                                  ! internal 
      integer num_atoms                                                 ! out 
      integer zelem(max_num_atoms)                                      ! out 
      integer num_atoms_element(nelem)                                  ! out
      integer i                                                         ! internal 
                                                                       
      integer :: l                                                      ! internal Niko
      integer :: inputdata_n_prop                                       ! internal Niko
!!                                               
      real*8 lattice(3,3)                                               ! out 
      real*8 xyzstruct(3,max_num_atoms)                                 ! out
      real*8 totalforce(3,max_num_atoms)                                ! out
      real*8 atomcharge(max_num_atoms)                                  ! out
      real*8 atomenergy(max_num_atoms)                                  ! out
      real*8 totalcharge                                                ! out
      real*8 totalenergy                                                ! out 
      real*8 threshold                                                  ! internal 
      real*8 atomicchargesum                                            ! internal 
      real*8 atomicenergysum                                            ! internal

      real*8 atomspin(max_num_atoms)                                    ! Niko, out 
      real*8, target :: inputdata_prop_arr(max_num_prop, max_num_atoms) ! internal Niko, array of all real values in input.data
!!
      character*20 keyword                                              ! internal
      character*2 elementsymbol(max_num_atoms)                          ! out
                                                                        
      character*200 begin_line                                          ! internal Niko, string containing data of 'begin' line in input.data 
      character*400 atom_line                                           ! internal Niko, string containing data of 'atom' line in input.data
!!                                                                      
      logical lbegin                                                    ! internal, true if 'begin' flag of structure found
      logical lend                                                      ! internal, true if 'end' flag of structure found
      logical lcharge                                                   ! internal, true if 'charge' keyword is found
      logical lenergy                                                   ! internal, true if 'energy' keyword is found
      logical lperiodic                                                 ! out, true if 3 lattice vectors are found 
      logical loldinputdata                                             ! true if input.data format of RuNNer V1.X
      logical lspin                                                     ! internal, true if atomic spins are found

!! Niko       

      type propertyindexes                                              ! internal Niko, holds column index
        integer :: x_coord                                              ! of respeective property 
        integer :: y_coord                                              ! in input.data
        integer :: z_coord 
        integer :: elem_sym
        integer :: atom_charge
        integer :: atom_spin
        integer :: atom_energy
        integer :: x_force
        integer :: y_force
        integer :: z_force
      endtype propertyindexes

      type(propertyindexes) :: inputdata_col_indx
!!
      type propertypointer                                              ! internal Niko, pointer on the respective row,
        real*8, pointer :: x_coord(:)                                   ! which contains the data of the respective 
        real*8, pointer :: y_coord(:)                                   ! property in the inputdata arrays
        real*8, pointer :: z_coord(:)                                   ! (inputdata_prop and inputdata_elem_sym)
        real*8, pointer :: atom_charge(:)
        real*8, pointer :: atom_spin(:)
        real*8, pointer :: atom_energy(:)
        real*8, pointer :: x_force(:)
        real*8, pointer :: y_force(:)
        real*8, pointer :: z_force(:)
      endtype propertypointer

      type(propertypointer) :: inputdata_pointer 
!!
!!=====================================================
!! initializations
!!=====================================================
      nlattice             = 0
      lbegin               = .false.
      lend                 = .false.
      lcharge              = .false.
      lenergy              = .false.
      lperiodic            = .false.
      num_atoms            = 0
      threshold            = 0.000001d0
      atomicenergysum      = 0.0d0
      atomicchargesum      = 0.0d0
      lattice(:,:)         = 0.0d0
      zelem(:)             = 0
      xyzstruct(:,:)       = 0.0d0
      totalforce(:,:)      = 0.0d0
      elementsymbol(:)     = '  '
      num_atoms_element(:) = 0
      totalcharge          = 0.0d0
      totalenergy          = 0.0d0
      loldinputdata        = .false. 
      
!! Niko      
      inputdata_col_indx   = propertyindexes(0,0,0,0,0,0,0,0,0,0)
      nullify(inputdata_pointer%x_coord, inputdata_pointer%y_coord, &
              inputdata_pointer%z_coord, inputdata_pointer%atom_charge, &
              inputdata_pointer%atom_spin, inputdata_pointer%atom_energy, &
              inputdata_pointer%x_force, inputdata_pointer%y_force, &
              inputdata_pointer%z_force)
      inputdata_n_prop            = 0
      inputdata_prop_arr(:,:)     = 0.0d0
      atomspin(:)                 = 0.0d0
      atomcharge(:)               = 0.0d0
      atomenergy(:)               = 0.0d0
!!
      outer: do
        read(dataunit,*)keyword
!!       
        begin_routine: if((keyword.eq.'begin') .and. (lbegin .eqv..false.)) then 
          lbegin=.true. 
          lspin=.false.
          backspace(dataunit)
          read(dataunit, '(A200)') begin_line      ! Niko, begin line cannot be longer than 200 characters
          call readbeginline(begin_line, loldinputdata, & 
          inputdata_col_indx, inputdata_n_prop)
          
          if(loldinputdata.eqv..false.) then
            if(inputdata_col_indx%x_coord/=0) then 
              inputdata_pointer%x_coord     => inputdata_prop_arr(inputdata_col_indx%x_coord, :)  ! Niko, aligns pointers to respective 
            endif
            if(inputdata_col_indx%y_coord/=0) then
              inputdata_pointer%y_coord     => inputdata_prop_arr(inputdata_col_indx%y_coord, :)  ! target row of inputdata array based
            endif
            if(inputdata_col_indx%z_coord/=0) then
              inputdata_pointer%z_coord     => inputdata_prop_arr(inputdata_col_indx%z_coord, :)  ! on determined column index
            endif
            if(inputdata_col_indx%atom_charge/=0) then
              inputdata_pointer%atom_charge => inputdata_prop_arr(inputdata_col_indx%atom_charge, :)
            endif
            if(inputdata_col_indx%atom_spin/=0) then
              inputdata_pointer%atom_spin   => inputdata_prop_arr(inputdata_col_indx%atom_spin, :)
              lspin=.true.
            endif
            if(inputdata_col_indx%atom_energy/=0) then
              inputdata_pointer%atom_energy => inputdata_prop_arr(inputdata_col_indx%atom_energy, :)
            endif
            if(inputdata_col_indx%x_force/=0) then
              inputdata_pointer%x_force     => inputdata_prop_arr(inputdata_col_indx%x_force, :)
            endif
            if(inputdata_col_indx%y_force/=0) then  
              inputdata_pointer%y_force     => inputdata_prop_arr(inputdata_col_indx%y_force, :)
            endif  
            if(inputdata_col_indx%z_force/=0) then
              inputdata_pointer%z_force     => inputdata_prop_arr(inputdata_col_indx%z_force, :)
            endif  
          else 
            continue      
          endif
        endif begin_routine

        if(keyword.eq.'lattice') then
          nlattice=nlattice+1
          backspace(dataunit)
          read(dataunit,*)keyword,(lattice(nlattice,i),i=1,3)
        endif

        if((keyword.eq.'atom').and.(loldinputdata.eqv..false.)) then
          num_atoms=num_atoms+1
          backspace(dataunit)
          if(inputdata_col_indx%elem_sym /= 1) then                           ! Niko, if element symbol not in first column
            read(dataunit, *)keyword, &
            inputdata_prop_arr(:(inputdata_col_indx%elem_sym-1), num_atoms), &  
            elementsymbol(num_atoms), &                                       ! Niko elem_sym already in final array 
            inputdata_prop_arr((inputdata_col_indx%elem_sym+1):inputdata_n_prop,&
            num_atoms)                                                        ! otherwise mismatched type (real and character)
          elseif(inputdata_col_indx%elem_sym.eq.1) then                       ! Niko, if element symbol in first column
            read(dataunit, *)keyword, &
            elementsymbol(num_atoms), &
            inputdata_prop_arr((inputdata_col_indx%elem_sym+1):inputdata_n_prop,&
            num_atoms)
          endif
          if((inputdata_col_indx%x_coord /= 0).and. & 
          (inputdata_col_indx%y_coord /= 0)  .and. &
          (inputdata_col_indx%z_coord /= 0)) then
            xyzstruct(1, num_atoms)  = inputdata_pointer%x_coord(num_atoms)      ! Niko, write into old arrays from pointers 
            xyzstruct(2, num_atoms)  = inputdata_pointer%y_coord(num_atoms) 
            xyzstruct(3, num_atoms)  = inputdata_pointer%z_coord(num_atoms)
          endif 
          if(inputdata_col_indx%atom_charge /= 0) then
            atomcharge(num_atoms)    = inputdata_pointer%atom_charge(num_atoms)
          endif
          if(inputdata_col_indx%atom_spin /= 0) then
            atomspin(num_atoms)      = inputdata_pointer%atom_spin(num_atoms)
          endif  
          if(inputdata_col_indx%atom_energy /= 0) then
            atomenergy(num_atoms)    = inputdata_pointer%atom_energy(num_atoms)
          endif  
          if((inputdata_col_indx%x_force /= 0).and. & 
          (inputdata_col_indx%y_force /= 0) .and. &
          (inputdata_col_indx%z_force /= 0)) then      
            totalforce(1, num_atoms) = inputdata_pointer%x_force(num_atoms)
            totalforce(2, num_atoms) = inputdata_pointer%y_force(num_atoms)
            totalforce(3, num_atoms) = inputdata_pointer%z_force(num_atoms)
          endif     

          call nuccharge(elementsymbol(num_atoms),zelem(num_atoms))

        elseif((keyword.eq.'atom').and.(loldinputdata.eqv..true.)) then
          num_atoms=num_atoms+1
          backspace(dataunit)
          read(dataunit,*)keyword,(xyzstruct(i,num_atoms),i=1,3),&
              elementsymbol(num_atoms),atomcharge(num_atoms),&
              atomenergy(num_atoms),(totalforce(i,num_atoms),i=1,3)

          call nuccharge(elementsymbol(num_atoms),zelem(num_atoms))
        endif
        if(keyword.eq.'charge') then
          backspace(dataunit)
          if(lcharge) then
            write(*,*)'Error: 2 total charges given for structure'
            stop
          endif 
          read(dataunit,*) keyword,totalcharge
          lcharge=.true.
        endif
        if(keyword.eq.'energy') then
          backspace(dataunit)
          if(lenergy) then
            write(*,*)'Error: 2 total energies given for structure'
            stop
          endif
          read(dataunit,*) keyword,totalenergy
          lenergy=.true.
        endif
        if((keyword.eq.'end').and.(lbegin)) then
          lend=.true. 
          exit
        elseif(keyword.ne.'end') then
          continue
        endif      
      
      enddo outer

      if(nlattice.eq.3) then
         lperiodic=.true.
      endif

!!
!!=====================================================
!! translate atoms back into cell if necessary
!!=====================================================
      if(lperiodic)then
        call translate(num_atoms,lattice,xyzstruct)
      endif
!!
!!=====================================================
!! count the number of atoms of each element
!!=====================================================
      do i=1,num_atoms
        num_atoms_element(elementindex(zelem(i)))=&
           num_atoms_element(elementindex(zelem(i)))+1
      enddo
!!
      return
!!=============================================================================
      nullify(inputdata_pointer%x_coord, inputdata_pointer%y_coord, &
              inputdata_pointer%z_coord, inputdata_pointer%atom_charge, &
              inputdata_pointer%atom_spin, inputdata_pointer%atom_energy, &
              inputdata_pointer%x_force, inputdata_pointer%y_force, &
              inputdata_pointer%z_force)
!!
      end subroutine
