!######################################################################
! This routine is part of
! RuNNer - RuNNer Neural Network Energy Representation
! (c) 2008-2024 Prof. Dr. Joerg Behler
! Ruhr-Universitaet Bochum, Germany
!
! This program is free software: you can redistribute it and/or modify it 
! under the terms of the GNU General Public License as published by the 
! Free Software Foundation, either version 3 of the License, or 
! (at your option) any later version.
!
! This program is distributed in the hope that it will be useful, but 
! WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
! or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
! for more details.
!
! You should have received a copy of the GNU General Public License along 
! with this program. If not, see http://www.gnu.org/licenses.
! WARNING: THIS FILE IS PART OF THE UNMAINTAINED CONDA PACKAGE OF RUNNER 1.3. 
!######################################################################
      module predictionoptions 

      implicit none

      integer enforcetotcharge
      integer nn_type_vdw

      real*8 vdw_screening(3)
      real*8 vdw_cutoff
      real*8, dimension(:,:) , allocatable :: vdw_coefficients
      real*8, dimension(:) , allocatable :: vdw_radius
      real*8 melem(108) ! atomic masses of elements (required in normal mode calculation)

      ! Hessian-related arrays are declared here
      real*8, dimension(:,:), allocatable :: nnhessian
      real*8, dimension(:), allocatable :: nnfrequencies
      real*8, dimension(:,:), allocatable :: nnnormalmodes

      logical ldoforces
      logical ldohessian
      logical lwritehessian ! Emir
      logical lcalculatefrequencies !Emir
      logical lcalculatenormalmodes !Emir
      logical lpreparemd
      logical lprintforcecomponents
      logical lwritesymfunctions
      logical lvdw

      logical lcustom_output_order !Niko
 
      type :: outpropertyindexes               ! internal Niko, holds column index
        integer :: x_coord                     ! of respeective property 
        integer :: y_coord                     ! in input.data
        integer :: z_coord 
        integer :: elem_sym
        integer :: atom_charge
        integer :: atom_spin
        integer :: atom_energy
        integer :: x_force
        integer :: y_force
        integer :: z_force
      endtype outpropertyindexes
      type(outpropertyindexes) :: output_col_indx
      
      integer :: output_n_prop
      integer :: output_max_prop
      end module predictionoptions 

