!######################################################################
! This routine is part of
! RuNNer - RuNNer Neural Network Energy Representation
! (c) 2008-2024 Prof. Dr. Joerg Behler 
! Ruhr-Universitaet Bochum, Germany
!
! This program is free software: you can redistribute it and/or modify it 
! under the terms of the GNU General Public License as published by the 
! Free Software Foundation, either version 3 of the License, or 
! (at your option) any later version.
!
! This program is distributed in the hope that it will be useful, but 
! WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
! or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
! for more details.
!
! You should have received a copy of the GNU General Public License along 
! with this program. If not, see http://www.gnu.org/licenses.
! WARNING: THIS FILE IS PART OF THE UNMAINTAINED CONDA PACKAGE OF RUNNER 1.3. 
!######################################################################
!! called by:
!! - geterror.f90
!! - geterrorpair.f90
!!

! this name is rather misleading as no ewaldsummation is performed, no energies are calcualted and nothing is parallel
! WHY!?!?!?!
      subroutine ewaldenergies_para_type5(nstruct,&
        num_atoms_mpi,zelem_mpi,ncharges,&
        minvalue_elec,maxvalue_elec,avvalue_elec,&
        symfunctione_mpi,xyzstruct_mpi,&
        rmse_charge,rmse_totalcharge,&
        mad_charge,mad_totalcharge,&
        atomcharge_mpi,nnatomcharge_mpi,&
        totalcharge_mpi,nnchargesum_mpi,&
        maxerror_charge,imaxerror_charge,&
        lperiodic_mpi, lattice_mpi)
!!
      use fileunits
      use globaloptions 
      use symfunctions
      use nnewald
      use nnflags
      use fittingoptions
      use eem
!!
      implicit none
!!
      integer nstruct                                    ! in
      integer num_atoms_mpi(nstruct)                     ! in
      integer zelem_mpi(nstruct,max_num_atoms)           ! in
      integer ncharges                                   ! out
      integer ndummy                                     ! internal
      integer i1,i2                                      ! internal 
      integer imaxerror_charge                           ! out 
!!
      real*8 minvalue_elec(nelem,maxnum_funcvalues_elec) ! in
      real*8 maxvalue_elec(nelem,maxnum_funcvalues_elec) ! in
      real*8 avvalue_elec(nelem,maxnum_funcvalues_elec)  ! in
      real*8 symfunctione_mpi(maxnum_funcvalues_elec,max_num_atoms,nstruct)   ! in/out
      real*8 atomhardness_mpi(nstruct,max_num_atoms)
      real*8 xyzstruct_mpi(3,max_num_atoms,nstruct)      ! in
      real*8 rmse_charge                                 ! out
      real*8 rmse_totalcharge                            ! out
      real*8 mad_charge                                  ! out
      real*8 mad_totalcharge                             ! out
      real*8 nnatomcharge_mpi(nstruct,max_num_atoms)     ! out
      real*8 totalcharge_mpi(nstruct)                    ! in
      real*8 atomcharge_mpi(nstruct,max_num_atoms)       ! in
      real*8 nnatomchi_mpi(nstruct,max_num_atoms)        ! internal
      real*8 nnchargesum_mpi(nstruct)                    ! out
!! symmetry function parameters
      real*8 maxerror_charge                             ! in/out
      logical, intent(in) :: lperiodic_mpi(nstruct)
      real*8, intent(in) :: lattice_mpi(3,3,nstruct)

!!
!!
!! initializations
!!
! scale the symmetry functions for the charge prediction
        call scalesym(nelem,nstruct,nstruct,&
          maxnum_funcvalues_elec,num_funcvalues_elec,num_atoms_mpi,&
          zelem_mpi,symfunctione_mpi,&
          minvalue_elec,maxvalue_elec,avvalue_elec,&
          scmin_elec,scmax_elec)
!! calculate the charges on the atoms
        call getcharges_type5(nstruct,nstruct,&
          xyzstruct_mpi,zelem_mpi,num_atoms_mpi,&
          symfunctione_mpi,totalcharge_mpi,nnatomcharge_mpi, &
          lperiodic_mpi, lattice_mpi)

!!
!! calculate the RMSE for the atomic charges and the total charge
        call calcrmse_charge(nstruct,nstruct,ncharges,&
          zelem_mpi,num_atoms_mpi,rmse_charge,mad_charge,&
          totalcharge_mpi,rmse_totalcharge,mad_totalcharge,&
          atomcharge_mpi,nnatomcharge_mpi,nnchargesum_mpi,&
          maxerror_charge,imaxerror_charge)
!!
      return
      end
