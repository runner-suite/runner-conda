! Created by Jonas A. Finkler on 2/7/20.

module eem
    use precision
    implicit none

    real(dp), parameter :: PI = 4._dp * atan (1.0_dp)

    contains


    ! DEFINITION: Energy = 1/2 * q^t * A * q + q^t * Chi + E_0
    ! Energy = E_{el} + q^t**2 * hardness + q^t * Chi + E_0
    ! A_{ij} is therefore d^2 E / dq_i / dq_j
    ! unlike in CENT sigma is the std. dev. of the Gaussians
    ! sigma = sqrt(2) * sigma_{CENT}
    ! Please note: althought E = 1/2 q^t * A * q can be used to calculate the electrostatic energy (in perdiodic and non-periodic case)
    ! Direct Ewald-summation should be preffered if possible in the periodic case,
    ! because the computational complexity is smaller (see notes to AmatrixRecip in ewaldSummation.f90)
    ! If A had been calculated already anyway, the of course it should also be used to calculate the energy

    ! IMPORTANT: only lower half is calculated because the matrix is symmetric
    ! unlike in CENT sigma is the std. dev. of the Gaussians
    subroutine eemMatrix(nAt, pos, sigma, hardness, A)
        implicit none
        integer, intent(in) :: nAt            ! number of atoms
        real(dp), intent(in) :: pos(3, nAt)     ! xyz coordinates
        real(dp), intent(in) :: hardness(nAt)   ! harndess
        real(dp), intent(in) :: sigma(nAt)
        real(dp), intent(out) :: A(nAt, nAt)    ! resulting matrix
        real(dp) :: rij, gamma
        integer :: i, j


        ! build the A_{ij} matrix. It is symmetric -> only need lower half
        do i=1,nAt
            do j=1,i-1
                rij = sqrt(sum((pos(:,i) - pos(:,j))**2))
                if (sigma(1) > 0._dp) then
                    gamma = sqrt(sigma(i)**2 + sigma(j)**2)
                    A(i,j) = erf(rij / (sqrt(2._dp) * gamma)) / rij
                else
                    A(i,j) = 1._dp / rij
                endif
                A(j,i) = A(i,j)
            enddo
            if (sigma(1) > 0._dp) then
                A(i,i) = hardness(i) + 1._dp / sigma(i) / sqrt(PI)
            else
                A(i,i) = hardness(i)
            endif
        enddo
    end subroutine

    subroutine eemCharges(nAt, A, Xi, Qtot, Q)
        implicit none
        integer, intent(in) :: nAt                ! number of atoms
        real(dp), intent(in) :: A(nAt,nAt)          ! A matrix
        real(dp), intent(in) :: Xi(nAt)             ! electronegativities
        real(dp), intent(in) :: Qtot                ! total charge of the system
        real(dp), intent(out) :: Q(nAt)             ! resulting partial charges
        real(dp) :: lm

        call eemChargesAndLm(nAt, A, Xi, Qtot, Q, lm)

    end subroutine

    ! solves A * q = -Chi (with Lagrange multiplier to ensure that sum q = qtot)
    subroutine eemChargesAndLm(nAt, A, Xi, Qtot, Q, lm)
        implicit none
        integer, intent(in) :: nAt                ! number of atoms
        real(dp), intent(in) :: A(nAt,nAt)          ! A matrix
        real(dp), intent(in) :: Xi(nAt)             ! electronegativities
        real(dp), intent(in) :: Qtot                ! total charge of the system
        real(dp), intent(out) :: Q(nAt)             ! resulting partial charges
        real(dp), intent(out) :: lm
        real(dp) :: M(nAt+1, nAt+1)
        real(dp) :: b(nAt + 1)


        M(:nat, :nat) = A(:,:)
        M(nAt + 1,:) = 1._dp
        M(:, nAt + 1) = 1._dp
        M(nAt + 1, nAt + 1) = 0._dp

        ! b is right hand side of eq. system
        b(:nAt) = -Xi(:)
        b(nAt + 1) = qTot

        call solveSymEqSystems(1, nat + 1, M, b)

        ! the solution is returned in the rhs vector
        Q(:) = b(:nAt)
        lm = b(nat+1)

    end subroutine

    ! solves sum_j A_ij dq_j/dChi_k = -delta_ik (with Lagrange mutiplier to ensure sum_i dq_i/dChi_j = 0)
    ! dQ(i,j) = dQ_i / dChi_j
    subroutine eemdQdChi(nAt, A, dQdChi)
        implicit none
        integer, intent(in) :: nat            ! number of atoms
        real(dp), intent(in) :: A(nat, nat)     ! A matrix
        real(dp), intent(out) :: dQdChi(nat, nat)   ! dQ_i / dChi_j | partial derivatives wrt. electronegativities

        real(dp) :: M(nAt+1, nAt+1), b(nAt+1,nAt)
        integer :: i

        M(nAt+1,:) = 1._dp
        M(:,nAt+1) = 1._dp
        M(nAt+1,nAt+1) = 0._dp
        M(:nAt,:nAt) = A(:,:)

        b(:,:) = 0._dp
        do i=1,nat
            b(i,i) = -1._dp
        enddo

        call solveSymEqSystems(nat, nat + 1, M, b)

        dQdChi(:,:) = b(:nAt,:)
    end subroutine

    ! todo: this could be done solving only n_element linear eq. systems
    ! calculates the partial derivatives of the charges with respect to the hardness
    ! IMPORTANT: before using this check if eemdQdChi is calculated
    ! ===>>> dQ_i/dJ_j = Q_j * dQ_i/dChi_j
    ! solves sum_j A_ij dq_j/dJ_k = -delta_ik*q_k (with Lagrange mutiplier to ensure sum_i dq_i/dChi_j = 0)
    subroutine eemdQdHardness(nAt, A, Q, dJ)
        implicit none
        integer, intent(in) :: nat                  ! number of atoms
        real(dp), intent(in) :: A(nat, nat)           ! A matrix
        real(dp), intent(in) :: Q(nat)                ! Charges that have been calculated using A
        real(dp), intent(out) :: dJ(nat, nat)         ! dQ_i / dHarndess_j

        real(dp) :: M(nAt+1, nAt+1), b(nAt+1, nAt)
        integer :: i

        M(nAt+1,:) = 1._dp
        M(:,nAt+1) = 1._dp
        M(nAt+1,nAt+1) = 0._dp
        M(:nAt,:nAt) = A(:,:)

        b(:,:) = 0._dp
        do i=1,nat
            b(i,i) = -Q(i)
        enddo

        call solveSymEqSystems(nat, nat + 1, M, b)

        dJ(:,:) = b(:nAt,:)
    end subroutine

    ! calculates the product of dAdxyz with vector Q (could be anything, does not have to be charge)
    ! takes advantage of the fact that dAdxyz is very sparse and avoid explicit calculation to save memory
    subroutine eemdAdxyzTimesQ(nat, ats, Q, sigma, dAdxyzQ)
        integer, intent(in) :: nat
        real(dp), intent(in) :: ats(3,nat), Q(nat), sigma(nat)
        real(dp), intent(out) ::  dAdxyzQ(nat, 3, nat)

        integer :: i,k
        real(dp) :: d(3), ddx(3), rij, gamma, tg, fi

        dAdxyzQ(:,:,:) = 0._dp
        ! calculate dA/dx * Q without explicit calculation of dA/dx because of memory and because it is very sparse
        ! has been tested -> result is correct
        do i=1,nat
            ! loop over columns of dA
            do k=i+1,nat
                d(:) = ats(:,i) - ats(:,k)
                rij = sqrt(sum(d**2))
                if (sigma(1)>0) then
                    gamma = sqrt(sigma(i)**2 + sigma(k)**2)
                    tg = 1._dp / (sqrt(2._dp) * gamma)
                    fi = (2._dp * tg * exp(-tg**2 * rij**2) / (sqrt(PI) * rij) - erf(tg * rij) / rij**2)
                else
                    fi = -1._dp / rij**2
                endif
                ddx(:) = d(:) / rij * fi ! this is dA_{ik} / dx_i
                dAdxyzQ(i, :, i) = dAdxyzQ(i, :, i) + ddx(:) * Q(k)
                dAdxyzQ(k, :, k) = dAdxyzQ(k, :, k) - ddx(:) * Q(i)
                dAdxyzQ(k, :, i) = ddx(:) * Q(i)
                dAdxyzQ(i, :, k) = -ddx(:) * Q(k)
            enddo
        enddo

    end subroutine

    ! todo: replace all calls to this subroutine
    subroutine eemdQdxyz(nat, Q, sigma, A, dAdxyzQ, dChidxyz, dQdxyz)
        integer, intent(in) :: nAt
        real(dp), intent(in) :: Q(nAt)
        real(dp), intent(in) :: sigma(nAt)
        real(dp), intent(in) :: A(nAt,nAt)
        real(dp), intent(in) :: dAdxyzQ(nat,3,nat)
        real(dp), intent(in) :: dChidxyz(nAt, 3, nat)
        real(dp), intent(out) :: dQdxyz(nAt, 3, nat) ! todo: order of indices
        real(dp) :: M(nAt+1, nAt+1)
        real(dp) :: b(nAt+1, 3*nat)
        real(dp) :: c(nAt)

        integer :: i, ixyz
        real(dp) :: rij, d(3), ddx(3)

        M(:nat,:nat) = A(:,:)

        b(:nat,:) = reshape(-dChidxyz, [nat, 3*nat]) - reshape(dAdxyzQ, [nat, 3*nat]) ! b = -dX/dx - dA/dx * Q

        ! dQtot / dx = 0
        M(nat+1,:) = 1._dp
        M(:,nat+1) = 1._dp
        M(nat+1,nat+1) = 0._dp
        b(nat+1,:) = 0._dp

        call solveSymEqSystems(3 * nat, nat + 1, M, b)

        ! the solution is returned in the rhs vector
        dQdxyz(:,:,:) = reshape(b(:nat,:), [nat, 3, nat])

        ! write(*,*) 'WARNING: eemdQdxyz is very inefficient and should not be used. Use eemdQdQTimesdQdx or calcTotalForcesEEM insted!'

    end subroutine

    ! calculates eem forces (the ones coming from charge transfer (sum_j dE/dQ_j dQ_j/dx)
    ! using trick from J.Chem.TheoryComput.2019, 15, 6213−6224
    ! avoid solving 3N eq systems using lagrange multipliers
    subroutine calcPartialForcesEEM(nat, A, dAdxyzQ, dEdQ, dChidxyz, Q, f)
        implicit none
        integer, intent(in) :: nat
        real(dp), intent(in) :: A(nat,nat)
        real(dp), intent(in) :: dAdxyzQ(nat, 3, nat)
        real(dp), intent(in) :: dEdQ(nat)               ! partial derivative of total energy w.r.t charges
        real(dp), intent(in) :: dChidxyz(nat, 3, nat)
        real(dp), intent(in) :: Q(nat)
        real(dp), intent(out) :: f(3,nat)

        call eemdEdQTimesdQdx(nat, A, dAdxyzQ, dEdQ, dChidxyz, Q, f)
        f(:,:) = -f(:,:)

    end subroutine

    ! todo: this is a bit of a replication.
    ! In principle the force trick can be used for any derivative, doesnt have to be wrt positions.
    ! If anyone ever implements something like that, please generalize these routines here. :)
    subroutine calcPartialLatticeDerivativesEEM(nat, A, dAdlatQ, dEdQ, dChidlat, Q, dEdLat)
        implicit none
        integer, intent(in) :: nat
        real(dp), intent(in) :: A(nat,nat)
        real(dp), intent(in) :: dAdlatQ(nat, 3, 3)
        real(dp), intent(in) :: dEdQ(nat)               ! partial derivative of total energy w.r.t charges
        real(dp), intent(in) :: dChidlat(nat, 3, 3)
        real(dp), intent(in) :: Q(nat)
        real(dp), intent(out) :: dEdLat(3,3)
        real(dp) :: MM(nat+1, nat+1)
        real(dp) :: lm(nat+1)                           ! the lagrange multipliers
        integer :: i, j

        MM(:nat,:nat) = A(:,:)
        MM(nat+1,:nat) = 1._dp
        MM(:nat,nat+1) = 1._dp
        MM(nat+1,nat+1) = 0._dp
        lm(:nat) = -dEdQ(:)
        lm(nat+1) = 0._dp

        call solveSymEqSystems(1, nat+1, MM, lm)

        do i=1,3
            do j=1,3
                dEdLat(j,i) = sum(lm(:nat) * dAdlatQ(:, j, i)) + sum(lm(:nat) * dChidlat(:, j, i))
            end do
        end do

    end subroutine

    ! calculates v_j = sum_i dQ_i/dx_j * v_i
    ! usually used for the force, but it doesnt have to be. dEdQ can be any vector
    ! using trick from J.Chem.TheoryComput.2019, 15, 6213−6224
    subroutine eemdEdQTimesdQdx(nat, A, dAdxyzQ, v, dChidxyz, Q, dQdxV)
        implicit none
        integer, intent(in) :: nat
        real(dp), intent(in) :: A(nat,nat)
        real(dp), intent(in) :: dAdxyzQ(nat, 3, nat)
        real(dp), intent(in) :: v(nat)               ! partial derivative of total energy w.r.t charges
        real(dp), intent(in) :: dChidxyz(nat, 3, nat)
        real(dp), intent(in) :: Q(nat)
        real(dp), intent(out) :: dQdxV(3,nat)

        real(dp) :: MM(nat+1, nat+1)
        real(dp) :: lm(nat+1)                           ! the lagrange multipliers
        integer :: i, iat

        MM(:nat,:nat) = A(:,:)
        MM(nat+1,:nat) = 1._dp
        MM(:nat,nat+1) = 1._dp
        MM(nat+1,nat+1) = 0._dp
        lm(:nat) = -v(:)
        lm(nat+1) = 0._dp

        call solveSymEqSystems(1, nat+1, MM, lm)

        do i=1,nat
            do iat=1,3
                dQdxV(iat,i) = sum(lm(:nat) * dAdxyzQ(:, iat, i)) + sum(lm(:nat) * dChidxyz(:, iat, i))
            enddo
        enddo

    end subroutine

    ! todo: use the force trick to only solve 1 instead of 9 equations
    ! using the trick is probably more efficient, but at least the number of equations is constant.
    subroutine eemdQdlat(nat, A, dAdlatQ, dChidlat, dQdlat)
        integer, intent(in) :: nAt
        real(dp), intent(in) :: A(nAt,nAt)
        real(dp), intent(in) :: dAdlatQ(nat,3,3)
        real(dp), intent(in) :: dChidlat(nAt, 3, 3)
        real(dp), intent(out) :: dQdlat(nAt, 3, 3)
        real(dp) :: M(nAt+1, nAt+1)
        real(dp) :: b(nAt+1, 3*3)

        M(:nat,:nat) = A(:,:)

        b(:nat,:) = reshape(-dChidlat, [nat, 3*3]) - reshape(dAdlatQ, [nat, 3*3]) ! b = -dX/dx - dA/dx * Q

        ! dQtot / dx = 0
        M(nat+1,:) = 1._dp
        M(:,nat+1) = 1._dp
        M(nat+1,nat+1) = 0._dp
        b(nat+1,:) = 0._dp

        call solveSymEqSystems(3 * 3, nat + 1, M, b)

        ! the solution is returned in the rhs vector
        dQdlat(:,:,:) = reshape(b(:nat,:), [nat, 3, 3])

    end subroutine


    subroutine electrostatics(nat, ats, q, sigma, e, f)
        integer, intent(in) :: nat
        real(dp), intent(in) :: ats(3,nat)
        real(dp), intent(in) :: q(nat)
        real(dp), intent(in) :: sigma(nat)
        real(dp), intent(out) :: e, f(3,nat)
        integer :: i, j
        real(dp) :: rij, gamma, dx(3), fi, tg, ei

        e = 0._dp
        f = 0._dp

        do i=1,nat
            do j=i+1, nat
                dx = ats(:,i) - ats(:,j)
                rij = sqrt(sum(dx**2))
                if (sigma(1) > 0._dp) then
                    gamma = sqrt(sigma(i)**2 + sigma(j)**2)
                    tg = 1._dp / (sqrt(2._dp) * gamma)
                    fi = q(i) * q(j) * (2._dp * tg * exp(-tg**2 * rij**2) / (sqrt(PI) * rij) - erf(tg * rij) / rij**2) / rij
                    ei = q(i) * q(j) * erf(rij * tg) / rij
                else
                    ei = q(i) * q(j) / rij
                    fi = -q(i) * q(j) / rij**3
                endif
                e = e + ei
                f(:,i) = f(:,i) - dx(:) * fi
                f(:,j) = f(:,j) + dx(:) * fi
            enddo
            if (sigma(1) > 0._dp) then
                e = e + q(i)**2 / sigma(i) / sqrt(PI) / 2.d0
            endif
        enddo

    end subroutine electrostatics


    subroutine solveSymEqSystems(neq, n, A, x)
        implicit none
        integer, intent(in) :: neq, n
        real(dp), intent(in) :: A(n,n)
        real(dp), intent(inout) :: x(n, neq)
        integer :: IPIV(n), info, lwork
        real(dp), allocatable :: work(:)
        real(dp) :: nwork
        real(dp) :: M(n,n)

        M(:,:) = A(:,:)

        ! this will get us nwork, the optimal size of the working array
        call dsysv('L', n, neq, M, n, IPIV, x, n, nwork,   -1, info)
        ! allocate working memory
        LWORK = int(nwork)
        allocate(WORK(LWork))
        ! solve equations
        call dsysv('L', n, neq, M, n, IPIV, x, n, WORK, LWORK, info)
        deallocate(WORK)
        if (info /= 0) then
            stop 'Error: could not solve linear equation system'
            ! call tracebackqq()
        endif
    end subroutine

end module eem
