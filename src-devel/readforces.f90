!######################################################################
! This routine is part of
! RuNNer - RuNNer Neural Network Energy Representation
! (c) 2008-2024 Prof. Dr. Joerg Behler 
! Ruhr-Universitaet Bochum, Germany
!
! This program is free software: you can redistribute it and/or modify it 
! under the terms of the GNU General Public License as published by the 
! Free Software Foundation, either version 3 of the License, or 
! (at your option) any later version.
!
! This program is distributed in the hope that it will be useful, but 
! WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
! or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
! for more details.
!
! You should have received a copy of the GNU General Public License along 
! with this program. If not, see http://www.gnu.org/licenses.
! WARNING: THIS FILE IS PART OF THE UNMAINTAINED CONDA PACKAGE OF RUNNER 1.3. 
!######################################################################
!! Multipurpose subroutine

!! called by: 
!! - fitting.f90
!! - fitting_batch.f90
!! - fittingpair.f90
!! - geterror.f90
!! - geterrorpair.f90
!!
      subroutine readforces(unit,npoints,force_list_local)
!!
      use globaloptions
      use structures
!! don't use module fileunits here, because unit can have several values
!!
      implicit none
!!
      integer unit                                               ! in
      integer npoints                                            ! in
      integer dummy                                              ! internal
      integer i1,i2,i3                                           ! internal
!!
      real*8 force_list_local(3,max_num_atoms,nblock)
!!       
      force_list_local(:,:,:)=0.0d0
!!
      do i1=1,npoints
        read(unit,*)dummy
        do i2=1,num_atoms_list(i1)
          read(unit,*)(force_list_local(i3,i2,i1),i3=1,3)
!          write(*,*) force_list_local(:,i2,i1)
        enddo
      enddo ! i1 
!!
      return
      end
