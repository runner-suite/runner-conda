subroutine MinimaHopping()
        use fileunits
        use nnflags
        use globaloptions
        use symfunctions
        use timings
        use nnewald
        use nnshort_atomic
        use nnshort_pair
        use predictionoptions
        use energyAndForces3G
        use energyAndForces4G


        implicit none

        ! in (nnStuff)
        integer :: zelem(max_num_atoms)
        integer :: num_atoms_element(nelem)
        integer :: dummy
        integer :: num_atoms
        integer :: i1,i2,istep     
    
        real*8 :: lattice(3,3)
        real*8 minvalue_short_atomic(nelem,maxnum_funcvalues_short_atomic)  ! internal
        real*8 maxvalue_short_atomic(nelem,maxnum_funcvalues_short_atomic)  ! internal
        real*8 avvalue_short_atomic(nelem,maxnum_funcvalues_short_atomic)   ! internal
        real*8 minvalue_elec(nelem,maxnum_funcvalues_elec)     ! internal
        real*8 maxvalue_elec(nelem,maxnum_funcvalues_elec)     ! internal
        real*8 avvalue_elec(nelem,maxnum_funcvalues_elec)      ! internal
        real*8 minvalue_short_pair(npairs,maxnum_funcvalues_short_pair)  ! internal
        real*8 maxvalue_short_pair(npairs,maxnum_funcvalues_short_pair)  ! internal
        real*8 avvalue_short_pair(npairs,maxnum_funcvalues_short_pair)   ! internal
        real*8 minvalue_charge(nelem) ! internal 
        real*8 maxvalue_charge(nelem) ! internal
        real*8 avvalue_charge(nelem)
        real*8 chargemin(nelem)
        real*8 chargemax(nelem)
        real*8 :: xyzstruct(3,max_num_atoms)
        real*8 :: atomspin(max_num_atoms)
        real*8 :: atomcharge(max_num_atoms) 
        real*8 :: atomenergy(max_num_atoms)
        real*8 :: totalcharge
        real*8 :: totalenergy
        real*8 :: nnatomcharge(max_num_atoms)
        real*8 :: nntotalenergy
        real*8 :: nntotalforces(3,max_num_atoms)
        real*8 :: eshortmin
        real*8 :: eshortmax
        real*8 :: totalforces(3,max_num_atoms)
        real*8 :: rdummy
        real*8 sens(nelem,maxnum_funcvalues_short_atomic)
        real*8 nnstress_short(3,3)
        real*8 atomenergysum
        real*8 nnshortenergy
        real*8 nnatomenergy(max_num_atoms)
        character*2 :: elementsymbol(max_num_atoms)
        character*40 :: filename
        logical :: lperiodic
        logical :: lspin

        ! local
        real*8,parameter :: ang2bohr = 1.88973d0
        integer :: count_min 
        integer :: n_structmin
        real*8 :: xyzstruct_new(3,max_num_atoms),xyzstruct_geoopt(3,max_num_atoms),velocity(3,max_num_atoms)
        real*8 :: nnforces(3,max_num_atoms),nnforces_new(3,max_num_atoms)  
        real*8 :: energy_latest, energy_current,energy_geoopt
        real*8 :: temperature=30.0d0
        real*8 :: atomic_mass(max_num_atoms)
        real*8 :: masstemp
        real*8 :: min_energy_list(100) !! assuming 1000 min configuraturations
        real*8 :: beta1, beta2,beta3
        real*8 :: alpha1,alpha2
        real*8 :: Ediff1,Ediff2

!!=====================================================================
!! initialization and allocate the variables for NN
!!=====================================================================
        nnatomenergy(:)=0.0d00
        nnshortenergy=0.0d0
        atomenergysum=0.0d0
        sens(:,:)=0.0d0
        nnstress_short(:,:)=0.0d0
        atomenergysum=0.0d0
        atomcharge(:)=0.0d0
        atomenergy(:) =0.0d0
        totalcharge = 0.0d0
        totalenergy = 0.0d0
        totalforces(:,:)=0.0d0
        minvalue_short_atomic(:,:)=0.0d0
        maxvalue_short_atomic(:,:)=0.0d0
        avvalue_short_atomic(:,:)=0.0d0
        chargemin(:)=0.0d0
        chargemax(:)=0.0d0
        minvalue_short_pair(:,:)=0.0d0
        maxvalue_short_pair(:,:)=0.0d0
        avvalue_short_pair(:,:)=0.0d0
        minvalue_elec(:,:)=0.0d0
        maxvalue_elec(:,:)=0.0d0
        avvalue_elec(:,:)=0.0d0
        minvalue_charge(:)=0.0d0
        maxvalue_charge(:)=0.0d0
        avvalue_charge(:)=0.0d0
!!=====================================================================
!! initialization of parameters for Minima hopping
!!=====================================================================
        beta1=1.05d0
        beta2=1.05d0
        beta3=1.0d0/1.05d0
        alpha1=1.02d0
        alpha2=1.0d0/1.02d0
        min_energy_list(:)=0.0d0
        count_min=1
        n_structmin=100
        Ediff1=0.000001d0
        Ediff2=1.0d0
!!=====================================================================
!! reading the weights and scaling file
!!=====================================================================
        call initmode3(1,&
            minvalue_short_atomic,maxvalue_short_atomic,avvalue_short_atomic,&
            minvalue_short_pair,maxvalue_short_pair,avvalue_short_pair,&
            minvalue_elec,maxvalue_elec,avvalue_elec,&
            minvalue_charge,maxvalue_charge,avvalue_charge,& 
            eshortmin,eshortmax,chargemin,chargemax)

!!=====================================================================
!! read structure and derive all structure related data, also mpi
!distribution
!!=====================================================================
        call getstructure_mode3(1,num_atoms,dummy,zelem,&
            num_atoms_element,lattice,xyzstruct,atomspin,&
            totalenergy,totalcharge,totalforces,atomenergy,atomcharge,&
            elementsymbol,lperiodic,lspin)

!!=====================================================================
!! get energies and forces
!!=====================================================================
        if(nn_type_short.eq.3)then
            call get_energies_and_forces(num_atoms,zelem,lattice,&
              xyzstruct,atomspin,totalcharge,lperiodic,minvalue_short_atomic,&
              maxvalue_short_atomic,avvalue_short_atomic,&
              nnatomcharge,nntotalenergy,nntotalforces)
        elseif(nn_type_short.eq.1)then
          if(nn_type_elec.eq.1)then
            call get_energies_and_forces_for_3G(num_atoms,zelem,lattice,&
              xyzstruct,atomspin,totalcharge,lperiodic,minvalue_short_atomic,&
              maxvalue_short_atomic,avvalue_short_atomic,&
              nnatomcharge,nntotalenergy,nntotalforces,enforcetotcharge,totalcharge,ldoforces)
          else
            call predictionshortatomic(num_atoms,num_atoms_element,zelem,lattice,&
              xyzstruct,atomspin,minvalue_short_atomic,maxvalue_short_atomic,&
              avvalue_short_atomic,eshortmin,eshortmax,nntotalenergy,&
              nntotalforces,nnatomenergy,nnshortenergy,&
              nnstress_short,atomenergysum,sens,lperiodic)
            if(lremoveatomenergies)then
               call addatoms(num_atoms,&
                zelem,num_atoms_element,&
                atomenergysum,nnatomenergy)
               nntotalenergy=nntotalenergy+atomenergysum
            endif ! lremoveatomenergies
          endif
        endif
!!=====================================================================
!! Do geometry optimizations using gradient descent fo initial local minimum
!!=====================================================================
        energy_current = 0.0d0
        energy_geoopt  = 1.0d0
        energy_latest = 0.0d0
!        nnforces(:,:) = 0.0d0
        if(nn_type_short.eq.1)then
            nnatomcharge(:)=0.0d0
        endif
        xyzstruct_new(:,:)=0.0d0
        xyzstruct_geoopt(:,:)=0.0d0
        open(outunit,file='output.data')
        if(lusecg)then
            call geometryopt_cg(num_atoms,num_atoms_element,zelem,elementsymbol,&
                 lattice,totalcharge,lperiodic,minvalue_short_atomic,&
                 maxvalue_short_atomic,avvalue_short_atomic, eshortmin,&
                 eshortmax, nntotalenergy,energy_geoopt,&
                 nntotalforces,xyzstruct,xyzstruct_geoopt,atomspin,nnatomcharge)
        else
            call geometryopt(num_atoms,num_atoms_element,zelem,elementsymbol,&
                 lattice,totalcharge,lperiodic,minvalue_short_atomic,&
                 maxvalue_short_atomic,avvalue_short_atomic, eshortmin,&
                 eshortmax, nntotalenergy,energy_geoopt,&
                 nntotalforces,xyzstruct,xyzstruct_geoopt,atomspin,nnatomcharge)
        endif
!!=====================================================================
!! Write the optimized structures in output.data
!!=====================================================================
!        write(outunit,'(a5)')'begin'
!        do i1 =1,num_atoms
!           write(outunit,'(a5,3f14.6,x,a2,x,5f14.6)')&
!                'atom ',(xyzstruct_geoopt(i2,i1),i2=1,3),&
!                 elementsymbol(i1),nnatomcharge(i1),atomenergy(i1),&
!                 (nnforces(i2,i1),i2=1,3)
!        enddo
!        write(outunit,'(a7,f20.6)')'energy ',energy_geoopt
!        write(outunit,'(a7,f20.6)')'charge ',totalcharge
!        write(outunit,'(a3)')'end'
        close(outunit)
!!=====================================================================
!! Do Minima Hopping !! 
!!=====================================================================
!        min_energy_list(1)=energy_geoopt
!        do i1 =1,num_atoms
!            masstemp=0.0d0
!            call atomicmass(zelem(i1),masstemp)
!            atomic_mass(i1)=masstemp
!        enddo 
!        do while (count_min<100)
!          xyzstruct(:,:)=xyzstruct_geoopt(:,:)
!          energy_latest=0.0d0
!          velocity(:,:)=0.0d0
!          call create_initvel(num_atoms,temperature,atomic_mass,velocity)
!          stop! by kenko
!!=====================================================================
!! Calculate the initial total energy (E_pot+ E_kin) for MD
!!=====================================================================
!          write(ounit,*)'MD_start for finding another local minimum'
!          write(ounit,'(1A10,3A18)')'STEP','K.E.', 'P.E.(NN energy)','T.E (K.E.+P.E.)'
!          call get_totalenergy(num_atoms,energy_geoopt,atomic_mass,&
!                velocity,kinetic_energy,totalenergy_MD)
!          write(ounit,'(I10,3f18.8)') '0',kinetic_energy,energy_geoopt,totalenergy_MD
!!=====================================================================
!! Do simple MD with 500 timestep
!!=====================================================================
!          do istep =1,500
!             call getacceleration(num_atoms,nnforces,atomic_mass,a_old)
!             call position_update(num_atoms,xyzstruct,velocity,atomic_mass,nnforces)
!             call get_energies_and_forces(num_atoms,num_atoms_element,zelem,lattice,&
!!               xyzstruct_new,atomspin,totalcharge,lperiodic,minvalue_short_atomic,&
!               maxvalue_short_atomic,avvalue_short_atomic,scmin_short_atomic,&
!               scmax_short_atomic,nnatomcharge,energy_latest,nnforces)
!             call getaccelaration(num_atoms,nnforces,atomic_mass,a_new)
!             call velocity_update(num_atoms,a_old,a_new,velocity)
!             call get_totalenergy(num_atoms,energy_latest,atomic_mass,&
!                velocity,kinetic_energy,totalenergy_MD)                
!             write(ounit,*) istep,kinetic_energy,energy_latest,totalenergy_MD 
!          enddo
!!=====================================================================
!! Do geometry optimization
!!=====================================================================
!          energy_geoopt=0.0d0
!          call geometryopt(num_atoms,num_atoms_element,zelem,lattice,totalcharge,lperiodic,&
!               minvalue_short_atomic,maxvalue_short_atomic,avvalue_short_atomic,&
!               scmin_short_atomic,scmax_short_atomic,energy_latest,energy_geoopt,nnforces,xyzstruct_new,&
!               xyzstruct_geoopt,atomspin,nnatomcharge)
!          do i2 = 1,count_min
!            if(abs(energy_geoopt-energy_current).lt.(Ediff1))then
!            temperature=temperature*beta1
!            elseif(abs(energy_geoopt-min_energy_list(i2)).lt.(Ediff1))then
!              temperature=temperature*beta2
!            else
!              temperature=temperature*beta3
!            endif
!          enddo  
!          if(abs(energy_geoopt-energy_current).lt.Ediff2)then 
!            Ediff2=Ediff2*alpha1
!            min_energy_list(i1)=energy_geoopt
!            energy_current=energy_geoopt
!            count_min=count_min+1
!            filename='min.000.data'
!            if(count_min.gt.99)then
!              write(filename(9:11),'(i3)')count_min
!            elseif(count_min.gt.9)then
!              write(filename(10:11),'(i2)')count_min
!            else
!              write(filename(11:11),'(i1)')count_min
!            endif
!            open(minunit,file=filename)
!            write(outunit,'(a5)')'begin'
!            do i1 =1,num_atoms
!              write(outunit,'(a5,3f14.6,x,a2,x,5f14.6)')&
!                   'atom ',(xyzstruct_geoopt(i2,i1),i2=1,3),&
!                   elementsymbol(i1),nnatomcharge(i1),atomenergy(i1),&
!                   (nnforces(i2,i1),i2=1,3)
!            enddo
!            write(outunit,'(a7,f20.6)')'energy ',energy_geoopt
!            write(outunit,'(a7,f20.6)')'charge ',totalcharge
!            write(outunit,'(a3)')'end'
!            close(ounit)
!          else   
!            Ediff2=Ediff2*alpha2
!          endif
!        enddo
!!============================================================================
!! Write the results for the output file
!!============================================================================
!        write(ounit,*) count_min,' local minima are founded by Minima Hopping'
!        write(ounit,*) 'The structure, charge, energy and forces will be &
!                        written in min.XXX.data'

end subroutine 
