!######################################################################
! This routine is part of
! RuNNer - RuNNer Neural Network Energy Representation
! (c) 2008-2024 Prof. Dr. Joerg Behler 
! Ruhr-Universitaet Bochum, Germany
!
! This program is free software: you can redistribute it and/or modify it 
! under the terms of the GNU General Public License as published by the 
! Free Software Foundation, either version 3 of the License, or 
! (at your option) any later version.
!
! This program is distributed in the hope that it will be useful, but 
! WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
! or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
! for more details.
!
! You should have received a copy of the GNU General Public License along 
! with this program. If not, see http://www.gnu.org/licenses.
! WARNING: THIS FILE IS PART OF THE UNMAINTAINED CONDA PACKAGE OF RUNNER 1.3. 
!######################################################################
!! multipurpose subroutine
!! Purpose: scale dsfuncdxyz and strs arrays

!! called by: 
!! - getallelectrostatic.f90
!! - getallshortforces.f90
!! - optimize_short_combined.f90
!! - optimize_short_combinedpair.f90
!!
      subroutine scaledQdxyz(&
        num_atoms,minvalue_local,maxvalue_local,&
        scmin_local,scmax_local,&
        zelem,dQdxyz_local,strs_local)
!!
      use fileunits
      use globaloptions
!!  
      implicit none
!!
      integer num_atoms                                                 ! in
      integer zelem(max_num_atoms)                                      ! in
      integer i1,i2                                            ! internal
      integer ielement
!!
      real*8 dQdxyz_local(num_atoms,3,num_atoms)               ! in
      real*8 minvalue_local(nelem)                             ! in 
      real*8 maxvalue_local(nelem)                             ! in 
      real*8 strs_local(3,3,num_atoms)                         ! in/out
      real*8 scmin_local                                       ! in
      real*8 scmax_local                                       ! in
!!
!!
      do i1=1,num_atoms
         ielement = elementindex(zelem(i1))
         !do i2=1,num_atoms
         dQdxyz_local(i1,:,:)=dQdxyz_local(i1,:,:) &
            / (maxvalue_local(ielement)-minvalue_local(ielement)) &
            * (scmax_local-scmin_local)
         !enddo
      enddo
!!
!! scale stress components, FIXME: CHECK IF THIS IS RIGHT!!!
      do i2=1,num_atoms
         ielement = elementindex(zelem(i2))
          strs_local(:,:,i2)=strs_local(:,:,i2)/&
          ((maxvalue_local(ielement))&
          -minvalue_local(ielement))&
          *(scmax_local-scmin_local)
      enddo ! i2
!!
      return
      end
