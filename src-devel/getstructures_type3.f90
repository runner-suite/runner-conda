!######################################################################
! This routine is part of
! RuNNer - RuNNer Neural Network Energy Representation
! (c) 2008-2024 Prof. Dr. Joerg Behler 
! Ruhr-Universitaet Bochum, Germany
!
! This program is free software: you can redistribute it and/or modify it 
! under the terms of the GNU General Public License as published by the 
! Free Software Foundation, either version 3 of the License, or 
! (at your option) any later version.
!
! This program is distributed in the hope that it will be useful, but 
! WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
! or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
! for more details.
!
! You should have received a copy of the GNU General Public License along 
! with this program. If not, see http://www.gnu.org/licenses.
! WARNING: THIS FILE IS PART OF THE UNMAINTAINED CONDA PACKAGE OF RUNNER 1.3. 
!######################################################################
!! called by: 
!! - fitting_batch.f90
!! - fittingpair.f90
!! - geterror.f90
!! - geterrorpair.f90
!! - precondition.f90
!! - preconditionpair.f90
!!
      subroutine getstructures_type3(unit,npoints,num_atoms_local)
!!
      use fileunits
      use globaloptions
      use structures
!!
      implicit none
!!
      integer unit                                                      ! in
      integer npoints                                                   ! in
      integer num_atoms_local(npoints)                                  ! in
      integer zelem(max_num_atoms)                                      ! internal
      integer i1                                                       
!!                                                                     
      real*8 xyzstruct(3,max_num_atoms)                                 ! internal
      real*8 atomspin(max_num_atoms)                                    ! internal
      real*8 totalforce(3,max_num_atoms)                                ! internal
      real*8 atomcharge(max_num_atoms)                                  ! internal
      real*8 atomenergy(max_num_atoms)                                  ! internal
                                                                       
      integer num_atoms                                                 ! internal
!!                                                                     
      logical lperiodic                                                 ! internal

!!
      do i1=1,npoints
!!
        lperiodic       = .false.
        zelem(:)        = 0
        xyzstruct(:,:)  = 0.0d0
        atomspin(:)     = 0.0d0
        totalforce(:,:) = 0.0d0
        num_atoms       = num_atoms_local(i1)
        atomcharge(:)   = 0.0d0
        atomenergy(:)   = 0.0d0
!!
!! read one structure from trainstruct.data
        call getonestructure(unit,num_atoms,zelem,&
          lattice_list(1,1,i1),xyzstruct,atomspin,totalforce,atomcharge,&
          atomenergy,lperiodic)
!!
        zelem_list(i1,:num_atoms)      = zelem(:num_atoms)
        xyzstruct_list(:,:num_atoms,i1)= xyzstruct(:,:num_atoms)
        atomspin_list(i1,:num_atoms)   = atomspin(:num_atoms)
        atomcharge_list(i1,:num_atoms) = atomcharge(:num_atoms)
        atomenergy_list(i1,:num_atoms) = atomenergy(:num_atoms)
        lperiodic_list(i1)             = lperiodic
!!
      enddo ! i1
!!
      return
      end
