!######################################################################
! This routine is part of
! RuNNer - RuNNer Neural Network Energy Representation
! (c) 2008-2024 Prof. Dr. Joerg Behler 
! Ruhr-Universitaet Bochum, Germany
!
! This program is free software: you can redistribute it and/or modify it 
! under the terms of the GNU General Public License as published by the 
! Free Software Foundation, either version 3 of the License, or 
! (at your option) any later version.
!
! This program is distributed in the hope that it will be useful, but 
! WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
! or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
! for more details.
!
! You should have received a copy of the GNU General Public License along 
! with this program. If not, see http://www.gnu.org/licenses.
! WARNING: THIS FILE IS PART OF THE UNMAINTAINED CONDA PACKAGE OF RUNNER 1.3. 
!######################################################################
!! subroutine written by Nikolas
!! called by:
!! - readkeywords.f90
!!
      subroutine readoutputorderline(keywordline)
!!
      use predictionoptions
      use globaloptions
!!      
      implicit none
!!
      integer :: indx_sort_arr(max_num_prop)                            ! internal Niko
      integer :: indx_assign_arr(max_num_prop)                          ! internal Niko
      integer :: j                                                      ! internal Niko
      integer :: k                                                      ! internal Niko
      integer :: n_assign_tmp                                           ! internal Niko
      integer :: max_val_tmp                                            ! internal Niko
      integer :: max_val_indx_tmp                                       ! internal Niko
                                                                        
      character*200 :: keywordline                                      ! internal Niko, string containing data of 'keyword' line in input.nn 
!!
!!=====================================================
!! initializations
!!=====================================================
      output_col_indx              = outpropertyindexes(0,0,0,0,0,0,0,0,0,0)
      indx_sort_arr(:)             = 0
      indx_assign_arr(:)           = 0
      output_n_prop                = 0
      n_assign_tmp                 = 0
      max_val_tmp                  = 999
      max_val_indx_tmp             = 0
!!
      indx_sort_arr(1)  = index(keywordline, ' x')   
      indx_sort_arr(2)  = index(keywordline, ' y')      
      indx_sort_arr(3)  = index(keywordline, ' z')      
      indx_sort_arr(4)  = index(keywordline, ' elem_symbol')      
      indx_sort_arr(5)  = index(keywordline, ' atom_charge')      
      indx_sort_arr(6)  = index(keywordline, ' atom_spin')      
      indx_sort_arr(7)  = index(keywordline, ' atom_energy')      
      indx_sort_arr(8)  = index(keywordline, ' fx')      
      indx_sort_arr(9)  = index(keywordline, ' fy')      
      indx_sort_arr(10) = index(keywordline, ' fz')     

      check_none_zero: do j=1,max_num_prop               ! Niko, number of non zero elements equal to
        if(indx_sort_arr(j)/=0) then                     ! number of given properties in input.nn
          output_n_prop = output_n_prop + 1 
        endif      
      enddo check_none_zero
          
      n_assign_tmp = output_n_prop                   ! Niko, assigns column indexes in decending
      indexes: do k=1,output_n_prop                  ! order to properties given in input.nn by comparing
        max_val_indx_tmp = maxloc(indx_sort_arr, &   ! substring indexes
        dim=1,mask=indx_sort_arr<max_val_tmp)
        max_val_tmp = indx_sort_arr(max_val_indx_tmp)
        indx_assign_arr(max_val_indx_tmp) = n_assign_tmp
        n_assign_tmp = n_assign_tmp - 1
      enddo indexes   

      output_col_indx%x_coord      = indx_assign_arr(1) 
      output_col_indx%y_coord      = indx_assign_arr(2)
      output_col_indx%z_coord      = indx_assign_arr(3)
      output_col_indx%elem_sym     = indx_assign_arr(4)
      output_col_indx%atom_charge  = indx_assign_arr(5)
      output_col_indx%atom_spin    = indx_assign_arr(6)
      output_col_indx%atom_energy  = indx_assign_arr(7)
      output_col_indx%x_force      = indx_assign_arr(8)
      output_col_indx%y_force      = indx_assign_arr(9)
      output_col_indx%z_force      = indx_assign_arr(10)

!!
      end subroutine readoutputorderline
