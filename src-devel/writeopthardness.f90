!######################################################################
! This routine is part of
! RuNNer - RuNNer Neural Network Energy Representation
! (c) 2008-2024 Prof. Dr. Joerg Behler 
! Ruhr-Universitaet Bochum, Germany
!
! This program is free software: you can redistribute it and/or modify it 
! under the terms of the GNU General Public License as published by the 
! Free Software Foundation, either version 3 of the License, or 
! (at your option) any later version.
!
! This program is distributed in the hope that it will be useful, but 
! WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
! or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
! for more details.
!
! You should have received a copy of the GNU General Public License along 
! with this program. If not, see http://www.gnu.org/licenses.
! WARNING: THIS FILE IS PART OF THE UNMAINTAINED CONDA PACKAGE OF RUNNER 1.3. 
!######################################################################
!! called by:
!! - fitting.f90 
!! - fitting_batch.f90 
!! - fittingpair.f90 
!!
      subroutine writeopthardness(ndim,hardness_local)
       
!!
      use fileunits
      use globaloptions
!!
      implicit none
!!
      integer ndim
      integer i1,i2
!!
      real*8 hardness_local(ndim)      
!!
      character*22 filename(ndim)
      character*22 filenametemp
!!
!!
!! prepare filenames
      filename(:)='opthardness.000.out'
      do i1=1,ndim
        filenametemp=filename(i1)
        if(nucelem(i1).gt.99)then
          write(filenametemp(13:15),'(i3)')nucelem(i1)
        elseif(nucelem(i1).gt.9)then
          write(filenametemp(14:15),'(i2)')nucelem(i1)
        else
          write(filenametemp(15:15),'(i1)')nucelem(i1)
        endif
        filename(i1)=filenametemp
      enddo
!!
      do i1=1,ndim
        open(hardnessunit,file=filename(i1),form='formatted',status='replace')
        write(hardnessunit,'(f18.10)')fixedhardness(i1)
        close(hardnessunit)
      enddo
!!
!!
      return
      end
