!###############################################################################
! This routine is part of
! RuNNer - RuNNer Neural Network Energy Representation
! (c) 2008-2024 Prof. Dr. Joerg Behler
! Ruhr-Universitaet Bochum, Germany
!
! This program is free software: you can redistribute it and/or modify it
! under the terms of the GNU General Public License as published by the
! Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! This program is distributed in the hope that it will be useful, but
! WITHout ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
! or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
! for more details.
!
! You should have received a copy of the GNU General Public License along
! with this program. If not, see http://www.gnu.org/licenses.
! WARNING: THIS FILE IS PART OF THE UNMAINTAINED CONDA PACKAGE OF RUNNER 1.3.
!###############################################################################

subroutine distribute_vdw_calculation(                                         &
  num_atoms, zelem,                                                            &
  lattice, xyzstruct, lperiodic,                                               &
  vdwenergy, atomvdw, vdwforces, vdwstress                                     &
)
  
!----------Module imports-------------------------------------------------------
  use mpi_mod
  use fileunits
  use nnflags
  use globaloptions
  use predictionoptions
  
  implicit none

!-------------------------------------------------------------------------------
! Description:
!   This routine distributes the calculation of dispersion energies to separate
!   processes.
!
! Method:
!   - The structure is divided into blocks of atoms. Only the number of atoms
!     per block is kept in memory at once.
!   - For each block of atoms, the dispersion energy is calculated by a separate
!     process and reported back to this module.
!   - In parallel runs the block of atoms is further split among the processes 
!     and each process does natoms atoms.
!   - At the end, all dispersion energies are accumulated.
!
! Current Code Maintainer: Alexander Knoll
!
! Last revision: 27.09.2020 
!
! Code placement in the RuNNer suite:
!   - Called by: predictionvdw.f90
!-------------------------------------------------------------------------------

! Subroutine arguments.
  ! Number of atoms in the structure.
  integer, intent(in   )                                 :: num_atoms
  ! Nucleus charge of all atoms in the structure.
  integer, intent(in   ), dimension(max_num_atoms)       :: zelem
  ! Lattice vector components.
  real*8,  intent(in   ), dimension(3, 3)                :: lattice
  ! Cartesian coordinates of all atoms in the structure.
  real*8,  intent(in   ), dimension(3, max_num_atoms)    :: xyzstruct
  ! Whether the structure is periodic or not.
  logical, intent(in   )                                 :: lperiodic

  ! Total dispersion energy of the structure.
  real*8,  intent(inout)                                 :: vdwenergy
  ! Total vdw energy of each atom in the structure.
  real*8,  intent(inout), dimension(max_num_atoms)       :: atomvdw
  ! Total vdw force components acting on each atom in the structure.
  real*8,  intent(inout), dimension(3, max_num_atoms)    :: vdwforces  
  ! Total vdw stress components.
  real*8,  intent(inout), dimension(3, 3)                :: vdwstress

! Internal variables.
  ! Loop running integers.
  integer :: i2
  ! MPI block slicing variables
  integer :: npoints, ncount, ndone, n_start, n_end, natoms

  ! First and last index of each atom's neighbor. Map to lstc, lste and lstb. 
  integer, allocatable, dimension(:, :)         :: lsta
  ! Atom ID (lstc) and nucleus charge (lste) of neighbors.
  integer, allocatable, dimension(:)            :: lstc, lste
  ! Neighbor xyz and distance.
  real*8,  allocatable, dimension(:, :)         :: lstb

  ! Total vdw energy of one block of atoms.
  real*8 :: vdwenergy_block

!----------Initializations------------------------------------------------------

  ! Numer of atoms to be calculated is set to total number of atoms initially.
  ncount  = num_atoms 
  ! Number of atoms to be calculated in this loop step by all processes together
  npoints = 0 
  ! Number of atoms calculated in previous loops.
  ndone   = 0 

  ! Calculate dispersion interaction for each block of atoms.
  loop_block_of_atoms: do while (ncount > 0)

    ! Allocate arrays.
    allocate(lsta(2, max_num_atoms))
    allocate(lstc(listdim))
    allocate(lste(listdim))
    allocate(lstb(listdim, 4))

    ! Reset vdw energy of this block.
    vdwenergy_block=0.0d0

    ! nblock determines the maximum number of points which can be held in memory
    ! If the remaining number of atoms (ncount) is larger than nblock, the 
    ! maximum number of atoms is loaded into memory (= nblock) and the count
    ! is reduced by this number. Otherwise, all remaining atoms are loaded.
    npoints = min(ncount, nblock)
    ncount = ncount - npoints

!----------MPI Preparations-----------------------------------------------------
    call mpifitdistribution(npoints, natoms, n_start, n_end)
    
    ! Adjust position by atoms already done.
    n_start = n_start + ndone
    n_end   = n_end   + ndone
    
    if (.not.lmd) then
      write(ounit,'(a,i6,a,i8,a,i8,a,i8)')                                     &
        ' process ', mpirank, ' calculates vdw dispersion E for atoms '        &
        , n_start, ' to ', n_end, ' natoms: ', natoms
    endif
    
    call mpi_barrier(mpi_comm_world, mpierror)

!----------Dispersion interaction calculation-----------------------------------

    ! Build neighbor list for each atom in vdw cutoff.
    call neighbor_vdw(                                                         &
      n_start, n_end, num_atoms, zelem,                                        &
      lsta, lstb, lstc, lste,                                                  &
      vdw_cutoff, lattice, xyzstruct, lperiodic                                &
    )
    
    ! Calculate simple environment-dependent dispersion correction.
    if(nn_type_vdw.eq.1)then
      call calc_vdw_simple(                                                    &
        n_start, n_end,                                                        &
        lsta, lstc, lste, lstb,                                                &
        lperiodic, xyzstruct, zelem,                                           &
        atomvdw, vdwenergy_block, vdwforces, vdwstress                         &
      )
    ! Calculate Grimme-D2-type dispersion interaction.
  elseif(nn_type_vdw.eq.2)then
      call calc_grimme_d2(                                                     &
        n_start, n_end,                                                        &
        lsta, lstc, lste, lstb,                                                &
        lperiodic, xyzstruct, zelem,                                           &
        atomvdw, vdwenergy_block, vdwforces, vdwstress                         &
      )
    endif

    ! Collect partial vdwenergy of each subprocesses.
    call mpi_allreduce(                                                        &
      mpi_in_place, vdwenergy_block,                                           &
      1, mpi_real8, mpi_sum, mpi_comm_world, mpierror                          &
    )
    
    ! Add vdwenergy of this block to total vdw energy of the structure.
    vdwenergy = vdwenergy + vdwenergy_block

!----------Clean up and prepare next iteration----------------------------------

    ! Increase atom counter.
    ndone = ndone + npoints

    ! Deallocate block-specific arrays.
    deallocate(lsta)
    deallocate(lstc)
    deallocate(lste)
    deallocate(lstb)
    
  enddo loop_block_of_atoms

  return
  
end subroutine distribute_vdw_calculation
