!######################################################################
! This routine is part of
! RuNNer - RuNNer Neural Network Energy Representation
! (c) 2008-2024 Prof. Dr. Joerg Behler 
! Ruhr-Universitaet Bochum, Germany
!
! This program is free software: you can redistribute it and/or modify it 
! under the terms of the GNU General Public License as published by the 
! Free Software Foundation, either version 3 of the License, or 
! (at your option) any later version.
!
! This program is distributed in the hope that it will be useful, but 
! WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
! or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
! for more details.
!
! You should have received a copy of the GNU General Public License along 
! with this program. If not, see http://www.gnu.org/licenses.
! WARNING: THIS FILE IS PART OF THE UNMAINTAINED CONDA PACKAGE OF RUNNER 1.3. 
!######################################################################
!! Purpose: calculate nnatomcharge() and sense()

!! called by: 
!!
      subroutine getchargesatomic_type5(n_start,natoms,&
        atomindex,max_num_neighbors_elec,&
        invneighboridx_elec,zelem,&
        lsta,lstc,lste,lstb,xyzstruct,atomspin,&
        minvalue_local,maxvalue_local,avvalue_local,&
        scmin_local,scmax_local,nnatomcharge,totalcharge,&
        Amatrix,lextrapolation, lattice, lperiodic)
!!
      use fileunits
      use globaloptions
      use nnewald
      use symfunctions
      use timings
      use predictionoptions
      use eem
      use ewaldSummation
!!
      implicit none
!!
      integer n_start                                                   ! in
      integer max_num_neighbors_elec                                    ! in
      integer ielem                                                     ! internal
      integer iindex                                                    ! internal
      integer zelem(max_num_atoms)                                      ! in
      integer i1,i2,i3,i4,i5                                            ! internal
      integer icount                                                    ! internal
      integer jcount                                                    ! internal
      integer natoms                                                    ! in
      integer atomindex(natoms)                                         ! in
      integer invneighboridx_elec(natoms,max_num_atoms)                 ! in
      integer lsta(2,max_num_atoms)                                     ! in
      integer lstc(listdim)                                             ! in, identification of atom
      integer lste(listdim)                                             ! in, nuclear charge of atom
      logical lperiodic
      real*8 lattice(3,3)
!!
      real*8 atomspin(natoms)                                           ! in
      real*8 xyzstruct(3,natoms)                                        ! in
      real*8 lstb(listdim,4)                                            ! in, xyz and r_ij
      real*8 minvalue_local(nelem,maxnum_funcvalues_elec)               ! in 
      real*8 maxvalue_local(nelem,maxnum_funcvalues_elec)               ! in 
      real*8 avvalue_local(nelem,maxnum_funcvalues_elec)                ! in 
      real*8 scmin_local                                                ! in
      real*8 scmax_local                                                ! in
      real*8 strs_dummy(3,3,maxnum_funcvalues_elec) 
      real*8 symfunctione(maxnum_funcvalues_elec)                       ! internal
!! CAUTION: just one output node is assumed here
      real*8 nnoutput                                                   ! internal 
      real*8 nnatomchi(natoms)                                          ! internal
      real*8 nodes_values(maxnum_layers_elec,maxnodes_elec)             ! internal
      real*8 nodes_sum(maxnum_layers_elec,maxnodes_elec)                ! internal
      real*8 nnatomcharge(natoms)                                       ! out
      real*8 atomhardness(natoms)                                       ! internal
      real*8 gausswidth(natoms)                                         ! internal 
      real*8 totalcharge                                                ! in
      real*8 threshold                                                  ! internal
      real*8 Amatrix(natoms,natoms)                                     ! internal/out
      real*8 dsfuncdxyz_dummy(0:max_num_neighbors_elec,3)               ! internal
!!
      logical lrmin                                                     ! internal 
      logical lextrapolation                                            ! out
!!
!! initialization
      lrmin           =.true.
      threshold       = 0.0001d0 
      nnatomchi(:)    = 0.0d0
      atomhardness(:) = 0.0d0
      gausswidth(:)   = 0.0d0
!!
!!====================================================================================
!! loop over all atoms 
!!====================================================================================
      jcount=n_start
      do i1=1,natoms
!!====================================================================================
!! initializations for this atom 
!!====================================================================================
        symfunctione(:)  = 0.0d0
        ielem=elementindex(zelem(atomindex(i1)))
        atomhardness(i1)=fixedhardness(ielem)
        gausswidth(i1)=fixedgausswidth(ielem)
        iindex=elementindex(zelem(jcount))
!!====================================================================================
!! get the symmetry functions for atom jcount 
!!====================================================================================
        do i2=1,num_funcvalues_elec(ielem) ! over all symmetry functions
          ! TODO: in getatomsymfunctions xyzstruct has dimension max_num_atoms
          call getatomsymfunctions(&
            i1,i2,iindex,natoms,atomindex,natoms,&
            max_num_atoms,max_num_neighbors_elec,&
            invneighboridx_elec,jcount,listdim,lsta,lstc,lste,&
            symelement_elec,maxnum_funcvalues_elec,&
            cutoff_type,cutoff_alpha,nelem,function_type_elec,&
            lstb,funccutoff_elec,xyzstruct,atomspin,symfunctione,dsfuncdxyz_dummy,strs_dummy,&
            eta_elec,zeta_elec,lambda_elec,rshift_elec,rmin,&
            .false.,.false.,.false.)
          if(.not.lrmin)then
            write(ounit,*)'Error in prediction: lrmin=.false. (atoms too close)'
            stop !'
          endif
!!====================================================================================
!! check for extrapolation atoms n_start to n_end
!! This needs to be done before scaling 
!!====================================================================================
          if(lfinetime)then
            dayextrapolationewald=0
            call abstime(timeextrapolationewaldstart,dayextrapolationewald)
          endif ! lfinetime
          lextrapolation=.false.
          if((symfunctione(i2)-maxvalue_local(ielem,i2)).gt.threshold)then
            count_extrapolation_warnings_symfunc = count_extrapolation_warnings_symfunc + 1
            write(ounit,'(a,a5,x,2i5,x,a,2f18.8)')&
            '### EXTRAPOLATION WARNING ### ','Ewald',&
            atomindex(i1),i2,'too large ',symfunctione(i2),maxvalue_local(ielem,i2)
            lextrapolation=.true.
          elseif((-symfunctione(i2)+minvalue_local(ielem,i2)).gt.threshold)then
            count_extrapolation_warnings_symfunc = count_extrapolation_warnings_symfunc + 1
            write(ounit,'(a,a5,x,2i5,x,a,2f18.8)')&
            '### EXTRAPOLATION WARNING ### ','Ewald',&
            atomindex(i1),i2,'too small ',symfunctione(i2),minvalue_local(ielem,i2)
            lextrapolation=.true.
          endif
          if(lfinetime)then
            call abstime(timeextrapolationewaldend,dayextrapolationewald)
            timeextrapolationewald=timeextrapolationewald+timeextrapolationewaldend-timeextrapolationewaldstart
          endif ! lfinetime
!!====================================================================================
!! scale symmetry functions for the electrostatic interaction
!! caution: internally nblock and npoints are set to 1 to avoid _list in zelem, symfunctione and num_atoms
!!====================================================================================
          if(lfinetime)then
            dayscalesymewald=0
            call abstime(timescalesymewaldstart,dayscalesymewald)
          endif ! lfinetime
          if(lcentersym.and..not.lscalesym)then
!! For each symmetry function remove the CMS of the respective element 
            symfunctione(i2)=symfunctione(i2)-avvalue_local(ielem,i2)
          elseif(lscalesym.and..not.lcentersym)then
!! Scale each symmetry function value for the respective element
            symfunctione(i2)=(symfunctione(i2)&
           -minvalue_local(ielem,i2))/ &
           (maxvalue_local(ielem,i2)-minvalue_local(ielem,i2))&
            *(scmax_local-scmin_local) + scmin_local
          elseif(lscalesym.and.lcentersym)then
            symfunctione(i2)=(symfunctione(i2)&
            -avvalue_local(ielem,i2))/ &
            (maxvalue_local(ielem,i2)-minvalue_local(ielem,i2))
          else
          endif
          if(lfinetime)then
            call abstime(timescalesymewaldend,dayscalesymewald)
            timescalesymewald=timescalesymewald+timescalesymewaldend-timescalesymewaldstart
          endif ! lfinetime
        enddo ! i2 loop over all symmetry functions
!!
!!====================================================================================
!! now we have all symmetry functions of atom i1/jcount, now calculate the atom energy 
!! calculation of the values on all nodes (nodes_values) in the NN (needed below for the derivatives)
!!====================================================================================
        nodes_sum(:,:)    =0.0d0 ! initialization
        nodes_values(:,:) =0.0d0 ! initialization
        call calconenn(1,maxnum_funcvalues_elec,maxnodes_elec,&
          maxnum_layers_elec,num_layers_elec(ielem),&
          maxnum_weights_elec,nodes_elec(0,ielem),&
          symfunctione,weights_elec(1,ielem),nodes_values,nodes_sum,&
          nnatomchi(i1),actfunc_elec(1,1,ielem))
!!
        jcount=jcount+1
      enddo ! i1 ! natoms

!!
!!=========================================
!! calculation of charges and dAdxyz by EEM
!!=========================================
      if (lperiodic) then
          call eemMatrixEwald(natoms,xyzstruct,lattice,gausswidth,atomhardness,Amatrix)
      else
          call eemMatrix(natoms,xyzstruct,gausswidth,atomhardness,Amatrix)
      endif
      call eemCharges(natoms,Amatrix,nnatomchi,totalcharge,nnatomcharge)
      !!
      return
      end
