!######################################################################
! This routine is part of
! RuNNer - RuNNer Neural Network Energy Representation
! (c) 2008-2024 Prof. Dr. Joerg Behler 
! Ruhr-Universitaet Bochum, Germany
!
! This program is free software: you can redistribute it and/or modify it 
! under the terms of the GNU General Public License as published by the 
! Free Software Foundation, either version 3 of the License, or 
! (at your option) any later version.
!
! This program is distributed in the hope that it will be useful, but 
! WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
! or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
! for more details.
!
! You should have received a copy of the GNU General Public License along 
! with this program. If not, see http://www.gnu.org/licenses.
! WARNING: THIS FILE IS PART OF THE UNMAINTAINED CONDA PACKAGE OF RUNNER 1.3. 
!######################################################################
!! called by:
!!
      subroutine writeoptfit_charge(optepoche,&
        optrmse_charge,optrmse_charge_test,optrmse_totalcharge,&
        optrmse_totalcharge_test)        
!!
      use fileunits
      use globaloptions
      use fittingoptions
      use nnflags
!!
      implicit none
!!
      integer optepoche                ! in
!!
      real*8 tounit                    ! in
      real*8 optrmse_charge            ! in
      real*8 optrmse_charge_test       ! in
      real*8 optrmse_totalcharge       ! in
      real*8 optrmse_totalcharge_test  ! in
 
!!
      write(ounit,*)'============================================================='
      if(optepoche.eq.0)then
        if(luseatomspins)then
            write(ounit,'(a,i5)')' No improvement of the spin fit has been obtained'
        else
            write(ounit,'(a,i5)')' No improvement of the charge fit has been obtained'
        endif
      else
        if(luseatomspins)then
          write(ounit,'(a,i5)')' Best spin fit has been obtained in epoch ',optepoche
          write(ounit,'(a)')'                     --- atom spins: ---  '
          write(ounit,'(a)')'                     train         test '
          write(ounit,'(a14,x,6f13.6)')' OPTSPIN      ',optrmse_charge,optrmse_charge_test
        else
          write(ounit,'(a,i5)')' Best charge fit has been obtained in epoch ',optepoche
          write(ounit,'(a36)')'                    --- charges: ---'
          write(ounit,'(a38)')'                   train        test '
          write(ounit,'(a11,f15.6,f13.6)')' OPTCHARGE ',&
            optrmse_charge,optrmse_charge_test
        endif
      endif ! optepoche.eq.0
      write(ounit,*)'-------------------------------------------------------------'
!! write maximum errors
      if(.not.luseatomspins)then
        if(fitting_unit.eq.1)then
          write(ounit,'(a,f14.6,a21,i8,a2)')' max RMSE of atomcic charges in last epoch (train set): ',&
            maxerror_charge_train,' e/atom (structure ',imaxerror_charge_train,')' 
          write(ounit,'(a56,f14.6,a21,i8,a2)')' max RMSE of atomic charges in last epoch (test set) :  ',&
            maxerror_charge_test, ' e/atom (structure ',imaxerror_charge_test,')' 
        elseif(fitting_unit.eq.2)then
          write(ounit,'(a,f14.6,a21,i8,a2)')' max RMSE of atomic charges in last epoch (train set): ',&
            maxerror_charge_train,' e/atom (structure ',imaxerror_charge_train,')' 
          write(ounit,'(a56,f14.6,a21,i8,a2)')' max RMSE of atomic charges in last epoch (test set) :  ',&
            maxerror_charge_test, ' e/atom (structure ',imaxerror_charge_test,')' 
        endif
      endif
!!
      return
      end
