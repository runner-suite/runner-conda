!######################################################################
! This routine is part of
! RuNNer - RuNNer Neural Network Energy Representation
! (c) 2008-2024 Prof. Dr. Joerg Behler
! Ruhr-Universitaet Bochum, Germany
!
! This program is free software: you can redistribute it and/or modify it
! under the terms of the GNU General Public License as published by the
! Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! This program is distributed in the hope that it will be useful, but
! WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
! or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
! for more details.
!
! You should have received a copy of the GNU General Public License along
! with this program. If not, see http://www.gnu.org/licenses.
! WARNING: THIS FILE IS PART OF THE UNMAINTAINED CONDA PACKAGE OF RUNNER 1.3.
!######################################################################
!! called by:
!! - getshortatomic.f90
!! Added by Emir

subroutine getvibrationalfrequencies(natoms,lmodes)

      use predictionoptions

      implicit none

      integer natoms ! in
      integer i,ii

      real*8  eig(3*natoms)

      logical lmodes ! in

      !! calculate eigenvalues and eigenvectors of Hessian matrix for a given structure
      !! if lmodes is true, hessian_local is converted into the eigenvector matrix
      call dia_hessian(nnhessian,eig,natoms,lmodes)

      !! convert these eigenvalues to frequencies
      call convert_frequencies(eig,natoms,nnfrequencies)


end subroutine getvibrationalfrequencies
