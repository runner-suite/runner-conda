!######################################################################
! This routine is part of
! RuNNer - RuNNer Neural Network Energy Representation
! (c) 2008-2024 Prof. Dr. Joerg Behler 
! Ruhr-Universitaet Bochum, Germany
!
! This program is free software: you can redistribute it and/or modify it 
! under the terms of the GNU General Public License as published by the 
! Free Software Foundation, either version 3 of the License, or 
! (at your option) any later version.
!
! This program is distributed in the hope that it will be useful, but 
! WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
! or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
! for more details.
!
! You should have received a copy of the GNU General Public License along 
! with this program. If not, see http://www.gnu.org/licenses.
! WARNING: THIS FILE IS PART OF THE UNMAINTAINED CONDA PACKAGE OF RUNNER 1.3. 
!######################################################################
!! This is a multipurpose subroutine to scale general symfunctions

!! called by:
!! - getshortenergies_para.f90
!! - ewaldenergies_para.f90
!! - scalesymfit_para.f90 
!! - precondition.f90
!!
      subroutine scalecharge(ndim1,ndim2,&
         num_atoms_local,&
         zelem_local,charge_local,&
         minvalue_local,maxvalue_local,avvalue_local,&
         scmin_local,scmax_local)
!!
      use fileunits
      use globaloptions
!!
      implicit none
!!
      integer ndim1                                                          ! in   ! number of element
      integer ndim2                                                          ! in   ! number of structures
      integer num_atoms_local(ndim2)                                         ! in
      integer zelem_local(ndim2,max_num_atoms)                               ! in
      integer i1,i2                                                          ! internal
      integer itemp                                                          ! internal
!!
      real*8 charge_local(ndim2,max_num_atoms)                               ! in/out
      real*8 minvalue_local(ndim1)                                           ! in
      real*8 maxvalue_local(ndim1)                                           ! in
      real*8 avvalue_local(ndim1)                                            ! in
      real*8 scmin_local                                                     ! in
      real*8 scmax_local                                                     ! in
!!
      do i1=1,ndim2
        do i2=1,num_atoms_local(i1)
          itemp=elementindex(zelem_local(i1,i2))
          if(lcentercharge.and..not.lscalecharge)then
!! For each symmetry function remove the CMS of the respective element 
            charge_local(i1,i2)=charge_local(i1,i2) &
             -avvalue_local(itemp)
          elseif(lscalecharge.and..not.lcentercharge)then
!! Scale each symmetry function value for the respective element
              charge_local(i1,i2)=&
             (charge_local(i1,i2)-minvalue_local(itemp))/ &
             (maxvalue_local(itemp)-minvalue_local(itemp))&
              *(scmax_local-scmin_local) + scmin_local
          elseif(lscalecharge.and.lcentercharge)then
              charge_local(i1,i2)=&
             (charge_local(i1,i2)-avvalue_local(itemp))&
             / (maxvalue_local(itemp)-minvalue_local(itemp))
          else
          endif
        enddo ! i2
      enddo ! i1
!!
      return
      end
