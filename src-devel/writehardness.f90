!######################################################################
! This routine is part of
! RuNNer - RuNNer Neural Network Energy Representation
! (c) 2008-2024 Prof. Dr. Joerg Behler 
! Ruhr-Universitaet Bochum, Germany
!
! This program is free software: you can redistribute it and/or modify it 
! under the terms of the GNU General Public License as published by the 
! Free Software Foundation, either version 3 of the License, or 
! (at your option) any later version.
!
! This program is distributed in the hope that it will be useful, but 
! WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
! or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
! for more details.
!
! You should have received a copy of the GNU General Public License along 
! with this program. If not, see http://www.gnu.org/licenses.
! WARNING: THIS FILE IS PART OF THE UNMAINTAINED CONDA PACKAGE OF RUNNER 1.3. 
!######################################################################
!! multipurpose subroutine

!! called by:
!! - fitting_electrostatic.f90 
!!
      subroutine writehardness(ndim)
!!
      use fileunits
      use fittingoptions
      use globaloptions
!!
      implicit none
!!
      integer ndim                                       ! in
      integer i1
!!
!!
      do i1=1,ndim
        if(lwriteunformatted)then
          open(hardnessunit,file=filenameah(i1),form='unformatted',status='replace')
        else !'
          open(hardnessunit,file=filenameah(i1),form='formatted',status='replace')
        endif !'
!! write weights
        write(hardnessunit,'(f18.8)')&
           fixedhardness(i1)
        close(hardnessunit)
      enddo
!!
!!
      return
      end
