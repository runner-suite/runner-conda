!######################################################################
! This routine is part of
! RuNNer - RuNNer Neural Network Energy Representation
! (c) 2008-2024 Prof. Dr. Joerg Behler 
! Ruhr-Universitaet Bochum, Germany
!
! This program is free software: you can redistribute it and/or modify it 
! under the terms of the GNU General Public License as published by the 
! Free Software Foundation, either version 3 of the License, or 
! (at your option) any later version.
!
! This program is distributed in the hope that it will be useful, but 
! WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
! or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
! for more details.
!
! You should have received a copy of the GNU General Public License along 
! with this program. If not, see http://www.gnu.org/licenses.
! WARNING: THIS FILE IS PART OF THE UNMAINTAINED CONDA PACKAGE OF RUNNER 1.3. 
!######################################################################
!! called by:
!! - ewaldenergies_para.f90  
!!
      subroutine calcrmse_charge(ndim,npoints,ncharges,&
           zelem_mpi,num_atoms_mpi,rmse_charge,mad_charge,&
           totalcharge_mpi,rmse_totalcharge,mad_totalcharge,&
           atomcharge_mpi,nnatomcharge_mpi,nnchargesum_mpi,&
           maxcharge,imaxcharge)
!!
      use fittingoptions
      use globaloptions
      use predictionoptions  
!!
      implicit none
!!
      integer npoints                                 ! in
      integer ndim                                    ! in
      integer num_atoms_mpi(ndim)                     ! in
      integer ncharges                                ! in/out
      integer i1,i2                                   ! internal
      integer zelem_mpi(ndim,max_num_atoms)           ! in
      integer, intent(out)::imaxcharge                ! in
!!
      real*8 atomcharge_mpi(ndim,max_num_atoms)       ! in
      real*8 nnatomcharge_mpi(ndim,max_num_atoms)     ! in
      real*8 rmse_charge                              ! in/out
      real*8 rmse_charge_local(ndim)                  
      real*8 rmse_totalcharge                         ! in/out
      real*8 mad_charge                               ! in/out
      real*8 mad_charge_local(ndim)
      real*8 mad_totalcharge                          ! in/out
      real*8 totalcharge_mpi(ndim)                    ! in
      real*8 chargesum                                ! internal
      real*8 nnchargesum_mpi(ndim)                    ! out 
      real*8, intent(out)::maxcharge                  ! out  
      real*8 rmse_temp
      real*8 mad_temp
!!
!!
      nnchargesum_mpi(:)=0.0d0
      ncharges=0
      rmse_totalcharge = 0.0d0
      mad_totalcharge = 0.0d0
      rmse_charge = 0.0d0
      mad_charge = 0.0d0
!!    KK: here we get scaled charges predicted by ANNs
      if (enforcetotcharge==1) then
        do i1=1,npoints
            nnatomcharge_mpi(i1,:)=nnatomcharge_mpi(i1,:)-(sum(nnatomcharge_mpi(i1,:))-totalcharge_mpi(i1))/num_atoms_mpi(i1)
        enddo
      endif  
      do i1=1,npoints
        chargesum=0.0d0
        rmse_temp =0.0d0
        mad_temp=0.0d0
        do i2=1,num_atoms_mpi(i1)
          if(.not.lupdatebyelement)then
            ncharges=ncharges+1
            rmse_temp=rmse_temp +(atomcharge_mpi(i1,i2)-nnatomcharge_mpi(i1,i2))**2
            mad_temp =mad_temp  +abs(atomcharge_mpi(i1,i2)-nnatomcharge_mpi(i1,i2))
          else
            if(elemupdate.eq.zelem_mpi(i1,i2))then
              ncharges=ncharges+1
              rmse_charge=rmse_charge +(atomcharge_mpi(i1,i2)-nnatomcharge_mpi(i1,i2))**2
              mad_charge =mad_charge +abs(atomcharge_mpi(i1,i2)-nnatomcharge_mpi(i1,i2))
            endif
          endif ! lupdatebyelement
          chargesum=chargesum+nnatomcharge_mpi(i1,i2)
        enddo ! i2
        rmse_charge = rmse_charge+rmse_temp
        mad_charge = mad_charge+mad_temp
        rmse_charge_local(i1) = sqrt(rmse_temp/dble(num_atoms_mpi(i1)))
        mad_charge_local(i1) = mad_temp/dble(num_atoms_mpi(i1))
        nnchargesum_mpi(i1)=chargesum
        rmse_totalcharge=rmse_totalcharge +(totalcharge_mpi(i1)-chargesum)**2
        mad_totalcharge =mad_totalcharge +abs(totalcharge_mpi(i1)-chargesum)
      enddo ! i1
      maxcharge = maxval(rmse_charge_local,dim=1)
      imaxcharge = maxloc(rmse_charge_local,dim=1)
!!
      return
      end
