!######################################################################
! This routine is part of
! RuNNer - RuNNer Neural Network Energy Representation
! (c) 2008-2024 Prof. Dr. Joerg Behler 
! Ruhr-Universitaet Bochum, Germany
!
! This program is free software: you can redistribute it and/or modify it 
! under the terms of the GNU General Public License as published by the 
! Free Software Foundation, either version 3 of the License, or 
! (at your option) any later version.
!
! This program is distributed in the hope that it will be useful, but 
! WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
! or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
! for more details.
!
! You should have received a copy of the GNU General Public License along 
! with this program. If not, see http://www.gnu.org/licenses.
! WARNING: THIS FILE IS PART OF THE UNMAINTAINED CONDA PACKAGE OF RUNNER 1.3. 
!######################################################################
!!
!! This subroutine checks if all relevant files are present
!!
!! called by:
!! - initialization.f90
!!
      subroutine checkfiles()
!!
      implicit none
!!
      logical lexist !
!!
      inquire(file='input.data',exist=lexist)
      if(.not.lexist) then
        write(*,*)'Error: file input.data not found'
        stop
      endif
!!
      inquire(file='input.nn',exist=lexist)
      if(.not.lexist) then
        write(*,*)'Error: file input.nn not found'
        stop
      endif
!!
      return
      end
