! Created by Jonas A. Finkler on 04.02.20.

! todo: ixyzmax = int(sqrt(1._dp / eigalat(1)) * radius_cutoff) + 1  <- can one use this to calc max number of neighbors to consider?
! todo: only half the elements have to be calculatied (lower half of A matrix)
! the ewald routine could also call the A matrix routines and use E = 1/2 q^t A q
! or make functions that calculate the matrix element and then either sum them or put them in matrix
! need more tests for the case of point charges

! todo: dAdxyzTimesQ, use A to calculate dEdQ
! todo: change order of lat and ats to be consistent throughout code

! IMPORTANT: if the first element of sigma is set to something <= 0, point charges are used
! in this case self interaction of the charges is ommitted

! IMPORTANT: in RuNNer the rows of lat are the lattice vectors, I used the columns
! sorry about the confusion. I therefore transpose the lattice matrix.
! The public methods accept the lattice in RuNNer format

module ewaldSummation
    use precision
    use nnconstants
    use globaloptions

    implicit none
    ! In RuNNer: ewaldalpha = 1 / (2 * eta)
    ! if you are using private routines, make sure that you take care of transposing the lattice
    private
    public :: ewaldEnergy,  eemMatrixEwald, eemdAdxyzTimesQEwald, eemdAdlatTimesQEwald


contains

    function det3D(M) result(det)
        real(dp), intent(in) :: M(3,3)
        real(dp) :: det

        det = M(1,1) * M(2,2) * M(3,3) &
            + M(1,2) * M(2,3) * M(3,1) &
            + M(1,3) * M(2,1) * M(3,2) &
            - M(1,1) * M(2,3) * M(3,2) &
            - M(1,2) * M(2,1) * M(3,3) &
            - M(1,3) * M(2,2) * M(3,1)

    end function det3D

    subroutine inv3DM(A, B)
        implicit none
        real(dp), intent(in), dimension(3, 3) :: A
        real(dp), intent(out), dimension(3, 3) :: B
        real(dp), dimension(3) :: a1, a2, a3
        real(dp), dimension(3) :: b1, b2, b3
        real(dp) :: det

        a1 = A(1, :)
        a2 = A(2, :)
        a3 = A(3, :)

        det = det3D(A)

        b1 = [a2(2) * a3(3) - a2(3) * a3(2), a1(3) * a3(2) - a1(2) * a3(3), a1(2) * a2(3) - a1(3) * a2(2)]
        b2 = [a2(3) * a3(1) - a2(1) * a3(3), a1(1) * a3(3) - a1(3) * a3(1), a1(3) * a2(1) - a1(1) * a2(3)]
        b3 = [a2(1) * a3(2) - a2(2) * a3(1), a1(2) * a3(1) - a1(1) * a3(2), a1(1) * a2(2) - a1(2) * a2(1)]

        b1 = b1 / det
        b2 = b2 / det
        b3 = b3 / det

        B(1, :) = b1
        B(2, :) = b2
        B(3, :) = b3
    end subroutine

    function vecMulVecT(n, a, b) result(c)
        integer, intent(in) :: n
        real(dp), intent(in) :: a(n), b(n)
        real(dp) :: c(n,n)
        integer :: i, j

        do i=1,n
            do j=1,n
                c(i,j) = a(i) * b(j)
            enddo
        enddo


    end function

    ! https://arxiv.org/pdf/1904.08875.pdf
    ! https://aip.scitation.org/doi/pdf/10.1063/1.5053968
    ! Be careful: in the first paper alpha != alpha_{RuNNer}
    ! alpha = 1 / (sqrt(2) eta)
    function getOptimalEta(nat, lat) result(eta)
        integer, intent(in) :: nat
        real(dp), intent(in) :: lat(3,3)
        real(dp) :: eta

        eta = 1._dp / sqrt(2._dp * PI) * (det3D(lat)**2 / nat)**(1._dp / 6._dp)

    end function getOptimalEta

    ! When the matrix is built, each reciprocal term takes O(nat**2) compuations
    ! Therefore reciprocal is not cheaper than real anymore and the optimal choice of eta is different (it is independant of nat)
    function getOptimalEtaMatrix(nat, lat) result(eta)
        integer, intent(in) :: nat
        real(dp), intent(in) :: lat(3,3)
        real(dp) :: eta

        eta = 1._dp / sqrt(2._dp * PI) * det3D(lat)**(1._dp / 3._dp)

    end function getOptimalEtaMatrix

    function getOptimalCutoffReal(eta, A) result(r)
        real(dp), intent(in) :: eta
        real(dp), intent(in) :: A
        real(dp) :: r

        if (A<0.d0) then
            print*, 'Error: Please specify ewald_prec in input.nn'
            stop
        endif
        r = sqrt(2._dp) * eta * sqrt(-log(A))

    end function getOptimalCutoffReal

    function getOptimalCutoffRecip(eta, A) result(r)
        real(dp), intent(in) :: eta
        real(dp), intent(in) :: A
        real(dp) :: r

        if (A<0.d0) then
            print*, 'Error: Please specify ewald_prec in input.nn'
            stop
        endif
        r = sqrt(2._dp) / eta * sqrt(-log(A))

    end function getOptimalCutoffRecip


    ! gaussian charges (and also ewald gaussians) are of the form 1/(2*pi*sigma**2)**(3/2) * exp(-r**2/(2*sigma**2))
    ! -> sigma = sqrt(2) * alpha_{cent}
    subroutine ewaldEnergy(nat, ats, tlat, q, sigma, e, f, stress)
        integer, intent(in) :: nat
        real(dp), intent(in) :: ats(3,nat), tlat(3,3), q(nat), sigma(nat)
        real(dp), intent(out) :: e, f(3,nat)
        real(dp), intent(out), optional :: stress(3,3)
        real(dp) :: ereal, erecip, eself
        real(dp) :: freal(3,nat), frecip(3,nat)
        real(dp) :: stressreal(3,3), stressrecip(3,3)
        real(dp) :: lat(3,3), eta

        lat = transpose(tlat)
        eta = getOptimalEta(nat, lat)
        eta = max(eta, maxval(sigma))

        call ewaldSumReal(nat, ats, lat, q, sigma, eta, ereal, freal, stressreal)
        call ewaldSumRecip(nat, ats, lat, q, eta, erecip, frecip, stressrecip)
        call ewaldSelf(nat, q, eta, eself)

        e = ereal + erecip + eself
        f = freal + frecip
        if (present(stress)) then
            stress = stressreal + stressrecip
        endif

    end subroutine ewaldEnergy

    subroutine recLattice(lat, reclat)
        real(dp), intent(in) :: lat(3,3)
        real(dp), intent(out) :: reclat(3,3)

        call inv3DM(transpose(lat), reclat)
        reclat(:,:) = reclat(:,:) * (2._dp * PI )

    end subroutine recLattice

    subroutine ewaldSumReal(nat, ats, lat, q, sigma, eta, e, f, stress)
        integer, intent(in) :: nat
        real(dp), intent(in) :: ats(3,nat), lat(3,3), q(nat), sigma(nat)
        real(dp), intent(in) :: eta
        real(dp), intent(out) :: e, f(3,nat), stress(3,3)

        real(dp) :: d(3), r, dlat(3), inv2eta, invsqrt2eta, interf, gamma, d2, cutoff, ftmp(3)
        integer :: i, j, k, iat, jat, n(3), sj, sk, jats

        invsqrt2eta = 1._dp / (sqrt(2._dp) * eta)
        inv2eta = 1._dp / (2._dp * eta)
        e = 0._dp
        f(:,:) = 0._dp
        stress(:,:) = 0._dp


        cutoff = getOptimalCutoffReal(eta, ewaldprec)
        call getNcells(lat, cutoff, n)

        !$omp parallel do private(i, sj, j, sk, k, dlat, jats, d, d2, r, interf, gamma, ftmp)&
        !$omp reduction(+:e,f,stress)
        do iat=1,nat
            do i=0,n(1)
                sj = -n(2)
                if (i==0) sj = 0
                do j=sj,n(2)
                    sk = -n(3)
                    if (i==0.and.j==0) sk = 0
                    do k=sk,n(3)
                        dlat(:) = i * lat(:,1) + j * lat(:,2) + k * lat(:,3)
                        jats = 1
                        if (i==0 .and. j==0 .and. k==0) jats=iat+1
                        do jat=jats,nat
                            if (i/=0 .or. j/=0 .or. k/=0 .or. iat/=jat) then
                                d(:) = ats(:,iat) - ats(:,jat) + dlat(:)
                                d2 = sum(d**2)
                                if (d2 > cutoff**2) cycle
                                r = sqrt(d2)
                                interf = erfc(r * invsqrt2eta)
                                if (sigma(1) > 0._dp) then
                                    gamma = sqrt(sigma(iat)**2 + sigma(jat)**2)
                                    !interf = erf(r / (sqrt(2._dp) * gamma)) - erf(r * inv2eta)
                                    interf = interf - erfc(r / (sqrt(2._dp) * gamma))
                                endif

                                e = e + 2._dp * q(iat) * q(jat) * interf / r

                                if (sigma(1) <= 0._dp) then
                                    ftmp(:) = &
                                        + q(iat) * q(jat) * d(:) / r**3 &
                                        * (2._dp * r * invsqrt2eta * exp(-invsqrt2eta**2 * r**2) / sqrt(PI) &
                                        + interf)
                                else
                                    ftmp(:) = &
                                        + q(iat) * q(jat) * d(:) / r**3 &
                                        * (2._dp * r * invsqrt2eta * exp(-invsqrt2eta**2 * r**2) / sqrt(PI) &
                                        - 2._dp * r / (sqrt(2._dp) * gamma) * exp(-1._dp / (2._dp * gamma**2) * r**2) / sqrt(PI) &
                                        + interf)
                                endif
                                f(:,iat) = f(:,iat) + ftmp(:)
                                f(:,jat) = f(:,jat) - ftmp(:)
                                stress(:,:) = stress(:,:) + vecMulVecT(3, d, ftmp)
                            endif
                        enddo
                    enddo
                enddo
            enddo
            if (sigma(1) > 0._dp) then ! self interaction
                e = e + q(iat)**2 / (sigma(iat) * 2._dp * sqrt(PI)) * 2._dp
            endif
        enddo
        !$omp end parallel do
        e = e / 2._dp
        stress = stress / det3D(lat)

    end subroutine ewaldSumReal

    subroutine ewaldSumRecip(nat, ats, lat, q, eta, e, f, stress)
        integer, intent(in) :: nat
        real(dp), intent(in) :: ats(3,nat), lat(3,3), q(nat)
        real(dp), intent(in) :: eta
        real(dp), intent(out) :: e, f(3,nat), stress(3,3)

        real(dp) :: reclat(3,3), dlat(3), V, r, Sreal, Simag, kr, dSreal, dSimag, factor, cutoff
        integer :: i, j, k, iat, n(3), sj, sk, nklats, ik
        integer, allocatable :: klats(:,:)
        real(dp), parameter :: unit(3,3) = reshape([1._dp, 0._dp, 0._dp, 0._dp, 1._dp, 0._dp, 0._dp, 0._dp, 1._dp], [3,3])

        call recLattice(lat, reclat)
        V = det3D(lat)
        e = 0._dp
        f = 0._dp
        stress = 0._dp

        cutoff = getOptimalCutoffRecip(eta, ewaldprec)
        call getNcells(reclat, cutoff, n)


        allocate(klats(3,(n(1)+1)*(n(2)*2+1)*(n(3)*2+1))) !! KK:here we just loop 1*n(1) to make use of the symmetry.
        nklats = 0


        ! build a list of all kpoints so that we can parallelize the loop over them
        do i=0,n(1)
            sj = -n(2)
            if (i==0) sj = 0
            do j=sj,n(2)
                sk = -n(3)
                if (i==0.and.j==0) sk = 0
                do k=sk,n(3)
                    nklats = nklats + 1
                    if (nklats>(n(1)+1)*(n(2)*2+1)*(n(3)*2+1)) stop 'too many k points'
                    klats(:,nklats) = [i,j,k]
                enddo
            enddo
        enddo


        !$omp parallel do default(shared) private(i,j,k,dlat,r,Sreal,Simag,kr,factor,iat,dSreal,dSimag) &
        !$omp reduction(+:e,f,stress)
        do ik=1,nklats
            i = klats(1,ik)
            j = klats(2,ik)
            k = klats(3,ik)
            if (i/=0 .or. j/=0 .or. k/=0) then
                dlat(:) = i * reclat(:,1) + j * reclat(:,2) + k * reclat(:,3)
                r = sum(dlat**2) ! r = k**2
                if (r > cutoff**2) cycle
                factor = exp(-eta**2 * r / 2._dp) / r
                Sreal = 0._dp
                Simag = 0._dp
                do iat=1,nat
                    kr = sum(dlat(:) * ats(:,iat))
                    Sreal = Sreal + q(iat) * cos(kr)
                    Simag = Simag + q(iat) * sin(kr)
                enddo
                e = e + 2._dp * factor * (Sreal**2 + Simag**2)
                stress(:,:) = stress(:,:) + (unit - vecMulVecT(3, dlat, dlat) *(eta**2 + 2._dp / r)) &
                        * 2._dp * factor * (Sreal**2 + Simag**2)
                do iat=1,nat
                    kr = sum(dlat(:) * ats(:,iat))
                    dSreal = q(iat) * (-sin(kr))
                    dSimag = q(iat) * cos(kr)

                    f(:,iat) = f(:,iat) - 2._dp * dlat(:) * factor &
                            * 2._dp * (Sreal * dSreal + Simag * dSimag)

                enddo
            endif
        enddo
        !$omp end parallel do

        e =          e  / V * 4._dp * PI  / 2._dp
        f(:,:) = f(:,:) / V * 4._dp * PI  / 2._dp
        stress(:,:) = stress(:,:) / V**2 * 4._dp * PI  / 2._dp

    end subroutine ewaldSumRecip

    subroutine ewaldSelf(nat, q, eta, e)
        integer, intent(in) :: nat
        real(dp), intent(in) :: q(nat)
        real(dp), intent(in) :: eta
        real(dp), intent(out) :: e
        e = -sum(q**2) / (sqrt(2._dp * PI) * eta)
    end subroutine ewaldSelf

    ! #######################################################################################################

    ! the periodic A matrix is not unique (depends on eta)
    ! however, because sum(q)==0, A*q is independent of eta
    ! therefore eem should work?!
    ! https://arxiv.org/pdf/1904.08875.pdf <- there might be a way to make A independant of eta
    subroutine eemMatrixEwald(nat, ats, tlat, sigma, hardness, A)
        integer, intent(in) :: nat
        real(dp), intent(in) :: ats(3,nat), tlat(3,3), sigma(nat), hardness(nat)
        real(dp), intent(out) :: A(nat, nat)
        real(dp) :: Atmp(nat, nat), lat(3,3), eta
        !        real(dp) :: et, st

        lat = transpose(tlat)
        eta = getOptimalEtaMatrix(nat, lat)
        eta = max(eta, maxval(sigma))

        A(:,:) = 0._dp
        !        call cpu_time(st)
        call AmatrixReal(nat, ats, lat, sigma, hardness, eta, Atmp)
        !        call cpu_time(et)
        !        write(*,*) 'real', et-st
        A(:,:) = A(:,:) + Atmp(:,:)
        !        call cpu_time(st)
        call AmatrixRecip(nat, ats, lat, eta, Atmp)
        !        call cpu_time(et)
        !        write(*,*) 'recip', et-st
        A(:,:) = A(:,:) + Atmp(:,:)
        !        call cpu_time(st)
        call AmatrixSelf(nat, ats, eta, Atmp)
        !        call cpu_time(et)
        !        write(*,*) 'self', et-st
        A(:,:) = A(:,:) + Atmp(:,:)

    end subroutine eemMatrixEwald

    subroutine AmatrixReal(nat, ats, lat, sigma, hardness, eta, A)
        integer, intent(in) :: nat
        real(dp), intent(in) :: ats(3,nat), lat(3,3), sigma(nat), hardness(nat)
        real(dp), intent(in) :: eta
        real(dp), intent(out) :: A(nat, nat)

        real(dp) ::  r, d(3), dlat(3), inv2eta, invsqrt2eta, gamma, interf, d2, cutoff
        integer :: i, j, k, iat, jat,n(3)

        invsqrt2eta = 1._dp / (sqrt(2._dp) * eta)
        inv2eta = 1._dp / (2._dp * eta)

        A = 0._dp

        cutoff = getOptimalCutoffReal(eta, ewaldprec)
        call getNcellsT(lat, cutoff, n)

        do iat=1,nat
            do i=-n(1),n(1)
                do j=-n(2),n(2)
                    do k=-n(3),n(3)
                        dlat(:) = i * lat(:,1) + j * lat(:,2) + k * lat(:,3)
                        do jat=iat,nat
                            if (i/=0 .or. j/=0 .or. k/=0 .or. iat/=jat) then
                                d(:) = ats(:,iat) - ats(:,jat) + dlat(:)
                                d2 = sum(d**2)
                                if (d2 > cutoff**2) cycle
                                r = sqrt(d2)
                                interf = erfc(r * invsqrt2eta)
                                if (sigma(1) > 0._dp) then
                                    gamma = sqrt(sigma(iat)**2 + sigma(jat)**2)
                                    interf = interf - erfc(r / (sqrt(2._dp) * gamma))
                                endif
                                A(iat, jat) = A(iat, jat) + interf / r
                            endif
                        enddo
                    enddo
                enddo
            enddo
            if (sigma(1) > 0._dp) then ! self interaction
                A(iat, iat) = A(iat, iat) + 1._dp / (sigma(iat) * sqrt(PI)) + hardness(iat)
            endif
        enddo

        do iat=1,nat
            do jat=iat+1,nat
                A(jat, iat) = A(iat,jat)
            enddo
        enddo

    end subroutine AmatrixReal

    ! please not that this routine has a different scaling that than the one ewaldEnergyRecip,
    ! as each reciprocal lattice point requires O(nat**2) instead of O(nat) computations
    ! hence the different optimal ewald eta for the matrix

    subroutine AmatrixRecip(nat, ats, lat, eta, A)
        integer, intent(in) :: nat
        real(dp), intent(in) :: ats(3,nat), lat(3,3)
        real(dp), intent(in) :: eta
        real(dp), intent(out) :: A(nat, nat)
        real(dp) :: reclat(3,3), dlat(3), V, r, Sreal, Simag, kri, krj, cutoff
        real(dp) :: dSreali, dSimagi, dSrealj, dSimagj
        real(dp) :: factor
        integer :: i, j, k, iat, jat, n(3)

        call recLattice(lat, reclat)
        V = det3D(lat)

        A = 0._dp

        cutoff = getOptimalCutoffRecip(eta, ewaldprec)
        call getNcellsT(reclat, cutoff, n)

        do i=-n(1),n(1)
            do j=-n(2),n(2)
                do k=-n(3),n(3)
                    if (i/=0 .or. j/=0 .or. k/=0) then
                        dlat(:) = i * reclat(:,1) + j * reclat(:,2) + k * reclat(:,3)
                        r = sum(dlat**2) ! r = k**2
                        if (r > cutoff**2) cycle
                        factor = exp(-eta**2 * r / 2._dp) / r
                        do iat=1,nat
                            kri = sum(dlat(:) * ats(:,iat))
                            do jat=iat,nat
                                krj = sum(dlat(:) * ats(:,jat))
                                A(iat,jat) = A(iat,jat) + factor * (cos(kri) * cos(krj) + sin(kri) * sin(krj))
                            enddo
                        enddo
                    endif
                enddo
            enddo
        enddo

        do iat=1,nat
            do jat=iat+1,nat
                A(jat, iat) = A(iat,jat)
            enddo
        enddo

        A = A / V * 4._dp * PI

    end subroutine AmatrixRecip

    subroutine AmatrixSelf(nat, ats, eta, A)
        integer, intent(in) :: nat
        real(dp), intent(in) :: ats(3,nat)
        real(dp), intent(in) :: eta
        real(dp), intent(out) :: A(nat, nat)
        integer :: i

        A(:,:) = 0._dp
        do i=1,nat
            A(i,i) = -2._dp / (sqrt(2._dp * PI) * eta)
        enddo

    end subroutine AmatrixSelf

    ! computes dAdxyzQ_ikl = sum_j (dA_ij / dx_kl) * q_j
    subroutine eemdAdxyzTimesQEwald(nat, ats, tlat, sigma, q, dAdxyzQ)
        integer, intent(in) :: nat
        real(dp), intent(in) :: ats(3,nat), tlat(3,3), sigma(nat), q(nat)
        real(dp), intent(out) :: dAdxyzQ(nat, 3, nat)
        real(dp) :: tmp(nat, 3, nat), lat(3,3), eta

        lat = transpose(tlat)
        eta = getOptimalEtaMatrix(nat, lat)
        eta = max(eta, maxval(sigma))

        call dAdxyzQReal(nat, ats, lat, sigma, q, eta, dAdxyzQ)
        call dAdxyzQRecip(nat, ats, lat, q, eta, tmp)
        dAdxyzQ(:,:,:) = dAdxyzQ(:,:,:) + tmp(:,:,:)

    end subroutine eemdAdxyzTimesQEwald

    subroutine dAdxyzQReal(nat, ats, lat, sigma, q, eta, dAdxyzQ)
        integer, intent(in) :: nat
        real(dp), intent(in) :: ats(3,nat), lat(3,3), sigma(nat), q(nat)
        real(dp), intent(in) :: eta
        real(dp), intent(out) :: dAdxyzQ(nat, 3, nat)

        real(dp) :: d(3), r, dlat(3), inv2eta, invsqrt2eta, interf, gamma, f(3), d2, cutoff
        integer :: i, j, k, iat, jat, n(3)

        inv2eta = 1._dp / (2._dp * eta)
        invsqrt2eta = 1._dp / (sqrt(2._dp) * eta)


        cutoff = getOptimalCutoffReal(eta, ewaldprec)
        call getNcellsT(lat, cutoff, n)

        dAdxyzQ(:,:,:) = 0._dp
        do iat=1,nat
            do i=-n(1),n(1)
                do j=-n(2), n(2)
                    do k=-n(3), n(3)
                        dlat(:) = i * lat(:,1) + j * lat(:,2) + k * lat(:,3)
                        do jat=1,nat
                            if (i/=0 .or. j/=0 .or. k/=0 .or. iat/=jat) then
                                d(:) = ats(:,iat) - ats(:,jat) + dlat(:)
                                d2 = sum(d**2)
                                if (d2 > cutoff**2) cycle
                                r = sqrt(d2)
                                interf = erfc(r * invsqrt2eta)
                                if (sigma(1) > 0._dp) then
                                    gamma = sqrt(sigma(iat)**2 + sigma(jat)**2)
                                    interf = interf - erfc(r / (sqrt(2._dp) * gamma))
                                endif

                                if (sigma(1) <= 0._dp) then
                                    f(:) = d(:) / r**3 &
                                        * (2._dp * r * invsqrt2eta * exp(-invsqrt2eta**2 * r**2) / sqrt(PI) &
                                        + interf) / 2._dp
                                else
                                    f(:) = d(:) / r**3 &
                                        * (2._dp * r * invsqrt2eta * exp(-invsqrt2eta**2 * r**2) / sqrt(PI) &
                                        - 2._dp * r / (sqrt(2._dp) * gamma) * exp(-1._dp / (2._dp * gamma**2) * r**2) / sqrt(pi) &
                                        + interf) / 2._dp
                                endif
                                ! f = dA_{iat,jat}/dxyz
                                dAdxyzQ(iat, :, iat) = dAdxyzQ(iat, :, iat) - f(:) * Q(jat)
                                dAdxyzQ(jat, :, jat) = dAdxyzQ(jat, :, jat) + f(:) * Q(iat)
                                dAdxyzQ(jat, :, iat) = dAdxyzQ(jat, :, iat) - f(:) * Q(iat)
                                dAdxyzQ(iat, :, jat) = dAdxyzQ(iat, :, jat) + f(:) * Q(jat)
                            endif
                        enddo
                    enddo
                enddo
            enddo
        enddo

    end subroutine dAdxyzQReal

    subroutine dAdxyzQrecip(nat, ats, lat, q, eta, dAdxyzQ)
        integer, intent(in) :: nat
        real(dp), intent(in) :: ats(3,nat), lat(3,3), q(nat)
        real(dp), intent(in) :: eta
        real(dp), intent(out) :: dAdxyzQ(nat, 3, nat)
        real(dp) :: reclat(3,3), dlat(3), V, r, Sreal, Simag, kri, krj, cutoff
        real(dp) :: dSreali, dSimagi, dSrealj, dSimagj
        real(dp) :: factor
        integer :: i, j, k, iat, jat, n(3)
        real(dp) :: dkri(3), dkrj(3)
        real(dp) :: dAdxi(3), dAdxj(3)

        call recLattice(lat, reclat)
        V = det3D(lat)

        cutoff = getOptimalCutoffRecip(eta, ewaldprec)
        call getNcellsT(reclat, cutoff, n)

        dAdxyzQ(:,:,:) = 0._dp

        do i=-n(1),n(1)
            do j=-n(2),n(2)
                do k=-n(3),n(3)
                    if (i/=0 .or. j/=0 .or. k/=0) then
                        dlat(:) = i * reclat(:,1) + j * reclat(:,2) + k * reclat(:,3)
                        r = sum(dlat**2) ! r = k**2
                        if (r > cutoff**2) cycle
                        factor = exp(-eta**2 * r / 2._dp) / r
                        do iat=1,nat
                            kri = sum(dlat(:) * ats(:,iat))
                            dkri(:) = dlat(:)
                            do jat=1,nat
                                krj = sum(dlat(:) * ats(:,jat))
                                dkrj(:) = dlat(:)
                                ! A(iat,jat) = A(iat,jat) + factor * (cos(kri) * cos(krj) + sin(kri) * sin(krj))
                                dAdxi(:) = factor * (-sin(kri) * dkri * cos(krj) + cos(kri) * dkri * sin(krj))
                                ! dAdxj(:) = factor * (cos(kri) * dkrj * -sin(krj) + sin(kri) * dkrj * cos(krj))

                                dAdxyzQ(iat, :, iat) = dAdxyzQ(iat, :, iat) + dAdxi(:) * Q(jat)
                                dAdxyzQ(jat, :, iat) = dAdxyzQ(jat, :, iat) + dAdxi(:) * Q(iat)

                                dAdxyzQ(jat, :, jat) = dAdxyzQ(jat, :, jat) - dAdxi(:) * Q(iat)
                                dAdxyzQ(iat, :, jat) = dAdxyzQ(iat, :, jat) - dAdxi(:) * Q(jat)

                            enddo
                        enddo
                    endif
                enddo
            enddo
        enddo

        dAdxyzQ = dAdxyzQ / V * 4._dp * PI / 2._dp
    end subroutine dAdxyzQrecip


    ! there is a dependence of A on eta. The partial derivative of this is not considered here since it cancels out when sum(q_i) == 0.
    ! these routines are not optimized, but they work and are tested vs finite differences.
    subroutine eemdAdlatTimesQEwald(nat, ats, latt, sigma, q, dAdlatQ)
        integer, intent(in) :: nat
        real(dp), intent(in) :: ats(3,nat), latt(3,3), sigma(nat), q(nat)
        real(dp), intent(out) :: dAdlatQ(nat, 3, 3)
        real(dp) :: tmp(nat, 3, 3), eta, lat(3,3)
        integer :: i

        lat = transpose(latt)

        !eta = getOptimalEtaMatrix(lat) ! Elements of A should not depend on eta. No derivative dependence here.
        eta = getOptimalEtaMatrix(nat, lat)
        eta = max(eta, maxval(sigma))

        call dAdlatQReal(nat, ats, lat, sigma, q, eta, dAdlatQ)
        call dAdLatQRecip(nat, ats, lat, q, eta, tmp)
        dAdlatQ(:,:,:) = dAdlatQ(:,:,:) + tmp(:,:,:)

        do i=1,nat
            dAdlatQ(i,:,:) = transpose(dAdlatQ(i,:,:))
        end do

    end subroutine eemdAdlatTimesQEwald

    ! this does not take the dependence on eta into account. However it should cancel out with the reciprocal part
    ! -> In theory dA/dlat = ... + dA/dEta dEta/dLat | dEta/dLat=0 (however not in real and recip. part individually, only the sum)
    subroutine dAdlatQReal(nat, ats, lat, sigma, q, eta, dAdlatQ)
        integer, intent(in) :: nat
        real(dp), intent(in) :: ats(3,nat), lat(3,3), sigma(nat), q(nat)
        real(dp), intent(in) :: eta
        real(dp), intent(out) :: dAdLatQ(nat, 3, 3)

        real(dp) ::  r, d(3), dlat(3), inv2eta, invsqrt2eta, gamma, interf, d2, cutoff
        integer :: i, j, k, iat, jat,n(3),p
        real(dp) :: invlat(3,3)
        real(dp) :: dd2(3,3), drel(3), dr(3,3), dinterf(3,3)


        dAdLatQ = 0._dp

        call inv3DM(lat, invlat)

        invsqrt2eta = 1._dp / (sqrt(2._dp) * eta)
        inv2eta = 1._dp / (2._dp * eta)

        cutoff = getOptimalCutoffReal(eta, ewaldprec)
        call getNcells(lat, cutoff, n)

        do iat=1,nat
            do i=-n(1),n(1)
                do j=-n(2),n(2)
                    do k=-n(3),n(3)
                        dlat(:) = i * lat(:,1) + j * lat(:,2) + k * lat(:,3)
                        do jat=iat,nat
                            if (i/=0 .or. j/=0 .or. k/=0 .or. iat/=jat) then
                                d(:) = ats(:,iat) - ats(:,jat) + dlat(:) ! d/dx
                                drel(:) = matmul(invlat, d) !
                                d2 = sum(d**2) ! d2/dx =
                                dd2(:,:) = 0._dp
                                do p=1,3
                                    dd2(:,p) = drel(p) * 2._dp * d(:)
                                end do
                                if (d2 > cutoff**2) cycle
                                r = sqrt(d2)
                                dr(:,:) = 1._dp / (2._dp * r) * dd2(:,:)
                                interf = erfc(r * invsqrt2eta)
                                dinterf = -2._dp * invsqrt2eta * exp(-invsqrt2eta**2 * r**2) / sqrt(PI) * dr(:,:)

                                if (sigma(1) > 0._dp) then
                                    gamma = sqrt(sigma(iat)**2 + sigma(jat)**2) ! dgamma/dx=0
                                    interf = interf - erfc(r / (sqrt(2._dp) * gamma))
                                    dinterf = dinterf + 2._dp / (sqrt(2._dp) * gamma) &
                                            * exp(-1._dp / (2._dp * gamma**2) * r**2) / sqrt(PI) * dr(:,:)
                                end if
                                dAdLatQ(iat,:,:) = dAdLatQ(iat,:,:) + q(jat) * (r * dinterf(:,:) - dr(:,:) * interf) / d2
                                if (iat /= jat) then
                                    dAdLatQ(jat,:,:) = dAdLatQ(jat,:,:) + q(iat) * (r * dinterf(:,:) - dr(:,:) * interf) / d2
                                end if
                            end if
                        end do
                    end do
                end do
            end do
        end do

    end subroutine

    subroutine dAdLatQRecip(nat, ats, lat, q, eta, dAdLatQ)
        integer, intent(in) :: nat
        real(dp), intent(in) :: ats(3,nat), lat(3,3)
        real(dp), intent(in) :: q(nat)
        real(dp), intent(in) :: eta
        real(dp), intent(out) :: dAdLatQ(nat, 3, 3)
        real(dp) :: reclat(3,3), dlat(3), V, r, kri, krj, cutoff
        real(dp) :: factor
        integer :: i, j, k, iat, jat, n(3), ii, jj
        real(dp) :: invlat(3,3), invlatt(3,3), ddlat(3,3,3), dreclat(3,3,3,3), dr(3,3)
        real(dp) :: dfactor(3,3), dkri(3,3), dkrj(3,3), relat(3), datsi(3,3,3), datsj(3,3,3), tmp(3,3), dV(3,3)

        call inv3DM(lat, invlat)
        invlatt = transpose(invlat)

        call recLattice(lat, reclat)
        V = det3D(lat)
        dV = V * invlatt

        dAdLatQ = 0._dp

        cutoff = getOptimalCutoffRecip(eta, ewaldprec)
        call getNcells(reclat, cutoff, n)

        do i=-n(1),n(1)
            do j=-n(2),n(2)
                do k=-n(3),n(3)
                    if (i/=0 .or. j/=0 .or. k/=0) then
                        dlat(:) = i * reclat(:,1) + j * reclat(:,2) + k * reclat(:,3)
                        do ii=1,3
                            do jj=1,3
                                dreclat(:,:,ii, jj) = -1._dp * vecMulVecT(3, invlatt(:,jj), invlatt(ii,:)) * 2._dp * PI
                            end do
                        end do
                        ddlat(:,:,:) = i * dreclat(:,1,:,:) + j * dreclat(:,2,:,:) + k * dreclat(:,3,:,:)

                        r = sum(dlat**2) ! r = k**2
                        dr = 0._dp
                        do ii=1,3
                            dr(:,:) = dr(:,:) + 2._dp * dlat(ii) * ddlat(ii,:,:)
                        end do
                        if (r > cutoff**2) cycle
                        factor = exp(-eta**2 * r / 2._dp) / r
                        dfactor = -1._dp * factor * (eta**2 / 2._dp * r + 1._dp) * dr(:,:) / r
                        do iat=1,nat
                            kri = sum(dlat(:) * ats(:,iat))
                            relat = matmul(invlat, ats(:,iat))
                            datsi = 0._dp
                            do ii=1,3
                                datsi(ii,ii,:) = relat(:)
                            end do
                            dkri = 0._dp
                            do ii=1,3
                                dkri(:,:) = dkri(:,:) + dlat(ii) * datsi(ii,:,:) + ddlat(ii,:,:) * ats(ii,iat)
                            end do
                            do jat=iat,nat
                                krj = sum(dlat(:) * ats(:,jat))
                                relat = matmul(invlat, ats(:,jat))
                                datsj = 0._dp
                                do ii=1,3
                                    datsj(ii,ii,:) = relat(:)
                                end do
                                dkrj = 0._dp
                                do ii=1,3
                                    dkrj(:,:) = dkrj(:,:) + dlat(ii) * datsj(ii,:,:) + ddlat(ii,:,:) * ats(ii,jat)
                                end do
                                tmp = dfactor * (cos(kri) * cos(krj) + sin(kri) * sin(krj)) &
                                    + factor * (-sin(kri) * dkri * cos(krj) - cos(kri) * sin(krj) * dkrj&
                                                + cos(kri) * dkri * sin(krj) + sin(kri) * cos(krj) * dkrj)
                                ! derivative wrt V
                                tmp = (V * tmp - factor * (cos(kri) * cos(krj) + sin(kri) * sin(krj)) * dV) / V**2

                                dAdLatQ(iat,:,:) = dAdLatQ(iat,:,:) + q(jat) * tmp
                                if (jat /= iat) then
                                    dAdLatQ(jat,:,:) = dAdLatQ(jat,:,:) + q(iat) * tmp
                                end if
                            end do
                        end do
                    end if
                end do
            end do
        end do

        dAdLatQ = 4._dp * PI * dAdLatQ

    end subroutine dAdLatQRecip

end module ewaldSummation
