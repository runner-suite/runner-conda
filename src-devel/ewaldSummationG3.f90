! Created by jonas on 28.08.20.


! this soubroutine takes also care of scaling the charges
subroutine electrostatics3G(nat, ats, lat, q, qtot, max_num_neighbors_elec, dchargedxyz, &
        invneighboridx_elec, zelem, e, f, lperiodic)
    use fileunits
    use globaloptions
    use nnflags
    use predictionoptions, only: enforcetotcharge
    use precision
    use eem
    use ewaldSummation

    integer, intent(in) :: nat
    real(dp), intent(in) :: ats(3,nat)
    real(dp), intent(in) :: lat(3,3)
    real(dp), intent(inout) :: q(nat)
    real(dp), intent(in) :: qtot ! total charge for the scaling
    integer, intent(in) :: max_num_neighbors_elec                                    ! in
    real(dp), intent(in) ::  dchargedxyz(max_num_atoms,0:max_num_neighbors_elec,3)
    integer, intent(in) :: invneighboridx_elec(nat,max_num_atoms)
    !integer, intent(in) :: num_funcvalues_elec(nelem)
    integer, intent(in) :: zelem(max_num_atoms)
    !integer, intent(in) :: atomindex(nat) ! todo: can we ignore this? KK: I think so
    real(dp), intent(out) :: e
    real(dp), intent(out) :: f(3,nat)
    logical, intent(in) :: lperiodic

    real(dp) :: sigma(nat), hardness(nat)
    real(dp) :: dAdxyzQ(nat, 3, nat)
    real(dp) :: A(nat, nat)
    real(dp) :: dqdx(nat, 3, nat)
    real(dp) :: dqdxcorr(3, nat)
    real(dp) :: dEdQ(nat)
    real(dp) :: escreen,fscreen(3,nat)
    integer :: i1, i2, i3, i4, i5


    ! TODO: finite differences have been tested, but further testing is still needed!
    ! Since point charges were abandoned quite early in the development of G4, there is no guarantee that they are correct!

    e = 0._dp
    f(:,:) = 0._dp
    dEdQ(:)=0.0d0
    dqdx(:,:,:) = 0.d0
    do i3=1,nat
        do i4=1,nat
            if(invneighboridx_elec(i3,i4) /= -1)then
                dqdx(i3,:,i4) = dchargedxyz(i3,invneighboridx_elec(i3,i4),:) ! todo: check if indices are used correctly!
                !do i5=1,num_funcvalues_elec(elementindex(zelem(atomindex(i3))))
                !    dqdx(i3,:,i4)=dqdx(i3,:,i4)+ &
                !            dchargedsfunc(i3,i5)*dsfuncdxyze(i5,i3,invneighboridx_elec(i3,i4),:)
                !enddo
            endif
        enddo
        if (sum(sum(dqdx(i3,:,:), 2)**2) > 1.d-10) then
            print*, sum(sum(dqdx(i3,:,:), 2)**2)
            print*, 'Error: dqdx in electrostatics3G is not invariant under translation!'
            stop
        endif
        !print*, 'dQdx', i3, sum(dqdx(i3,:,:), 2)
    enddo

    ! if the charges are being scaled, we have to make sure the dqdx are correct
    if (enforcetotcharge==1) then
        q(:) = q(:) - (sum(q) - qtot) / nat
        dqdxcorr(:,:) = sum(dqdx, 1) / nat
        do i3=1,nat
            dqdx(i3,:,:) = dqdx(i3,:,:) - dqdxCorr(:,:)
        enddo
    else
        if (lperiodic) then
            print*, 'Calculation electrostatic energy for a period system with non-zero charge (no scaling) does not make sense!'
            stop
        endif
    endif

    ! setting sigma to -1 indicates point charges
    sigma(:) = -1.d0
    hardness = 0.d0
    !call ewaldEnergy(num_atoms, xyzstruct, lattice, nnatomcharge, sigma, nnelecenergypart, nnelecforcepart)
    if (lperiodic) then
        call eemMatrixEwald(nat, ats, lat, sigma, hardness, A)
        call eemdAdxyzTimesQEwald(nat, ats, lat, sigma, q, dAdxyzQ)
    else
        call eemMatrix(nat, ats, sigma, hardness, A)
        call eemdAdxyzTimesQ(nat, ats, q, sigma, dAdxyzQ)
    endif

    ! we use the fact that E_{elec} = 0.5 * q^t Q q
    e = 0.5_dp * sum(q * matmul(A,q))
    do i1=1,nat
        f(:,:) = f(:,:) - 0.5_dp * q(i1) * dAdxyzQ(i1,:,:)
    enddo

    if(lscreen)then
        if (lperiodic) then
            call getscreeningperiodic(nat, lat, ats, q, sigma, escreen, fscreen, dedq)
        else
            call getscreeningfree(nat, ats, q, sigma, escreen, fscreen, dedq)
        endif
        e = e+escreen
        f(:,:) = f(:,:)+fscreen(:,:)
    endif
    ! but of course we also have to add these additional terms with the dq/dx
    do i1=1,nat
        f(:,:) = f(:,:) &
                - (sum(A(i1,:) * q(:)) + dEdQ(i1)) * dqdx(i1,:,:)
    enddo
    if (sum(sum(f,2)**2) > 1.d-10) then
        print*, sum(f,2)
        print*, 'Error: force sum on all atoms is not zero in electrostatics3G'
        stop
    endif
end subroutine electrostatics3G
