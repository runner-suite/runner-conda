module energyAndForces3G

    implicit none
    contains

subroutine get_energies_and_forces_for_3G(num_atoms,zelem,lattice,&
               xyzstruct,atomspin,totalcharge,lperiodic,minvalue,maxvalue,&
               avvalue,nnatomcharge,nntotalenergy,nntotalforces,&
               enforcetotcharge_local,qtot,ldoforces_local, elecenergy, elecforce, atomicenergy)
    use globaloptions 
    use symfunctions
    use nnewald
    use nnshort_atomic
    use fileunits
    use nnflags

    implicit none
 
    integer,intent(in) :: num_atoms
    integer :: num_neighbors(num_atoms)
    integer :: max_num_neighbors 
    integer lsta(2,max_num_atoms)                          ! internal, numbers of neighbors
    integer lstc(listdim)                                  ! internal, identification of atom
    integer lste(listdim)                                  ! internal, nuclear charge of atom
    integer :: i1,i2,i3,i4,i5
    integer,allocatable,dimension(:) :: atomindex_dummy
    integer,allocatable,dimension(:,:) :: neighboridx
    integer,allocatable,dimension(:,:) :: invneighboridx
    integer :: ielem
    integer :: zelem(max_num_atoms)
    integer :: num_atoms_element(nelem)
    integer :: enforcetotcharge_local

    real*8,intent(in) :: atomspin(num_atoms)
    real*8,intent(in) :: xyzstruct(3,num_atoms)
    real*8,intent(out) :: nntotalenergy
    real*8,intent(out) :: nntotalforces(3,num_atoms)
    real*8,intent(in) :: lattice(3,3)
    real*8,intent(in) :: minvalue(nelem,maxnum_funcvalues_short_atomic)
    real*8,intent(in) :: maxvalue(nelem,maxnum_funcvalues_short_atomic)
    real*8,intent(in) :: avvalue(nelem,maxnum_funcvalues_short_atomic)
    real*8, intent(in) :: totalcharge !! KK: total charge of the system
    real*8, intent(in) :: qtot  !! KK: enforce_totcharge
    real*8, intent(out), optional :: elecenergy, elecforce(3,num_atoms), atomicenergy(num_atoms)
    real*8 :: nnelecenergy
    real*8 :: nnelecforces(3,num_atoms)
    real*8 :: nnshortenergy
    real*8 :: nnatomenergy(num_atoms)
    real*8 :: nnshortforces(3,num_atoms)
    real*8 :: nnatomcharge(num_atoms)
    real*8 :: symfunction(maxnum_funcvalues_short_atomic,max_num_atoms)
    real*8 :: rdummy
    real*8 lstb(listdim,4)
    real*8, allocatable :: dchargedxyz(:,:,:)
    real*8 :: dchargedsfunc(max_num_atoms,maxnum_funcvalues_short_atomic)
    real*8,allocatable,dimension(:,:,:,:) :: dsfuncdxyz
    real*8 strs(3,3,maxnum_funcvalues_short_atomic,max_num_atoms)       ! internal
    real*8 :: threshold
    real*8,allocatable,dimension(:,:) :: deshortdsfunc
    real*8 :: nodes_values(maxnum_layers_elec, maxnodes_elec) ! apparently just dummy 
    real*8 :: nodes_sum(maxnum_layers_elec, maxnodes_elec) ! just dummy too
    real*8 :: atomenergysum
    real*8 :: dqdxCorr(3,num_atoms)
    real*8 :: fscreen,fscreenderiv    
    real*8 :: d, r(3), e, de, f(3)

    integer :: max_num_atoms_old
    logical, intent(in) :: lperiodic
    logical lrmin 
    logical lextrapolation
    logical ldoforces_local
    ! Changing max_num_atoms might be a bit risky... Whatever, it works for now!
    !max_num_atoms_old = max_num_atoms
    !max_num_atoms = num_atoms
    !! initialization



    lrmin=.true.
    dchargedsfunc(:,:)=0.0d0
    dqdxCorr(:,:)=0.0d0
    threshold =0.0001d0
    nnelecenergy = 0.d0
    nnelecforces(:,:) = 0.d0
    !!
    call getneighborsatomic(num_atoms,num_neighbors,&
         zelem, max_num_neighbors,&
         lsta,lstc,lste,maxcutoff_short_atomic,&
         lattice,xyzstruct,lstb,lperiodic)
    allocate(dsfuncdxyz(maxnum_funcvalues_short_atomic,max_num_atoms,0:max_num_neighbors,3))
    allocate(neighboridx(num_atoms,0:max_num_neighbors))
    allocate(invneighboridx(num_atoms,max_num_atoms))
    call getneighboridxatomic(num_atoms,listdim,&
         max_num_atoms,max_num_neighbors,&
         lsta,lstc,neighboridx,invneighboridx)
    allocate(atomindex_dummy(max_num_atoms))
    do i2 =1,num_atoms
      ielem=elementindex(zelem(i2))
      atomindex_dummy(i2)=i2
    enddo
    allocate(dchargedxyz(max_num_atoms,0:max_num_neighbors,3)) 

    if(lusefixedcharge)then
      nnatomcharge(:)=0.0d0
      do i2=1,num_atoms  
        nnatomcharge(i2)=fixedcharge(elementindex(zelem(i2)))
      enddo
      dchargedxyz(:,:,:)=0.0d0
      call electrostatics3G(num_atoms,xyzstruct,lattice,nnatomcharge,totalcharge,max_num_neighbors,&
           dchargedxyz,invneighboridx,zelem,&
           nnelecenergy,nnelecforces,lperiodic)
    elseif(luseatomcharges)then
      nnatomcharge(:)=0.0d0
      call readcharges(max_num_atoms,num_atoms,nnatomcharge)
      dchargedxyz(:,:,:)=0.0d0 
      call electrostatics3G(num_atoms,xyzstruct,lattice,nnatomcharge,totalcharge,max_num_neighbors,&
           dchargedxyz,invneighboridx,zelem,&
           nnelecenergy,nnelecforces,lperiodic)
    else
        if(.not.lshort)then
            !! KK: Here we want to take care the case of the prediction of the electrostatic part,
            !!     but this subroutine only consider parameters of short-range symmetry functions
            !!     Therefore, here we put all parameters obtained from electrostatic part to short
            !!     -range part
            function_type_short_atomic(:,:)=function_type_elec(:,:) 
            symelement_short_atomic(:,:,:)=symelement_elec(:,:,:) 
            funccutoff_short_atomic(:,:)=funccutoff_elec(:,:)
            eta_short_atomic(:,:)=eta_elec(:,:) 
            zeta_short_atomic(:,:)=zeta_elec(:,:) 
            lambda_short_atomic(:,:)=lambda_elec(:,:) 
            rshift_short_atomic(:,:)=rshift_elec(:,:)     
        endif
        dchargedxyz(:,:,:)=0.0d0
        !function_type_elec = function_type_short_atomic
        !call getchargesatomic_type5(1,num_atoms,&
        !        atomindex_dummy,max_num_neighbors,&
        !        invneighboridx,zelem,&
        !        lsta,lstc,lste,lstb,xyzstruct,atomspin,&
        !        minvalue,maxvalue,avvalue,&
        !        scmin_short_atomic,scmax_short_atomic,nnatomcharge,totalcharge,&
        !        Amatrix,lextrapolation, lattice, lperiodic)
        !write(*,*) 'QQQX', nnatomcharge
        call calconefunction_atomic(cutoff_type,cutoff_alpha,&
                max_num_neighbors,max_num_atoms,1,num_atoms,&
                atomindex_dummy,max_num_atoms,elementindex,&
                maxnum_funcvalues_short_atomic,num_funcvalues_short_atomic,nelem,&
                zelem,listdim,lsta,lstc,lste,invneighboridx,&
                function_type_short_atomic,symelement_short_atomic,&
                xyzstruct,atomspin,symfunction,rmin,funccutoff_short_atomic,&
                eta_short_atomic,rshift_short_atomic,lambda_short_atomic,&
                zeta_short_atomic,dsfuncdxyz,strs,lstb,lperiodic,.true.,.false.,.false.,&
                lrmin)
        !!==============================================================================
        !! Do the scaling or centering for the SFs
        !!==============================================================================
        do i2 =1,num_atoms
            ielem=elementindex(zelem(i2))
            do i3 =1,num_funcvalues_short_atomic(ielem)
                lextrapolation=.false.
                if((symfunction(i3,i2)-maxvalue(ielem,i3)).gt.threshold)then
                    write(ounit,'(a,a5,x,2i5,x,a,2f18.8)')&
                            '### EXTRAPOLATION WARNING ### ','short',&
                            i3,i2,'too large',symfunction(i3,i2),maxvalue(ielem,i2)
                    lextrapolation=.true.
                elseif((-symfunction(i3,i2)+minvalue(ielem,i3)).gt.threshold)then
                    write(ounit,'(a,a5,x,2i5,x,a,2f18.8)')&
                            '### EXTRAPOLATION WARNING ### ','short',&
                            i3,i2,'too small',symfunction(i3,i2),minvalue(ielem,i2)
                    lextrapolation=.true.
                endif
                if(lcentersym.and..not.lscalesym)then
                    !! For each symmetry function remove the CMS of the respective element
                    symfunction(i3,i2)=symfunction(i3,i2)-avvalue(ielem,i3)
                elseif(lscalesym.and..not.lcentersym)then
                    !! Scale each symmetry function value for the respective element
                    symfunction(i3,i2)=(symfunction(i3,i2)&
                            -minvalue(ielem,i3))/ &
                            (maxvalue(ielem,i3)-minvalue(ielem,i3))&
                            *(scmax_short_atomic-scmin_short_atomic) + scmin_short_atomic
                elseif(lscalesym.and.lcentersym)then
                    symfunction(i3,i2)=(symfunction(i3,i2)&
                            -avvalue(ielem,i3))/ &
                            (maxvalue(ielem,i3)-minvalue(ielem,i3))
                else
                endif
            enddo
        enddo
        !!==============================================================================
        !! Calculate atomic charges
        !!==============================================================================
        do i2 =1,num_atoms
            ielem=elementindex(zelem(i2))
            nodes_values(:,:) = 0.d0
            nodes_sum(:,:) = 0.d0
            call calconenn(1,maxnum_funcvalues_elec,maxnodes_elec,&
                    maxnum_layers_elec,num_layers_elec(ielem),&
                    maxnum_weights_elec,nodes_elec(0,ielem),&
                    symfunction(1,i2),&
                    weights_elec(1,ielem),&
                    nodes_values,nodes_sum,&
                    nnatomcharge(i2),actfunc_elec(1,1,ielem))
            !print*, 'A', weights_elec
        enddo
        !!==============================================================================
        !! Do the scaling for dsfuncdxyz
        !!==============================================================================
        if(.not.luseatomspins)then
            if(lscalesym)then
                call scaledsfunc(max_num_neighbors,&
                        maxnum_funcvalues_short_atomic,num_funcvalues_short_atomic,&
                        nelem,num_atoms,minvalue,maxvalue,&
                        scmin_short_atomic, scmax_short_atomic,&
                        zelem,dsfuncdxyz,strs)
            endif
            call getdchargedsfunc(num_atoms,zelem,symfunction,dchargedsfunc)
        !!==============================================================================
        !! Calculating the electrostatic energy and forces
        !!==============================================================================
        ! todo: this is a code duplication of coulombpara.f90

            do i3=1,num_atoms
               do i4=1,num_atoms
                  do i5=1,num_funcvalues_short_atomic(elementindex(zelem(i3)))
                     if(invneighboridx(i3,i4)/=-1)then
                       dchargedxyz(i3,invneighboridx(i3,i4),:)=dchargedxyz(i3,invneighboridx(i3,i4),:)+ &
                              dchargedsfunc(i3,i5)*dsfuncdxyz(i5,i3,invneighboridx(i3,i4),:)
                     endif
                  enddo
               enddo
            enddo
            call electrostatics3G(num_atoms,xyzstruct,lattice,nnatomcharge,totalcharge,max_num_neighbors,&
                 dchargedxyz,invneighboridx,zelem,&
                 nnelecenergy,nnelecforces,lperiodic)
        endif    
    endif
!!==============================================================================
!! Calculating the short range energies and forces
!!==============================================================================
    if(lshort)then
        call calconeshort(num_atoms,zelem,&
              symfunction,nnshortenergy,nnatomenergy)
        allocate(deshortdsfunc(max_num_atoms,maxnum_funcvalues_short_atomic))
        deshortdsfunc(:,:)=0.0d0
        call getshortforces(max_num_neighbors,&
             num_atoms,num_neighbors,neighboridx,zelem,&
             symfunction,dsfuncdxyz,deshortdsfunc,&
             nnshortforces)
!!=================================================================================
!! Calculating the total energies and forces
!!=================================================================================   
        if(lremoveatomenergies.and.lshort)then
            num_atoms_element(:) = 0
            do i1=1,num_atoms ! loop over all atoms
                num_atoms_element(elementindex(zelem(i1))) = num_atoms_element(elementindex(zelem(i1))) + 1
            enddo
          call addatoms(num_atoms,&
               zelem,num_atoms_element,&
               atomenergysum,nnatomenergy)
          nnshortenergy=nnshortenergy+atomenergysum
        endif ! lremoveatomenergies
        deallocate(deshortdsfunc)
    else !! no short-range contributions
        nnatomenergy(:)=0.0d0
        nnshortenergy=0.0d0
        nnshortforces(:,:)=0.0d0 
    endif
    nntotalenergy = nnelecenergy + nnshortenergy
    !write(*,*) 'Eelec', nnelecenergy
    !write(*,*) 'Eshort', nnshortenergy
    !write(*,*) 'Etotal', nntotalenergy
    nntotalforces(:,:num_atoms) = nnelecforces(:,:num_atoms)+nnshortforces(:,:num_atoms)
    !max_num_atoms = max_num_atoms_old

    if (present(elecenergy)) then
        elecenergy = nnelecenergy
    endif
    if (present(elecforce)) then
        elecforce = nnelecforces(:,:num_atoms)
    endif
    if(present(atomicenergy)) then
        atomicenergy = nnatomenergy(:num_atoms)
    endif

!!
end subroutine

end module energyAndForces3G
