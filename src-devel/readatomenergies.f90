!######################################################################
! This routine is part of
! RuNNer - RuNNer Neural Network Energy Representation
! (c) 2008-2024 Prof. Dr. Joerg Behler 
! Ruhr-Universitaet Bochum, Germany
!
! This program is free software: you can redistribute it and/or modify it 
! under the terms of the GNU General Public License as published by the 
! Free Software Foundation, either version 3 of the License, or 
! (at your option) any later version.
!
! This program is distributed in the hope that it will be useful, but 
! WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
! or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
! for more details.
!
! You should have received a copy of the GNU General Public License along 
! with this program. If not, see http://www.gnu.org/licenses.
! WARNING: THIS FILE IS PART OF THE UNMAINTAINED CONDA PACKAGE OF RUNNER 1.3. 
!######################################################################
!! called by:
!! - readinput.f90
!!
      subroutine readatomenergies()
!!
      use fileunits
      use globaloptions
!!
      implicit none
!!
      integer i1
      integer ztemp
      logical stop_flag
!!
      character*2 elementtemp                           ! internal
      character*40 dummy                                ! internal
      character*2 elementsymbol(nelem)                  ! internal
      character*40 keyword                              ! internal
!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!! Open input.nn and read in all lines with keyword 'atom_energy'.
      open(nnunit,file='input.nn',form='formatted',status='old')
        rewind(nnunit)
31      continue

!! Search for keyword atom_energy.
        read(nnunit,*,END=32)keyword
        if(keyword.eq.'atom_energy')then
          backspace(nnunit)

!! Read in element symbols and store their nuclear charges.
          read(nnunit,*,err=99)dummy,elementtemp
          call nuccharge(elementtemp,ztemp)

!! Check if these elements are part of the nucelem list, i.e.
!! if they were specified as being part of the system.
          do i1=1,nelem
            if(ztemp.eq.nucelem(i1))then
!! If the value of atomrefenergies(i1) is not 0.0d0,
!! this atom energy has been read before, which must not happen
              if(atomrefenergies(i1).ne.0.0d0)then
                write(ounit,*) &
                '### ERROR ### Atomic reference energy for atom ', &
                elementtemp, &
                ' has been specified before. ',atomrefenergies(i1)
!! JB added a hard stop here, because multiple specifications of the same element are critical errors
                stop

!! If the atom is in the system and no warning is prompted,
!! read in the value of the atom energy.
              else
                backspace(nnunit)
                read(nnunit,*,err=99) &
                  dummy, &
                  elementsymbol(i1), &
                  atomrefenergies(i1)
                goto 31
              endif
            endif
          enddo

!! If the atom is not in nucelem, ignore this energy.
          write(ounit,*) &
            '### WARNING ### Atomic reference energy for atom ', &
            elementtemp, &
            ' is ignored.'
        endif
        goto 31
32      continue
      close(nnunit)

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!check if all energies have been found.

!! Assume everything is okay, therefore set stop_flag to false.
      stop_flag = .false.
      do i1=1,nelem
        if (atomrefenergies(i1).eq.0.0d0)then
          write(ounit,*) &
            '### ERROR ### atom_energy for atom ', &
            nucelem(i1), &
            ' not specified.'
          stop_flag = .true.
        endif
      enddo

!! If any energy is missing, stop.
      if (stop_flag.eqv..true.)then
        stop
      endif
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!write reference energies to runner.out
      write(ounit,*)'-------------------------------------------------------------'
      write(ounit,*)'Atomic reference energies read from input.nn:'
      do i1=1,nelem
        write(ounit,'(a1, a2, x,f18.8)') &
          ' ', &
          elementsymbol(i1), &
          atomrefenergies(i1)
      enddo
      write(ounit,*)'-------------------------------------------------------------'
!!
      return
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!! If the number of keyword arguments is wrong, print error and stop.
99    continue
      write(ounit,*)'Error: keyword ',keyword
      write(ounit,*)'is missing arguments '
      stop
      end
