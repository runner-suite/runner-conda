!######################################################################
! This routine is part of
! RuNNer - RuNNer Neural Network Energy Representation
! (c) 2008-2024 Prof. Dr. Joerg Behler 
! Ruhr-Universitaet Bochum, Germany
!
! This program is free software: you can redistribute it and/or modify it 
! under the terms of the GNU General Public License as published by the 
! Free Software Foundation, either version 3 of the License, or 
! (at your option) any later version.
!
! This program is distributed in the hope that it will be useful, but 
! WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
! or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
! for more details.
!
! You should have received a copy of the GNU General Public License along 
! with this program. If not, see http://www.gnu.org/licenses.
! WARNING: THIS FILE IS PART OF THE UNMAINTAINED CONDA PACKAGE OF RUNNER 1.3. 
!######################################################################
!! called by:
!! - checkstructures.f90
!!
      subroutine checkonestructure(inumber,lelement)
!!
      use fileunits
      use globaloptions
      use nnflags
!!
      implicit none
!!
      integer nlattice                                                  ! number of lattice vectors found
      integer num_atoms                                                 ! number of atoms in structure
      integer zelem(max_num_atoms)                                      ! nuclear charges of the atoms
      integer inumber
      integer i                                                         ! internal 
!!
      integer :: ios                                                    ! internal Niko
      integer :: l                                                      ! internal Niko
      integer :: inputdata_n_prop                                       ! internal Niko
!!      
      real*8 lattice(3,3)                                               ! lattice vectors if present
      real*8 xyzstruct(3,max_num_atoms)
      real*8 totalforce(3,max_num_atoms)
      real*8 atomcharge(max_num_atoms)
      real*8 atomenergy(max_num_atoms)
      real*8 totalcharge                                                ! total charge of structure
      real*8 totalenergy                                                ! total energy of structure
      real*8 threshold                                                  ! internal threshold for consistency checks of energy and charge
      real*8 atomicchargesum                                            ! temp variable
      real*8 atomicenergysum                                            ! temp variable
      real*8 volume                                                     ! internal

      real*8, target :: inputdata_prop_arr(max_num_prop, max_num_atoms) ! internal Niko, array of all real values in input.data
!!
      character*21 keyword
      character*2 elementsymbol(max_num_atoms)

      character*200 begin_line                                          ! internal Niko, string containing data of 'begin' line in input.data
      character*400 atom_line                                           ! internal Niko, string containing data of 'atom' line in input.data
      character*20, allocatable, dimension(:) :: atom_line_check        ! internal, Niko array for checking number of columns
!!                                                                      
      logical lbegin                                                    ! true if 'begin' flag of structure found
      logical lend                                                      ! true if 'end' flag of structure found
      logical lcharge                                                   ! true if 'charge' keyword is found
      logical lenergy                                                   ! true if 'energy' keyword is found
      logical lperiodic                                                 ! true if 3 lattice vectors are found
      logical loldinputdata 
      logical lelement(102)

!! Niko, input.data

      type propertyindexes                                              ! internal Niko, holds column index
        integer :: x_coord                                              ! of respeective property 
        integer :: y_coord                                              ! in input.data
        integer :: z_coord 
        integer :: elem_sym
        integer :: atom_charge
        integer :: atom_spin
        integer :: atom_energy
        integer :: x_force
        integer :: y_force
        integer :: z_force
      endtype propertyindexes

      type(propertyindexes) :: inputdata_col_indx
!!=============================================================================
!! initializations
!!=============================================================================
      nlattice   = 0
      lbegin     = .false.
      lend       = .false.
      lcharge    = .false.
      lenergy    = .false.
      lperiodic  = .false.
      num_atoms  = 0
      threshold  = 0.005d0
      atomicenergysum = 0.0d0
      atomicchargesum = 0.0d0
      lattice(:,:)    = 0.0d0
      zelem(:) = 0
      xyzstruct(:,:)       = 0.0d0
      totalforce(:,:)      = 0.0d0
      elementsymbol(:)     = '  '
      totalcharge          = 0.0d0
      totalenergy          = 0.0d0
      loldinputdata        = .false.

!! Niko
      inputdata_col_indx = propertyindexes(0,0,0,0,0,0,0,0,0,0)
      ios                         = 0
      inputdata_n_prop            = 0
      inputdata_prop_arr(:,:)     = 0.0d0
!!
      outer: do
        read(dataunit,*)keyword
!! 
        begin_routine: if((keyword.eq.'begin') .and. (lbegin .eqv..false.)) then 
          lbegin=.true. 
          backspace(dataunit)
        
          read(dataunit, '(A200)', IOSTAT=ios) begin_line      ! Niko, begin line cannot be longer than 200 characters
          if(ios.eq.0) then
            call readbeginline(begin_line, loldinputdata, & 
            inputdata_col_indx, inputdata_n_prop)
          else
            write(*,*) 'Error while reading begin line in input data& 
            in structure ', inumber
            stop
          endif
        endif begin_routine

        if(keyword.eq.'lattice') then
          nlattice=nlattice+1
          backspace(dataunit)
          read(dataunit,*,err=90)keyword,(lattice(nlattice,i),i=1,3)
        endif
!!
        if((keyword.eq.'atom').and.(loldinputdata.eqv..false.)) then
          num_atoms=num_atoms+1
          backspace(dataunit)
          read(dataunit, '(A400)', IOSTAT=ios) atom_line
          if(ios.eq.0) then
            continue
          else
            write(*,*) 'Error while reading atom line in inputdata&
            in structure ', inumber
            stop
          endif 
          count_col: do l=1, (max_num_prop+3)     ! Niko, +3 to check if too many properties were given
            if(l>(max_num_prop+2)) then
              write(*,*) 'Error. Number of columns in input.data& 
              exceeds maximum of: ', max_num_prop, 'in structure ', inumber
              stop
            endif       
            allocate(atom_line_check(l))
            read(atom_line, *, IOSTAT=ios) atom_line_check(:l)
            if(ios < 0) then                       ! Niko, negative ios means that EOR was encountered
              deallocate(atom_line_check)               
              exit
            endif
            deallocate(atom_line_check)
          enddo count_col

          check_num_col: if((l-2).eq.inputdata_n_prop) then  ! Niko, -2 because l starts at 1 and first col is 'atom'
            continue
          else
            write(*,*) 'Error. Number of columns does not match number &
            of given properties in input.data in structure ', inumber
            stop
          endif check_num_col

          backspace(dataunit)
          if(inputdata_col_indx%elem_sym /= 1) then                           ! Niko, if element symbol not in first column
            read(dataunit, *, IOSTAT=ios)keyword, &
            inputdata_prop_arr(:(inputdata_col_indx%elem_sym-1), num_atoms), &  
            elementsymbol(num_atoms), &                                       ! Niko elem_sym already in final array 
            inputdata_prop_arr((inputdata_col_indx%elem_sym+1):inputdata_n_prop,&
            num_atoms)                                                        ! otherwise mismatched type (real and character)
          elseif(inputdata_col_indx%elem_sym.eq.1) then                       ! Niko, if element symbol in first column
            read(dataunit, *, IOSTAT=ios)keyword, &
            elementsymbol(num_atoms), &
            inputdata_prop_arr((inputdata_col_indx%elem_sym+1):inputdata_n_prop,&
            num_atoms)
          endif
          if(ios.eq.0) then
            continue
          elseif(ios>0) then
            write(*,*) "Error. Wrong data type encountered while reading &
            input.data. Check column of element symbol with &
            respect to the location of the 'elem_symbol' keyword& 
            in structure ", inumber
            stop
          elseif(ios<0) then
            write(*,*) 'Error. Number of columns does not match number &
            of given properties in input.data&
            in structure ', inumber
            stop
          endif 

          call nuccharge(elementsymbol(num_atoms),zelem(num_atoms))
          lelement(zelem(num_atoms))=.true. ! element found   
!! 
      elseif((keyword.eq.'atom').and.loldinputdata.eqv..true.) then
        num_atoms=num_atoms+1
        backspace(dataunit)
        read(dataunit,*,err=91)keyword,(xyzstruct(i,num_atoms),i=1,3),&
          elementsymbol(num_atoms),atomcharge(num_atoms),&
          atomenergy(num_atoms),(totalforce(i,num_atoms),i=1,3)
        call nuccharge(elementsymbol(num_atoms),zelem(num_atoms))
        lelement(zelem(num_atoms))=.true. ! element found   
      endif
!!      
      if(keyword.eq.'charge') then
        backspace(dataunit)
        if(lcharge)  then
          write(ounit,*)'Error: 2 total charges given for structure ',inumber
          stop
        endif
        read(dataunit,*,err=92) keyword,totalcharge
        lcharge=.true.
      endif
      if(keyword.eq.'energy')  then
        backspace(dataunit)
        if(lenergy) then
          write(ounit,*)'Error: 2 total energies given for structure ',inumber
          stop
        endif
        read(dataunit,*,err=93) keyword,totalenergy
        lenergy=.true.
      endif

      if((keyword.eq.'end').and.(lbegin)) then
        lend=.true. 
        exit
      elseif(keyword.ne.'end') then
        continue
      endif

      enddo outer      
!!
!! final check of structure
      if(num_atoms.eq.0) then
        write(ounit,*)'Error: No atoms found in structure ',inumber
        stop
      endif
      if(.not.lbegin) then
        write(ounit,*)'Error: No begin flag in structure ',inumber
        stop
      endif
      if(.not.lend) then
        write(ounit,*)'Error: No end flag in structure ',inumber
        stop
      endif
      if((nlattice.ne.0).and.(nlattice.ne.3))then
        write(ounit,*)'Error: Strange number of lattice vectors ',nlattice,inumber
        stop
      endif
      if(nlattice.eq.3) then
        lperiodic=.true.
      endif
      if(.not.lcharge) then
        write(ounit,*)'### WARNING ### no total charge given,&
        assuming 0'
        totalcharge=0.0d0
      endif
      if(.not.lenergy) then
        write(ounit,*)'Error: no total energy given for structure ',inumber
        stop
      endif
!! check if sum of atomic energies is equal to total energy
      if(luseatomenergies)then
      do i=1,num_atoms
        atomicenergysum=atomicenergysum + atomenergy(i)
      enddo
      if(abs(atomicenergysum-totalenergy).gt.threshold) then
        write(ounit,*)'Error: sum of atomic energies differs from& 
        total energy',inumber
        write(ounit,*)'Sum of energies:  Total energy:'
        write(ounit,'(2f20.8)')atomicenergysum,totalenergy
        stop
      endif
      endif
!!
!! check if lattice vectors make sense
      if(lperiodic)then
        call getvolume(lattice,volume)
        if(volume.lt.0.0000001d0)then
          write(ounit,*)'ERROR: volume of a periodic structure is very small ',volume
          stop !'
        endif
      endif
!!
!! check if sum of atomic charges is equal to total charge
      if((lelec.and.(nn_type_elec.eq.1)).or.(lelec.and.(nn_type_elec.eq.5)))then
        if(luseatomcharges)then
          do i=1,num_atoms
            atomicchargesum=atomicchargesum + atomcharge(i)
          enddo
          if(abs(atomicchargesum-totalcharge).gt.threshold) then
            write(ounit,*)'### WARNING ### sum of atomic charges differs from total charge',inumber
            write(ounit,'(a,2f20.8)')'Sum of charges:  Total charge: ',atomicchargesum,totalcharge
          endif !'
        endif
      endif
!!
!! write all read-in information for debugging
!!      if(ldebug)then
      if(.false.)then
         call writeinputdata(num_atoms,&
           lattice,xyzstruct,totalforce,&
           totalcharge,totalenergy,atomcharge,&
           atomenergy,elementsymbol,lperiodic)
      endif
!!
!!
      return
!!
 90   continue
      write(ounit,*)'ERROR in reading lattice vectors of structure ',inumber
      stop
      return
!!
 91   continue
      write(ounit,'(a,i10,a,i10)')'ERROR in reading atom ',num_atoms,&
      ' in structure ',inumber
      stop
      return
!!
 92   continue
      write(ounit,*)'ERROR in reading total charge of structure ',inumber
      stop
      return
!!
 93   continue
      write(ounit,*)'ERROR in reading total energy of structure ',inumber
      stop
      return

      end subroutine
