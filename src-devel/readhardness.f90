!######################################################################
! This routine is part of
! RuNNer - RuNNer Neural Network Energy Representation
! (c) 2008-2024 Prof. Dr. Joerg Behler 
! Ruhr-Universitaet Bochum, Germany
!
! This program is free software: you can redistribute it and/or modify it 
! under the terms of the GNU General Public License as published by the 
! Free Software Foundation, either version 3 of the License, or 
! (at your option) any later version.
!
! This program is distributed in the hope that it will be useful, but 
! WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
! or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
! for more details.
!
! You should have received a copy of the GNU General Public License along 
! with this program. If not, see http://www.gnu.org/licenses.
! WARNING: THIS FILE IS PART OF THE UNMAINTAINED CONDA PACKAGE OF RUNNER 1.3. 
!######################################################################
!! multipurpose subroutine

!! called by: 
!!            - fitting.f90 
!!            - fitting_batch.f90 
!!            - fittingpair.f90 
!!            - predict.f90 
!!            - getsymmetryfunctions.f90
!!            - getpairsymmetryfunctions.f90
!!
      subroutine readhardness(ndim,hardness_local)
!!
      use fileunits
      use globaloptions
      use nnflags
!!
      implicit none
!!
      integer ndim                                ! in
      integer i1,i2,i3                            ! internal
!! 
      real*8 hardness_local(ndim)     ! out
!!
      character*40 filename
!!
      logical lexist
!!
      do i1=1,ndim
        filename='hardness.000.data'
        if(nucelem(i1).gt.99)then
          write(filename(10:12),'(i3)')nucelem(i1)
        elseif(nucelem(i1).gt.9)then
          write(filename(11:12),'(i2)')nucelem(i1)
        else
          write(filename(12:12),'(i1)')nucelem(i1)
        endif
        inquire(file=filename,exist=lexist)
        if(.not.lexist) then
          write(*,*)'Error: file not found ',filename
          stop
        endif
        open(hunit,file=filename,form='formatted',status='old')
        rewind(hunit)
!! we keep here an inconsistent order of the weights_local to be backwards compatible
        read(hunit,*)hardness_local(i1)
        close(hunit)
      enddo ! i2
!!
!!
      return
      end
