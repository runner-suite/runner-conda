!######################################################################
! This routine is part of
! RuNNer - RuNNer Neural Network Energy Representation
! (c) 2008-2024 Prof. Dr. Joerg Behler 
! Ruhr-Universitaet Bochum, Germany
!
! This program is free software: you can redistribute it and/or modify it 
! under the terms of the GNU General Public License as published by the 
! Free Software Foundation, either version 3 of the License, or 
! (at your option) any later version.
!
! This program is distributed in the hope that it will be useful, but 
! WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
! or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
! for more details.
!
! You should have received a copy of the GNU General Public License along 
! with this program. If not, see http://www.gnu.org/licenses.
! WARNING: THIS FILE IS PART OF THE UNMAINTAINED CONDA PACKAGE OF RUNNER 1.3. 
!######################################################################
!! called by:
!! - ewaldenergies_para.f90 
!! - optimize_ewald.f90
!!
!! very similar to geteshort.f90
!!
      subroutine getcharges_type5(ndim1,npoints,&
        xyzstruct_local,zelem_local,num_atoms_local,&
        symfunctione_local,totalcharge_local,nnatomcharge_local,&
              lperiodic_local, lattice_local)
!!
      use fileunits
      use globaloptions
      use nnewald
!!
      implicit none
!!
      integer npoints
      integer ndim1                                                    ! in
      integer,intent(in)::zelem_local(ndim1,max_num_atoms)                         ! in
      integer zelem(max_num_atoms)
      integer num_atoms_local(ndim1)
      integer i1
      integer num_atoms
!! 
      real*8,intent(in):: symfunctione_local(maxnum_funcvalues_elec,max_num_atoms,ndim1) ! in
      real*8,intent(out):: nnatomcharge_local(ndim1,max_num_atoms)     ! out
      real*8 nnatomcharge(max_num_atoms)                               ! internal
      real*8,intent(in):: xyzstruct_local(3,max_num_atoms,npoints)     ! in
      real*8 xyzstruct(3,max_num_atoms)                                ! internal
      real*8 symfunctione(maxnum_funcvalues_elec,max_num_atoms)        ! internal
      real*8, intent(in):: totalcharge_local(npoints)                  ! in
      logical, intent(in) :: lperiodic_local(npoints)
      real*8, intent(in) :: lattice_local(3,3,npoints)
      real*8 totalcharge                                               ! internal
      logical :: lperiodic
      real*8 :: lat(3,3)
!!
!!
      do i1=1,npoints
!!
!! calculate the atomic charges 
        zelem(:)          = zelem_local(i1,:)
        xyzstruct(:,:)    = xyzstruct_local(:,:,i1)
        symfunctione(:,:) = symfunctione_local(:,:,i1)
        num_atoms         = num_atoms_local(i1)
        totalcharge       = totalcharge_local(i1)
        lat(:,:)          = lattice_local(:,:,i1)
        lperiodic         = lperiodic_local(i1)
!!
        nnatomcharge(:)=0.0d0
        call calc_charges(num_atoms, xyzstruct(:,:num_atoms), zelem(:num_atoms), &
                symfunctione, totalcharge, nnatomcharge(:num_atoms), lperiodic, lat)
!!
        nnatomcharge_local(i1,:)=nnatomcharge(:)
!!
      enddo ! i1
!!
      return
      end
