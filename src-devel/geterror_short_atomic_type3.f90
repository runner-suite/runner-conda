!######################################################################
! This routine is part of
! RuNNer - RuNNer Neural Network Energy Representation
! (c) 2008-2024 Prof. Dr. Joerg Behler 
! Ruhr-Universitaet Bochum, Germany
!
! This program is free software: you can redistribute it and/or modify it 
! under the terms of the GNU General Public License as published by the 
! Free Software Foundation, either version 3 of the License, or 
! (at your option) any later version.
!
! This program is distributed in the hope that it will be useful, but 
! WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
! or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
! for more details.
!
! You should have received a copy of the GNU General Public License along 
! with this program. If not, see http://www.gnu.org/licenses.
! WARNING: THIS FILE IS PART OF THE UNMAINTAINED CONDA PACKAGE OF RUNNER 1.3. 
!######################################################################
!! called by:
!!
      subroutine geterror_short_atomic_type3(iswitch,countepoch,ntrain,&
        imaxerror_eshort,&
        minvalue,maxvalue,avvalue,&
        minvalue_Q,maxvalue_Q,avvalue_Q,&
        rmse_short,rmse_force_s,rmse_short_ref,rmse_force_s_ref,mad_short,&
        mad_force_s,maxerror_eshort)
!!
      use mpi_mod
      use fileunits
      use fittingoptions
      use nnflags
      use globaloptions
      use symfunctions
      use nnshort_atomic
      use structures
      use nnconstants
!!
      implicit none
!!
      integer i1,i2,i3                                                  ! internal
      integer imaxerror_eshort                                          ! out
      integer iswitch                                                   ! in 0=train, 1=test
      integer nforces                                                   ! internal
      integer ntrain                                                    ! in
      integer ndone                                                     ! internal
      integer ndonepara                                                 ! internal
      integer ncount                                                    ! internal
      integer npoints                                                   ! internal
      integer nstruct                                                   ! internal
      integer n_start                                                   ! internal
      integer n_end                                                     ! internal
      integer, dimension(:), allocatable :: num_atoms_mpi               ! internal
      integer, dimension(:,:), allocatable :: zelem_mpi                 ! internal
      integer, dimension(:), allocatable :: imaxerrortemp               ! internal
      integer icount                                                    ! internal
      integer countepoch                                                ! in
      integer nenergies                                                 ! internal
      integer tempunit                                                  ! internal
      integer pxunit                                                    ! internal
      integer fxunit                                                    ! internal
!!
      real*8, dimension(:), allocatable :: shortenergy_mpi              ! internal
      real*8, dimension(:,:,:), allocatable :: xyzstruct_mpi            ! internal
      real*8, dimension(:,:), allocatable :: atomspin_mpi               ! internal
      real*8, dimension(:,:), allocatable :: atomcharge_mpi             ! internal
      real*8, dimension(:), allocatable ::  nneshort_mpi                ! internal
      real*8, dimension(:,:,:), allocatable :: nnshortforce_mpi         ! internal
      real*8, dimension(:), allocatable :: maxerrortemp                 ! internal
      real*8 minvalue(nelem,maxnum_funcvalues_short_atomic)             ! in
      real*8 maxvalue(nelem,maxnum_funcvalues_short_atomic)             ! in
      real*8 avvalue(nelem,maxnum_funcvalues_short_atomic)              ! in
      real*8 minvalue_Q(nelem)                                          ! in
      real*8 maxvalue_Q(nelem)                                          ! in
      real*8 avvalue_Q(nelem)                                           ! in 
      real*8 rmse_short                                                 ! out
      real*8 rmse_short_ref                                             ! in 
      real*8 rmse_force_s                                               ! out
      real*8 rmse_force_s_ref                                           ! in 
      real*8 mad_short                                                  ! out
      real*8 mad_force_s                                                ! out
      real*8 maxerror_eshort                                            ! out
      real*8 nneshort_list(nblock)                                      ! internal
      real*8 nnshortforce_list(3,max_num_atoms,nblock)                  ! internal
      real*8 edummy                                                     ! internal
      real*8 errorenergy                                                ! internal
      real*8 errorforce                                                 ! internal
      real*8 forcesum(3)                                                ! internal
      real*8 maxforce_dummy                                             ! internal
      real*8 offset_local                                               ! internal
!!                                                                      
      character*25 filename                                             ! internal
      character*25 errorlabel                                           ! internal
!!
      logical, dimension(:), allocatable :: lperiodic_mpi               ! internal
!!
!!============================================================
!! initializations
!!============================================================
      nenergies                = 0
      nforces                  = 0
      ncount                   = ntrain
      ndone                    = 0
      nneshort_list(:)         = 0.0d0
      shortforce_list(:,:,:)   = 0.0d0
      nnshortforce_list(:,:,:) = 0.0d0
      atomcharge_list(:,:)   = 0.0d0
      edummy                   = 1.d12
      maxforce_dummy           = 100000.d0
!!
!! open files and write headers
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      if(mpirank.eq.0)then
        if(iswitch.eq.0)then
          open(symunit,file='function.data',form='formatted',status='old')
          rewind(symunit)
          if(lwritetrainpoints)then
            filename='trainpoints.000000.out'
            if(countepoch.gt.9999)then
              write(ounit,*)'Error: too many epochs in geterror'
              write(ounit,*)'switch off lwritetrainpoints'
              stop
            elseif(countepoch.gt.999)then
              write(filename(15:18),'(i4)')countepoch
            elseif(countepoch.gt.99)then
              write(filename(16:18),'(i3)')countepoch
            elseif(countepoch.gt.9)then
              write(filename(17:18),'(i2)')countepoch
            else
              write(filename(18:18),'(i1)')countepoch
            endif
            open(trainpxunit,file=filename,form='formatted',status='replace')
            rewind(trainpxunit) !'
            if(.not.lbindingenergyonly)then
                write(trainpxunit,'(2A10,3A30)')'Conf.','atoms','Ref. total energy(Ha)',&
                  'NN total energy(Ha)','E(Ref.) - E(NN) (Ha/atom)'
            else
                write(trainpxunit,'(2A10,3A30)')'Conf.','atoms','Ref. Ebind(Ha/atom)  ',&
                  'NN Ebind(Ha/atom)  ','E(Ref.) - E(NN) (Ha/atom)'
            endif
          endif
          open(trainstructunit,file='trainstruct.data',form='formatted',status='old')
          rewind(trainstructunit)
!!
          if(luseforces)then
            if(lwritetrainforces)then
              filename='trainforces.000000.out'
              if(countepoch.gt.9999)then
                write(ounit,*)'Error: too many epochs in geterror'
                write(ounit,*)'switch off lwritetrainforces'
                stop
              elseif(countepoch.gt.999)then
                write(filename(15:18),'(i4)')countepoch
              elseif(countepoch.gt.99)then
                write(filename(16:18),'(i3)')countepoch
              elseif(countepoch.gt.9)then
                write(filename(17:18),'(i2)')countepoch
              else
                write(filename(18:18),'(i1)')countepoch
              endif
              open(trainfxunit,file=filename,form='formatted',status='replace')
              write(trainfxunit,'(2A10,6A25)')'Conf.','atom','Ref. Fx(Ha/Bohr)','Ref. Fy(Ha/Bohr)',&
                'Ref. Fz(Ha/Bohr)','NN Fx(Ha/Bohr)','NN Fy(Ha/Bohr)','NN Fz(Ha/Bohr)'
            endif
            open(trainfunit,file='trainforces.data',form='formatted',status='old')
            rewind(trainfunit)
          endif !' luseforces
!!
        elseif(iswitch.eq.1)then ! test error
          open(tymunit,file='testing.data',form='formatted',status='old')
          rewind(tymunit)
          if(lwritetrainpoints)then
            filename='testpoints.000000.out'
            if(countepoch.gt.999)then
              write(filename(16:17),'(i4)')countepoch
            elseif(countepoch.gt.99)then
              write(filename(15:17),'(i3)')countepoch
            elseif(countepoch.gt.9)then
              write(filename(16:17),'(i2)')countepoch
            else
              write(filename(17:17),'(i1)')countepoch
            endif
            open(testpxunit,file=filename,form='formatted',status='replace')
            rewind(testpxunit) !'
            if(.not.lbindingenergyonly)then
                write(testpxunit,'(2A10,3A30)')'Conf.','atoms','Ref. total energy(Ha)',&
                  'NN total energy(Ha)','E(Ref.) - E(NN) (Ha/atom)' 
            else
                write(testpxunit,'(2A10,3A30)')'Conf.','atoms','Ref. Ebind(Ha/atom)  ',&
                  'NN Ebind(Ha/atom)  ','E(Ref.) - E(NN) (Ha/atom)'
            endif
          endif
          open(teststructunit,file='teststruct.data',form='formatted',status='old')
          rewind(teststructunit)
!!
          if(luseforces)then
            if(lwritetrainforces)then
              filename='testforces.000000.out'
              if(countepoch.gt.999)then
                write(filename(14:17),'(i4)')countepoch
              elseif(countepoch.gt.99)then
                write(filename(15:17),'(i3)')countepoch
              elseif(countepoch.gt.9)then
                write(filename(16:17),'(i2)')countepoch
              else
                write(filename(17:17),'(i1)')countepoch
              endif
              open(testfxunit,file=filename,form='formatted',status='replace')
              write(testfxunit,'(2A10,6A25)')'Conf.','atom','Ref. Fx(Ha/Bohr)','Ref. Fy(Ha/Bohr)',&
                'Ref. Fz(Ha/Bohr)','NN Fx(Ha/Bohr)','NN Fy(Ha/Bohr)','NN Fz(Ha/Bohr)'
            endif
            open(testfunit,file='testforces.data',form='formatted',status='old')
            rewind(testfunit)
          endif !' luseforces
        endif ! iswitch
      endif ! mpirank.eq.0
!!
!!============================================================
!! loop block-wise over structures
!!============================================================
 10   continue
      if(ncount.gt.nblock)then
        npoints=nblock
        ncount=ncount-nblock
      else
        npoints=ncount
        ncount=ncount-npoints
      endif
!!
!!============================================================
!! determine which structures of this block should be calculated by this process
!!============================================================
      call mpifitdistribution(npoints,nstruct,n_start,n_end)
      call mpi_barrier(mpi_comm_world,mpierror)
!!
!!============================================================
!! do all file reading for training error here at one place to allow for parallelization
!!============================================================
!!
      if(mpirank.eq.0)then
!!============================================================
!! read npoint short range data sets (train or test)
!!============================================================
        if(iswitch.eq.0)then ! train
          tempunit=symunit          
        elseif(iswitch.eq.1)then ! test
          tempunit=tymunit          
        else
          write(ounit,*)'ERROR: unknown iswitch in geterror ',iswitch
          stop
        endif
        call readfunctions(1,tempunit,npoints,nelem,&
          max_num_atoms,maxnum_funcvalues_short_atomic,num_funcvalues_short_atomic,&
          symfunction_short_atomic_list)
!!
!!============================================================
!! read the structures needed for the calculation of the forces 
!! must be called after readfunctions because it needs num_atoms_list
!!============================================================
        if(iswitch.eq.0)then ! train
          tempunit=trainstructunit          
        elseif(iswitch.eq.1)then ! test
          tempunit=teststructunit          
        else
          write(ounit,*)'ERROR: unknown iswitch in geterror ',iswitch
          stop
        endif
        call getstructures_type3(tempunit,npoints,num_atoms_list)
!!
!!============================================================
!! read short range forces from trainforces.data or testforces.data => shortforce_list
!!============================================================
        if(luseforces.and.lshort)then
          if(iswitch.eq.0)then ! train
            tempunit=trainfunit          
          elseif(iswitch.eq.1)then ! test
            tempunit=testfunit          
          else
            write(ounit,*)'ERROR: unknown iswitch in geterror ',iswitch
            stop
          endif
          call readforces(tempunit,npoints,shortforce_list)
        endif
!!
      endif ! mpirank.eq.0
!!
!!============================================================
!! distribute the data to all processes
!!============================================================
      call mpi_bcast(num_atoms_list,nblock,mpi_integer,0,mpi_comm_world,mpierror)
      call mpi_bcast(zelem_list,nblock*max_num_atoms,mpi_integer,0,mpi_comm_world,mpierror)
      call mpi_bcast(shortenergy_list,nblock,mpi_real8,0,mpi_comm_world,mpierror)
      call mpi_bcast(symfunction_short_atomic_list,nblock*max_num_atoms*maxnum_funcvalues_short_atomic,&
           mpi_real8,0,mpi_comm_world,mpierror)
      call mpi_bcast(xyzstruct_list,nblock*max_num_atoms*3,mpi_real8,0,mpi_comm_world,mpierror)
      call mpi_bcast(atomspin_list,nblock*max_num_atoms,mpi_real8,0,mpi_comm_world,mpierror)
      call mpi_bcast(lattice_list,nblock*9,mpi_real8,0,mpi_comm_world,mpierror)
      call mpi_bcast(lperiodic_list,nblock,mpi_logical,0,mpi_comm_world,mpierror)
      call mpi_bcast(shortforce_list,nblock*max_num_atoms*3,&
           mpi_real8,0,mpi_comm_world,mpierror)
!!
!!============================================================
!! end of file reading for training error
!!============================================================
!!
!!============================================================
!! allocate local arrays for this process
!!============================================================
      allocate(num_atoms_mpi(nstruct))
      allocate(zelem_mpi(nstruct,max_num_atoms))
      allocate(shortenergy_mpi(nstruct))
      allocate(xyzstruct_mpi(3,max_num_atoms,nstruct))
      allocate(atomspin_mpi(nstruct,max_num_atoms))
      allocate(lperiodic_mpi(nstruct))
      allocate(atomcharge_mpi(nstruct,max_num_atoms))
      allocate(nneshort_mpi(nstruct))
      nneshort_mpi(:)=0.0d0
      allocate(nnshortforce_mpi(3,max_num_atoms,nstruct))
      nnshortforce_mpi(:,:,:)=0.0d0
!!
!!============================================================
!! fill local arrays for this process
!!============================================================
      icount=0
      do i1=n_start,n_end
        icount=icount+1
        num_atoms_mpi(icount)       = num_atoms_list(i1)
        zelem_mpi(icount,:)         = zelem_list(i1,:)
        shortenergy_mpi(icount)     = shortenergy_list(i1)
        xyzstruct_mpi(:,:,icount)   = xyzstruct_list(:,:,i1)
        atomspin_mpi(icount,:)      = atomspin_list(i1,:)
        lperiodic_mpi(icount)       = lperiodic_list(i1)
        atomcharge_mpi(icount,:)    = atomcharge_list(i1,:)
      enddo ! i1
!!
!!============================================================
!! get the short range energies 
!!============================================================
        ndonepara=ndone+n_start-1
        call getshortenergies_para_type3(nstruct,ndonepara,&
          nenergies,imaxerror_eshort,&
          num_atoms_mpi,zelem_mpi,&
          minvalue,maxvalue,avvalue,&
          minvalue_Q,maxvalue_Q,avvalue_Q,&
          symfunction_short_atomic_list(1,1,n_start),lattice_list(1,1,n_start),&
          nnshortforce_mpi,xyzstruct_mpi,atomspin_mpi,atomcharge_mpi,nneshort_mpi,&
          rmse_short,mad_short,maxerror_eshort,shortenergy_mpi,&
          lperiodic_mpi)

!!
!!============================================================
!! for parallel case: find imaxerror_eshort and maxerror_eshort from all processes
!!============================================================
        if(mpisize.gt.1)then
          allocate(imaxerrortemp(mpisize))
          imaxerrortemp(:)=0
          allocate(maxerrortemp(mpisize))
          maxerrortemp(:) =0.0d0
          imaxerrortemp(mpirank+1)=imaxerror_eshort
          maxerrortemp(mpirank+1) =maxerror_eshort
          call mpi_allreduce(mpi_in_place,imaxerrortemp,mpisize,&
            mpi_integer,mpi_sum,mpi_comm_world,mpierror)
          call mpi_allreduce(mpi_in_place,maxerrortemp,mpisize,&
            mpi_real8,mpi_sum,mpi_comm_world,mpierror)
          do i1=1,mpisize
            if(maxerrortemp(i1).gt.maxerror_eshort)then
              maxerror_eshort = maxerrortemp(i1)
              imaxerror_eshort= imaxerrortemp(i1)
            endif
          enddo ! i1
          deallocate(imaxerrortemp)
          deallocate(maxerrortemp)
        endif
!!
!!============================================================
!! copy results back on full array
!!============================================================
        icount=0
        nneshort_list(:)       =0.0d0
        nnshortforce_list(:,:,:) =0.0d0
        do i1=n_start,n_end
          icount=icount+1
          nneshort_list(i1)       = nneshort_mpi(icount)
          nnshortforce_list(:,:,i1) = nnshortforce_mpi(:,:,icount)
        enddo
!!
!!============================================================
!! distribute results to all processes
!!============================================================
        call mpi_allreduce(mpi_in_place,nneshort_list,nblock,&
             mpi_real8,mpi_sum,mpi_comm_world,mpierror)
        call mpi_allreduce(mpi_in_place,nnshortforce_list,nblock*max_num_atoms*3,&
             mpi_real8,mpi_sum,mpi_comm_world,mpierror)
!!
!!============================================================
!! check NN forces if requested
!!============================================================
        if(mpirank.eq.0)then
        if(lcheckf)then
          do i1=1,npoints
            forcesum(:)=0.0d0
            do i2=1,num_atoms_list(i1)
              do i3=1,3
                forcesum(i3)=forcesum(i3)+nnshortforce_list(i3,i2,i1)
              enddo ! i3 
            enddo ! i2
            do i2=1,3
              if(abs(forcesum(i2)).gt.0.000001d0)then
                write(ounit,*)'Error in force sum ',i1,i2,forcesum(i2)
                stop
              endif
            enddo ! i2
          enddo ! i1
        endif ! lcheckf
        endif ! mpirank.eq.0
!!
!!============================================================
!! calculate the RMSE of the short range forces here 
!!============================================================
      if(luseforces)then
        call calcrmse_forces(npoints,&
          nforces,rmse_force_s,mad_force_s,&
          shortforce_list,nnshortforce_list,maxforce)
      endif
!!
!!============================================================
!! set file units depending on training or test set 
!!===========================================================
      if(iswitch.eq.0)then ! train set
        pxunit=trainpxunit
        fxunit=trainfxunit
      elseif(iswitch.eq.1)then ! test set
        pxunit=testpxunit
        fxunit=testfxunit
      else
        write(ounit,*)'ERROR: unknown iswitch in geterror ',iswitch
        stop
      endif
!!
!!===========================================================
!! if requested write energy details for all points to trainpoints.XXXXXX.out
!!===========================================================
      if(mpirank.eq.0)then
        if(lwritetrainpoints)then
!!          write(ounit,*)'rmse_short_ref is ',rmse_short_ref
          do i1=1,npoints
            offset_local = 0.0d0
            if(.not.lbindingenergyonly)then
                do i2 =1, num_atoms_list(i1)
                  offset_local=offset_local+atomrefenergies(elementindex(zelem_list(i1,i2)))
                enddo
            endif
            errorenergy=shortenergy_list(i1)-nneshort_list(i1)
            if(.not.lbindingenergyonly)then
                write(pxunit,'(2I10,3f30.8)')i1+ndone,num_atoms_list(i1),&
                 shortenergy_list(i1)*dble(num_atoms_list(i1))+offset_local,&
                 nneshort_list(i1)*dble(num_atoms_list(i1))+offset_local,&
                 errorenergy 
            else
                write(pxunit,'(2i10,3f30.8)')i1+ndone,num_atoms_list(i1),&
                 shortenergy_list(i1),&
                 nneshort_list(i1),&
                 errorenergy
            endif
          enddo
        endif
!!
!!===========================================================
!! write force details for all points to trainforces.XXXXXX.out
!!===========================================================
        if(luseforces.and.lwritetrainforces)then
!!          write(ounit,*)'rmse_force_s_ref is ',rmse_force_s_ref
          do i1=1,npoints
            do i2=1,num_atoms_list(i1)
               write(fxunit,'(2I10,6f25.8)')&
                 i1+ndone,i2,(shortforce_list(i3,i2,i1),i3=1,3),(nnshortforce_list(i3,i2,i1),i3=1,3)
            enddo ! i2 
          enddo ! i1
        endif ! luseforces
!!
      endif ! mpirank.eq.0
!!
      ndone=ndone+npoints
!!
!!============================================================
!! deallocate local arrays 
!!============================================================
      deallocate(num_atoms_mpi)
      deallocate(zelem_mpi)
      deallocate(shortenergy_mpi)
      deallocate(xyzstruct_mpi)
      deallocate(atomspin_mpi)
      deallocate(lperiodic_mpi)
      deallocate(nneshort_mpi)
      deallocate(nnshortforce_mpi)
      deallocate(atomcharge_mpi)
!!
!!============================================================
!! if there are structures left go to next block of structures 
!!============================================================
      if(ncount.gt.0) goto 10
!============================================================
!! end of block wise loop 
!!============================================================
!!
!!============================================================
!! close files
!!============================================================
      if(mpirank.eq.0)then
        if(iswitch.eq.0)then
          close(symunit)
          close(trainstructunit)
          if(luseforces)                       close(trainfunit)
          if(lwritetrainpoints)                close(trainpxunit)
          if(luseforces.and.lwritetrainforces) close(trainfxunit)
        elseif(iswitch.eq.1)then
          close(tymunit)
          close(teststructunit)
          if(luseforces)                       close(testfunit)
          if(lwritetrainpoints)                close(testpxunit)
          if(luseforces.and.lwritetrainforces) close(testfxunit)
        else
          write(ounit,*)'ERROR: unknown iswitch in geterror ',iswitch
          stop
        endif ! iswitch
      endif ! mpirank.eq.0
!!
!!============================================================
!! combine the partial rmse values of all processes here only to avoid double counting
!!============================================================
      call mpi_allreduce(mpi_in_place,rmse_short,1,&
           mpi_real8,mpi_sum,mpi_comm_world,mpierror)
      call mpi_allreduce(mpi_in_place,mad_short,1,&
           mpi_real8,mpi_sum,mpi_comm_world,mpierror)
      call mpi_allreduce(mpi_in_place,nenergies,1,&
           mpi_integer,mpi_sum,mpi_comm_world,mpierror)
!!
!!============================================================
!! calculate the final RMSEs 
!!============================================================
      call getrmse_short(nenergies,&
        nforces,rmse_short,rmse_force_s,mad_short,mad_force_s)
!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!1
!! end calculation of the first training error
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!1
!!
      return
      end
