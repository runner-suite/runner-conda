    subroutine geometryopt_cg(num_atoms,num_atoms_element,zelem,symbol,&
               lattice,totalQ,lperiodic,&
               minvalue,maxvalue,avvalue,emin,emax,&
               Eold,Enew,F,xyz_old,xyz_new,atomspin,Qnew) 
    
    use globaloptions
    use fileunits
    use nnflags
    use predictionoptions
    use energyAndForces3G
    use energyAndForces4G

    implicit none

    integer :: num_atoms
    integer :: num_atoms_element(nelem)
    integer :: zelem(num_atoms),i1,i2,i3
    integer :: count_index
    integer zelem_constraint

    real*8,intent(in) :: totalQ
    real*8,intent(in) :: lattice(3,3)
    real*8 Eold
    real*8,intent(in) :: minvalue(nelem,maxnum_funcvalues_short_atomic)
    real*8,intent(in) :: maxvalue(nelem,maxnum_funcvalues_short_atomic)
    real*8,intent(in) :: avvalue(nelem,maxnum_funcvalues_short_atomic)
    real*8 Enew
    real*8,intent(inout) :: F(3,num_atoms)
    real*8 xyz_old(3,num_atoms)
    real*8,intent(out) :: xyz_new(3,num_atoms)
    real*8 atomspin(num_atoms)
    real*8 Qnew(num_atoms)
    real*8  gam   !! gamma in CG
    real*8  old_gradient(3,num_atoms)
    real*8  new_gradient(3,num_atoms)
    real*8  vector_new_gradient(3*num_atoms)
    real*8  vector_old_gradient(3*num_atoms)
    real*8  oldstep(3,num_atoms) !! current_geometry - previous_geometry
    real*8  newstep(3,num_atoms) !! new_geometry - current_geometry
    real*8 max_force_component 
    real*8 max_force_component_temp
    real*8 max_force_component_temp2
!!  for old method
    real*8 sens(nelem,maxnum_funcvalues_short_atomic)
    real*8 atomenergysum
    real*8 stress(3,3)
    real*8,intent(in) :: emin
    real*8,intent(in) :: emax
    real*8 nnatomenergy(num_atoms)
    real*8 nnshortenergy
!!
    real*8 trestart !! criteria for restarting CG 
    logical lperiodic
    character*2 symbol(num_atoms)
    !! initialization
    zelem_constraint=0
    i2 =0 
    xyz_new(:,:)=0.0d0
    old_gradient(:,:)=-F(:,:)
    new_gradient(:,:)=0.0d0
    trestart = 0.0d0
    oldstep(:,:) = 0.0d0
    newstep(:,:) = 0.0d0
    Enew = 0.0d0
    count_index =0 
    vector_old_gradient(:)=0.0d0
    vector_new_gradient(:)=0.0d0
    max_force_component =maxval(abs(F(:,:)))
    max_force_component_temp=-999.9d0
    max_force_component_temp2=-999.9d0
    !! check if it is constraint relaxation
    if (luserelaxconstraint)then
        call nuccharge(constraint_element,zelem_constraint)
    endif
    !! set 1st step CG by using steepest descent
    oldstep(:,:)=-stepsize*old_gradient(:,:)
    xyz_new(:,:)=xyz_old(:,:)+oldstep(:,:)
    write(outunit,'(a35,i10)')'Geometry during the relaxation step',i2
    write(outunit,'(a5)')'begin'
    if(lperiodic)then
       write(outunit,'(a7,3f20.12)')'lattice  ',(lattice(1,i1),i1=1,3)
       write(outunit,'(a7,3f20.12)')'lattice  ',(lattice(2,i1),i1=1,3)
       write(outunit,'(a7,3f20.12)')'lattice  ',(lattice(3,i1),i1=1,3)
    endif
    do i1 =1,num_atoms
       write(outunit,*)'atom ',xyz_new(:,i1),symbol(i1),Qnew(i1),'0.0',F(:,i1)
    enddo
    write(outunit,'(a7,f20.6)')'energy ',Eold
    write(outunit,'(a7,f20.6)')'charge ',sum(Qnew(:))
    write(outunit,'(a3)')'end'
    !! Geo optimizations by conjugate gradient
    do while((abs(Enew-Eold).gt.threshold_E).or.(abs(max_force_component).gt.threshold_F))
        i2 = i2+1
        !! get the new gradient
        if (nn_type_short.eq.3) then
            call get_energies_and_forces(num_atoms,zelem,lattice,&
                 xyz_new,atomspin,totalQ,lperiodic,minvalue,maxvalue,avvalue,&
                 Qnew,Enew,F)
        elseif(nn_type_short.eq.1)then
          if(nn_type_elec.eq.1)then
            call get_energies_and_forces_for_3G(num_atoms,zelem,lattice,&
                 xyz_new,atomspin,totalQ,lperiodic,minvalue,maxvalue,avvalue,&
                 Qnew,Enew,F,enforcetotcharge,totalQ, ldoforces)
          else
            call predictionshortatomic(num_atoms,num_atoms_element,zelem,&
                 lattice,xyz_new,atomspin,minvalue,maxvalue,avvalue,emin,emax,&
                 Enew,F,nnatomenergy,nnshortenergy,&
                 stress,atomenergysum,sens,lperiodic)
            if(lremoveatomenergies)then
               call addatoms(num_atoms,&
                zelem,num_atoms_element,&
                atomenergysum,nnatomenergy)
               Enew=Enew+atomenergysum
            endif ! lremoveatomenergies
          endif
        endif    
        !! store new_gradient 
        new_gradient(:,:)=-F(:,:)
        !! store vector for old and new gradient
        count_index=0
        do i1 = 1,num_atoms ! loop over atoms
            do i3 = 1,3 ! loop over x,y,z
                count_index = count_index+1
                vector_old_gradient(count_index)=old_gradient(i3,i1)
                vector_new_gradient(count_index)=new_gradient(i3,i1)
            enddo
        enddo    
        !! Powell-Beale Restarts
        trestart=dot_product(vector_old_gradient(:),vector_new_gradient(:))
        !! calculate the oldstep
        oldstep(:,:)=xyz_new(:,:)-xyz_old(:,:)
        if(trestart.gt.0.2d0*dot_product(vector_new_gradient(:),vector_new_gradient(:)))then
            gam = 0.0d0
            write(ounit,*)'Restarting CG alorithm => gamma=0.0d0'
        else
            gam=dot_product(vector_new_gradient(:)-vector_old_gradient(:),vector_new_gradient(:))/&
                    dot_product(vector_old_gradient(:),vector_old_gradient(:))
            write(ounit,*)'Continuing CG algorithm'
        endif
        !! return current_gradient to old_gradient and xyz_new to xyz_old
        old_gradient(:,:) = new_gradient(:,:)
        xyz_old(:,:) = xyz_new(:,:)
        max_force_component= maxval(abs(F(:,:)))
        !! get the new xyz coordinate
        if (luserelaxconstraint)then 
           do i1=1,num_atoms
                if(zelem(i1).eq.zelem_constraint)then
                    max_force_component_temp = maxval(F(:,i1))
                    max_force_component_temp2=max(max_force_component_temp2,max_force_component_temp)
                    max_force_component = max_force_component_temp2
                    newstep(:,i1)=stepsize*new_gradient(:,i1)+gam*oldstep(:,i1)
                    xyz_new(:,i1)= xyz_new(:,i1)-newstep(:,i1)
                endif
           enddo
        else
            newstep(:,:)=stepsize*new_gradient(:,:)+gam*oldstep(:,:)
            xyz_new(:,:)= xyz_new(:,:)-newstep(:,:)
        endif
        Eold = Enew
!        if (mod(i2,10)==0)then !! avoid too many configurations
        write(outunit,'(a35,i10)')'Geometry during the relaxation step',i2
        write(outunit,'(a5)')'begin'
        if(lperiodic)then
          write(outunit,'(a7,3f20.12)')'lattice  ',(lattice(1,i1),i1=1,3)
          write(outunit,'(a7,3f20.12)')'lattice  ',(lattice(2,i1),i1=1,3)
          write(outunit,'(a7,3f20.12)')'lattice  ',(lattice(3,i1),i1=1,3)
        endif
        do i1 =1,num_atoms
           write(outunit,*)'atom ',xyz_new(:,i1),symbol(i1),Qnew(i1),'0.0',F(:,i1)
        enddo
        write(outunit,'(a7,f20.6)')'energy ',Enew
        write(outunit,'(a7,f20.6)')'charge ',sum(Qnew(:))
        write(outunit,'(a3)')'end'
!        endif 
    enddo ! Geometry optimization is completed
    write(ounit,*)'-------------------------------------------------------------'
    write(ounit,*)'Geometry optimization is completed and the optimized geometry is written in geometry_opt.data'
    open(geooptunit,file='geometry_opt.data')
    write(geooptunit,'(a5)')'begin'
    if(lperiodic)then
        write(geooptunit,'(a7,3f20.12)')'lattice  ',(lattice(1,i1),i1=1,3)
        write(geooptunit,'(a7,3f20.12)')'lattice  ',(lattice(2,i1),i1=1,3)
        write(geooptunit,'(a7,3f20.12)')'lattice  ',(lattice(3,i1),i1=1,3)
    endif
    do i1=1,num_atoms
        write(geooptunit,*)&
            'atom ',(xyz_new(i2,i1),i2=1,3),&
            symbol(i1),Qnew(i1),'0.0',&
            (F(i2,i1),i2=1,3)
    enddo
    write(geooptunit,'(a7,f20.12)')'energy ',Enew
    write(geooptunit,'(a7,f20.12)')'charge ',sum(Qnew(:))
    write(geooptunit,'(a3)')'end'
    close(geooptunit)
    end subroutine
