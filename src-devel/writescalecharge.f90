!######################################################################
! This routine is part of
! RuNNer - RuNNer Neural Network Energy Representation
! (c) 2008-2024 Prof. Dr. Joerg Behler 
! Ruhr-Universitaet Bochum, Germany
!
! This program is free software: you can redistribute it and/or modify it 
! under the terms of the GNU General Public License as published by the 
! Free Software Foundation, either version 3 of the License, or 
! (at your option) any later version.
!
! This program is distributed in the hope that it will be useful, but 
! WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
! or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
! for more details.
!
! You should have received a copy of the GNU General Public License along 
! with this program. If not, see http://www.gnu.org/licenses.
! WARNING: THIS FILE IS PART OF THE UNMAINTAINED CONDA PACKAGE OF RUNNER 1.3. 
!######################################################################
!! multipurpose subroutine

!! called by:
!! - fitting.f90
!! - fittingpair.f90
!! - fitting_batch.f90
!!
      subroutine writescalecharge(ndim,&
           minvalue_local,maxvalue_local,avvalue_loal)
!!
      use fileunits
      use globaloptions
!!
      implicit none
!!
      integer ndim                               ! in
      integer i1,i2                              ! internal
      integer iswitch                            ! in
!!
      real*8 minvalue_local(ndim)                  ! in 
      real*8 maxvalue_local(ndim)                  ! in 
      real*8 avvalue_loal(ndim)                   ! in 
!!
!!
      open(scalechargeunit,file='scaling_charge.data',form='formatted',status='replace')

      do i1=1,ndim
          write(scalechargeunit,'(i4,3f18.9)')i1,minvalue_local(i1),&
            maxvalue_local(i1),avvalue_loal(i1)
      enddo ! i2
!!      
      close(scalechargeunit)
!!
      return
      end
