

subroutine calc_charges(nat, pos, elements, symFunc, totalCharge, charges, lperiodic, lat)
  use fittingoptions
  use structures
  use nnewald
  use globaloptions
  use fileunits
  use ewaldSummation
  use eem
  use nnflags

  implicit none
  integer, intent(in) :: nat
  real*8, intent(in) :: pos(3, nat)
  integer, intent(in) :: elements(nat)
  real*8, intent(in) :: symFunc(maxnum_funcvalues_elec, max_num_atoms)
  real*8, intent(in) :: totalcharge
  real*8, intent(out) :: charges(nat)
  logical, intent(in) :: lperiodic
  real*8, intent(in) :: lat(3,3)

  integer :: i
  real*8 :: nnatomchi(nat)
  integer :: ielement
  real*8 :: hardness(nat)
  real*8 :: gausswidth(nat)
  real*8 :: AMatrix(nat, nat)
  real*8 :: nodes_values(maxnum_layers_elec, maxnodes_elec) ! apparently just dummy 
  real*8 :: nodes_sum(maxnum_layers_elec, maxnodes_elec) ! just dummy too


  ! initialization
  nnatomchi(:)=0.0d0
  gausswidth(:)=0.0d0
  do i=1,nat
    ielement = elements(i)
    gausswidth(i) = fixedgausswidth(elementindex(ielement))
    hardness(i) = fixedhardness(elementindex(ielement))
    nodes_values(:,:) = 0.d0
    nodes_sum(:,:) = 0.d0

    call calconenn(1,maxnum_funcvalues_elec,maxnodes_elec,&
      maxnum_layers_elec,num_layers_elec(elementindex(ielement)),&
      maxnum_weights_elec,nodes_elec(0,elementindex(ielement)),&
      symfunc(1,i),&
      weights_elec(1,elementindex(ielement)),&
      nodes_values,nodes_sum,&
      nnatomchi(i),actfunc_elec(1,1,elementindex(ielement)))
  enddo
  if(nn_type_elec.eq.5)then  
      if (lperiodic) then
        call eemMatrixEwald(nat, pos, lat, gausswidth, hardness, AMatrix)
      else
        call eemMatrix(nat, pos, gausswidth, hardness, AMatrix)
      endif
      call eemCharges(nat, AMatrix, nnatomchi, totalcharge, charges)
  else !! KK: here we simply copy back the charge value from nnatomchi to nnatomcharge in 3G and spin
      charges(:nat)=nnatomchi(:nat) 
  endif  
end subroutine
