! Created by Jonas A. Finkler on 3/2/20.

! calculates the energy and force corrections due to screening
! if start of screening goes to infinity, the energy becomes the negative of the total electrostatic energy for point charges
! for Gaussian charges the self interaction remains
subroutine getScreeningPeriodic(nat, lat, ats, q, sigma, e, f, dEdq, dEdlat)
    use globaloptions
    use linalg
    implicit none

    integer, intent(in) :: nat
    real*8, intent(in) :: lat(3,3)
    real*8, intent(in) :: ats(3,nat)
    real*8, intent(in) :: q(nat)
    real*8, intent(in) :: sigma(nat)
    real*8, intent(out) :: e
    real*8, intent(out) :: f(3,nat)
    real*8, intent(out) :: dEdq(nat)
    real*8, intent(out) :: dEdlat(3,3)
    integer :: n(3)
    integer :: iat, jat, i1, i2, i3
    real*8 :: dlat(3), d(3), r, interf, interfderiv
    real*8 :: fscreen, fscreenderiv
    real*8 :: gamma
    real*8 :: ftmp(3)
    real*8 :: invlat(3,3)


    call getNcells(lat, rscreen_cut, n)
    call inv3DM(transpose(lat), invlat) ! invlat is transposed (wrt RuNNer convention)

    e = 0.d0
    f(:,:) = 0.d0
    dEdQ(:) = 0.d0
    dEdlat(:,:) = 0.d0

    do i1=-n(1),n(1)
        do i2=-n(2),n(2)
            do i3=-n(3),n(3)
                dlat(:) = i1 * lat(1,:) + i2 * lat(2,:) + i3 * lat(3,:)
                call getScreeningOneUnitCell(nat, ats, invlat, q, sigma, dlat, e, f, dEdQ, dEdlat)
            end do
        end do
    end do
    call getScreeningSelfInteration(nat, ats, q, sigma, e, dEdQ)

end subroutine getScreeningPeriodic


subroutine getScreeningFree(nat, ats, q, sigma, e, f, dEdQ)
    implicit none
    integer, intent(in) :: nat
    real*8, intent(in) :: ats(3,nat)
    real*8, intent(in) :: q(nat)
    real*8, intent(in) :: sigma(nat)
    real*8, intent(out) :: e
    real*8, intent(out) :: f(3,nat)
    real*8, intent(out) :: dEdQ(nat)

    real*8 :: dlat(3)
    real*8 :: invlat(3,3)
    real*8 :: dEdlat(3,3) ! dummy, is not used

    dlat(:) = 0.d0
    invlat(:,:) = 0.d0

    e = 0.d0
    f(:,:) = 0.d0
    dEdQ(:) = 0.d0

    call getScreeningOneUnitCell(nat, ats, invlat, q, sigma, dlat, e, f, dEdQ, dEdlat)
    call getScreeningSelfInteration(nat, ats, q, sigma, e, dEdQ)

end subroutine

subroutine getScreeningSelfInteration(nat, ats, q, sigma, e, dEdQ)
    use nnconstants
    implicit none
    integer, intent(in) :: nat
    real*8, intent(in) :: ats(3,nat)
    real*8, intent(in) :: q(nat)
    real*8, intent(in) :: sigma(nat)
    real*8, intent(inout) :: e
    real*8, intent(inout) :: dEdQ(nat)
    integer :: i

    if (sigma(1) > 0.d0) then
        do i=1,nat
            E = E - q(i)**2 / (2.d0 * sigma(i) * sqrt(PI))
            dEdQ(i) = dEdQ(i) - q(i) / (sigma(i) * sqrt(PI))
        end do
    end if


end subroutine


! calculates the screening for one repeated unit cell
! energy and force are added (!) to e and f - same for dEdQ
! no self interaction, if |dlat|^2 < 1.e-10
subroutine getScreeningOneUnitCell(nat, ats, invlat, q, sigma, dlat, e, f, dEdQ, dEdlat)
    use globaloptions
    use nnconstants

    implicit none
    integer, intent(in) :: nat
    real*8, intent(in) :: ats(3,nat)
    real*8, intent(in) :: invlat(3,3) ! tranposed wrt runner convention. This way we can use matrix mult. to calculate rel. lattice vectors
    real*8, intent(in) :: q(nat)
    real*8, intent(in) :: sigma(nat)
    real*8, intent(in) :: dlat(3)
    real*8, intent(inout) :: e
    real*8, intent(inout) :: f(3,nat)
    real*8, intent(inout) :: dEdQ(nat)
    real*8, intent(inout) :: dEdlat(3,3)

    integer :: iat, jat, i
    real*8 :: d(3), r, interf, interfderiv
    real*8 :: fscreen, fscreenderiv
    real*8 :: gamma
    real*8 :: ftmp(3), relat(3)

    do iat=1,nat
        do jat=1,nat
            if (iat/=jat .or. sum(dlat**2) > 1.d-10) then
                d(:) = ats(:,iat) - ats(:,jat) + dlat(:)
                r = sqrt(sum(d**2))
                call getscreenfunctionforelectrostatics(&
                        r,fscreen,fscreenderiv,1.d0)
                fscreen = fscreen - 1.d0
                if (sigma(1) <= 0.d0) then
                    interf = 1.d0
                    interfderiv = 0.d0
                else
                    gamma = sqrt(sigma(iat)**2 + sigma(jat)**2)
                    interf = erf(r / (sqrt(2.d0) * gamma))
                    interfderiv = sqrt(2.d0 / PI) * exp(-r**2 /(2.d0 * gamma**2)) / gamma
                end if

                e = e + q(iat) * q(jat) / r * interf * fscreen / 2.d0
                dEdQ(iat) = dEdQ(iat) + q(jat) / r * interf * fscreen / 2.d0
                dEdQ(jat) = dEdQ(jat) + q(iat) / r * interf * fscreen / 2.d0
                ftmp(:) = q(iat) * q(jat) * d(:) / r &
                        * (interfderiv * fscreen / r + interf * fscreenderiv / r - interf * fscreen / r**2) / 2.d0
                f(:,iat) = f(:,iat) - ftmp(:)
                f(:,jat) = f(:,jat) + ftmp(:)
                relat = matmul(invlat, ats(:,iat) + dlat(:))
                do i=1,3
                    dEdlat(:,i) = dEdlat(:,i) + ftmp(i) * relat(:)
                end do
                relat = matmul(invlat, ats(:,jat))
                do i=1,3
                    dEdlat(:,i) = dEdlat(:,i) - ftmp(i) * relat(:)
                end do
            end if
        end do
    end do

end subroutine

! uses lattice other convention (v1 = lat(:,1))
subroutine getNcellsT(lattice, cutoff, n)
    implicit none
    real*8, intent(in) :: lattice(3,3)
    real*8, intent(in) :: cutoff
    integer, intent(out) :: n(3)

    call getNcells(transpose(lattice), cutoff, n)

end subroutine

! copied from neighbor.f90
! uses lattice convention of RuNNer (v1 = lat(1,:))
subroutine getNcells(lattice, cutoff, n)
    implicit none
    real*8, intent(in) :: lattice(3,3)
    real*8, intent(in) :: cutoff
    integer, intent(out) :: n(3)
    real*8 :: axb(3), absaxb, axc(3), absaxc, bxc(3), absbxc, proja, projb, projc


    !! calculate the norm vectors of the 3 planes
    axb(1)=lattice(1,2)*lattice(2,3)-lattice(1,3)*lattice(2,2)
    axb(2)=lattice(1,3)*lattice(2,1)-lattice(1,1)*lattice(2,3)
    axb(3)=lattice(1,1)*lattice(2,2)-lattice(1,2)*lattice(2,1)
    absaxb=sqrt(axb(1)**2+axb(2)**2+axb(3)**2)
    axb(1)=axb(1)/absaxb
    axb(2)=axb(2)/absaxb
    axb(3)=axb(3)/absaxb
    axc(1)=lattice(1,2)*lattice(3,3)-lattice(1,3)*lattice(3,2)
    axc(2)=lattice(1,3)*lattice(3,1)-lattice(1,1)*lattice(3,3)
    axc(3)=lattice(1,1)*lattice(3,2)-lattice(1,2)*lattice(3,1)
    absaxc=sqrt(axc(1)**2+axc(2)**2+axc(3)**2)
    axc(1)=axc(1)/absaxc
    axc(2)=axc(2)/absaxc
    axc(3)=axc(3)/absaxc
    bxc(1)=lattice(2,2)*lattice(3,3)-lattice(2,3)*lattice(3,2)
    bxc(2)=lattice(2,3)*lattice(3,1)-lattice(2,1)*lattice(3,3)
    bxc(3)=lattice(2,1)*lattice(3,2)-lattice(2,2)*lattice(3,1)
    absbxc=sqrt(bxc(1)**2+bxc(2)**2+bxc(3)**2)
    bxc(1)=bxc(1)/absbxc
    bxc(2)=bxc(2)/absbxc
    bxc(3)=bxc(3)/absbxc
    !! calculate the projections
    proja=lattice(1,1)*bxc(1)+lattice(1,2)*bxc(2)+lattice(1,3)*bxc(3)
    projb=lattice(2,1)*axc(1)+lattice(2,2)*axc(2)+lattice(2,3)*axc(3)
    projc=lattice(3,1)*axb(1)+lattice(3,2)*axb(2)+lattice(3,3)*axb(3)
    proja=abs(proja)
    projb=abs(projb)
    projc=abs(projc)
    !! determine how often we have to multiply the cell in which direction
    n(1) = ceiling(cutoff / proja)
    n(2) = ceiling(cutoff / projb)
    n(3) = ceiling(cutoff / projc)
end subroutine getNcells
