!######################################################################
! This routine is part of
! RuNNer - RuNNer Neural Network Energy Representation
! (c) 2008-2024 Prof. Dr. Joerg Behler 
! Ruhr-Universitaet Bochum, Germany
!
! This program is free software: you can redistribute it and/or modify it 
! under the terms of the GNU General Public License as published by the 
! Free Software Foundation, either version 3 of the License, or 
! (at your option) any later version.
!
! This program is distributed in the hope that it will be useful, but 
! WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
! or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
! for more details.
!
! You should have received a copy of the GNU General Public License along 
! with this program. If not, see http://www.gnu.org/licenses.
! WARNING: THIS FILE IS PART OF THE UNMAINTAINED CONDA PACKAGE OF RUNNER 1.3. 
!######################################################################
!! called by: 
!! - getstructures.f90
!! - getstructurespair.f90
!!
      subroutine getonestructure(unit,num_atoms,zelem,&
        lattice,xyzstruct,atomspin,totalforce,atomcharge,atomenergy,&
        lperiodic)
!!
      use fileunits
      use globaloptions
!!
      implicit none
!!
      integer unit                                                      ! in
      integer num_atoms                                                 ! in 
      integer zelem(max_num_atoms)                                      ! out
      integer idummy                                                    ! internal    
      integer i1,i2                                                     
!!                                                                      
      real*8 lattice(3,3)                                               ! out
      real*8 xyzstruct(3,max_num_atoms)                                 ! out
      real*8 atomspin(max_num_atoms)                                    ! out
      real*8 totalforce(3,max_num_atoms)                                ! out
      real*8 atomcharge(max_num_atoms)                                  ! out
      real*8 atomenergy(max_num_atoms)                                  ! out
!!                                                                      
      logical lperiodic                                                 ! out 
!!
      read(unit,*)idummy,lperiodic
      if(lperiodic)then
        do i1=1,3
          read(unit,*)(lattice(i1,i2),i2=1,3)
        enddo
      endif
      do i1=1,num_atoms
        read(unit,*)zelem(i1),(xyzstruct(i2,i1),i2=1,3),atomcharge(i1),&
        atomenergy(i1),(totalforce(i2,i1),i2=1,3)
!! ATTENTION: atomenergy and atomspin are read from the same column
        atomspin(i1)=atomenergy(i1)
      enddo
!!
      return
      end
