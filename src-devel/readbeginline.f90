!######################################################################
! This routine is part of
! RuNNer - RuNNer Neural Network Energy Representation
! (c) 2008-2024 Prof. Dr. Joerg Behler 
! Ruhr-Universitaet Bochum, Germany
!
! This program is free software: you can redistribute it and/or modify it 
! under the terms of the GNU General Public License as published by the 
! Free Software Foundation, either version 3 of the License, or 
! (at your option) any later version.
!
! This program is distributed in the hope that it will be useful, but 
! WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
! or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
! for more details.
!
! You should have received a copy of the GNU General Public License along 
! with this program. If not, see http://www.gnu.org/licenses.
! WARNING: THIS FILE IS PART OF THE UNMAINTAINED CONDA PACKAGE OF RUNNER 1.3. 
!######################################################################
!! subroutine written by Nikolas
!! called by:
!! - readonestructure.f90
!! - checkonestructure.f90
!! - paircount.f90
!!
      subroutine readbeginline(begin_line, loldinputdata,& 
      inputdata_col_indx, inputdata_n_prop)
!!
      use globaloptions
!!      
      implicit none
!!
      type propertyindexes                     ! internal Niko, holds column index
        integer :: x_coord                     ! of respeective property 
        integer :: y_coord                     ! in input.data
        integer :: z_coord 
        integer :: elem_sym
        integer :: atom_charge
        integer :: atom_spin
        integer :: atom_energy
        integer :: x_force
        integer :: y_force
        integer :: z_force
      endtype propertyindexes 
!!
      type(propertyindexes) :: inputdata_col_indx
      
      integer :: indx_sort_arr(max_num_prop)   ! internal Niko
      integer :: indx_assign_arr(max_num_prop) ! internal Niko
      integer :: j                             ! internal Niko
      integer :: k                             ! internal Niko
      integer :: inputdata_n_prop              ! internal Niko
      integer :: n_assign_tmp                  ! internal Niko
      integer :: max_val_tmp                   ! internal Niko
      integer :: max_val_indx_tmp              ! internal Niko
!!      
      character*200 :: begin_line              ! internal Niko, string containing data of 'begin' line in input.data 
!!      
      logical :: loldinputdata                 ! true if input.data format of RuNNer V1.X 
!!
!!=====================================================
!! initializations
!!=====================================================
      inputdata_col_indx          = propertyindexes(0,0,0,0,0,0,0,0,0,0)
      indx_sort_arr(:)            = 0
      indx_assign_arr(:)          = 0
      inputdata_n_prop            = 0
      n_assign_tmp                = 0
      max_val_tmp                 = 999
      max_val_indx_tmp            = 0
      loldinputdata               = .false.
!!
      format_check: if(trim(adjustl(begin_line)).eq.'begin') then   ! Niko, checks if line contains more than 'begin'
        loldinputdata=.true.
!!      elseif(index(begin_line, ' custom ') /= 0) then      ! Niko, exemplary keyword 'custom' to check for new format
      elseif((index(begin_line, ' x') /= 0).and.&
        (index(begin_line, ' y') /= 0).and.&
        (index(begin_line, ' z') /= 0).and.&
        (index(begin_line, ' elem_symbol') /= 0)) then                    
        loldinputdata=.false.
      else
        write(*,*) "ERROR in 'begin' line in input.data: ",& 
          trim(begin_line)
        stop    
      endif format_check

      check_keywords: if(loldinputdata.eqv..false.) then   ! Niko, gathers all indexes (position of first match from left)
        indx_sort_arr(1)  = index(begin_line, ' x')       ! of all named keywords
        indx_sort_arr(2)  = index(begin_line, ' y')      
        indx_sort_arr(3)  = index(begin_line, ' z')      
        indx_sort_arr(4)  = index(begin_line, ' elem_symbol')      
        indx_sort_arr(5)  = index(begin_line, ' atom_charge')      
        indx_sort_arr(6)  = index(begin_line, ' atom_spin')      
        indx_sort_arr(7)  = index(begin_line, ' atom_energy')      
        indx_sort_arr(8)  = index(begin_line, ' fx')      
        indx_sort_arr(9)  = index(begin_line, ' fy')      
        indx_sort_arr(10) = index(begin_line, ' fz')     

        check_none_zero: do j=1,max_num_prop               ! Niko, number of non zero elements equal to
          if(indx_sort_arr(j)/=0) then                     ! number of given properties in input.data
            inputdata_n_prop = inputdata_n_prop + 1 
          endif      
        enddo check_none_zero
          
        n_assign_tmp = inputdata_n_prop                   ! Niko, assigns column indexes in decending
        indexes: do k=1,inputdata_n_prop                  ! order to properties given in input.data by comparing
          max_val_indx_tmp = maxloc(indx_sort_arr, &      ! substring indexes
          dim=1,mask=indx_sort_arr<max_val_tmp)
          max_val_tmp = indx_sort_arr(max_val_indx_tmp)
          indx_assign_arr(max_val_indx_tmp) = n_assign_tmp
          n_assign_tmp = n_assign_tmp - 1
        enddo indexes   
          
        inputdata_col_indx%x_coord      = indx_assign_arr(1) 
        inputdata_col_indx%y_coord      = indx_assign_arr(2)
        inputdata_col_indx%z_coord      = indx_assign_arr(3)
        inputdata_col_indx%elem_sym     = indx_assign_arr(4)
        inputdata_col_indx%atom_charge  = indx_assign_arr(5)
        inputdata_col_indx%atom_spin    = indx_assign_arr(6)
        inputdata_col_indx%atom_energy  = indx_assign_arr(7)
        inputdata_col_indx%x_force      = indx_assign_arr(8)
        inputdata_col_indx%y_force      = indx_assign_arr(9)
        inputdata_col_indx%z_force      = indx_assign_arr(10)
      else
        continue

      endif check_keywords
!!
      end subroutine readbeginline
