

module linalg
    use precision
    implicit none
contains
    function det3D(M) result(det)
        real(dp), intent(in) :: M(3,3)
        real(dp) :: det

        det = M(1,1) * M(2,2) * M(3,3) &
                + M(1,2) * M(2,3) * M(3,1) &
                + M(1,3) * M(2,1) * M(3,2) &
                - M(1,1) * M(2,3) * M(3,2) &
                - M(1,2) * M(2,1) * M(3,3) &
                - M(1,3) * M(2,2) * M(3,1)

    end function det3D

    subroutine inv3DM(A, B)
        implicit none
        real(dp), intent(in), dimension(3, 3) :: A
        real(dp), intent(out), dimension(3, 3) :: B
        real(dp), dimension(3) :: a1, a2, a3
        real(dp), dimension(3) :: b1, b2, b3
        real(dp) :: det

        a1 = A(1, :)
        a2 = A(2, :)
        a3 = A(3, :)

        det = det3D(A)

        b1 = [a2(2) * a3(3) - a2(3) * a3(2), a1(3) * a3(2) - a1(2) * a3(3), a1(2) * a2(3) - a1(3) * a2(2)]
        b2 = [a2(3) * a3(1) - a2(1) * a3(3), a1(1) * a3(3) - a1(3) * a3(1), a1(3) * a2(1) - a1(1) * a2(3)]
        b3 = [a2(1) * a3(2) - a2(2) * a3(1), a1(2) * a3(1) - a1(1) * a3(2), a1(1) * a2(2) - a1(2) * a2(1)]

        b1 = b1 / det
        b2 = b2 / det
        b3 = b3 / det

        B(1, :) = b1
        B(2, :) = b2
        B(3, :) = b3
    end subroutine
end module linalg