


subroutine getChargeScaling(ntrain, minvalue, maxvalue, avalue)
    use precision
    use globaloptions
    use structures
    use fileunits
    use nnshort_atomic
    implicit none
    integer, intent(in) :: ntrain
    real(dp), intent(out) :: minvalue(nelem), maxvalue(nelem), avalue(nelem)

    integer :: i,j
    integer :: zelem(max_num_atoms)
    integer :: num_atoms
    real(dp) :: lattice(3,3)
    real(dp) :: atomcharge(max_num_atoms)
    real(dp) :: atomenergy(max_num_atoms)
    real(dp) :: xyzstruct(3,max_num_atoms)
    real(dp) :: atomspin(max_num_atoms)
    real(dp) :: totalforce(3,max_num_atoms)
    logical :: lperiodic
    integer :: countEl(nelem)
    real(dp) :: avaluesquared(nelem)
    integer :: iElement
    real(dp) :: std(nelem)
    real(dp) :: thres

    countEl(:) = 0._dp
    minvalue(:) = huge(1._dp)
    maxvalue(:) = -huge(1._dp)
    avalue(:) = 0._dp
    avaluesquared(:) = 0._dp

    open(symunit,file='function.data',form='formatted',status='old')
    rewind(symunit)
    open(trainstructunit,file='trainstruct.data',form='formatted',status='old')
    rewind(trainstructunit)

    do i=1,ntrain
        ! we only call readfunctions to get num_atoms
        call readfunctions(1,symunit,1,nelem,&
                max_num_atoms,maxnum_funcvalues_short_atomic,num_funcvalues_short_atomic,&
                symfunction_short_atomic_list)
        num_atoms = num_atoms_list(1)
        ! then read the atomic charges from trainstruct
        call getonestructure(trainstructunit,num_atoms,zelem,&
                lattice,xyzstruct,atomspin,totalforce,atomcharge,atomenergy,&
                lperiodic)

        do j=1,num_atoms
            ielement = elementindex(zelem(j))
            countEl(ielement) = countEl(ielement) + 1
            minvalue(iElement) = min(minvalue(iElement), atomcharge(j))
            maxvalue(iElement) = max(maxvalue(iElement), atomcharge(j))
            avalue(iElement) = avalue(iElement) + atomcharge(j)
            avaluesquared(iElement) = avaluesquared(iElement) + atomcharge(j)**2
        enddo
    enddo

    do i=1,nelem
        avalue(i) = avalue(i) / real(countEl(i), dp)
        avaluesquared(i) = avaluesquared(i) / real(countEl(i), dp)
        std(i) = sqrt(avaluesquared(i) - avalue(i)**2)
    enddo


    close(trainstructunit)
    close(symunit)


    thres = 0.0001d0
    do i=1,nelem
        write(ounit,*)'============================================================='
        write(ounit,'(a,a2)')' Partial charge values  for element ',element(i)
        write(ounit,'(a)')' Training set:           min           max       average           std         range'
        write(ounit,'(A14,5f14.8)')element(i),minvalue(i),maxvalue(i),&
                avalue(i),std(i),abs(maxvalue(i)-minvalue(i))
        if(abs(minvalue(i)-maxvalue(i)) < thres)then
            write(ounit,*)'### WARNING ###: minvalue=maxvalue',i,nucelem(i)
            if(lscalecharge)then
                write(ounit,*)'scaling partial charges cannot be used with minvalue=maxvalue'
                stop
            endif
        endif
    enddo ! i2

end subroutine getChargeScaling
