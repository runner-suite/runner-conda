!######################################################################
! This routine is part of
! RuNNer - RuNNer Neural Network Energy Representation
! (c) 2008-2024 Prof. Dr. Joerg Behler 
! Ruhr-Universitaet Bochum, Germany
!
! This program is free software: you can redistribute it and/or modify it 
! under the terms of the GNU General Public License as published by the 
! Free Software Foundation, either version 3 of the License, or 
! (at your option) any later version.
!
! This program is distributed in the hope that it will be useful, but 
! WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
! or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
! for more details.
!
! You should have received a copy of the GNU General Public License along 
! with this program. If not, see http://www.gnu.org/licenses.
! WARNING: THIS FILE IS PART OF THE UNMAINTAINED CONDA PACKAGE OF RUNNER 1.3. 
!######################################################################
!! called by: - geteshort.f90
!!
      subroutine calconeshort_type3(num_atoms,&
        zelem,symfunction,charge_local,&
        nneshort,nnatomenergy)
!!
      use fileunits
      use globaloptions
      use nnshort_atomic
!!
      implicit none
!!
      integer num_atoms                 ! in
      integer zelem(max_num_atoms)
!!
      integer i1
!!
      real*8 symfunction(maxnum_funcvalues_short_atomic,max_num_atoms)    ! in
      real*8 symfunction_atom(maxnum_funcvalues_short_atomic)             ! internal
      real*8 charge_local(max_num_atoms)                             ! in
      real*8 charge_atom 
      real*8 weights(maxnum_weights_short_atomic)                    ! internal
!! CAUTION: nnoutput assumes just one output node here
      real*8 nnoutput                                     ! internal
      real*8 nneshort                                       ! out
      real*8 nodes_values_dummy(maxnum_layers_short_atomic,maxnodes_short_atomic) ! just dummy in this routine
      real*8 nodes_sum_dummy(maxnum_layers_short_atomic,maxnodes_short_atomic)    ! just dummy in this routine
      real*8 nnatomenergy(num_atoms)                  ! out
      character*1 actfunc_short_atomic_local(maxnodes_short_atomic,maxnum_layers_short_atomic)
!!
!!
      nneshort=0.0d0
      nnatomenergy(:)=0.0d0
      charge_atom = 0.0d0
      actfunc_short_atomic_local(:,:)=" "
!!
!!#################################################################
!! serial original:
!!
      do i1=1,num_atoms
!!
!!
        symfunction_atom(:)=symfunction(:,i1)
        weights(:)=weights_short_atomic(:,elementindex(zelem(i1)))
        charge_atom = charge_local(i1)
        actfunc_short_atomic_local(:,:) = actfunc_short_atomic(:,:,elementindex(zelem(i1)))
!!
!! calculate nodes_values and nnoutput for atom i1
        call calconenn_type3(1,maxnum_funcvalues_short_atomic,maxnodes_short_atomic,&
          maxnum_layers_short_atomic,num_layers_short_atomic(elementindex(zelem(i1))),&
          maxnum_weights_short_atomic,nodes_short_atomic(0,elementindex(zelem(i1))),&
          symfunction_atom,charge_atom,weights,nodes_values_dummy,nodes_sum_dummy,&
          nnoutput,actfunc_short_atomic_local)
!!
        nnatomenergy(i1)=nnoutput
        nneshort=nneshort+nnoutput
!!
      enddo ! i1
!!
!!
      return
      end
