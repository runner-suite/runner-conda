!######################################################################
! This routine is part of
! RuNNer - RuNNer Neural Network Energy Representation
! (c) 2008-2024 Prof. Dr. Joerg Behler
! Ruhr-Universitaet Bochum, Germany
!
! This program is free software: you can redistribute it and/or modify it 
! under the terms of the GNU General Public License as published by the 
! Free Software Foundation, either version 3 of the License, or 
! (at your option) any later version.
!
! This program is distributed in the hope that it will be useful, but 
! WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
! or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
! for more details.
!
! You should have received a copy of the GNU General Public License along 
! with this program. If not, see http://www.gnu.org/licenses.
! WARNING: THIS FILE IS PART OF THE UNMAINTAINED CONDA PACKAGE OF RUNNER 1.3. 
!######################################################################!!
!! called by: -getsymmetryfunctions.f90
!!
!! FIXME: implement effective neighbor lists for pair case to reduce memory
!!
      subroutine calcfunctions(npoints,ndone,&
        iseed,numtrain,numtest,numrej,pointnumber,&
        minvalue_elec,maxvalue_elec,avvalue_elec)
!!
      use fileunits
      use nnflags
      use fittingoptions
      use globaloptions 
      use mode1options
      use symfunctions
      use symfunctiongroups ! Emir
      use nnshort_atomic
      use nnshort_pair
      use nnewald
      use structures
      use ewaldSummation
      use eem
!!
      implicit none
!!
      integer npoints                                                   ! in
      integer num_atoms                                                 ! internal
      integer zelem(max_num_atoms)                                      ! internal
      integer*8 iseed                                                   ! in/out
      integer numtrain                                                  ! in/out
      integer numtest                                                   ! in/out
      integer numrej                                                    ! in/out
      integer ndone                                                     ! in
      integer pointnumber                                               ! in/out
      integer i1,i2,i3                                                  ! internal
      integer j                                                         ! internal
      integer istruct                                                   ! internal
      integer ielem                                                     ! internal
      integer num_pairs                                                 ! internal
      integer, allocatable :: pairs_charge(:,:)                         ! internal
      integer, allocatable :: pairs_charge_list(:,:,:)                  ! internal
!! neighbor arrays for short range atomic case
      integer, allocatable :: lsta_shortatomic(:,:)                     ! internal, numbers of neighbors
      integer, allocatable :: lstc_shortatomic(:)                       ! internal, identification of atom
      integer, allocatable :: lste_shortatomic(:)                       ! internal, nuclear charge of atom
      integer, allocatable :: num_neighbors_short_atomic(:)             ! internal
      integer max_num_neighbors_short_atomic                            ! internal
      integer, allocatable :: neighboridx_short_atomic(:,:)             ! internal
      integer, allocatable :: invneighboridx_short_atomic(:,:)          ! internal
!! neighbor arrays for electrostatic case
      integer, allocatable :: lsta_elec(:,:)                            ! internal, numbers of neighbors
      integer, allocatable :: lstc_elec(:)                              ! internal, identification of atom
      integer, allocatable :: lste_elec(:)                              ! internal, nuclear charge of atom
      integer, allocatable :: num_neighbors_elec(:)                     ! internal
      integer max_num_neighbors_elec                                    ! internal
      integer, allocatable :: neighboridx_elec(:,:)                     ! internal
      integer, allocatable :: invneighboridx_elec(:,:)                  ! internal
      integer, allocatable :: atomindex_dummy(:)                        ! internal
!!
      real*8 lattice(3,3)                                               ! internal
      real*8 totalcharge                                                ! internal
      real*8 totalenergy                                                ! internal
      real*8 elecenergy                                                 ! internal
      real*8 atomcharge(max_num_atoms)                                  ! internal
      real*8 atomspin(max_num_atoms)                                    ! internal
      real*8 atomenergy(max_num_atoms)                                  ! internal
      real*8 totalforce(3,max_num_atoms)                                ! internal
      real*8 elecforce(3,max_num_atoms)                                 ! internal
      real*8 ranx                                                       ! internal
      real*8 random                                                     ! internal
      real*8 symfunction(maxnum_funcvalues_short_atomic,max_num_atoms)  ! internal
      real*8 symfunctionp(maxnum_funcvalues_short_pair,max_num_pairs)   ! internal
      real*8 symfunctione(maxnum_funcvalues_elec,max_num_atoms)         ! internal
      real*8 symfunctionework(maxnum_funcvalues_elec,max_num_atoms)     ! internal
      real*8 strs(3,3,maxnum_funcvalues_short_atomic,max_num_atoms)     ! internal
      real*8 strs_pair(3,3,maxnum_funcvalues_short_pair,max_num_pairs)  ! internal
      real*8 strse(3,3,maxnum_funcvalues_elec,max_num_atoms)            ! internal
      real*8, allocatable :: dsfuncdxyz(:,:,:,:)                        ! internal
      real*8, allocatable :: dsfuncdxyz_pair(:,:,:,:)                   ! internal
      real*8, allocatable :: dsfuncdxyze(:,:,:,:)                       ! internal
      real*8, allocatable :: dchargedxyz(:,:,:)                         ! internal
      real*8 forcesum(3)                                                ! internal
      real*8 absforcesum                                                ! internal
      real*8 highfcomponent                                             ! internal
      real*8 nodes_values(maxnum_layers_elec,maxnodes_elec)             ! internal
      real*8 nodes_sum(maxnum_layers_elec,maxnodes_elec)                ! internal

      real*8 minvalue_elec(nelem,maxnum_funcvalues_elec)                ! in 
      real*8 maxvalue_elec(nelem,maxnum_funcvalues_elec)                ! in
      real*8 avvalue_elec(nelem,maxnum_funcvalues_elec)                 ! in

      real*8 nnatomcharge(max_num_atoms)                                ! internal
      real*8 nnatomcharge_list(npoints,max_num_atoms)                   ! internal
      real*8, allocatable :: lstb_shortatomic(:,:)                      ! xyz and r_ij 
      real*8, allocatable :: lstb_elec(:,:)                             ! xyz and r_ij 

!!
      character*2 elementsymbol(max_num_atoms)                          ! internal
!!
      logical lperiodic                                                 ! internal
      logical ldoforces                                                 ! internal here!!!
      logical lrmin(npoints)                                            ! internal
      logical lforceok                                                  ! internal
!! vdw dispersion correction
      real*8, dimension(:   ), allocatable :: atomvdw                   ! internal
      real*8, dimension(:, :), allocatable :: vdwforces                 ! internal
      real*8, dimension(:, :), allocatable :: vdwstress                 ! internal
      real*8 vdwenergy                                                  ! internal
!! 4G variables
      integer iatom,jatom
      real*8 gausswidth(max_num_atoms),hardness(max_num_atoms)          ! internal
      real*8 nnatomChi(max_num_atoms),dChidsfunc(max_num_atoms,maxnum_funcvalues_elec) ! internal
      real*8 dAdxyzQ(max_num_atoms,3,max_num_atoms) 
      real*8 Amatrix(max_num_atoms,max_num_atoms)
      real*8 dChidxyz(max_num_atoms,3,max_num_atoms)
      !! KK: dummy variable for 4G electrostatic tensor, not used for the short-range part
      real*8 dAdLatQ_dummy(max_num_atoms,3,3)
      real*8 dChidLat_dummy(max_num_atoms,3,3)
      real*8 dEdLat_dummy(3,3)
!!
!!=============================================================================
!! initializations
!!=============================================================================
      elecforce(:,:)        = 0.0d0
      elecforce_list(:,:,:) = 0.0d0
      elecenergy            = 0.0d0
      elecenergy_list(:)    = 0.0d0
      lrmin(:)              = .true.
!!      ldoforces             = luseforces
      ldoforces             = .false. !! avoid redundant calculation of SF derivatives here
      symfunction(:,:)      = 0.0d0
      symfunctionp(:,:)     = 0.0d0
      symfunctione(:,:)     = 0.0d0
      lforceok              = .true.
      nnatomcharge_list(:,:)=0.0d0

      ! JF: we need the sf derivatives for the electrostatics (dQ/dx)
      if (lelec) ldoforces = .true.

!! caution: pairs_charge arrays must not be allocated for atomic case, because max_num_pairs is not available => segmentation fault
      if((nn_type_short.eq.2))then
        allocate(pairs_charge_list(2,listdim,nblock)) 
        allocate(pairs_charge(2,listdim))
      endif
!!
!!=============================================================================
!! prepare input for calculating the symmetry functions of one structure
!!=============================================================================
      do i1=1,npoints
        num_atoms        = num_atoms_list(i1)
        zelem(:)         = zelem_list(i1,:) 
        lattice(:,:)     = lattice_list(:,:,i1)
        totalcharge      = totalcharge_list(i1)
        totalenergy      = totalenergy_list(i1)
        atomcharge(:)    = atomcharge_list(i1,:)
        atomspin(:)      = atomspin_list(i1,:)
        atomenergy(:)    = atomenergy_list(i1,:)
        totalforce(:,:)  = totalforce_list(:,:,i1)
        elementsymbol(:) = elementsymbol_list(i1,:)
        lperiodic        = lperiodic_list(i1)
        istruct          = ndone+i1
!!
!!=============================================================================
!! allocate arrays for neighbor lists
!!=============================================================================
        if(lshort.and.((nn_type_short.eq.1).or.(nn_type_short.eq.3)))then
          allocate(lsta_shortatomic(2,max_num_atoms))
          allocate(lstc_shortatomic(listdim))
          allocate(lste_shortatomic(listdim))
          allocate(lstb_shortatomic(listdim,4))
          allocate(num_neighbors_short_atomic(num_atoms))
        endif
        if((lelec.and.(nn_type_elec.eq.1)).or.(lelec.and.(nn_type_elec.eq.5)))then
          allocate(lsta_elec(2,max_num_atoms))
          allocate(lstc_elec(listdim))
          allocate(lste_elec(listdim))
          allocate(lstb_elec(listdim,4))
          allocate(num_neighbors_elec(num_atoms))
        endif
        if(lremovevdwenergies)then
          allocate(atomvdw(max_num_atoms))
          allocate(vdwforces(3, max_num_atoms))
          allocate(vdwstress(3, 3))
          atomvdw(:)        = 0.0d0
          vdwforces(:, :)   = 0.0d0
          vdwstress(:, :)   = 0.0d0
          vdwenergy         = 0.0d0
        endif !lvdw
!!
!!=============================================================================
!! determine neighbor lists of all atoms (lsta,lstb,lstc,lste), max_num_neighbors and num_neighbors
!!=============================================================================
        if(lshort.and.((nn_type_short.eq.1).or.(nn_type_short.eq.3)))then
          call getneighborsatomic(&
            num_atoms,num_neighbors_short_atomic,zelem,&
            max_num_neighbors_short_atomic,&
            lsta_shortatomic,lstc_shortatomic,lste_shortatomic,&
            maxcutoff_short_atomic,lattice,xyzstruct_list(1,1,i1),&
            lstb_shortatomic,lperiodic)
        endif
        if((lelec.and.(nn_type_elec.eq.1)).or.(lelec.and.(nn_type_elec.eq.5)))then
          call getneighborsatomic(&
            num_atoms,num_neighbors_elec,zelem,&
            max_num_neighbors_elec,&
            lsta_elec,lstc_elec,lste_elec,&
            maxcutoff_elec,lattice,xyzstruct_list(1,1,i1),&
            lstb_elec,lperiodic)
        endif

!!
!!=============================================================================
!! allocate further arrays and determine neighboridx arrays 
!!=============================================================================
        if(lshort.and.((nn_type_short.eq.1).or.(nn_type_short.eq.3)))then
          allocate(dsfuncdxyz(maxnum_funcvalues_short_atomic,max_num_atoms,0:max_num_neighbors_short_atomic,3))
          allocate(neighboridx_short_atomic(num_atoms,0:max_num_neighbors_short_atomic))  
          allocate(invneighboridx_short_atomic(num_atoms,max_num_atoms))  
          call getneighboridxatomic(num_atoms,listdim,&
            max_num_atoms,max_num_neighbors_short_atomic,&
            lsta_shortatomic,lstc_shortatomic,neighboridx_short_atomic,&
            invneighboridx_short_atomic)
        elseif(lshort.and.(nn_type_short.eq.2))then
          allocate(dsfuncdxyz_pair(maxnum_funcvalues_short_pair,max_num_pairs,max_num_atoms,3)) 
        endif
        if((lelec.and.(nn_type_elec.eq.1)).or.(lelec.and.(nn_type_elec.eq.5)))then
          allocate(dsfuncdxyze(maxnum_funcvalues_elec,max_num_atoms,0:max_num_neighbors_elec,3))
          allocate(neighboridx_elec(num_atoms,0:max_num_neighbors_elec))  
          allocate(invneighboridx_elec(num_atoms,max_num_atoms))  
          call getneighboridxatomic(num_atoms,listdim,&
            max_num_atoms,max_num_neighbors_elec,&
            lsta_elec,lstc_elec,neighboridx_elec,invneighboridx_elec)
        endif
!!
        allocate(atomindex_dummy(num_atoms))
        do i2=1,num_atoms
          atomindex_dummy(i2)=i2
        enddo
!!=============================================================================
!! calculate the short range symmetry functions for one structure here
!!=============================================================================
        if(lshort.and.((nn_type_short.eq.1).or.(nn_type_short.eq.3)).and..not.lusesfgroups)then
          call calconefunction_atomic(cutoff_type,cutoff_alpha,max_num_neighbors_short_atomic,&
            max_num_atoms,1,num_atoms,atomindex_dummy,max_num_atoms,elementindex,& ! JF: changed natomsdim to max_num_atoms since otherwise dsfuncdxyz is incorrect. Does it make sense?
            maxnum_funcvalues_short_atomic,num_funcvalues_short_atomic, &
            nelem,zelem,listdim,&
            lsta_shortatomic,lstc_shortatomic,lste_shortatomic,&
            invneighboridx_short_atomic,&
            function_type_short_atomic,symelement_short_atomic,&
            xyzstruct_list(1,1,i1),atomspin,symfunction(:,:num_atoms),rmin,&
            funccutoff_short_atomic,eta_short_atomic,rshift_short_atomic,&
            lambda_short_atomic,zeta_short_atomic,dsfuncdxyz,strs,lstb_shortatomic,&
            lperiodic,ldoforces,ldostress,.false.,lrmin(i1))
          !!! Calculate SFs using SF groups (Emir)
        elseif(lshort.and.(nn_type_short.eq.1).and.lusesfgroups)then
          call calconefunction_atomic_sfg(cutoff_type,cutoff_alpha,max_num_neighbors_short_atomic,&
            max_num_atoms,1,num_atoms,atomindex_dummy,elementindex,maxnum_funcvalues_short_atomic,&
            nelem,zelem,listdim,lsta_shortatomic,lstb_shortatomic,lstc_shortatomic,lste_shortatomic,&
            invneighboridx_short_atomic,function_type_short_atomic_sfg,sf_count_sfg,sf_index_sfg,&
            neighbors_sfg,maxnum_sfgroups_short_atomic,num_groups_short_atomic,funccutoff_short_atomic_sfg,&
            eta_short_atomic_sfg,rshift_short_atomic_sfg,lambda_short_atomic_sfg,&
            zeta_short_atomic_sfg,xyzstruct_list(1,1,i1),rmin,symfunction,dsfuncdxyz,strs,&
            lperiodic,ldoforces,.false.,lrmin(i1))
        elseif(lshort.and.(nn_type_short.eq.2))then
          call calconefunction_pair(1,&
            istruct,num_atoms,zelem,&
            num_pairs,pairs_charge,&
            lattice,xyzstruct_list(1,1,i1),symfunctionp,&
            dsfuncdxyz_pair,strs_pair,&
            lperiodic,ldoforces,lrmin(i1))
!!
            pairs_charge_list(:,:,i1)=pairs_charge(:,:)
        endif
!!
!!=============================================================================
!! calculate the electrostatic symmetry functions for one structure here 
!!=============================================================================
        if((lelec.and.(nn_type_elec.eq.1)).or.(lelec.and.(nn_type_elec.eq.5)))then
          call calconefunction_atomic(cutoff_type,cutoff_alpha,max_num_neighbors_elec,&
            max_num_atoms,1,num_atoms,atomindex_dummy,max_num_atoms,elementindex,&
            maxnum_funcvalues_elec,num_funcvalues_elec,&
            nelem,zelem,listdim,&
            lsta_elec,lstc_elec,lste_elec,invneighboridx_elec,&
            function_type_elec,symelement_elec,&
            xyzstruct_list(1,1,i1),atomspin,symfunctione,rmin,&
            funccutoff_elec,eta_elec,rshift_elec,lambda_elec,zeta_elec,dsfuncdxyze,strse,lstb_elec,&
            lperiodic,ldoforces,ldostress,.false.,lrmin(i1))
        endif

!!
!!=============================================================================
!! deallocate arrays 
!!=============================================================================
        deallocate(atomindex_dummy)
        if(lshort)then
          if((nn_type_short.eq.1).or.(nn_type_short.eq.3))then
            deallocate(lsta_shortatomic)
            deallocate(lstc_shortatomic)
            deallocate(lste_shortatomic)
            deallocate(lstb_shortatomic)
!            deallocate(invneighboridx_short_atomic)  
          endif
          if((nn_type_short.eq.1).or.(nn_type_short.eq.3))then
!            deallocate(dsfuncdxyz)
          elseif(nn_type_short.eq.2)then
            deallocate(dsfuncdxyz_pair)
          endif
        endif !lshort
        if((lelec.and.(nn_type_elec.eq.1)).or.(lelec.and.(nn_type_elec.eq.5)))then
          deallocate(lsta_elec)
          deallocate(lstc_elec)
          deallocate(lste_elec)
          deallocate(lstb_elec)
        endif
!!
!!=============================================================================
!! first initialize shortforce_list as totalforce_list 
!!=============================================================================
        shortforce_list(:,:,i1) =totalforce_list(:,:,i1)
!!
!!=============================================================================
!! calculate the electrostatic contribution to the total energy and forces for 3G and 4G
!!=============================================================================
        if(lelec.and.lshort)then !! kk: some keywords are needed to switch on here
          elecforce(:,:)=0.0d0 !! kk: need to be reinitialized
          elecenergy=0.0d0 !! kk: need to be reinitialized
!!=============================================================================
!! fixed charge case: 
!!=============================================================================
          if(lusefixedcharge)then 
            allocate(dchargedxyz(max_num_atoms,0:max_num_neighbors_short_atomic,3))
            dchargedxyz(:,:,:)=0.0d0
            nnatomcharge(:)=0.0d0
            do i2=1,num_atoms
                nnatomcharge(i2)=fixedcharge(elementindex(zelem(i2)))
            enddo
            nnatomcharge_list(i1,:)=nnatomcharge(:)
            call electrostatics3G(num_atoms, xyzstruct_list(:,:num_atoms,i1), lattice, nnatomcharge, &
                    totalcharge, max_num_neighbors_short_atomic, dchargedxyz, &
                    invneighboridx_short_atomic, zelem, elecenergy, elecforce, lperiodic)
            elecforce_list(:,:,i1)  =elecforce(:,:)
            shortforce_list(:,:,i1) =shortforce_list(:,:,i1)-elecforce_list(:,:,i1)
            deallocate(dchargedxyz) 
!!=============================================================================
!! environment-dependent charges for 3G
!! We need to get the charges and forces from a prepared NN fit here!
!!=============================================================================
          elseif(nn_type_short.eq.1)then !! KK:for 3G case
                        
!!
!!=============================================================================
!! Step 1: prepare symmetry functions
!!=============================================================================
!! for the scaling we have to make a working copy of symfuncione -> symfunctionework
!! (symfunctione itself must be written to file unscaled!)
!! for dsfuncdxyze this is not necessary, because it is not written and can be modified
            symfunctionework(:,:)=symfunction(:,:)
            allocate(dchargedxyz(max_num_atoms,0:max_num_neighbors_short_atomic,3))
            dchargedxyz(:,:,:)=0.0d0
            nnatomcharge(:)=0.0d0
!!=============================================================================
!! scale the symmetry functions for the charge prediction
!!=============================================================================
            ! todo: check when this part would be called
            !print*, 'WARING: If you are using electrostatics type 1 (2nd NN) the results are probably wrong!'
!            if (.false.) then
            call scalesymone(nelem,&
              maxnum_funcvalues_elec,num_funcvalues_elec,num_atoms,&
              zelem,symfunctionework,&
              minvalue_elec,maxvalue_elec,avvalue_elec,&
              scmin_short_atomic,scmax_short_atomic)
!!=============================================================================
!! we also need to scale the derivative terms dsfuncdxyze and strse 
!!=============================================================================
            call scaledsfunc(max_num_neighbors_short_atomic,&
              maxnum_funcvalues_elec,num_funcvalues_elec,&
              nelem,num_atoms,minvalue_elec,maxvalue_elec,&
              scmin_short_atomic,scmax_short_atomic,&
              zelem,dsfuncdxyz,strs)
!!=============================================================================
!! predict the atomic charges 'nnatomcharge' for this structure
!!=============================================================================
            nnatomcharge(:)=0.0d0
            call calc_charges(num_atoms, xyzstruct_list(:,:num_atoms,i1), zelem, symfunctionework, &
                       sum(atomcharge(:num_atoms)), nnatomcharge, lperiodic, lattice)
            nnatomcharge_list(i1,:num_atoms)=nnatomcharge(:num_atoms)
!!=============================================================================
!! Step 2: calculate dchargedxyz array for forces
!!=============================================================================
            if(lperiodic)then
              call getdchargedxyz(max_num_neighbors_short_atomic,&
                num_neighbors_short_atomic,neighboridx_short_atomic,num_atoms,zelem,&
                dsfuncdxyz,dchargedxyz,symfunctionework)
            else ! not periodic
              call getcoulombdchargedxyz(max_num_neighbors_short_atomic,&
                num_neighbors_short_atomic,num_atoms,zelem,&
                dsfuncdxyz,symfunctionework,dchargedxyz)
            endif ! lperiodic
!!
!! FIXME: Dirty workaround in case we are intending to do the first charge fit without short range part
!! (if we want to fit short range part then NN electrostatic E and F should be removed, but otherwise not)
!            if(.not.lshort)then
!              nnatomcharge(:)=atomcharge(:)
!            endif
!!
!!=============================================================================
!! Step 3: calculate the NN electrostatic energy and forces
!!=============================================================================
           ! call electrostaticsG3(num_atoms,xyzstruct_list(:,:,i1),lattice,nnatomcharge,max_num_neighbors_short_atomic,&
           !     dchargedxyz,invneighboridx_short_atomic,num_funcvalues_elec,zelem,&
           !     atomindex_dummy,elecenergy,elecforce, lperiodic)
            call electrostatics3G(num_atoms, xyzstruct_list(:,:num_atoms,i1), lattice, nnatomcharge, &
                    totalcharge, max_num_neighbors_short_atomic, dchargedxyz, &
                    invneighboridx_short_atomic, zelem, elecenergy, elecforce, lperiodic)
                !call getewaldenergy(max_num_neighbors_short_atomic,invneighboridx_short_atomic,&
                !  neighboridx_short_atomic,num_neighbors_short_atomic,num_atoms,zelem,&
                !  lattice,xyzstruct_list(1,1,i1),nnatomcharge,elecenergy,&
                !  dchargedxyz,elecforce,.true.)
!            endif ! todo: check why the part until this one is inside if(.false.)
            elecforce_list(:,:,i1)  =elecforce(:,:)
            shortforce_list(:,:,i1) =shortforce_list(:,:,i1)-elecforce_list(:,:,i1)
            deallocate(dchargedxyz) 
!!
!!=============================================================================
!! calculate electrostatic part for 4G
!!=============================================================================
          elseif(nn_type_short.eq.3)then
!!      initializations for 4G variables
            symfunctionework(:,:)=symfunction(:,:)
            gausswidth(:)=0.0d0
            hardness(:)=0.0d0
            dAdxyzq(:,:,:)=0.0d0
            Amatrix(:,:)=0.0d0
            nnatomChi(:)=0.0d0
            dChidsfunc(:,:)=0.0d0
            nnatomcharge(:)=0.0d0
            dChidxyz(:,:,:)       = 0.0d0
            dAdLatQ_dummy(:,:,:)  = 0.0d0
            dChidLat_dummy(:,:,:) = 0.0d0
            dEdLat_dummy(:,:)     = 0.0d0

!!      get the gausswudth and hardness first
            do i2 =1,num_atoms_list(i1)
                ielem = elementindex(zelem(i2))
                gausswidth(i2)=fixedgausswidth(ielem) 
                hardness(i2)  =fixedhardness(ielem)   
            enddo
            call scalesymone(nelem,&
              maxnum_funcvalues_elec,num_funcvalues_elec,num_atoms,&
              zelem,symfunctionework,&
              minvalue_elec,maxvalue_elec,avvalue_elec,&
              scmin_short_atomic,scmax_short_atomic)
            if(lscalesym)then
                call scaledsfunc(max_num_neighbors_short_atomic,&
                  maxnum_funcvalues_elec,num_funcvalues_elec,&
                  nelem,num_atoms,minvalue_elec,maxvalue_elec,&
                  scmin_short_atomic,scmax_short_atomic,&
                  zelem,dsfuncdxyz,strs)
            endif
!!      calculate electronegativity
            do i2 =1,num_atoms_list(i1)
                ielem = elementindex(zelem(i2))
                nodes_values(:,:)=0.0d0
                nodes_sum(:,:)=0.0d0
                call calconenn(1,maxnum_funcvalues_elec,maxnodes_elec,&
                    maxnum_layers_elec,num_layers_elec(ielem),&
                    maxnum_weights_elec, nodes_elec(0,ielem),&
                    symfunctionework(1,i2),weights_elec(1,ielem),&
                    nodes_values,nodes_sum,nnatomChi(i2),&
                    actfunc_elec(1,1,ielem))
            enddo
!!      get charges through Qeq
            if (lperiodic) then
                call eemMatrixEwald(num_atoms, xyzstruct_list(1,1,i1), lattice, gausswidth, hardness, Amatrix)
                call eemCharges(num_atoms,Amatrix,nnatomchi,totalcharge,nnatomcharge)
                call eemdAdxyzTimesQEwald(num_atoms, xyzstruct_list(1,1,i1), lattice, gausswidth, nnatomcharge, dAdxyzQ)
            else
                call eemMatrix(num_atoms,xyzstruct_list(1,1,i1),gausswidth,hardness,Amatrix)
                call eemCharges(num_atoms,Amatrix,nnatomChi,totalcharge,nnatomcharge)
                call eemdAdxyzTimesQ(num_atoms, xyzstruct_list(1,1,i1), nnatomcharge, gausswidth, dAdxyzQ)
            endif
            nnatomcharge_list(i1,:)=nnatomcharge(:)
!!      get dChidsfunc 
            call getdchargedsfunc(num_atoms,zelem,symfunctionework,dChidsfunc)
!!      get dChidxyz
            do iatom =1,num_atoms
                do jatom = 1,num_atoms
                    if (invneighboridx_short_atomic(iatom,jatom) /=-1)then
                        do i3 =1,num_funcvalues_short_atomic(elementindex(zelem(iatom))) 
                            dChidxyz(iatom,:,jatom) = dChidxyz(iatom,:,jatom) &
                                    + dChidsfunc(iatom,i3)*dsfuncdxyz(i3,iatom,invneighboridx_short_atomic(iatom,jatom),:)
                        enddo
                    endif
                enddo
            enddo 
!!      get electrostatic energy and forces
            call electrostatics_type5(num_atoms,zelem,&
            Amatrix,dAdxyzQ, dAdLatQ_dummy(:num_atoms,:,:),nnatomcharge(:num_atoms),xyzstruct_list(1,1,i1), &
            dChidxyz(:num_atoms,:,:num_atoms),dChidLat_dummy(:num_atoms,:,:),& ! todo: check :num_atoms?
            elecenergy,elecforce(:,:num_atoms),lattice, lperiodic,dEdLat_dummy)
!!     get the short-range contributions
            elecforce_list(:,:,i1)  =elecforce(:,:)
            shortforce_list(:,:,i1) =shortforce_list(:,:,i1)-elecforce_list(:,:,i1) 
          endif
!!
        else ! no electrostatics
          elecenergy=0.0d0
        endif ! lelec
        if(lelec.and.((nn_type_elec.eq.1).or.(nn_type_elec.eq.5)))then
          symfunction_elec_list(:,:,i1)=symfunctione(:,:)
          deallocate(invneighboridx_elec)
          deallocate(num_neighbors_elec)
          deallocate(neighboridx_elec)
          deallocate(dsfuncdxyze)
        endif
!!
        if(lshort.and.(.not.lelec))then  !KK: for 2G only
          if((nn_type_short.eq.1))then
            symfunction_short_atomic_list(:,:,i1) = symfunction(:,:)
            deallocate(num_neighbors_short_atomic)
            deallocate(neighboridx_short_atomic)
            deallocate(invneighboridx_short_atomic)
            deallocate(dsfuncdxyz)
          elseif(nn_type_short.eq.2)then
            symfunction_short_pair_list(:,:,i1) = symfunctionp(:,:)
          endif
        endif ! lshort
!        if((lelec.and.(nn_type_elec.eq.1)).or.(lelec.and.(nn_type_elec.eq.5)))then
        if(lelec.and.lshort)then
          symfunction_short_atomic_list(:,:,i1)= symfunction(:,:)
          elecenergy_list(i1)          = elecenergy
          shortenergy_list(i1)         = totalenergy_list(i1)-elecenergy_list(i1)
          deallocate(invneighboridx_short_atomic)
          deallocate(num_neighbors_short_atomic)
          deallocate(neighboridx_short_atomic)
          deallocate(dsfuncdxyz)
        else
          shortenergy_list(i1)     = totalenergy_list(i1)
        endif
!!
!!==============================================================================
!! remove vdw energies and/or forces from total energies/forces if requested.
!!==============================================================================
        if(lremovevdwenergies)then
          call predictionvdw(&
            num_atoms,zelem,&
            lattice,xyzstruct_list(1,1,i1),lperiodic,&
            vdwenergy, atomvdw, vdwforces, vdwstress)

          totalenergy_list(i1)         = totalenergy_list(i1)      - vdwenergy
          shortforce_list(:, :, i1)    = shortforce_list(:, :, i1) - vdwforces(:, :)

          deallocate(atomvdw)
          deallocate(vdwforces)
          deallocate(vdwstress)
        endif ! lremovevdwenergies
      enddo ! i1 over npoints
!!
!!=============================================================================
!!=============================================================================
!! write points of this block of structures to files
!!=============================================================================
!!=============================================================================
      do i1=1,npoints
        pointnumber=pointnumber+1
!!
!!=============================================================================
!! check if the sum force vector is close to zero
!!=============================================================================
        if(lcheckinputforces)then
          forcesum(:)=0.0d0
          do i2=1,num_atoms_list(i1)
            forcesum(1)=forcesum(1)+totalforce_list(1,i2,i1)
            forcesum(2)=forcesum(2)+totalforce_list(2,i2,i1)
            forcesum(3)=forcesum(3)+totalforce_list(3,i2,i1)
          enddo 
          absforcesum=dsqrt(forcesum(1)**2 + forcesum(2)**2 + forcesum(3)**2)
!!          absforcesum=absforcesum/dble(num_atoms) ! should we do this to be independent of system size???
          if(absforcesum.gt.inputforcethreshold)then
            write(ounit,'(a,i10,f14.6,4x,3f14.6)')&
              'WARNING: net force vector is large for structure ',&
              pointnumber,absforcesum,forcesum(1),forcesum(2),forcesum(3)
          endif
        endif

!!=============================================================================
!! decide if the point should be used (no high energies/forces and no too close atoms)
!!=============================================================================
        if(lfitfthres)then
          lforceok=.true.     ! by default use structure
          do i2=1,num_atoms_list(i1)
            do i3=1,3
              if(abs(totalforce_list(i3,i2,i1)).gt.fitfthres)then
                lforceok=.false. ! one too large force component found
                highfcomponent=totalforce_list(i3,i2,i1)
              endif
            enddo
          enddo
        endif

        if(((lfitethres.and.(totalenergy_list(i1).lt.(num_atoms_list(i1)*fitethres)))&
           .or.(.not.lfitethres)).and.lrmin(i1).and.&
           ((lfitfthres.and.lforceok).or.(.not.lfitfthres)))then
!!
!!=============================================================================
!! get random number for splitting in training and test set
!!=============================================================================
          random=ranx(iseed)
!!
!!=============================================================================
!! check if write format statements are sufficient for the number of short range symmetry functions
!!=============================================================================
          if(lshort)then
            if((nn_type_short.eq.1).or.(nn_type_short.eq.3))then
              do i2=1,nelem
                if(num_funcvalues_short_atomic(i2).gt.500)then
                  write(ounit,*)'Error: only 500 funcvalues possible'
                  stop
                endif
              enddo
            elseif(nn_type_short.eq.2)then
              do i2=1,npairs
                if(num_funcvalues_short_pair(i2).gt.500)then
                  write(ounit,*)'Error: only 500 funcvalues possible'
                  stop
                endif
              enddo
            endif
          endif ! lshort
          if((lelec.and.(nn_type_elec.eq.1)).or.(lelec.and.(nn_type_elec.eq.5)))then
            do i2=1,nelem
              if(num_funcvalues_elec(i2).gt.500)then
                write(ounit,*)'Error: only 500 funcvaluese possible'
                stop
              endif
            enddo
          endif
!!
!!=============================================================================
!! decide if point is for training or test set
!!=============================================================================
          if(random.gt.splitthres)then ! this is a training point
!!
!!=============================================================================
!! write function.data
!!=============================================================================
            if(lshort)then
              if((nn_type_short.eq.1).or.(nn_type_short.eq.3))then
                call writeatomicsymfunctions(symunit,i1,&
                  maxnum_funcvalues_short_atomic,&
                  num_funcvalues_short_atomic,symfunction_short_atomic_list)
              elseif(nn_type_short.eq.2)then
                call writepairsymfunctions(symunit,i1,pairs_charge_list,&
                  maxnum_funcvalues_short_pair,&
                  num_funcvalues_short_pair,symfunction_short_pair_list)
              endif
            endif ! lshort
!!
!!=============================================================================
!! write functione.data
!!=============================================================================
            if((lelec.and.(nn_type_elec.eq.1)).or.(lelec.and.(nn_type_elec.eq.5)))then
              call writeatomicsymfunctions(symeunit,i1,&
                maxnum_funcvalues_elec,num_funcvalues_elec,symfunction_elec_list)
            endif ! lelec
!!
!!=============================================================================
!! write trainstruct.data
!!=============================================================================
!! CHANGE ANDI: GFORTRAN: gfortran has different default width for logicals (1 instead of 2 for ifort),
!!                        this results in a missing space between i8 and l -> problem when reading file,
!!                        so I inserted an additional space. 
           !write(trainstructunit,'(i8,l)')numtrain+1,lperiodic_list(i1)
            write(trainstructunit,'(i8,tr1,l)')numtrain+1,lperiodic_list(i1)
!! END CHANGE
            if(lperiodic_list(i1))then
              do i2=1,3
                write(trainstructunit,'(3f20.14)')(lattice_list(i2,i3,i1),i3=1,3)
              enddo
            endif
            if(latom_spin_list(i1))then
              if(lelec.and.lshort)then !! KK: second NN of 3G and 4G cases
                  do i2=1,num_atoms_list(i1)
!! Here we write the total forces 
                    write(trainstructunit,'(i3,8(x,f15.10))')zelem_list(i1,i2),&
                      (xyzstruct_list(i3,i2,i1),i3=1,3),nnatomcharge_list(i1,i2),&
                      atomspin_list(i1,i2),(shortforce_list(i3,i2,i1),i3=1,3)
                  enddo ! i2
              else
                  do i2=1,num_atoms_list(i1)
!! Here we write the total forces 
                    write(trainstructunit,'(i3,8(x,f15.10))')zelem_list(i1,i2),&
                      (xyzstruct_list(i3,i2,i1),i3=1,3),atomcharge_list(i1,i2),&
                      atomspin_list(i1,i2),(shortforce_list(i3,i2,i1),i3=1,3)
                  enddo ! i2
              endif
            else
              if(lelec.and.lshort)then !! KK: second NN of 3G and 4G cases
                  do i2=1,num_atoms_list(i1)
!! Here we write the total forces 
                    write(trainstructunit,'(i3,8(x,f15.10))')zelem_list(i1,i2),&
                      (xyzstruct_list(i3,i2,i1),i3=1,3),nnatomcharge_list(i1,i2),&
                      atomenergy_list(i1,i2),(shortforce_list(i3,i2,i1),i3=1,3)
                  enddo ! i2
              else
                  do i2=1,num_atoms_list(i1)
!! Here we write the total forces 
                    write(trainstructunit,'(i3,8(x,f15.10))')zelem_list(i1,i2),&
                      (xyzstruct_list(i3,i2,i1),i3=1,3),atomcharge_list(i1,i2),&
                      atomenergy_list(i1,i2),(shortforce_list(i3,i2,i1),i3=1,3)
                  enddo ! i2
              endif
            endif
!!
            if(luseforces)then
!!=============================================================================
!! write trainforces.data
!!=============================================================================
              if(lshort)then
                write(trainfunit,'(i8)')numtrain+1
                do i2=1,num_atoms_list(i1)
                  write(trainfunit,'(3f15.10)')(shortforce_list(i3,i2,i1),i3=1,3)
                enddo ! i2
              endif ! lshort
!!
!!=============================================================================
!! write trainforcese.data
!! FOR FITTING WE DO NOT NEED THIS FILE because electrostatic forces can be calculated exactly and are not fitted
!!=============================================================================
              if(lelec.and.(.not.lshort))then
                write(trainfeunit,'(i8)')numtrain+1
                do i2=1,num_atoms_list(i1)
                  write(trainfeunit,'(3f15.10)')(elecforce_list(i3,i2,i1),i3=1,3)
                enddo ! i2
              endif ! lelec
!!
            endif ! luseforces
!!
            numtrain=numtrain+1
            write(ounit,*)pointnumber,' Point is used for training ',numtrain
!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
          else ! point is in test set
!!
!!=============================================================================
!! write testing.data
!!=============================================================================
            if(lshort)then
              if((nn_type_short.eq.1).or.(nn_type_short.eq.3))then
                call writeatomicsymfunctions(tymunit,i1,&
                  maxnum_funcvalues_short_atomic,&
                  num_funcvalues_short_atomic,symfunction_short_atomic_list)
              elseif(nn_type_short.eq.2)then
                call writepairsymfunctions(tymunit,i1,pairs_charge_list,&
                  maxnum_funcvalues_short_pair,&
                  num_funcvalues_short_pair,symfunction_short_pair_list)
              endif 
            endif ! lshort
!!
!! write testinge.data
            if((lelec.and.(nn_type_elec.eq.1)).or.(lelec.and.(nn_type_elec.eq.5)))then
              call writeatomicsymfunctions(tymeunit,i1,&
                maxnum_funcvalues_elec,num_funcvalues_elec,&
                symfunction_elec_list)
            endif ! lelec
!!
!!=============================================================================
!! write teststruct.data
!!=============================================================================
!! CHANGE ANDI: GFORTRAN: gfortran has different default width for logicals (1 instead of 2 for ifort),
!!                        this results in a missing space between i8 and l -> problem when reading file,
!!                        so I inserted an additional space. 
           !write(teststructunit,'(i8,l)')numtest+1,lperiodic_list(i1)
            write(teststructunit,'(i8,tr1,l)')numtest+1,lperiodic_list(i1)
!! END CHANGE

            if(lperiodic_list(i1))then
              do i2=1,3
                write(teststructunit,'(3f20.14)')(lattice_list(i2,i3,i1),i3=1,3)
              enddo
            endif ! lperiodic
            if(latom_spin_list(i1))then
              if(lelec.and.lshort)then !! KK: for the second NN of 3G and 4G
                  do i2=1,num_atoms_list(i1)
                    write(teststructunit,'(i3,8(x,f15.10))')zelem_list(i1,i2),&
                      (xyzstruct_list(i3,i2,i1),i3=1,3),nnatomcharge_list(i1,i2),&
                      atomspin_list(i1,i2),(shortforce_list(i3,i2,i1),i3=1,3)
                  enddo ! i2
              else
                  do i2=1,num_atoms_list(i1)
                    write(teststructunit,'(i3,8(x,f15.10))')zelem_list(i1,i2),&
                      (xyzstruct_list(i3,i2,i1),i3=1,3),atomcharge_list(i1,i2),&
                      atomspin_list(i1,i2),(shortforce_list(i3,i2,i1),i3=1,3)
                  enddo ! i2
              endif
            else
              if(lelec.and.lshort)then !! KK: for the second NN of 3G and 4G
                  do i2=1,num_atoms_list(i1)
                    write(teststructunit,'(i3,8(x,f15.10))')zelem_list(i1,i2),&
                      (xyzstruct_list(i3,i2,i1),i3=1,3),nnatomcharge_list(i1,i2),&
                      atomenergy_list(i1,i2),(shortforce_list(i3,i2,i1),i3=1,3)
                  enddo ! i2
              else
                  do i2=1,num_atoms_list(i1)
                    write(teststructunit,'(i3,8(x,f15.10))')zelem_list(i1,i2),&
                      (xyzstruct_list(i3,i2,i1),i3=1,3),atomcharge_list(i1,i2),&
                      atomenergy_list(i1,i2),(shortforce_list(i3,i2,i1),i3=1,3)
                  enddo ! i2
              endif
            endif
!!
            if(luseforces)then
!!
!! write testforces.data
              if(lshort)then
                write(testfunit,'(i8)')numtest+1
                do i2=1,num_atoms_list(i1)
                  write(testfunit,'(3f15.10)')(shortforce_list(i3,i2,i1),i3=1,3)
                enddo ! i2
              endif ! lshort
!!
!! write testforcese.data
!! FOR FITTING WE DO NOT NEED THIS FILE
              if(lelec.and.(.not.lshort))then
                write(testfeunit,'(i8)')numtest+1
                do i2=1,num_atoms_list(i1)
                  write(testfeunit,'(3f15.10)')(elecforce_list(i3,i2,i1),i3=1,3)
                enddo ! i2
              endif ! lelec
            endif ! luseforces
!!
            numtest=numtest+1
            write(ounit,*)pointnumber,' Point is used for testing ',numtest
          endif ! random
!!
        else ! point is rejected because of lfitethres, lfitfthres or lrmin
!!
          numrej=numrej+1
          if(.not.lrmin(i1))then
            write(ounit,*)pointnumber,' Point is rejected (too short bond) ',numrej
          elseif(lfitethres.and.(totalenergy_list(i1).gt.(num_atoms_list(i1)*fitethres)))then
            write(ounit,*)pointnumber,' Point is rejected (high E) ',numrej
          elseif(lfitfthres.and.(.not.lforceok))then !'
            write(ounit,*)pointnumber,' Point is rejected (high F) ',&
              numrej,highfcomponent
          else
            write(ounit,*)'ERROR: point rejected for unknown reason ',numrej
            stop
          endif
!!
        endif ! lfitethres
!!
      enddo ! i1 loop over all structures
!!
      if(nn_type_short.eq.2)then
        deallocate(pairs_charge_list) 
        deallocate(pairs_charge)             
      endif
!!
      
      return
!!
      end
