!######################################################################
! This routine is part of
! RuNNer - RuNNer Neural Network Energy Representation
! (c) 2008-2024 Prof. Dr. Joerg Behler 
! Ruhr-Universitaet Bochum, Germany
!
! This program is free software: you can redistribute it and/or modify it 
! under the terms of the GNU General Public License as published by the 
! Free Software Foundation, either version 3 of the License, or 
! (at your option) any later version.
!
! This program is distributed in the hope that it will be useful, but 
! WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
! or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
! for more details.
!
! You should have received a copy of the GNU General Public License along 
! with this program. If not, see http://www.gnu.org/licenses.
! WARNING: THIS FILE IS PART OF THE UNMAINTAINED CONDA PACKAGE OF RUNNER 1.3. 
!######################################################################

!!
!! This is a dummy routine for old random number generators in pre-release versions of RuNNer
!!
      function ran0(seed)
      integer seed
      real*8 ran0
        write(*,*)'ERROR ran0'
        stop
      ran0=0
      return
      end
!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!
      function ran1(seed)
      integer seed
      real*8 ran1
        write(*,*)'ERROR ran1'
        stop
      ran1=0
      return
      end
!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!
      function ran2(seed)
      integer seed
      real*8 ran2
        write(*,*)'ERROR ran2'
        stop
      ran2=0
      return
      end
!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!
      function ran3(seed)
      integer seed
      real*8 ran3
        write(*,*)'ERROR ran3'
        stop
      ran3=0
      return
      end
