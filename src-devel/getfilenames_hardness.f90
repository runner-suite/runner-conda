!######################################################################
! This routine is part of
! RuNNer - RuNNer Neural Network Energy Representation
! (c) 2008-2024 Prof. Dr. Joerg Behler 
! Ruhr-Universitaet Bochum, Germany
!
! This program is free software: you can redistribute it and/or modify it 
! under the terms of the GNU General Public License as published by the 
! Free Software Foundation, either version 3 of the License, or 
! (at your option) any later version.
!
! This program is distributed in the hope that it will be useful, but 
! WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
! or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
! for more details.
!
! You should have received a copy of the GNU General Public License along 
! with this program. If not, see http://www.gnu.org/licenses.
! WARNING: THIS FILE IS PART OF THE UNMAINTAINED CONDA PACKAGE OF RUNNER 1.3. 
!######################################################################
!! called by:
!!
      subroutine getfilenames_hardness()
!!
      use globaloptions
      use fittingoptions
!!
      implicit none
!!
      integer icount
      integer i1,i2
!!
      character*23 filenametemp                              ! internal
!!
      filenameah(:)          ='000000.hardness.000.out'
      do i1=1,nelem
!! electrostatic weights
        filenametemp=filenameah(i1)
        if(nucelem(i1).gt.99)then
          write(filenametemp(17:19),'(i3)')nucelem(i1)
        elseif(nucelem(i1).gt.9)then
          write(filenametemp(18:19),'(i2)')nucelem(i1)
        else
          write(filenametemp(19:19),'(i1)')nucelem(i1)
        endif
        filenameah(i1)=filenametemp
      enddo
!!
      return
      end
