!######################################################################
! This routine is part of
! RuNNer - RuNNer Neural Network Energy Representation
! (c) 2008-2024 Prof. Dr. Joerg Behler 
! Ruhr-Universitaet Bochum, Germany
!
! This program is free software: you can redistribute it and/or modify it 
! under the terms of the GNU General Public License as published by the 
! Free Software Foundation, either version 3 of the License, or 
! (at your option) any later version.
!
! This program is distributed in the hope that it will be useful, but 
! WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
! or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
! for more details.
!
! You should have received a copy of the GNU General Public License along 
! with this program. If not, see http://www.gnu.org/licenses.
! WARNING: THIS FILE IS PART OF THE UNMAINTAINED CONDA PACKAGE OF RUNNER 1.3. 
!######################################################################
!! called by: 
!! - predict.f90
!!
      subroutine predictionelectrostatic_type5(&
        num_atoms,zelem,minvalue_elec,maxvalue_elec,avvalue_elec,&
        lattice,xyzstruct,atomspin,nnatomcharge,totalcharge,&
        chargemin,chargemax,nnelecenergy,nnelecforce,&
        nnstress_elec,sense,lperiodic)
!!
      use globaloptions
      use fileunits
      use predictionoptions
      use nnewald
      use timings
!!
      implicit none
!!
      integer zelem(max_num_atoms)                                      ! in
      integer num_atoms                                                 ! in
!!
      real*8 minvalue_elec(nelem,maxnum_funcvalues_elec)                ! in
      real*8 maxvalue_elec(nelem,maxnum_funcvalues_elec)                ! in
      real*8 avvalue_elec(nelem,maxnum_funcvalues_elec)                 ! in
      real*8 chargemin(nelem)                                           ! in
      real*8 chargemax(nelem)                                           ! in
      real*8 nnatomcharge(num_atoms)                                    ! out 
      real*8 nnelecenergy                                               ! out
      real*8 xyzstruct(3,num_atoms)                                     ! in
      real*8 atomspin(num_atoms)                                        ! in
      real*8 nnelecforce(3,num_atoms)                                   ! out
      real*8 totalcharge                                                ! in         
      real*8 lattice(3,3)                                               ! in      
      real*8 nnstress_elec(3,3)                                         ! out
      real*8 sense(nelem,maxnum_funcvalues_elec)                        ! out
!! 
      logical lperiodic
!!
!!======================================================================
!! Initialzations
!!======================================================================
!! initializations electrostatic part
      nnatomcharge(:)        = 0.0d0
      nnelecenergy           = 0.0d0
      nnelecforce(:,:)       = 0.0d0
      nnstress_elec(:,:)     = 0.0d0
!! initialization of timings
      timeelec               = 0.0d0
!! initialization of sensitivities
      sense(:,:)             = 0.0d0
!!
!!======================================================================
!!======================================================================
!! Start electrostatic part
!!======================================================================
!!======================================================================
      if(lfinetime)then
        dayelec=0
        call abstime(timeelecstart,dayelec)
      endif ! lfinetime
!!
      call predictelec_type5(&
        num_atoms,zelem,lattice,xyzstruct,atomspin,&
        minvalue_elec,maxvalue_elec,avvalue_elec,chargemin,chargemax,&
        nnelecenergy,nnatomcharge,totalcharge,nnelecforce,&
        nnstress_elec,sense,lperiodic)
!!
      if(lfinetime)then
        call abstime(timeelecend,dayelec)
        timeelec=timeelec+timeelecend-timeelecstart
      endif ! lfinetime
!!======================================================================
!!======================================================================
!! End Electrostatic part 
!!======================================================================
!!======================================================================
      return
      end
