!######################################################################
! This routine is part of
! RuNNer - RuNNer Neural Network Energy Representation
! (c) 2008-2024 Prof. Dr. Joerg Behler 
! Ruhr-Universitaet Bochum, Germany
!
! This program is free software: you can redistribute it and/or modify it 
! under the terms of the GNU General Public License as published by the 
! Free Software Foundation, either version 3 of the License, or 
! (at your option) any later version.
!
! This program is distributed in the hope that it will be useful, but 
! WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
! or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
! for more details.
!
! You should have received a copy of the GNU General Public License along 
! with this program. If not, see http://www.gnu.org/licenses.
! WARNING: THIS FILE IS PART OF THE UNMAINTAINED CONDA PACKAGE OF RUNNER 1.3. 
!######################################################################
!! called by:
!! - readinput.f90
!!
      subroutine readelementlayersatomic(maxnodes_local,&
        maxnum_layers_local,num_layers_local,nodes_local,actfunc_local)
!!
      use fileunits
      use globaloptions
!!
      implicit none
!!
      integer i1
      integer ztemp                                     ! internal
      integer maxnum_layers_local                       ! in 
      integer num_layers_local(nelem)                   ! out
      integer nodes_local(maxnum_layers_local,nelem)    ! out
      integer maxnodes_local                            ! in
!!
      character*40 dummy                                ! internal
      character*2 elementtemp                           ! internal
      character*1 actfunc_local(maxnodes_local,maxnum_layers_local,nelem) ! in/out

!!
      backspace(nnunit)
      read(nnunit,*,ERR=99)dummy,elementtemp
      call checkelement(elementtemp)
      call nuccharge(elementtemp,ztemp)
      backspace(nnunit)
      read(nnunit,*,ERR=99)dummy,elementtemp,num_layers_local(elementindex(ztemp))
      num_layers_local(elementindex(ztemp))=num_layers_local(elementindex(ztemp))+1
      if(num_layers_local(elementindex(ztemp)).gt.maxnum_layers_local)then
        write(ounit,*)'Error: element ',ztemp,' has too many hidden layers'
        stop !'
      endif
!! set number of nodes in new output layer to 1
      nodes_local(num_layers_local(elementindex(ztemp)),elementindex(ztemp))=1
!! delete activation functions for other output nodes
      do i1=2,maxnodes_local
        actfunc_local(i1,num_layers_local(elementindex(ztemp)),elementindex(ztemp))=' '
      enddo
!!
      return
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!
 99   continue
      write(ounit,*)'Error: keyword ',dummy
      write(ounit,*)'is missing arguments '
      stop

      end
