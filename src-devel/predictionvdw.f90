!###############################################################################
! This routine is part of
! RuNNer - RuNNer Neural Network Energy Representation
! (c) 2008-2024 Prof. Dr. Joerg Behler
! Ruhr-Universitaet Bochum, Germany
!
! This program is free software: you can redistribute it and/or modify it
! under the terms of the GNU General Public License as published by the
! Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! This program is distributed in the hope that it will be useful, but
! WITHout ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
! or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
! for more details.
!
! You should have received a copy of the GNU General Public License along
! with this program. If not, see http://www.gnu.org/licenses.
! WARNING: THIS FILE IS PART OF THE UNMAINTAINED CONDA PACKAGE OF RUNNER 1.3.
!###############################################################################

subroutine predictionvdw(                                                      &
  num_atoms, zelem,                                                            &
  lattice, xyzstruct, lperiodic,                                               &
  vdwenergy, atomvdw, vdwforces, vdwstress                                     &
)

!----------Module imports-------------------------------------------------------
  use globaloptions
  use timings
  
  implicit none

!-------------------------------------------------------------------------------
! Description:
!   This routine initiates the dispersion interaction calculation and provides
!   the timing framework. 
!
! Method:
!   - Calls predictvdw routine.
!
! Current Code Maintainer: Alexander Knoll
!
! Last revision: 22.09.2020
!
! Code placement in the RuNNer suite:
!   - Called by: predict.f90
!-------------------------------------------------------------------------------

! Subroutine arguments.
  integer, intent(in   )                                 :: num_atoms
  integer, intent(in   ), dimension(max_num_atoms)       :: zelem 
  real*8,  intent(in   ), dimension(3, 3)                :: lattice
  real*8,  intent(in   ), dimension(3, max_num_atoms)    :: xyzstruct   
  logical, intent(in   )                                 :: lperiodic 
  
  ! Total dispersion energy of the structure.
  real*8,  intent(inout)                                 :: vdwenergy
  ! Total vdw energy of each atom in the structure.
  real*8,  intent(inout), dimension(max_num_atoms)       :: atomvdw
  ! Total vdw force components acting on each atom in the structure.
  real*8,  intent(inout), dimension(3, max_num_atoms)    :: vdwforces  
  ! Total vdw stress components.
  real*8,  intent(inout), dimension(3, 3)                :: vdwstress

!----------Initializations------------------------------------------------------

  ! Initializations for timings.
  timevdw = 0.0d0

!----------Main dispersion routine calls----------------------------------------

  ! Start timer.
  if (lfinetime) then
    dayelec=0
    call abstime(timevdwstart, dayvdw)
  endif 

  ! Call vdw prediction routine.
  call distribute_vdw_calculation(                                             &
    num_atoms, zelem,                                                          &
    lattice, xyzstruct, lperiodic,                                             &
    vdwenergy, atomvdw, vdwforces, vdwstress                                   &
  )

  ! Take time.
  if (lfinetime) then
    call abstime(timevdwend, dayvdw)
    timevdw = timevdw + timevdwend - timevdwstart
  endif

  return
end subroutine predictionvdw
