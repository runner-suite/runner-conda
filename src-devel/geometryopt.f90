    subroutine geometryopt(num_atoms,num_atoms_element,zelem,&
               symbol,lattice,totalQ,lperiodic,&
               minvalue,maxvalue,avvalue,emin,emax,&
               Eold,Enew,F,xyz_old,xyz_new,atomspin,Qnew)
    
    use globaloptions
    use fileunits
    use nnflags
    use predictionoptions
    use energyAndForces3G
    use energyAndForces4G

    implicit none

    integer :: num_atoms
    integer :: num_atoms_element(nelem)
    integer :: zelem(num_atoms),i1,i2
    integer zelem_constraint 
    
    real*8,intent(in) :: totalQ
    real*8,intent(in) :: lattice(3,3)
    real*8 Eold
    real*8,intent(in) :: minvalue(nelem,maxnum_funcvalues_short_atomic)
    real*8,intent(in) :: maxvalue(nelem,maxnum_funcvalues_short_atomic)
    real*8,intent(in) :: avvalue(nelem,maxnum_funcvalues_short_atomic)
    real*8 Enew
    real*8,intent(inout) :: F(3,num_atoms)
    real*8,intent(in) :: xyz_old(3,num_atoms)
    real*8,intent(out) :: xyz_new(3,num_atoms)
    real*8,intent(in) :: atomspin(num_atoms)
    real*8,intent(out) :: Qnew(num_atoms)
    real*8 max_force_component 
    real*8 max_force_component_temp
    real*8 max_force_component_temp2
!!  for old method
    real*8 sens(nelem,maxnum_funcvalues_short_atomic)
    real*8 atomenergysum
    real*8 stress(3,3)
    real*8,intent(in) :: emin
    real*8,intent(in) :: emax
    real*8 nnatomenergy(num_atoms)
    real*8 nnshortenergy
    logical lperiodic
    character*2 symbol(num_atoms)
    !! initialization
    Qnew(:)=0.0d0
    xyz_new(:,:)=0.0d0
    nnatomenergy(:)=0.0d0
    nnshortenergy=0.0d0
    atomenergysum=0.0d0
    Enew=0.0d0
    i2 =0
    i1 =0
    zelem_constraint=0
    stress(:,:)=0.0d0
    max_force_component_temp=-999.9d0
    max_force_component_temp2=-999.9d0
    !! check if it is constraint relaxation
    if (luserelaxconstraint)then
           call nuccharge(constraint_element,zelem_constraint)
    endif
    !! Geo optimizations by simple gradient descent

    ! Get first energy and forces
    xyz_new(:,:num_atoms) = xyz_old(:,:num_atoms)
    if (nn_type_short.eq.3) then
        call get_energies_and_forces(num_atoms,zelem,lattice,&
             xyz_new,atomspin,totalQ,lperiodic,minvalue,maxvalue,avvalue,&
             Qnew,Enew,F)
    elseif(nn_type_short.eq.1)then
        if(nn_type_elec.eq.1)then
            call get_energies_and_forces_for_3G(num_atoms,zelem,lattice,&
                 xyz_new,atomspin,totalQ,lperiodic,minvalue,maxvalue,avvalue,&
                 Qnew,Enew,F,enforcetotcharge,totalQ,ldoforces)
        else
            call predictionshortatomic(num_atoms,num_atoms_element,zelem,&
                 lattice,xyz_new,atomspin,minvalue,maxvalue,avvalue,emin,emax,&
                 Enew,F,nnatomenergy,nnshortenergy,&
                 stress,atomenergysum,sens,lperiodic)
            if(lremoveatomenergies)then
                call addatoms(num_atoms,&
                     zelem,num_atoms_element,&
                     atomenergysum,nnatomenergy)
                Enew=Enew+atomenergysum
            endif ! lremoveatomenergies
        endif
    endif
    max_force_component = maxval(abs(F(:,:)))
    Eold = huge(Eold)
    ! write the energy and forces for initial structure
    write(outunit,'(a35,i10)')'Geometry during the relaxation step',i2
    write(outunit,'(a5)')'begin'
    if(lperiodic)then
       write(outunit,'(a7,3e20.13)') 'lattice  ',(lattice(1,i1),i1=1,3)
       write(outunit,'(a7,3e20.13)') 'lattice  ',(lattice(2,i1),i1=1,3)
       write(outunit,'(a7,3e20.13)') 'lattice  ',(lattice(3,i1),i1=1,3)
    endif
    do i1 =1,num_atoms
       write(outunit,*)'atom ',xyz_new(:,i1),symbol(i1),Qnew(i1),'0.0',F(:,i1)
    enddo
    write(outunit,'(a7,e20.13)') 'energy ',Enew
    write(outunit,'(a7,e20.13)') 'charge ',sum(Qnew(:))
    write(outunit,'(a3)')'end'
    do while((abs(Enew-Eold).gt.threshold_E).or.(max_force_component.gt.threshold_F))
        i2 = i2 +1
        if ((stepsize*stepsize).gt.(max_displacement/max_force_component)) then !!  check maximum displacement during geometry optimizations
            stepsize = sqrt(max_displacement /max_force_component)
        endif

        ! update atomic positions
        if (luserelaxconstraint)then
           do i1=1,num_atoms
                if(zelem(i1).eq.zelem_constraint)then
                    xyz_new(:,i1)=xyz_new(:,i1)+F(:,i1)*stepsize
                endif
           enddo  
        else
            xyz_new(:,:)=xyz_new(:,:)+F(:,:)*stepsize
        endif
        Eold=Enew

        ! get energy and forces
        if (nn_type_short.eq.3) then
            call get_energies_and_forces(num_atoms,zelem,lattice,&
                 xyz_new,atomspin,totalQ,lperiodic,minvalue,maxvalue,avvalue,&
                 Qnew,Enew,F)
        elseif(nn_type_short.eq.1)then 
          if(nn_type_elec.eq.1)then
            call get_energies_and_forces_for_3G(num_atoms,zelem,lattice,&
                 xyz_new,atomspin,totalQ,lperiodic,minvalue,maxvalue,avvalue,&
                 Qnew,Enew,F,enforcetotcharge,totalQ,ldoforces)
          else    
            call predictionshortatomic(num_atoms,num_atoms_element,zelem,&
                 lattice,xyz_new,atomspin,minvalue,maxvalue,avvalue,emin,emax,&
                 Enew,F,nnatomenergy,nnshortenergy,&
                 stress,atomenergysum,sens,lperiodic)
            if(lremoveatomenergies)then
               call addatoms(num_atoms,&
                zelem,num_atoms_element,&
                atomenergysum,nnatomenergy)
               Enew=Enew+atomenergysum
            endif ! lremoveatomenergies
          endif
        endif

        ! use this to test finite differences of forces
        !print*, sum(F(:,:num_atoms)**2 * stepsize), Enew-Eold
        ! update stepsize during descent
        if(Enew <= Eold)then
            stepsize=stepsize*1.2d0
        else
            stepsize=stepsize*0.5d0
        endif

        if (luserelaxconstraint)then
            max_force_component = 0.d0
            do i1=1,num_atoms
                if(zelem(i1).eq.zelem_constraint)then
                    max_force_component_temp = maxval(abs(F(:,i1)))
                    max_force_component = max(max_force_component_temp, max_force_component)
                endif
            enddo
        else
            max_force_component = maxval(abs(F(:,:)))
        endif

        print*, Enew, Enew-Eold, max_force_component

        write(outunit,'(a35,i10)')'Geometry during the relaxation step',i2
        write(outunit,'(a5)')'begin'
        if(lperiodic)then
           write(outunit,'(a7,3e20.13)') 'lattice  ',(lattice(1,i1),i1=1,3)
           write(outunit,'(a7,3e20.13)') 'lattice  ',(lattice(2,i1),i1=1,3)
           write(outunit,'(a7,3e20.13)') 'lattice  ',(lattice(3,i1),i1=1,3)
        endif
        do i1 =1,num_atoms
           write(outunit,*)'atom ',xyz_new(:,i1),symbol(i1),Qnew(i1),'0.0',F(:,i1)
        enddo
        write(outunit,'(a7,e20.13)') 'energy ',Enew
        write(outunit,'(a7,e20.13)') 'charge ',sum(Qnew(:))
        write(outunit,'(a3)')'end'
    enddo
    write(ounit,*)'-------------------------------------------------------------'
    write(ounit,*)'Geometry optimization is completed and the optimized geometry is written in geometry_opt.data'
    open(geooptunit,file='geometry_opt.data')  
    write(geooptunit,'(a5)')'begin' 
    if(lperiodic)then
        write(geooptunit,'(a7,3e20.13)')'lattice  ',(lattice(1,i1),i1=1,3)
        write(geooptunit,'(a7,3e20.13)')'lattice  ',(lattice(2,i1),i1=1,3)
        write(geooptunit,'(a7,3e20.13)')'lattice  ',(lattice(3,i1),i1=1,3)
    endif
    do i1=1,num_atoms
        write(geooptunit,*)&
            'atom ', xyz_new(:,i1),symbol(i1),Qnew(i1),'0.0',F(:,i1)
    enddo
    write(geooptunit,'(a7,e20.13)')'energy ',Enew
    write(geooptunit,'(a7,e20.13)')'charge ',sum(Qnew(:))
    write(geooptunit,'(a3)')'end'
    close(geooptunit)
    end subroutine
