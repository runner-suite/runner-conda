!######################################################################
! This routine is part of
! RuNNer - RuNNer Neural Network Energy Representation
! (c) 2008-2024 Prof. Dr. Joerg Behler
! Ruhr-Universitaet Bochum, Germany
!
! This program is free software: you can redistribute it and/or modify it
! under the terms of the GNU General Public License as published by the
! Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! This program is distributed in the hope that it will be useful, but
! WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
! or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
! for more details.
!
! You should have received a copy of the GNU General Public License along
! with this program. If not, see http://www.gnu.org/licenses.
! WARNING: THIS FILE IS PART OF THE UNMAINTAINED CONDA PACKAGE OF RUNNER 1.3.
!######################################################################
!! called by:
!! - readinput.f90
!!
      subroutine read_vdw_radius_type_1()
!!
      use fileunits
      use globaloptions
      use predictionoptions
!!
      implicit none
!!
      integer i1
      integer ztemp1
      logical stop_flag
!!
      character*2 elementtemp1                           ! internal
      character*40 dummy                                 ! internal
      character*2 elementsymbol1(nelem)                  ! internal
      character*40 keyword                               ! internal

      logical lfound(nelem)                              ! internal
!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      lfound(:)=.false.

!! Open input.nn and read in all lines with keyword 'vdw_radius'.

      open(nnunit,file='input.nn',form='formatted',status='old')
        rewind(nnunit)
31      continue

!! Search for keyword vdw_radius.
        read(nnunit,*,END=32)keyword
        if((keyword.eq.'vdw_radius').or.(keyword.eq.'vdW_radius'))then
          backspace(nnunit)

!! Read in element symbol and store its nuclear charge.
          read(nnunit,*,err=99)dummy,elementtemp1
          call nuccharge(elementtemp1,ztemp1)

!! Check if this element is part of the nucelem list, i.e.
!! if it was specified as being part of the system.
          do i1=1,nelem
            if(ztemp1.eq.nucelem(i1))then
!! If the value of vdw_radius(i1) is not 0.0d0,
!! this radius has been read before, stop, because this is a user error.
              if(vdw_radius(i1).ne.0.0d0)then
                write(ounit,*) &
                  '### ERROR ### vdw_radius for atom ', &
                  elementtemp1, &
                  ' has been specified twice. ', vdw_radius(i1)
                stop

!! If the atom is in the system and no warning is prompted,
!! read in the value of the atom radius.
              else
                backspace(nnunit)
                read(nnunit,*,err=99) &
                  dummy, &
                  elementsymbol1(i1), &
                  vdw_radius(i1)
                lfound(i1)=.true.

                goto 31
              endif
            endif
          enddo
!! If one element of the pair is not in nucelem, ignore it.
          write(ounit,*) &
            '### WARNING ### vdw_radius for atom ', &
            elementtemp1, &
            ' is ignored.'
        endif
        goto 31
32      continue
      close(nnunit)

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!check if all element pairs have been found

!! Assume everything is okay, therefore set stop_flag to false.
      stop_flag = .false.

      do i1=1,nelem
        if (.not.lfound(i1))then
          write(ounit,*) &
            '### ERROR ### vdw_radius for atom ', &
            nucelem(i1), &
            ' not specified.'
          stop_flag = .true.
        endif
      enddo

      ! If any energy is missing, stop.
      if (stop_flag.eqv..true.)then
        write(ounit,*)'RuNNer terminated because of incomplete vdW radii list'
        stop
      endif

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!write reference energies to runner.out
      write(ounit,*)'-------------------------------------------------------------'
      write(ounit,*)'vdw_radii read from input.nn:'
      do i1=1,nelem
        write(ounit,'(a1,a2,x,f18.8)') &
          ' ', &
          elementsymbol1(i1),&
          vdw_radius(i1)
      enddo
      write(ounit,*)'-------------------------------------------------------------'
!!
      return
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!! If the number of keyword arguments is wrong, print error and stop.
99  continue
    write(ounit,*)'Error: keyword ',keyword
    write(ounit,*)'is missing arguments '
    stop
    end
