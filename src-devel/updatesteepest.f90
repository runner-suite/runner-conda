!######################################################################
! This routine is part of
! RuNNer - RuNNer Neural Network Energy Representation
! (c) 2008-2024 Prof. Dr. Joerg Behler 
! Ruhr-Universitaet Bochum, Germany
!
! This program is free software: you can redistribute it and/or modify it 
! under the terms of the GNU General Public License as published by the 
! Free Software Foundation, either version 3 of the License, or 
! (at your option) any later version.
!
! This program is distributed in the hope that it will be useful, but 
! WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
! or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
! for more details.
!
! You should have received a copy of the GNU General Public License along 
! with this program. If not, see http://www.gnu.org/licenses.
! WARNING: THIS FILE IS PART OF THE UNMAINTAINED CONDA PACKAGE OF RUNNER 1.3. 
!######################################################################
!! called by:
!! - optimize_short.f90 (1x)
!! - optimize_ewald.f90 (3x)
!! - fitforcesshort.f90
!!
      subroutine updatesteepest(num_weights,weights,dedw,error,step)
!!
      use fileunits
!!    DON'T use fittingoptions here, because step corresponds to different variables
!!
      implicit none
!!
      integer num_weights
      integer i1
!!
      real*8 weights(num_weights)
      real*8 dedw(num_weights)
      real*8 error
      real*8 step
!!
!!
      do i1=1,num_weights
        weights(i1)=weights(i1)+step*error*dedw(i1)
!!        weights(i1)=weights(i1)+0.01d0*error*dedw(i1)
      enddo ! i1
!!
      return
      end
