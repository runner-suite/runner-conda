#!/usr/bin/env python3
# encoding: utf-8
"""Define regression tests for RuNNer."""

import os

import pytest
from pytest_regtest import RegTestFixture
from _pytest.fixtures import SubRequest

import numpy as np

from ase.data import chemical_symbols

from runnerase import Runner, read, read_runnerconfig


# Set precision of floating point number to 10^-8.
np.set_printoptions(precision=8)


@pytest.fixture(name='calc', scope='class', params=[
    ['examples/example01', 30],
    ['examples/example03_graphene', 30]
])
def fixture_calculator(
    request: SubRequest,
    executable: str
) -> Runner:
    """Configure a test system with fixed dataset and RuNNer options."""
    path, n_structures = request.param

    # Read the training dataset.
    dataset_path = os.path.join(path, 'input.data')
    dataset = read(dataset_path, index=f'0:{n_structures}', format='runner')

    # Create a calculator object using the ASE interface.
    command = f'{executable} > PREFIX.out'
    calc = Runner(dataset=dataset, command=command, directory=path)

    # Read the RuNNer settings.
    config_filenames = ['input.nn', 'input.nn-1']
    for configfile in config_filenames:
        config_path = os.path.join(path, configfile)
        if os.path.exists(config_path):
            calc.parameters = read_runnerconfig(config_path)

    return calc


class TestRegression:
    """Define functional tests for the runner calculator.

    These tests pass if they complete without an error.
    """

    seed = 42

    def test_mode1(self, calc: Runner, regtest: RegTestFixture) -> None:
        """Run RuNNer Mode 1 for a given test system."""
        # Prepare the calculator and run mode 1.
        calc.set(random_seed=self.seed)

        calc.directory += '/mode1'
        calc.run(mode=1)

        sfvalues = calc.results['sfvalues']
        splittraintest = calc.results['splittraintest']

        with regtest:

            print('Running: ', calc.label)
            print('Executable: ', calc.command)

            print_heading('Mode 1 Output', size=1)

            # Make sure the split between train and test set is the same.
            print_heading('Split between Training and Testing Set', size=2)
            print('Training', splittraintest.train)
            print('Testing', splittraintest.test)

            # Check the symmetry function values.
            print_heading('Symmetry Function Values', size=2)

            for idx_structure, structure in enumerate(sfvalues.data):
                print_heading(f'Structure {idx_structure}', size=3)

                for atomic_sfvalues in structure.by_atoms():
                    print(atomic_sfvalues)

    def test_mode2(self, calc: Runner, regtest: RegTestFixture) -> None:
        """Run RuNNer Mode 2 for a given test system."""
        # Prepare the calculator and run mode 2.
        calc.set(random_seed=self.seed)
        calc.set(epochs=5)

        calc.directory = calc.directory.replace('mode1', 'mode2')
        calc.prefix = 'mode2'
        calc.run(mode=2)

        fitresults = calc.results['fitresults']
        weights = calc.results['weights']
        scaling = calc.results['scaling']

        with regtest:

            print('Running: ', calc.label)
            print('Executable: ', calc.command)

            print_heading('Mode 2 Output', size=1)

            # Assert same RMSE values and best found epoch.
            print_heading('Results of the Fitting Process', size=2)
            print('Epochs:', fitresults.epochs)
            print('RMSE Energy Train:', fitresults.rmse_energy['train'])
            print('RMSE Energy Test:', fitresults.rmse_energy['test'])
            print('RMSE Forces Train:', fitresults.rmse_forces['train'])
            print('RMSE Forces Test:', fitresults.rmse_forces['test'])
            print('Best Fit in Epoch:', fitresults.opt_rmse_epoch)

            # Assert the same weights.
            print_heading('Atomic Neural Network Weights', size=2)
            for element in chemical_symbols:
                if element in weights.data:
                    print_heading(f'Element: {element}', size=3)
                    print(weights.data[element])

            # Assert same scaling.
            print_heading('Symmetry Function Scaling Data', size=2)
            for idx in np.arange(1, len(scaling.data) + 1, 1):
                print_heading(f'Element ID: {idx}', size=3)
                print(scaling.data[str(idx)])

    def test_mode3(self, calc: Runner, regtest: RegTestFixture) -> None:
        """Run RuNNer Mode 3 for a given test system."""
        # Prepare the calculator and run mode 3.
        calc.set(random_seed=self.seed)

        calc.directory = calc.directory.replace('mode2', 'mode3')
        calc.prefix = 'mode3'
        calc.run(mode=3)

        energy = calc.results['energy']
        forces = calc.results['forces']

        with regtest:

            print('Running: ', calc.label)
            print('Executable: ', calc.command)

            print_heading('Mode 3 Output', size=1)

            # Assert same total energies.
            print_heading('Total Energy', size=2)
            print(energy)

            # Assert same forces.
            print_heading('Atomic Forces', size=2)
            for idx_structure, structure_forces in enumerate(forces):
                print_heading(f'Structure {idx_structure}', size=3)
                print(structure_forces)


def print_heading(text: str, size: int = 1) -> None:
    """Print section headings to prettify regtest standard output."""
    if size == 1:
        prematter = '=' * 20
        postmatter = '=' * 20
        message = f'\n{prematter} {text} {postmatter}'
        print(message)

    elif size == 2:
        message = f'\n{text}'
        underline = '-' * len(text)
        print(message)
        print(underline)

    else:
        message = f'\n{text}'
        print(message)
