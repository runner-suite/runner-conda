#!/usr/bin/env python3
# encoding: utf-8
"""Test setup and fixture definitions for RuNNer regression tests."""

import sys
import pytest
from _pytest.fixtures import SubRequest
from _pytest.config.argparsing import Parser


def pytest_addoption(parser: Parser) -> None:
    """Add command line arguments to the pytest command."""
    parser.addoption("--executable", action="store")


def executable_fixture(request: SubRequest) -> str:
    """Read path to the compiled RuNNer executable(s) for all tests."""
    executable_value: str = request.param

    if executable_value is None:
        pytest.skip()

    yield executable_value


def pytest_sessionstart(session):
    """Create the fixtures with the correct params at session start."""
    executable = session.config.option.executable
    if executable is not None:
        executable = executable.split()
        name = "executable"
        func = pytest.fixture(scope='session', name=name,
                              params=executable)(executable_fixture)
        setattr(sys.modules[__name__], f'{name}_func', func)
