#!/usr/bin/env python3
# encoding: utf-8
"""Define regression tests for RuNNer."""

from typing import Tuple, Any, Dict, Iterable

import os

import pytest
from _pytest.fixtures import SubRequest
from pytest_benchmark.fixture import BenchmarkFixture

from runnerase import Runner, read, read_runnerconfig


@pytest.fixture(name='calc', scope='class',
                params=['examples/example01', 'examples/example03_graphene'])
def fixture_calculator(
    request: SubRequest,
    executable: str
) -> Runner:
    """Configure a test system with fixed dataset and RuNNer options."""
    path = request.param

    # Read the training dataset.
    dataset_path = os.path.join(path, 'input.data')
    dataset = read(dataset_path, index=':', format='runner')

    # Create a calculator object using the ASE interface.
    command = f'{executable} > PREFIX.out'
    calc = Runner(dataset=dataset, command=command, directory=path)

    # Read the RuNNer settings.
    config_filenames = ['input.nn', 'input.nn-1']
    for configfile in config_filenames:
        config_path = os.path.join(path, configfile)
        if os.path.exists(config_path):
            calc.parameters = read_runnerconfig(config_path)

    return calc


class TestBenchmark:
    """Define functional tests for the runner calculator.

    These tests pass if they complete without an error.
    """

    seed = 42

    def test_mode1(self, calc: Runner, benchmark: BenchmarkFixture) -> None:
        """Run RuNNer Mode 1 for a given test system."""
        def setup() -> Tuple[Iterable[Any], Dict[str, Any]]:
            if 'sfvalues' in calc.results:
                del calc.results['sfvalues']
            if 'splittraintest' in calc.results:
                del calc.results['splittraintest']

            calc.directory += '/mode1'
            calc.set(random_seed=self.seed)
            return [], {'mode': 1}

        # Benchmark Mode 1. The setup() function chooses the right mode.
        benchmark.pedantic(calc.run, setup=setup, rounds=3, warmup_rounds=1)

        # Store the results of Mode 1 for use in Mode 2.
        self.__class__.results = calc.results.copy()

    def test_mode2(self, calc: Runner, benchmark: BenchmarkFixture) -> None:
        """Run RuNNer Mode 2 for a given test system."""
        def setup() -> Tuple[Iterable[Any], Dict[str, Any]]:
            if 'fitresults' in calc.results:
                del calc.results['fitresults']
            if 'weights' in calc.results:
                del calc.results['weights']
            if 'scaling' in calc.results:
                del calc.results['scaling']

            calc.directory = calc.directory.replace('mode1', 'mode2')
            calc.prefix = 'mode2'
            calc.set(random_seed=self.seed)
            calc.set(epochs=30)
            return [], {'mode': 2}

        # Benchmark Mode 2. The setup() function chooses the right mode.
        benchmark.pedantic(calc.run, setup=setup, rounds=3)

        # Store the results of Mode 2 for use in Mode 3.
        self.__class__.results = calc.results.copy()

    def test_mode3(self, calc: Runner, benchmark: BenchmarkFixture) -> None:
        """Run RuNNer Mode 3 for a given test system."""
        def setup() -> Tuple[Iterable[Any], Dict[str, Any]]:
            if 'energy' in calc.results:
                del calc.results['energy']
            if 'forces' in calc.results:
                del calc.results['forces']

            calc.directory = calc.directory.replace('mode2', 'mode3')
            calc.prefix = 'mode3'
            calc.set(random_seed=self.seed)
            return [], {'mode': 3}

        # Benchmark Mode 2. The setup() function chooses the right mode.
        benchmark.pedantic(calc.run, setup=setup, rounds=3)

        # Store the results of Mode 2 for use in Mode 3.
        self.__class__.results = calc.results.copy()
