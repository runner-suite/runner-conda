`RuNNer` has been written to a large extent by Jörg Behler. In late 2018 it has
been released under the GPL3.0 license. If you are interested in `RuNNer` 
please contact [Jörg Behler](mailto:joerg.behler@uni-goettingen.de). Some other
contributors to `RuNNer` are:

* _Jovan Jose Kochumannil Varghese_: part of the pair symmetry functions and
pair NNP approach.
* _Tobias Morawietz_: Implementation of the screening of the electrostatic
interactions at short distances and the Nguyen Widrow weights initialization.
* _Andreas Singraber_: Some bugfixes and a performance optimization of the
angular symmetry function type 3. Further Andi has written a C++ implementation
of the HDNNP method in LAMMPs, which is fully compatible with `RuNNer`.
* _Michael Gastegger and Philipp Marquetand_: Implementation of the element
decoupled Kalman filter.
* _Tsz Wai Ko (Kenko)_: Implementation of the noise matrix for the Kalman filter
and additional cutoff functions for compatibility with n2p2. Further Kenko has
reconstructed the electrostatic part of RuNNer for constructing 3G-HDNNP based on
fixed charge, environment dependent charges. In addition to this, Kenko has
implemented 4G-HDNNP model in RuNNer.
* _Jonas A. Finkler_: Implementation of the 4G-HDNNP and reconstruction of the
electrostatic part of RuNNer with Kenko. Also contributed the random number
generator 6 and the mergesort implementation. In addition, Jonas also
implemented OPENMP parallization in RuNNer to optimize the speed of calculating
symmetry functions.
* _Marco Eckhoff_: Implementation of spin-dependent atom-centered symmetry
functions to construct magnetic high-dimensional neural network potentials.
Further, Marco developed the RuNNerActiveLearn tool.

The author is grateful to all his past and present group members, collaborators
and users of `RuNNer` for discussions and feedback.
