When using `RuNNer`, please cite the following two references:

* [J. Behler, _Int. J. Quant. Chem._ **2015**, _115_, 1032](https://doi.org/10.1002/qua.24890).
* [J. Behler, _Angew. Chem. Int. Ed._ **2017**, _56_, 12828](https://doi.org/10.1002/anie.201703114).

The original reference for high-dimensional neural network potentials, which 
should **always be cited** irrespective of the used implementation, is

* [J. Behler and M. Parrinello, _Phys. Rev. Lett._ **2007**, _98_, 146401](https://doi.org/10.1103/PhysRevLett.98.146401).

When using **atom-centered symmetry functions**, please cite:

* [J. Behler, _J. Chem. Phys._ **2011**, _134_ 074106](https://doi.org/10.1063/1.3553717).

When using **3G-HDNNP**, please cite:

* [N. Artrith, T. Morawietz, and J. Behler, _Phys. Rev. B_ **2011**, _83_ 153101](https://doi.org/10.1103/PhysRevB.83.153101).

When using **4G-HDNNP**, please cite:

* [T. W. Ko, J. A. Finkler, S. Goedecker, J. Behler, _Nat. Comm._ **2021**, _12_, 398](https://doi.org/10.1038/s41467-020-20427-2).

When using **mHDNNP**, please cite:

* [M. Eckhoff, J. Behler, _npj Comput. Mater._ **2021**, _7_, 170](https://doi.org/10.1038/s41524-021-00636-z).

When using **pair symmetry functions**, please cite:

* [Jovan Jose K.V., N. Artrith, and J. Behler, _J. Chem. Phys._ **2012**, _136_ 194111](https://doi.org/https://doi.org/10.1063/1.4712397).

For the general use of neural network potentials in chemistry, physics and materials science the following reviews are recommended:

* [C. M. Handley and P. L. A. Popelier, _J. Phys. Chem. A_ **2010**, _114_ 3371](https://doi.org/10.1021/jp9105585).
* [J. Behler, _Phys. Chem. Chem. Phys._ **2011**, _13_ 17930](https://doi.org/10.1039/C1CP21668F).
* [J. Behler, _J. Chem. Phys._ **2016**, _145_ 170901](https://doi.org/10.1039/C1CP21668F).
