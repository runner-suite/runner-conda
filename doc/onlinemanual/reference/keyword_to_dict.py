#!/usr/bin/env python3
#!encoding: utf-8

import numpy as np
import re
import pprint

BRACKET_ARGUMENTS = ['[parameters]', '[element...]', '[type...]', '[i1...]' ]

def remove_links(strine):
    # Find links and make them shorter for pretty printing.
    strine1 = strine
    
    if '/#' in strine:
        if 'keywords' in line:
            match = re.search('/#(?P<keyword>\w+)', strine)
            keyword = match.group('keyword')
            strine = re.sub(r"\[`#!runner-config\s\w+`\]\(.+\)", keyword, strine)
        elif 'files' in line:
            match = re.search('\[`(?P<keyword>[\w\.]+)`\]', strine)
            keyword = match.group('keyword')
            strine = re.sub(r"\[`[\w\.]+`\](.+)", keyword, strine)
            
        else:
            print(f'Unrecognized link to be replaced {strine}.')
            exit()
    
        if strine == strine1:
            print(f'Problem in link replacement for {strine}.')
            exit()

    return strine

keywords = {}

read_modes = False
read_description = False
read_abstract = False
read_arguments = False
read_argdescription = False

modes = [False, False, False]
description = ''
skip_keyword = False

with open('keywords.md', 'r') as infile:
    for line in infile:
        
        # Jump over blank lines.
        if line.strip() == '':
            continue
        
        # Find a keyword block beginning.
        if line.startswith('###') and not line.startswith('####'):
            
            skip_keyword = False
            keyword = line.split()[1].strip('`')
            if keyword == 'weight_constraint' or keyword == 'weighte_constraint':
                skip_keyword = True
            
            # Reset all relevant keywords.
            modes = [False, False, False]
            description = ''
            
            # Modes will be read next.
            read_modes = True
            
        if not skip_keyword:
            
            # At beginning of abstract block, stop reading description.
            if '!!! Abstract' in line:
                read_abstract = True
                read_description = False
            
            # Code tags effectively mark the end of the Abstract block.
            if 'material-code-tags' in line:
                
                # # Pretty print the current keyword.
                # pp = pprint.PrettyPrinter(sort_dicts=False)
                # print('------------------------------', keyword, '------------------------------')
                # pp.pprint(keywords[keyword])
                
                # For non-logical keywords, which are the ones which have arguments,
                # reset everything and do checks.
                if read_arguments:
                    if any([True if i in arguments else False for i in BRACKET_ARGUMENTS]):
                        idx_args += 1
                        
                    if keyword == 'symfunction_short' or keyword == 'symfunction_electrostatic':
                        idx_args += 1
                        
                    if idx_args + 1 != len(arguments):
                        print(f'Not enough arguments for keyword {keyword}')
                        break

                    idx_args = -1
                    idx_argopts = -1
                    
                    read_arguments = False   
                    read_argdescription = False 
                    read_argoptions = False
            
            #---------- Read in the keyword description ---------------------------#    
            if read_description:
                strine = line.strip('\n')
                strine = remove_links(strine)

                if strine != "": 
                    description += ' ' + strine
                    
            #---------- Read in the RuNNer modes ----------------------------------#            
            if read_modes:
                if '**Mode 1:**' in line:
                    if 'yes' in line:
                        modes[0] = True
                elif '**Mode 2:**' in line:
                    if 'yes' in line:
                        modes[1] = True 
                elif '**Mode 3:**' in line:
                    if 'yes' in line:
                        modes[2] = True
                        
                    read_modes = False
                    read_description = True                

            #---------- Read the header line of the Abstract block ----------------#    
            if read_abstract:
                
                # Read the header line and save all information.
                if '!!! Abstract' in line:

                    # Find logicals, they are different from other keywords.
                    if 'logical' in line:
                        # Match keyword, type, and default value using regex.
                        match = re.findall(r'\w+`', line)
                        match  = [i.strip('`') for i in match]
                        format = re.search(r'runner-config [\w+\s]+', line)
                        format = format[0][14:]
                        
                        keywords[keyword] = {
                            'type': 'np.bool',
                            'description': description,
                            'format': format,
                            'modes': {
                                'mode1': modes[0],
                                'mode2': modes[1],
                                'mode3': modes[2],
                            },
                            # Performs logical comparison of string in match to 
                            # convert to Boolean type.
                            'default_value': match[2] == 'True'
                        }

                    # All other keywords have parameters.
                    else:
                        match = re.findall(r'runner-config [\[\w+\s\.\]]*', line)
                        format = match[0][14:]
                        match = match[0][14:].split()
                        
                        arguments = match[1:]   
                        idx_args = -1
                        
                        read_abstract = False
                        read_arguments = True                    

                    if match[0] != keyword:
                        print('Seems like there is a problem with the Abstract ' 
                        + f'keyword for {keyword}. Either not matching or wrong.')
                        break 
            
            #---------- Read the Abstract block arguments -------------------------#            
            if read_arguments:
                
                #---------- Read the argument description -------------------------#
                if read_argdescription:
                    strine = line.strip('\n').strip('    :')
                    strine = re.sub("\s{4,}", " ", strine)

                    strine = remove_links(strine)

                    if strine != "" and not read_argoptions: 
                        if not strine.strip().startswith('**`#!runner-config'):
                            keywords[keyword]['arguments'][argname]['description'] += ' ' + strine
                        
                    if '* **`' in line:      
                        read_argoptions = True
                        
                    if read_argoptions:
                        # Read in the option value.
                        if '* **`' in line:

                            optmatch = re.search('`[\w\-]+`', strine)
                            optname = optmatch.group(0).strip('`')
                            
                            argopt_dict = {'description': ''}
                            
                            try:
                                keywords[keyword]['arguments'][argname]['options'][optname] = argopt_dict

                            except KeyError:
                                keywords[keyword]['arguments'][argname]['options'] = {
                                    optname: argopt_dict
                                }
                    
                            idx_argopts += 1
                        # All other lines contain the description of the current 
                        # option.
                        else:
                            keywords[keyword]['arguments'][argname]['options'][optname]['description'] += ' ' + strine
                            
                #---------- Read the first line of an argument --------------------#            
                if '**`#!runner-config' in line:
                    
                    # For the special case of an argument with varying parameters,
                    # mostly symmetry functions, we do a different treatment.
                    if any([True if i in line else False for i in BRACKET_ARGUMENTS]):
                        match = re.findall(r'[\-\w\.]+\s[\-\[\w\.\]]+`', line)
                        match = [i.strip('`') for i in match]     

                    else:     
                        match = re.findall(r'[\-\w\.]+`', line)
                        match = [i.strip('`') for i in match]
                    
                    if match[1] == 'real':
                        ktype = 'np.float64'
                        if len(match) > 2:
                            
                            try:
                                kvalue = float(match[2])
                            except ValueError:
                                kvalue = None
                            
                        else: kvalue = None
                    
                    elif any([True if i in match[1] else False for i in ['integer', 'integer [integers]', 'integer [reals]']]):
                        ktype = 'np.integer'
                        if len(match) > 2:
                            try:
                                kvalue = int(match[2])
                            # if it is not a number, it is usually a link to 
                            # another keyword.
                            except ValueError:
                                kvalue = remove_links(match[2])
                        else: kvalue = None
                    
                    elif match[1] in ['string', 'character'] or match[1] == 'character [characters]':
                        ktype = 'np.str'
                        # For a string there is no default.
                        if len(match) > 2:
                            kvalue = match[2]
                        else: kvalue = None

                    else:
                        print('This argument type is unknown.')
                        break
                    
                    argname = match[0]
                    arg_dict = {
                        'description': "",
                        'type': ktype,
                        'default_value': kvalue
                    }
                    
                    try:
                        keywords[keyword]['arguments'][argname] = arg_dict
                    except KeyError:
                        keywords[keyword] = {
                            'description': description,
                            'format': format,
                            'modes': {
                                'mode1': modes[0],
                                'mode2': modes[1],
                                'mode3': modes[2],
                            },
                            'arguments': {
                                argname: arg_dict
                            }
                        }
                    
                    idx_args += 1
                    read_argdescription = True
                    read_argoptions = False
                    idx_argopts = -1

def print_nested_dict(dict_obj, indent = 0):
    ''' Pretty Print nested dictionary with given indent level  
    '''
    # Iterate over all key-value pairs of dictionary
    for key, value in dict_obj.items():
            
        ci = ' ' * indent
        # If value is dict type, then print nested dict 
        if isinstance(value, dict):
            print(f"{ci}'{key}': {'{'}")
            print_nested_dict(value, indent + 4)
            print(f"{ci}{'}'},")
            
        else:
                
            if isinstance(value, np.str) and 'np.' not in value:
                # +1 for whitespace.
                # prematter = len(f"{ci}'{key}': '''") + 1
                # if len(value) + prematter > 80:
                #     n = 80 - prematter
                #     chunks = [value[i:i+n] for i in range(0, len(value), n)]
                # 
                #     print(f"{ci}'{key}': '''{chunks[0]}")
                # 
                #     for chunk in chunks[1:-1]:
                #         print(f"{' ' * prematter}{chunk}")
                # 
                #     print(f"{' ' * prematter}{chunks[-1]}''',")
                # else:
                #     if 'np.' in value:
                #         print(f"{ci}'{key}': {value},")
                #     else:
                print(f"{ci}'{key}': \"{value}\",")
            else:
                print(f"{ci}'{key}': {value},")

def display_dict(dict_obj):
    ''' Pretty print nested dictionary
    '''
    print('import numpy as np')
    print('\n')
    
    print('RUNNERCONFIG_KEYWORDS = {')
    print_nested_dict(dict_obj, 4)
    print('}')
    
    print("print(RUNNERCONFIG_KEYWORDS['analyze_error_force_step'])")
    
display_dict(keywords)