
### I use only short range energies for the fit, but the RMSEs of the short range energies and the total energies are different. Shouldn't they be the same?
    
The RMSE of the total energy is always calculated using all training points. If
for the fitting the keywords `max_energy` is used, only points with a lower
energy are used for the fitting and the calculation of the RMSE of the 
short-range energy.

### For which systems can `RuNNer` be used?

### What is the required system size for the training structures?

In the training set structures with different numbers of atoms and also
periodic and non-periodic structures can be mixed as long as they are
numerically consistent (in particular the k-point meshes in case of
periodic structures need to be very well converged, Gamma point only
calculations are usually not sufficient). Ideally the largest structures
should have a diameter of at least two times the cutoff radius to avoid
artificial periodicity in the atomic environments. Atoms close to the
cutoff radius (but still inside) may, however, contribute only a little
to the symmetry function values and thus may not be so important. It is
therefore often not critical to use structures, which are somewhat
smaller in diameter than two times the cutoff radius.

### I get extrapolation warnings. What can I do?

Extraplation warnings in `runner_mode 3` indicate that one or more
symmetry function values of one or more atoms are outside the respective
interval(s) spanned by the training data. In this case, the potential
may not be reliable. Extrapolation warnings may also be used to search
for structures, which extend the range of values of the symmetry
functions. Be aware that the absence of extrapolation warnings does not
indicate that the training data is sufficiently dense close to the
structure of interest and further tests may be required (e.g. comparing
predictions of several independently constructed NNPs) to validate the
potential.

### How can I validate the NNP for systems being too large for DFT calculations?

There are several options:

1.   While the total system easily may become too large for a DFT
    calculation, the same local atomic environments often can often be
    represented in smaller systems, e.g. by cutting a cluster around the
    atom of interest. The forces acting on these atoms can be directly
    compared, but please be aware that formally the forces depend on the
    environment with twice the cutoff radius (see e.g. J. Behler, Angew.
    Chem. Int. Ed. 56 (2017) 12828.). For large cutoffs the true
    physical interaction range may be smaller of course.

2.  A very cheap method, which does not require additional electronic
    structure calculation for validation, is to compare the prediction
    of two or more NNPs (see e.g. Fig. 11 Angew. Chem. Int. Ed.
    56 (2017) 12828 or Fig. 3 Phys. Rev. B 85 (2012) 045439) regarding
    the predicted energies or better the atomic forces.

### In which cases does it make sense to use the mpi version of `RuNNer`?

Currently only `runner_mode` 3 is parallelized using mpi. So far we have
found the Kalman filter to be most efficient, and it shows the optimum
performance when updating the weights after each energy and force
component, which makes an efficient parallelization of the optimization
in `runner_mode 2` very difficult. Still there is a trivial openmp
parallelization of the symmetry function calculation, which can provide
a speed-up of a factor of 2 in mode 2 (See the section on compiling
`RuNNer` for details). It would in principle be possible to parallelize
`runner_mode `1 efficiently, but only a minor part of the CPU time is
spent here, which is why it has not been parallelized so far.

### I have the RMSE of the full training and test sets, but I want to analyze the error for specific subgroups of structures. Can this be done?

Yes, this is possible by restarting the fit with 0 epochs using the
optimized weight files for the full data set (cf. keyword
`use_old_weights`). Use then new data files generated using only the
subgroup of structures you are interested in. For generating these data
files you need to rerun `RuNNer` in `runner_mode` 1 for this subgroup
first. You need to set also the keyword `use_old_scaling` and read in
the original `scaling.data` file of the fit, because any change in the
training set would change the range of symmetry functions and modify the
fit you like to investigate. Please be aware that the `scaling.data`
file is part of the fit. In particular in case the subgroup of
structures contains less elements than the full training set, e.g. if
the fit has been constructed for a ternary system but you are interested
in a subgroup of binary structures, `RuNNer` would stop otherwise with
an error due to a zero range of some symmetry function values. Still,
also in case the chemical composition does not change, reading in the
old `scaling.data` is mandatory even if you do not get an error message.

### My fits die after epoch 0 without a message

Please try to adjust the stack size of your terminal session using

```bash
ulimit -s unlimited
```

### Can I construct a potential using only energies, because I have no reference forces?

Yes, this is possible. Just enter `0.0` for all force components in the
`input.data` file and make sure that in all modes the keyword
`use_short_forces` is commented. If it would be active, `RuNNer` would
assume that all forces are physically 0.0 and try to adjust the NN
weights accordingly, which must be avoided.

### The numbers are slightly different on a different platform, why?

In some cases we have observed that the combination
[rounding]{#rounding error problem} error problem ifort/mkl with high
optimization settings (O3) can result in slightly different numbers
depending on the version, in particular in `runner_mode 2`. RuNNer.To
exclude this case,it can be fixed by adding -fp-model precise in FFLAGS
from Makefile in RuNNer and run checks with lower optimization levels or
using other blas/lapack libraries (e.g. from netlib). The gfortran
compiler seems to be more robust in this respect.

### What is a good initial choice for the symmetry functions?

For a new system it is recommended to begin with symmetry functions of
type 2 and type 3. For type 2 for each element pair a set of 5-6 radial
functions with $R_s=0$ should be used. The first $\eta$ exponent should
be zero (corresponding to the maximum extension of the function). The
largest $\eta$ exponent should be chosen such that the turning point of
$e^{-\eta R_{ij}^2}\cdot f_c(R_{ij})$ is at the minimum interatomic
distance of that element pair present in the data set. The remaining
$\eta$ values should be in between such that equidistant turning points
are obtained. For the angular functions of type 3 use for each element
triple $\zeta=1,2,4,$ and $16$ in combination with both $\lambda=+1,-1$
and always $\eta=0$. All symmetry functions should have the same cutoff,
e.g. 6 Å. Starting from this first set, which should already provide a
reasonable potential, variations can be used (e.g. shifted radial
functions with $R_s\ne 0$ or a second set of contracted angular
functions with $\eta>0$).

### Can I combine periodic and non-periodic structures in a single training set?

This is possible. Note that a numerical consistency between both sets of
structures is of utmost importance, by e.g. using the same basis set.
Also considering k-points properly is important.