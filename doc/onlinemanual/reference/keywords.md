The following keywords can be used to control `RuNNer` by including them
in the `input.nn` file. Some keywords are mandatory and `RuNNer` will
stop with an error message if they are not specified. For other
keywords, if omitted, reasonable default values will be set by `RuNNer`.
In any case the specific settings can be found in the `RuNNer` output
file. In general the order of the keywords in the `input.nn` file is
arbitrary and dependencies will be handeled by `RuNNer` by searching the
`input.data` file in the right order. Several combinations of keywords
may result in contradictory instructions for `RuNNer` and cannot be
executed. During initialization `RuNNer` performs a set of checks to
identify such combinations and may either try to correct the settings
for the current run (the `input.nn` file is not modified!), in this case
a WARNING is issued and the used settings are written in the output
file. Please regularly check your output file for these warnings. In
case of unresolvable conflicts `RuNNer` may stop with an ERROR message.
Due to the large number of possible combinations of keywords there is,
however, a certain probability that some potentially confliciting
keyword combinations will not be detected in the present version (any
report about this is highly appreciated).

<!-- 
| Keyword                                                                                         | Parameters                               | Mode                           |
| ----------------------------------------------------------------------------------------------- | ------------------------------------------------------------------------- | ------------------------------ |
| [`#!runner-config analyze_composition`](#analyze_composition)                                   | ---                                                                       | Mode 2 :custom-neural-network: | 
| [`#!runner-config analyze_error`](#analyze_error)                                               | ---                                                                       | Mode 2 :custom-neural-network: |
| [`#!runner-config analyze_error_charge_step`](#analyze_error_charge_step)                       | `#!runner-config a0` (`real`, default: `0.001 e`)                         | Mode 2 :custom-neural-network: |
| [`#!runner-config analyze_error_energy_step`](#analyze_error_energy_step)                       | `#!runner-config a0` (`real`, default: `0.01 Ha`)                         | Mode 2 :custom-neural-network: |
| [`#!runner-config analyze_error_force_step`](#analyze_error_force_step)                         | `#!runner-config a0` (`real`, default: `0.01 Ha/Bohr`)                    | Mode 2 :custom-neural-network: |
| [`#!runner-config atom_energy`](#atom_energy)                                                   | `threshold` (integer, default 1)                                          | Mode 2 :custom-neural-network: |
| [`#!runner-config biasweights_max`](#biasweights_max)                                           | `#!runner-config a0` (`real`, default: `+1.0`)                            | Mode 2 :custom-neural-network: |
| [`#!runner-config biasweights_min`](#biasweights_min)                                           | `#!runner-config a0` (`real`, default: `-1.0`)                            | Mode 2 :custom-neural-network: |
| [`#!runner-config bond_threshold`](#bond_threshold)                                             | `#!runner-config a0` (`real`, default: `+0.5 Bohr`)                       | All modes :material-chart-bell-curve-cumulative: :custom-neural-network: :material-head-cog-outline: |
| [`#!runner-config calculate_final_force`](#calculate_final_force)                               | ---                                                                       | Mode 2 :custom-neural-network: |
| [`#!runner-config calculate_forces`](#calculate_forces)                                         | ---                                                                       | Mode 2 :custom-neural-network: |
| [`#!runner-config calculate_hessian`](#calculate_hessian)                                       | ---                                                                       | Mode 2 :custom-neural-network: |
| [`#!runner-config calculate_stress`](#calculate_stress)                                         | ---                                                                       | Mode 2 :custom-neural-network: |
| [`#!runner-config center_symmetry_functions`](#center_symmetry_functions)                       | ---                                                                       | Mode 2 :custom-neural-network: |
| [`#!runner-config charge_error_threshold`](#charge_error_threshold)                             | ---                                                                       | Mode 2 :custom-neural-network: |
| [`#!runner-config charge_fraction`](#charge_fraction)                                           | ---                                                                       | Mode 2 :custom-neural-network: |
| [`#!runner-config charge_group`](#charge_group)                                                 | ---                                                                       | Mode 2 :custom-neural-network: |
| [`#!runner-config charge_grouping_by_structure`](#charge_grouping_by_structure)                 | ---                                                                       | Mode 2 :custom-neural-network: |
| [`#!runner-config charge_update_scaling`](#charge_update_scaling)                               | ---                                                                       | Mode 2 :custom-neural-network: |
| [`#!runner-config check_forces`](#check_forces)                                                 | ---                                                                       | Mode 2 :custom-neural-network: |
| [`#!runner-config check_input_forces`](#check_input_forces)                                     | ---                                                                       | Mode 2 :custom-neural-network: |
| [`#!runner-config cutoff_type`](#cutoff_type)                                                   | ---                                                                       | Mode 2 :custom-neural-network: |
| [`#!runner-config data_clustering`](#data_clustering)                                           | ---                                                                       | Mode 2 :custom-neural-network: |
| [`#!runner-config debug_mode`](#debug_mode)                                                     | ---                                                                       | Mode 2 :custom-neural-network: |
| [`#!runner-config detailed_timing`](#detailed_timing)                                           | ---                                                                       | Mode 2 :custom-neural-network: |
| [`#!runner-config detailed_timing_epoch`](#detailed_timing_epoch)                               | ---                                                                       | Mode 2 :custom-neural-network: |
| [`#!runner-config detect_saturation`](#detect_saturation)                                       | ---                                                                       | Mode 2 :custom-neural-network: |
| [`#!runner-config dynamic_force_grouping`](#dynamic_force_grouping)                             | ---                                                                       | Mode 2 :custom-neural-network: |
| [`#!runner-config electrostatic_type`](#electrostatic_type)                                     | ---                                                                       | Mode 2 :custom-neural-network: |
| [`#!runner-config element_activation_electrostatic`](#element_activation_electrostatic)         | ---                                                                       | Mode 2 :custom-neural-network: |
| [`#!runner-config element_activation_pair`](#element_activation_pair)                           | ---                                                                       | Mode 2 :custom-neural-network: |
| [`#!runner-config element_activation_short`](#element_activation_short)                         | ---                                                                       | Mode 2 :custom-neural-network: |
| [`#!runner-config element_decoupled_forces_v2`](#element_decoupled_forces_v2)                   | ---                                                                       | Mode 2 :custom-neural-network: |
| [`#!runner-config element_decoupled_kalman`](#element_decoupled_kalman)                         | ---                                                                       | Mode 2 :custom-neural-network: |
| [`#!runner-config element_hidden_layers_electrostatic`](#element_hidden_layers_electrostatic)   | ---                                                                       | Mode 2 :custom-neural-network: |
| [`#!runner-config element_hidden_layers_pair`](#element_hidden_layers_pair)                     | ---                                                                       | Mode 2 :custom-neural-network: |
| [`#!runner-config element_hidden_layers_short`](#element_hidden_layers_short)                   | ---                                                                       | Mode 2 :custom-neural-network: |
| [`#!runner-config element_nodes_electrostatic`](#element_nodes_electrostatic)                   | ---                                                                       | Mode 2 :custom-neural-network: |
| [`#!runner-config element_nodes_pair`](#element_nodes_pair)                                     | ---                                                                       | Mode 2 :custom-neural-network: |
| [`#!runner-config element_nodes_short`](#element_nodes_short)                                   | ---                                                                       | Mode 2 :custom-neural-network: |
| [`#!runner-config element_pairsymfunction_short`](#element_pairsymfunction_short)               | ---                                                                       | Mode 2 :custom-neural-network: |
| [`#!runner-config elements`](#elements)                                                         | ---                                                                       | Mode 2 :custom-neural-network: |
| [`#!runner-config element_symfunction_electrostatic`](#element_symfunction_electrostatic)       | ---                                                                       | Mode 2 :custom-neural-network: |
| [`#!runner-config element_symfunction_short`](#element_symfunction_short)                       | ---                                                                       | Mode 2 :custom-neural-network: |
| [`#!runner-config enable_on_the_fly_input`](#enable_on_the_fly_input)                           | ---                                                                       | Mode 2 :custom-neural-network: |
| [`#!runner-config energy_threshold`](#energy_threshold)                                         | ---                                      | Mode 2 :custom-neural-network: |
| [`#!runner-config enforce_max_num_neighbors_atomic`](#enforce_max_num_neighbors_atomic)         | ---                                      | Mode 2 :custom-neural-network: |
| [`#!runner-config enforce_totcharge`](#enforce_totcharge)                                       | ---                                      | Mode 2 :custom-neural-network: |
| [`#!runner-config environment_analysis`](#environment_analysis)                                 | ---                                      | Mode 2 :custom-neural-network: |
| [`#!runner-config epochs`](#epochs)                                                             | ---                                      | Mode 2 :custom-neural-network: |
| [`#!runner-config ewald_alpha`](#ewald_alpha)                                                   | ---                                      | Mode 2 :custom-neural-network: |
| [`#!runner-config ewald_cutoff`](#ewald_cutoff)                                                 | ---                                      | Mode 2 :custom-neural-network: |
| [`#!runner-config ewald_kmax`](#ewald_kmax)                                                     | ---                                      | Mode 2 :custom-neural-network: |
| [`#!runner-config find_contradictions`](#find_contradictions)                                   | ---                                      | Mode 2 :custom-neural-network: |
| [`#!runner-config fitmode`](#fitmode)                                                           | ---                                      | Mode 2 :custom-neural-network: |
| [`#!runner-config fitting_unit`](#fitting_unit)                                                 | ---                                      | Mode 2 :custom-neural-network: |
| [`#!runner-config fix_weights`](#fix_weights)                                                   | ---                                      | Mode 2 :custom-neural-network: |
| [`#!runner-config fixed_charge`](#fixed_charge)                                                 | ---                                      | Mode 2 :custom-neural-network: |
| [`#!runner-config fixed_short_energy_error_threshold`](#fixed_short_energy_error_threshold)     | ---                                      | Mode 2 :custom-neural-network: |
| [`#!runner-config fixed_short_force_error_threshold`](#fixed_short_force_error_threshold)       | ---                                      | Mode 2 :custom-neural-network: |
| [`#!runner-config force_grouping_by_structure`](#force_grouping_by_structure)                   | ---                                      | Mode 2 :custom-neural-network: |
| [`#!runner-config force_threshold`](#force_threshold)                                           | ---                                      | Mode 2 :custom-neural-network: |
| [`#!runner-config force_update_scaling`](#force_update_scaling)                                 | ---                                      | Mode 2 :custom-neural-network: |
| [`#!runner-config global_activation_electrostatic`](#global_activation_electrostatic)           | ---                                      | Mode 2 :custom-neural-network: |
| [`#!runner-config global_activation_pair`](#global_activation_pair)                             | ---                                      | Mode 2 :custom-neural-network: |
| [`#!runner-config global_activation_short`](#global_activation_short)                           | ---                                      | Mode 2 :custom-neural-network: |
| [`#!runner-config global_hidden_layers_electrostatic`](#global_hidden_layers_electrostatic)     | ---                                      | Mode 2 :custom-neural-network: |
| [`#!runner-config global_hidden_layers_pair`](#global_hidden_layers_pair)                       | ---                                      | Mode 2 :custom-neural-network: |
| [`#!runner-config global_hidden_layers_short`](#global_hidden_layers_short)                     | ---                                      | Mode 2 :custom-neural-network: |
| [`#!runner-config global_nodes_electrostatic`](#global_nodes_electrostatic)                     | ---                                      | Mode 2 :custom-neural-network: |
| [`#!runner-config global_nodes_pair`](#global_nodes_pair)                                       | ---                                      | Mode 2 :custom-neural-network: |
| [`#!runner-config global_nodes_short`](#global_nodes_short)                                     | ---                                      | Mode 2 :custom-neural-network: |
| [`#!runner-config global_output_nodes_electrostatic`](#global_output_nodes_electrostatic)       | ---                                      | Mode 2 :custom-neural-network: |
| [`#!runner-config global_output_nodes_pair`](#global_output_nodes_pair)                         | ---                                      | Mode 2 :custom-neural-network: |
| [`#!runner-config global_output_nodes_short`](#global_output_nodes_short)                       | ---                                      | Mode 2 :custom-neural-network: |
| [`#!runner-config global_pairsymfunction_short`](#global_pairsymfunction_short)                 | ---                                      | Mode 2 :custom-neural-network: |
| [`#!runner-config global_symfunction_electrostatic`](#global_symfunction_electrostatic)         | ---                                      | Mode 2 :custom-neural-network: |
| [`#!runner-config global_symfunction_short`](#global_symfunction_short)                         | ---                                      | Mode 2 :custom-neural-network: |
| [`#!runner-config growth_mode`](#growth_mode)                                                   | ---                                      | Mode 2 :custom-neural-network: |
| [`#!runner-config initialization_only`](#initialization_only)                                   | ---                                      | Mode 2 :custom-neural-network: |
| [`#!runner-config ion_forces_only`](#ion_forces_only)                                           | ---                                      | Mode 2 :custom-neural-network: |
| [`#!runner-config joint_energy_force_update`](#joint_energy_force_update)                       | ---                                      | Mode 2 :custom-neural-network: |
| [`#!runner-config kalman_damp_charge`](#kalman_damp_charge)                                     | ---                                      | Mode 2 :custom-neural-network: |
| [`#!runner-config kalman_damp_force`](#kalman_damp_force)                                       | ---                                      | Mode 2 :custom-neural-network: |
| [`#!runner-config kalman_damp_short`](#kalman_damp_short)                                       | ---                                      | Mode 2 :custom-neural-network: |
| [`#!runner-config kalman_epsilon`](#kalman_epsilon)                                             | ---                                      | Mode 2 :custom-neural-network: |
| [`#!runner-config kalman_lambda_charge`](#kalman_lambda_charge)                                 | ---                                      | Mode 2 :custom-neural-network: |
| [`#!runner-config kalman_lambda_charge_constraint`](#kalman_lambda_charge_constraint)           | ---                                      | Mode 2 :custom-neural-network: |
| [`#!runner-config kalman_lambda_short`](#kalman_lambda_short)                                   | ---                                      | Mode 2 :custom-neural-network: |
| [`#!runner-config kalman_nue_charge`](#kalman_nue_charge)                                       | ---                                      | Mode 2 :custom-neural-network: |
| [`#!runner-config kalman_nue_charge_constraint`](#kalman_nue_charge_constraint)                 | ---                                      | Mode 2 :custom-neural-network: |
| [`#!runner-config kalman_nue_short`](#kalman_nue_short)                                         | ---                                      | Mode 2 :custom-neural-network: |
| [`#!runner-config kalman_q0`](#kalman_q0)                                                       | ---                                      | Mode 2 :custom-neural-network: |
| [`#!runner-config kalman_qmin`](#kalman_qmin)                                                   | ---                                      | Mode 2 :custom-neural-network: |
| [`#!runner-config kalman_qtau`](#kalman_qtau)                                                   | ---                                      | Mode 2 :custom-neural-network: |
| [`#!runner-config max_energy`](#max_energy)                                                     | ---                                      | Mode 2 :custom-neural-network: |
| [`#!runner-config max_force`](#max_force)                                                       | ---                                      | Mode 2 :custom-neural-network: |
| [`#!runner-config md_mode`](#md_mode)                                                           | ---                                      | Mode 2 :custom-neural-network: |
| [`#!runner-config mix_all_points`](#mix_all_points)                                             | ---                                      | Mode 2 :custom-neural-network: |
| [`#!runner-config nguyen_widrow_weights_electrostatic`](#nguyen_widrow_weights_electrostatic)   | ---                                      | Mode 2 :custom-neural-network: |
| [`#!runner-config nguyen_widrow_weights_short`](#nguyen_widrow_weights_short)                   | ---                                      | Mode 2 :custom-neural-network: |
| [`#!runner-config nn_type_elec`](#nn_type_elec)                                                 | ---                                      | Mode 2 :custom-neural-network: |
| [`#!runner-config nn_type_short`](#nn_type_short)                                               | ---                                      | Mode 2 :custom-neural-network: |
| [`#!runner-config noise_charge`](#noise_charge)                                                 | ---                                      | Mode 2 :custom-neural-network: |
| [`#!runner-config noise_energy`](#noise_energy)                                                 | ---                                      | Mode 2 :custom-neural-network: |
| [`#!runner-config noise_force`](#noise_force)                                                   | ---                                      | Mode 2 :custom-neural-network: |
| [`#!runner-config normalize_nodes`](#normalize_nodes)                                           | ---                                      | Mode 2 :custom-neural-network: |
| [`#!runner-config number_of_elements`](#number_of_elements)                                     | ---                                      | Mode 2 :custom-neural-network: |
| [`#!runner-config optmode_charge`](#optmode_charge)                                             | ---                                      | Mode 2 :custom-neural-network: |
| [`#!runner-config optmode_short_energy`](#optmode_short_energy)                                 | ---                                      | Mode 2 :custom-neural-network: |
| [`#!runner-config optmode_short_force`](#optmode_short_force)                                   | ---                                      | Mode 2 :custom-neural-network: |
| [`#!runner-config pairsymfunction_short`](#pairsymfunction_short)                               | ---                                      | Mode 2 :custom-neural-network: |
| [`#!runner-config parallel_mode`](#parallel_mode)                                               | ---                                      | Mode 2 :custom-neural-network: |
| [`#!runner-config points_in_memory`](#points_in_memory)                                         | ---                                      | Mode 2 :custom-neural-network: |
| [`#!runner-config precondition_weights`](#precondition_weights)                                 | ---                                      | Mode 2 :custom-neural-network: |
| [`#!runner-config prepare_md`](#prepare_md)                                                     | ---                                      | Mode 2 :custom-neural-network: |
| [`#!runner-config print_all_deshortdw`](#print_all_deshortdw)                                   | ---                                      | Mode 2 :custom-neural-network: |
| [`#!runner-config print_all_dfshortdw`](#print_all_dfshortdw)                                   | ---                                      | Mode 2 :custom-neural-network: |
| [`#!runner-config print_all_electrostatic_weights`](#print_all_electrostatic_weights)           | ---                                      | Mode 2 :custom-neural-network: |
| [`#!runner-config print_all_short_weights`](#print_all_short_weights)                           | ---                                      | Mode 2 :custom-neural-network: |
| [`#!runner-config print_convergence_vector`](#print_convergence_vector)                         | ---                                      | Mode 2 :custom-neural-network: |
| [`#!runner-config print_date_and_time`](#print_date_and_time)                                   | ---                                      | Mode 2 :custom-neural-network: |
| [`#!runner-config print_force_components`](#print_force_components)                             | ---                                      | Mode 2 :custom-neural-network: |
| [`#!runner-config print_mad`](#print_mad)                                                       | ---                                      | Mode 2 :custom-neural-network: |
| [`#!runner-config print_sensitivity`](#print_sensitivity)                                       | ---                                      | Mode 2 :custom-neural-network: |
| [`#!runner-config random_number_type`](#random_number_type)                                     | ---                                      | Mode 2 :custom-neural-network: |
| [`#!runner-config random_order_training`](#random_order_training)                               | ---                                      | Mode 2 :custom-neural-network: |
| [`#!runner-config random_seed`](#random_seed)                                                   | ---                                      | Mode 2 :custom-neural-network: |
| [`#!runner-config read_kalman_matrices`](#read_kalman_matrices)                                 | ---                                      | Mode 2 :custom-neural-network: |
| [`#!runner-config read_unformatted`](#read_unformatted)                                         | ---                                      | Mode 2 :custom-neural-network: |
| [`#!runner-config remove_atom_energies`](#remove_atom_energies)                                 | ---                                      | Mode 2 :custom-neural-network: |
| [`#!runner-config remove_vdw_energies`](#remove_vdw_energies)                                   | ---                                      | Mode 2 :custom-neural-network: |
| [`#!runner-config repeated_energy_update`](#repeated_energy_update)                             | ---                                      | Mode 2 :custom-neural-network: |
| [`#!runner-config reset_kalman`](#reset_kalman)                                                 | ---                                      | Mode 2 :custom-neural-network: |
| [`#!runner-config restrict_weights`](#restrict_weights)                                         | ---                                      | Mode 2 :custom-neural-network: |
| [`#!runner-config runner_mode`](#runner_mode)                                                   | ---                                      | Mode 2 :custom-neural-network: |
| [`#!runner-config save_kalman_matrices`](#save_kalman_matrices)                                 | ---                                      | Mode 2 :custom-neural-network: |
| [`#!runner-config scale_max_elec`](#scale_max_elec)                                             | ---                                      | Mode 2 :custom-neural-network: |
| [`#!runner-config scale_max_short`](#scale_max_short)                                           | ---                                      | Mode 2 :custom-neural-network: |
| [`#!runner-config scale_max_short_atomic`](#scale_max_short_atomic)                             | ---                                      | Mode 2 :custom-neural-network: |
| [`#!runner-config scale_max_short_pair`](#scale_max_short_pair)                                 | ---                                      | Mode 2 :custom-neural-network: |
| [`#!runner-config scale_min_elec`](#scale_min_elec)                                             | ---                                      | Mode 2 :custom-neural-network: |
| [`#!runner-config scale_min_short`](#scale_min_short)                                           | ---                                      | Mode 2 :custom-neural-network: |
| [`#!runner-config scale_min_short_atomic`](#scale_min_short_atomic)                             | ---                                      | Mode 2 :custom-neural-network: |
| [`#!runner-config scale_min_short_pair`](#scale_min_short_pair)                                 | ---                                      | Mode 2 :custom-neural-network: |
| [`#!runner-config scale_symmetry_functions`](#scale_symmetry_functions)                         | ---                                      | Mode 2 :custom-neural-network: |
| [`#!runner-config screen_electrostatics`](#screen_electrostatics)                               | ---                                      | Mode 2 :custom-neural-network: |
| [`#!runner-config separate_bias_ini_short`](#separate_bias_ini_short)                           | ---                                      | Mode 2 :custom-neural-network: |
| [`#!runner-config separate_kalman_short`](#separate_kalman_short)                               | ---                                      | Mode 2 :custom-neural-network: |
| [`#!runner-config short_energy_error_threshold`](#short_energy_error_threshold)                 | ---                                      | Mode 2 :custom-neural-network: |
| [`#!runner-config short_energy_fraction`](#short_energy_fraction)                               | ---                                      | Mode 2 :custom-neural-network: |
| [`#!runner-config short_energy_group`](#short_energy_group)                                     | ---                                      | Mode 2 :custom-neural-network: |
| [`#!runner-config short_force_error_threshold`](#short_force_error_threshold)                   | ---                                      | Mode 2 :custom-neural-network: |
| [`#!runner-config short_force_fraction`](#short_force_fraction)                                 | ---                                      | Mode 2 :custom-neural-network: |
| [`#!runner-config short_force_group`](#short_force_group)                                       | ---                                      | Mode 2 :custom-neural-network: |
| [`#!runner-config shuffle_weights_short_atomic`](#shuffle_weights_short_atomic)                 | ---                                      | Mode 2 :custom-neural-network: |
| [`#!runner-config silent_mode`](#silent_mode)                                                   | ---                                      | Mode 2 :custom-neural-network: |
| [`#!runner-config steepest_descent_step_charge`](#steepest_descent_step_charge)                 | ---                                      | Mode 2 :custom-neural-network: |
| [`#!runner-config steepest_descent_step_energy_short`](#steepest_descent_step_energy_short)     | ---                                      | Mode 2 :custom-neural-network: |
| [`#!runner-config steepest_descent_step_force_short`](#steepest_descent_step_force_short)       | ---                                      | Mode 2 :custom-neural-network: |
| [`#!runner-config symfunction_correlation`](#symfunction_correlation)                           | ---                                      | Mode 2 :custom-neural-network: |
| [`#!runner-config symfunction_electrostatic`](#symfunction_electrostatic)                       | ---                                      | Mode 2 :custom-neural-network: |
| [`#!runner-config symfunction_short`](#symfunction_short)                                       | ---                                      | Mode 2 :custom-neural-network: |
| [`#!runner-config test_fraction`](#test_fraction)                                               | ---                                      | Mode 2 :custom-neural-network: |
| [`#!runner-config total_charge_error_threshold`](#total_charge_error_threshold)                 | ---                                      | Mode 2 :custom-neural-network: |
| [`#!runner-config update_single_element`](#update_single_element)                               | ---                                      | Mode 2 :custom-neural-network: |
| [`#!runner-config update_worst_charges`](#update_worst_charges)                                 | ---                                      | Mode 2 :custom-neural-network: |
| [`#!runner-config update_worst_short_energies`](#update_worst_short_energies)                   | ---                                      | Mode 2 :custom-neural-network: |
| [`#!runner-config update_worst_short_forces`](#update_worst_short_forces)                       | ---                                      | Mode 2 :custom-neural-network: |
| [`#!runner-config use_atom_charges`](#use_atom_charges)                                         | ---                                      | Mode 2 :custom-neural-network: |
| [`#!runner-config use_atom_spins`](#use_atom_spins)                                             | ---                                      | Mode 2 :custom-neural-network: |
| [`#!runner-config use_atom_energies`](#use_atom_energies)                                       | ---                                      | Mode 2 :custom-neural-network: |
| [`#!runner-config use_charge_constraint`](#use_charge_constraint)                               | ---                                      | Mode 2 :custom-neural-network: |
| [`#!runner-config use_damping`](#use_damping)                                                   | ---                                      | Mode 2 :custom-neural-network: |
| [`#!runner-config use_electrostatic_nn`](#use_electrostatic_nn)                                 | ---                                      | Mode 2 :custom-neural-network: |
| [`#!runner-config use_electrostatics`](#use_electrostatics)                                     | ---                                      | Mode 2 :custom-neural-network: |
| [`#!runner-config use_fixed_charges`](#use_fixed_charges)                                       | ---                                      | Mode 2 :custom-neural-network: |
| [`#!runner-config use_ipi`](#use_ipi)                                                           | ---                                      | Mode 2 :custom-neural-network: |
| [`#!runner-config use_noisematrix`](#use_noisematrix)                                           | ---                                      | Mode 2 :custom-neural-network: |
| [`#!runner-config use_old_scaling`](#use_old_scaling)                                           | ---                                      | Mode 2 :custom-neural-network: |
| [`#!runner-config use_old_weights_charge`](#use_old_weights_charge)                             | ---                                      | Mode 2 :custom-neural-network: |
| [`#!runner-config use_old_weights_short`](#use_old_weights_short)                               | ---                                      | Mode 2 :custom-neural-network: |
| [`#!runner-config use_omp_mkl`](#use_omp_mkl)                                                   | ---                                      | Mode 2 :custom-neural-network: |
| [`#!runner-config use_short_forces`](#use_short_forces)                                         | ---                                      | Mode 2 :custom-neural-network: |
| [`#!runner-config use_short_nn`](#use_short_nn)                                                 | ---                                      | Mode 2 :custom-neural-network: |
| [`#!runner-config use_systematic_weights_electrostatic`](#use_systematic_weights_electrostatic) | ---                                      | Mode 2 :custom-neural-network: |
| [`#!runner-config use_systematic_weights_short`](#use_systematic_weights_short)                 | ---                                      | Mode 2 :custom-neural-network: |
| [`#!runner-config use_vdw`](#use_vdw)                                                           | ---                                      | Mode 2 :custom-neural-network: |
| [`#!runner-config vdw_coefficient`](#vdw_coefficient)                                           | ---                                      | Mode 2 :custom-neural-network: |
| [`#!runner-config vdw_cutoff`](#vdw_cutoff)                                                     | ---                                      | Mode 2 :custom-neural-network: |
| [`#!runner-config vdw_radius`](#vdw_radius)                                                     | ---                                      | Mode 2 :custom-neural-network: |
| [`#!runner-config vdw_screening`](#vdw_screening)                                               | ---                                      | Mode 2 :custom-neural-network: |
| [`#!runner-config vdw_type`](#vdw_type)                                                         | ---                                      | Mode 2 :custom-neural-network: |
| [`#!runner-config weight_analysis`](#weight_analysis)                                           | ---                                      | Mode 2 :custom-neural-network: |
| [`#!runner-config weight_constraint`](#weight_constraint)                                       | ---                                      | Mode 2 :custom-neural-network: |
| [`#!runner-config weighte_constraint`](#weighte_constraint)                                     | ---                                      | Mode 2 :custom-neural-network: |
| [`#!runner-config weights_max`](#weights_max)                                                   | `#!runner-config a0` (`real`, default: `+1.0`) | Mode 2 :custom-neural-network: |
| [`#!runner-config weights_min`](#weights_min)                                                   | `#!runner-config a0` (`real`, default: `-1.0`) | Mode 2 :custom-neural-network: |
| [`#!runner-config weightse_max`](#weightse_max)                                                 | `#!runner-config a0` (`real`, default: `+1.0`) | Mode 2 :custom-neural-network: |
| [`#!runner-config weightse_min`](#weightse_min)                                                 | `#!runner-config a0` (`real`, default: `-1.0`) | Mode 2 :custom-neural-network: |
| [`#!runner-config write_fit_statistics`](#write_fit_statistics)                                 | ---                                      | Mode 2 :custom-neural-network: |
| [`#!runner-config write_pdb`](#write_pdb)                                                       | Outdated                                 | Outdated                       |
| [`#!runner-config write_pov`](#write_pov)                                                       | Outdated                                 | Outdated                       |
| [`#!runner-config write_pwscf`](#write_pwscf)                                                   | Outdated                                 | Outdated                       |
| [`#!runner-config write_temporary_weights`](#write_temporary_weights)                           | ---                                      | Mode 2 :custom-neural-network: |
| [`#!runner-config write_traincharges`](#write_traincharges)                                     | ---                                      | Mode 2 :custom-neural-network: |
| [`#!runner-config write_trainforces`](#write_trainforces)                                       | ---                                      | Mode 2 :custom-neural-network: |
| [`#!runner-config write_symfunctions`](#write_symfunctions)                                     | ---                                      | Mode 3 :material-head-cog-outline: |
| [`#!runner-config write_binding_energy_only`](#write_binding_energy_only)                       | ---                                      | Mode 2 :custom-neural-network: |
| [`#!runner-config write_trainpoints`](#write_trainpoints)                                       | ---                                      | Mode 2 :custom-neural-network: |
| [`#!runner-config write_unformatted`](#write_unformatted)                                       | --- | All modes :material-chart-bell-curve-cumulative: :custom-neural-network: :material-head-cog-outline: |
| [`#!runner-config write_weights_epoch`](#write_weights_epoch)                                   | `#!runner-config i1` (`integer`, default: `1`) | Mode 2 :custom-neural-network: |
| [`#!runner-config write_xyz`](#write_xyz)                                                       | Outdated                                 | Outdated                       | 
-->

## Alphabetical List of Keywords

Please be aware that only a few of the available keyword will be needed
regularly, and many keyword refer to experimental features or features,
which have not been fully implemented yet. The following list of symbols
can be used to assess the relevance of a specific keyword:

!!! help "Legend"
    ++ = very important keyword (frequently used).
    
    \+ = important keyword (can be useful).
    
    \* = experimental feature (usefulness unclear).
    
    \- = not yet fully implemented, probably not working.
    
    \-- = deprecated

### `analyze_composition` 

:material-chart-bell-curve-cumulative: **Mode 1:** :material-close:{ .no } **•** 
:custom-neural-network:                **Mode 2:** :material-check:{ .yes } **•** 
:material-head-cog-outline:            **Mode 3:** :material-close:{ .no }

Print detailed information about the element composition of the data set in 
`input.data`.

!!! Abstract "**Format:** `#!runner-config analyze_composition` (`logical`, default: `True`)"
    
    This keyword does not have any further options.
    
    [:material-code-tags: **Source variable** `lanalyzecomposition`](https://gitlab.com/TheochemGoettingen/RuNNer/-/blob/master/src-devel/fittingoptions.f90#L184)

---

### `analyze_error` 

:material-chart-bell-curve-cumulative: **Mode 1:** :material-close:{ .no } **•** 
:custom-neural-network:                **Mode 2:** :material-check:{ .yes } **•** 
:material-head-cog-outline:            **Mode 3:** :material-close:{ .no }

Print detailed information about the training error.

!!! Abstract "**Format:** `#!runner-config analyze_error` (`logical`, default: `False`)"
    
    This keyword does not have any further options.
    
    [:material-code-tags: **Source variable** `lanalyzeerror`](https://gitlab.com/TheochemGoettingen/RuNNer/-/blob/master/src-devel/fittingoptions.f90#L170)

---

### `analyze_error_charge_step` 

:material-chart-bell-curve-cumulative: **Mode 1:** :material-close:{ .no } **•** 
:custom-neural-network:                **Mode 2:** :material-check:{ .yes } **•** 
:material-head-cog-outline:            **Mode 3:** :material-close:{ .no }

When a detailed analysis of the training error with 
[`#!runner-config analyze_error`](/runner/reference/keywords/#analyze_error)
is performed, this keyword allows for the definition of the interval in which 
atoms with the same charge error are grouped together.

!!! Abstract "**Format:** `#!runner-config analyze_error_charge_step a0`"
    
    **`#!runner-config a0`: (`real`, default: `0.001`)**
    :   The interval in which atoms with the same charge error are grouped 
        together (unit: electron charge).
    
    [:material-code-tags: **Source variable** `lanalyze_error_charge_step`](https://gitlab.com/TheochemGoettingen/RuNNer/-/blob/master/src-devel/fittingoptions.f90#L70)

!!! warning "Hint"
    For this keyword to take effect, please also specify
    [`#!runner-config analyze_error`](/runner/reference/keywords/#analyze_error).


---

### `analyze_error_energy_step`

:material-chart-bell-curve-cumulative: **Mode 1:** :material-close:{ .no } **•** 
:custom-neural-network:                **Mode 2:** :material-check:{ .yes } **•** 
:material-head-cog-outline:            **Mode 3:** :material-close:{ .no }

When a detailed analysis of the training error with
[`#!runner-config analyze_error`](/runner/reference/keywords/#analyze_error)
is performed, this keyword allows for the definition of the interval in which 
atoms with the same energy error are grouped together.

!!! Abstract "**Format:** `#!runner-config analyze_error_energy_step a0`"
    
    **`#!runner-config a0`: (`real`, default: `0.01`)**
    :   The interval in which atoms with the same energy error are 
        grouped together (unit: Hartree).
    
    [:material-code-tags: **Source variable** `lanalyze_error_energy_step`](https://gitlab.com/TheochemGoettingen/RuNNer/-/blob/master/src-devel/fittingoptions.f90#L68)

!!! warning "Hint"
    For this keyword to take effect, please also specify
    [`#!runner-config analyze_error`](/runner/reference/keywords/#analyze_error).

---

### `analyze_error_force_step` 

:material-chart-bell-curve-cumulative: **Mode 1:** :material-close:{ .no } **•** 
:custom-neural-network:                **Mode 2:** :material-check:{ .yes } **•** 
:material-head-cog-outline:            **Mode 3:** :material-close:{ .no }

When a detailed analysis of the training error with
[`#!runner-config analyze_error`](/runner/reference/keywords/#analyze_error)
is performed, this keyword allows for the definition of the interval in which 
atoms with the same total force error are grouped together.

!!! Abstract "**Format:** `#!runner-config analyze_error_force_step a0`"
    
    **`#!runner-config a0`: (`real`, default: `0.01`)**
    :   The interval in which atoms with the same force error are grouped 
        together (unit: Hartree per Bohr).
    
    [:material-code-tags: **Source variable** `lanalyze_error_force_step`](https://gitlab.com/TheochemGoettingen/RuNNer/-/blob/master/src-devel/fittingoptions.f90#L69)

!!! warning "Hint"
    For this keyword to take effect, please also specify
    [`#!runner-config analyze_error`](/runner/reference/keywords/#analyze_error).

---

### `atom_energy` ++

:material-chart-bell-curve-cumulative: **Mode 1:** :material-check:{ .yes } **•** 
:custom-neural-network:                **Mode 2:** :material-close:{ .no } **•** 
:material-head-cog-outline:            **Mode 3:** :material-check:{ .yes }

Specification of the energies of the free atoms. This keyword must be used for
each element if the keyword 
[`#!runner-config remove_atom_energies`](/runner/reference/keywords/#remove_atom_energies)
is used. 

In [`#!runner-config runner_mode`](/runner/reference/keywords/#runner_mode) 1
the atomic energies are removed from the total energies, in 
[`#!runner-config runner_mode`](/runner/reference/keywords/#runner_mode) 3
the atomic energies are added to the fitted energy to yield the correct total
energy. Internally, `RuNNer` always works with binding energies, if 
[`#!runner-config remove_atom_energies`](/runner/reference/keywords/#remove_atom_energies)
is specified.

!!! Abstract "**Format:** `#!runner-config atom_energy element energy`"

    **`#!runner-config element`: (`string`)**
    :   Element symbol.
        
    **`#!runner-config energy`: (`real`, default: `0.0`)**
    :   Atomic reference energy in Hartree.
    
    [:material-code-tags: **Source array** `atomrefenergies(nelem)`](https://gitlab.com/TheochemGoettingen/RuNNer/-/blob/master/src-devel/globaloptions.f90#L94)

#### Example

``` runner-config
atom_energy Zn -1805.01857147
```

!!! warning "Hint"
    For this keyword to take effect, please also specify
    [`#!runner-config remove_atom_energies`](/runner/reference/keywords/#remove_atom_energies).

---

### `biasweights_max` 

:material-chart-bell-curve-cumulative: **Mode 1:** :material-close:{ .no } **•** 
:custom-neural-network:                **Mode 2:** :material-check:{ .yes } **•** 
:material-head-cog-outline:            **Mode 3:** :material-close:{ .no }

`RuNNer` allows for a separate random initialization of the bias weights at the
beginning of 
[`#!runner-config runner_mode`](/runner/reference/keywords/#runner_mode) 2
through
[`#!runner-config separate_bias_ini_short`](/runner/reference/keywords/#separate_bias_ini_short).
In that case the bias weights are randomly initialized on an interval between 
[`#!runner-config biasweights_max`](/runner/reference/keywords/#biasweights_max) 
and [`#!runner-config biasweights_min`](/runner/reference/keywords/#biasweights_min). 

!!! Abstract "**Format:** `#!runner-config biasweights_max a0`"
    
    **`#!runner-config a0`: (`real`, default: `+1.0`)**
    :   The maximum value that is assigned to bias weights during 
        initialization of the weights.
    
    [:material-code-tags: **Source variable** `biasweights_max`](https://gitlab.com/TheochemGoettingen/RuNNer/-/blob/master/src-devel/fittingoptions.f90#L79)

!!! warning "Hint"
    For this keyword to take effect please also specify
    [`#!runner-config separate_bias_ini_short`](/runner/reference/keywords/#separate_bias_ini_short).
    
---

### `biasweights_min` 

:material-chart-bell-curve-cumulative: **Mode 1:** :material-close:{ .no } **•** 
:custom-neural-network:                **Mode 2:** :material-check:{ .yes } **•** 
:material-head-cog-outline:            **Mode 3:** :material-close:{ .no }

`RuNNer` allows for a separate random initialization of the bias weights at the
beginning of 
[`#!runner-config runner_mode`](/runner/reference/keywords/#runner_mode) 2
through
[`#!runner-config separate_bias_ini_short`](/runner/reference/keywords/#separate_bias_ini_short).
In that case the bias weights are randomly initialized on an interval between 
[`#!runner-config biasweights_max`](/runner/reference/keywords/#biasweights_max) 
and [`#!runner-config biasweights_min`](/runner/reference/keywords/#biasweights_min).

!!! Abstract "**Format:** `#!runner-config biasweights_min a0`"
    
    **`#!runner-config a0`: (`real`, default: `-1.0`)**
    :   The maximum value that is assigned to bias weights during 
        initialization of the weights.
    
    [:material-code-tags: **Source variable** `biasweights_min`](https://gitlab.com/TheochemGoettingen/RuNNer/-/blob/master/src-devel/fittingoptions.f90#L100)

!!! warning "Hint"
    This keyword does not have any effect unless 
    `#!runner-config separate_bias_ini_short`
    is specified too.

---

### `bond_threshold` ++

:material-chart-bell-curve-cumulative: **Mode 1:** :material-check:{ .yes } **•** 
:custom-neural-network:                **Mode 2:** :material-check:{ .yes } **•** 
:material-head-cog-outline:            **Mode 3:** :material-check:{ .yes }

Threshold for the shortest bond in the structure in Bohr units. If a shorter
bond occurs `RuNNer` will stop with an error message in 
[`#!runner-config runner_mode`](/runner/reference/keywords/#runner_mode)
2 and 3. In 
[`#!runner-config runner_mode`](/runner/reference/keywords/#runner_mode) 1 the 
structure will be eliminated from the data set.

!!! Abstract "**Format:** `#!runner-config bond_threshold a0`"
    
    **`#!runner-config a0`: (`real`, default: `0.5`)**
    :   The minimum bond length between any two atoms in the structure
        (unit: Bohr).
    
    [:material-code-tags: **Source variable** `rmin`](https://gitlab.com/TheochemGoettingen/RuNNer/-/blob/master/src-devel/globaloptions.f90#L93)

---

### `calculate_final_force` 

:material-chart-bell-curve-cumulative: **Mode 1:** :material-close:{ .no } **•** 
:custom-neural-network:                **Mode 2:** :material-check:{ .yes } **•** 
:material-head-cog-outline:            **Mode 3:** :material-close:{ .no }

Print detailed information about the forces in the training and testing set at 
the end of the NNP training process. 

!!! Abstract "**Format:** `#!runner-config calculate_final_force` (`logical`, default: `False`)"
    
    This keyword does not have any further options.
    
    [:material-code-tags: **Source variable** `lfinalforce`](https://gitlab.com/TheochemGoettingen/RuNNer/-/blob/master/src-devel/fittingoptions.f90#L166)

!!! warning "Hint"
    This keyword is only necessary if the 
    usage of force fitting has not been requested in 
    [`#!runner-config runner_mode`](/runner/reference/keywords/#runner_mode) 2
    with the keyword 
    [`#!runner-config use_short_forces`](/runner/reference/keywords/#use_short_forces).

---

### `calculate_forces` ++

:material-chart-bell-curve-cumulative: **Mode 1:** :material-close:{ .no } **•** 
:custom-neural-network:                **Mode 2:** :material-close:{ .no } **•** 
:material-head-cog-outline:            **Mode 3:** :material-check:{ .yes }

Calculate the atomic forces in 
[`#!runner-config runner_mode`](/runner/reference/keywords/#runner_mode) 3 and
write them to the files 
[`runner.out`](/runner/reference/files/#runnerout) and 
[`nnforces.out`](/runner/reference/files/#nnforcesout).

!!! Abstract "**Format:** `#!runner-config calculate_forces` (`logical`, default: `False`)"
    
    This keyword does not have any further options.
    
    [:material-code-tags: **Source variable** `ldoforces`](https://gitlab.com/TheochemGoettingen/RuNNer/-/blob/master/src-devel/predictionoptions.f90#L33)

---

### `calculate_hessian` ++

:material-chart-bell-curve-cumulative: **Mode 1:** :material-close:{ .no } **•** 
:custom-neural-network:                **Mode 2:** :material-close:{ .no } **•** 
:material-head-cog-outline:            **Mode 3:** :material-check:{ .yes }

Calculate the Hessian matrix in 
[`#!runner-config runner_mode`](/runner/reference/keywords/#runner_mode) 3. 
<!-- The implementation is currently in progress and the keyword is not yet ready for
use. -->

!!! Abstract "**Format:** `#!runner-config calculate_hessian` (`logical`, default: `False`)"
    
    This keyword does not have any further options.
    
    [:material-code-tags: **Source variable** `ldohessian`](https://gitlab.com/TheochemGoettingen/RuNNer/-/blob/master/src-devel/predictionoptions.f90#L34)

---

### `calculate_stress` ++

:material-chart-bell-curve-cumulative: **Mode 1:** :material-close:{ .no } **•** 
:custom-neural-network:                **Mode 2:** :material-close:{ .no } **•** 
:material-head-cog-outline:            **Mode 3:** :material-check:{ .yes }

Calculate the stress tensor (only for periodic systems) in 
[`#!runner-config runner_mode`](/runner/reference/keywords/#runner_mode) 3 and 
write it to the files [`runner.out`](/runner/reference/files/#runnerout) and 
[`nnstress.out`](/runner/reference/files/#nnstressout).
This is at the moment only implemented for the short range part and for the
contributions to the stress tensor through vdW interactions.

!!! Abstract "**Format:** `#!runner-config calculate_stress` (`logical`, default: `False`)"
    
    This keyword does not have any further options.
    
    [:material-code-tags: **Source variable** `ldostress`](https://gitlab.com/TheochemGoettingen/RuNNer/-/blob/master/src-devel/globaloptions.f90#L165)

---

### `center_symmetry_functions` ++

:material-chart-bell-curve-cumulative: **Mode 1:** :material-close:{ .no } **•** 
:custom-neural-network:                **Mode 2:** :material-check:{ .yes } **•** 
:material-head-cog-outline:            **Mode 3:** :material-check:{ .yes }

Shift the symmetry function values individually for each symmetry function such
that the average is moved to zero. This may have numerical advantages, because 
zero is the center of the non-linear regions of most activation functions.

!!! Abstract "**Format:** `#!runner-config center_symmetry_functions` (`logical`, default: `False`)"
    
    This keyword does not have any further options.
    
    [:material-code-tags: **Source variable** `lcentersym`](https://gitlab.com/TheochemGoettingen/RuNNer/-/blob/master/src-devel/globaloptions.f90#L121)

---

### `charge_error_threshold` ++

:material-chart-bell-curve-cumulative: **Mode 1:** :material-close:{ .no } **•** 
:custom-neural-network:                **Mode 2:** :material-check:{ .yes } **•** 
:material-head-cog-outline:            **Mode 3:** :material-close:{ .no }

Threshold value for the error of the charges in units of the RMSE of the
previous epoch. A value of 0.3 means that only charges with an error larger than
0.3RMSE will be used for the weight update. Large values (about 1.0) will speed
up the first epochs, because only a few points will be used. 

!!! Abstract "**Format:** `#!runner-config charge_error_threshold a0`"
    
    **`#!runner-config a0`: (`real`, default: `0.0`)**
    :   Fraction of charge RMSE that a charge needs to reach to be used in the
        weight update.
    
    [:material-code-tags: **Source variable** `kalmanthresholde`](https://gitlab.com/TheochemGoettingen/RuNNer/-/blob/master/src-devel/fittingoptions.f90#L92)

!!! warning "Hint"
    For this keyword to take effect, you need to use the Kalman filter algorithm.

---

### `charge_fraction` ++

:material-chart-bell-curve-cumulative: **Mode 1:** :material-close:{ .no } **•** 
:custom-neural-network:                **Mode 2:** :material-check:{ .yes } **•** 
:material-head-cog-outline:            **Mode 3:** :material-close:{ .no }

Defines the random fraction of atomic charges used for fitting the electrostatic
weights in 
[`#!runner-config runner_mode`](/runner/reference/keywords/#runner_mode) 2.

!!! Abstract "**Format:** `#!runner-config charge_fraction a0`"
    
    **`#!runner-config a0`: (`real`, default: `0.0`)**
    :   Fraction of atomic charges used for fitting of the electrostatic weights.
        100% = 1.0.
    
    [:material-code-tags: **Source variable** `chargernd`](https://gitlab.com/TheochemGoettingen/RuNNer/-/blob/master/src-devel/fittingoptions.f90#L72)

!!! warning "Hint"
    This keyword is used in case of 
    [`#!runner-config electrostatic_type`](/runner/reference/keywords/#electrostatic_type)
    1 only.

---

### `charge_group` ++

:material-chart-bell-curve-cumulative: **Mode 1:** :material-close:{ .no } **•** 
:custom-neural-network:                **Mode 2:** :material-check:{ .yes } **•** 
:material-head-cog-outline:            **Mode 3:** :material-close:{ .no }

Do not update the electrostatic NN weights after the presentation of an 
individual atomic charge, but average the derivatives with respect to the 
weights over the specified number of charges for each element.

!!! Abstract "**Format:** `#!runner-config charge_group i0`"
    
    **`#!runner-config i0`: (`integer`, default: `1`)**
    :   Number of atomic charges per group. The maximum is given by
        [`#!runner-config points_in_memory`](/runner/reference/keywords/#points_in_memory).
    
    [:material-code-tags: **Source variable** `nchargegroup`](https://gitlab.com/TheochemGoettingen/RuNNer/-/blob/master/src-devel/fittingoptions.f90#L28)

---

<!-- ### `charge_grouping_by_structure` 

This is a dummy keyword without any effect.

--- -->

<!-- ### `charge_update_scaling` 

This is a dummy keyword without any effect.

--- -->

### `check_forces` *

:material-chart-bell-curve-cumulative: **Mode 1:** :material-close:{ .no } **•** 
:custom-neural-network:                **Mode 2:** :material-check:{ .yes } **•** 
:material-head-cog-outline:            **Mode 3:** :material-check:{ .yes }

This keyword allows to check if the sum of all NN force vectors is zero, It is
for debugging purposes only, but does not cost much CPU time.

!!! Abstract "**Format:** `#!runner-config check_forces` (`logical`, default: `False`)"
    
    This keyword does not have any further options.
    
    [:material-code-tags: **Source variable** `lcheckf`](https://gitlab.com/TheochemGoettingen/RuNNer/-/blob/master/src-devel/globaloptions.f90#L135)

---

### `check_input_forces` *

:material-chart-bell-curve-cumulative: **Mode 1:** :material-check:{ .yes } **•** 
:custom-neural-network:                **Mode 2:** :material-close:{ .no } **•** 
:material-head-cog-outline:            **Mode 3:** :material-close:{ .no }

Check, if the sum of all forces of the training structures is sufficiently close
to the zero vector.

!!! Abstract "**Format:** `#!runner-config check_input_forces a0`"
    
    **`#!runner-config a0`: (`real`, default: `0.0`)**
    :   Threshold for the absolute value of the sum of all force vectors per 
        atom.
    
    :material-code-tags: **Source variables** [`lcheckinputforce`](https://gitlab.com/TheochemGoettingen/RuNNer/-/blob/master/src-devel/mode1options.f90#L31) and [`inputforcethreshold`](https://gitlab.com/TheochemGoettingen/RuNNer/-/blob/master/src-devel/mode1options.f90#L27)

---

### `cutoff_type` ++

:material-chart-bell-curve-cumulative: **Mode 1:** :material-check:{ .yes } **•** 
:custom-neural-network:                **Mode 2:** :material-check:{ .yes } **•** 
:material-head-cog-outline:            **Mode 3:** :material-check:{ .yes }

This keyword determines the cutoff function to be used for the symmetry
functions.

!!! Abstract "**Format:** `#!runner-config cutoff_type i0`"
    
    **`#!runner-config i0`: (`integer`, default: `1`)**
    :   Threshold for the absolute value of the sum of all force vectors per 
        atom. Can be one of:

        * **`0`:**
        :   Hard function: $1$
        * **`1`:**
        :   Cosine function: $\frac{1}{2}[\cos(\pi x)+ 1]$
        * **`2`:**
        :   Hyperbolic tangent function 1: $\tanh^{3} (1-\frac{R_{ij}}{R_{\mathrm{c}}})$
        * **`3`:**
        :   Hyperbolic tangent function 2: $(\frac{e+1}{e-1})^3 \tanh^{3}(1-\frac{R_{ij}}{R_{\mathrm{c}}})$
        * **`4`:**
        :   Exponential function: $\exp(1-\frac{1}{1-x^2})$
        * **`5`:**
        :   Polynomial function 1: $(2x -3)x^2+1$
        * **`6`:**
        :   Polynomial function 2: $((15-6x)x-10)x^3+1$
        * **`7`:**
        :   Polynomial function 3: $(x(x(20x-70)+84)-35)x^4+1$
        * **`8`:**
        :   Polynomial function 4: $(x(x(x(315-70x)-540)+420)-126)x^5+1$

    [:material-code-tags: **Source variable** `cutoff_type`](https://gitlab.com/TheochemGoettingen/RuNNer/-/blob/master/src-devel/globaloptions.f90#L25)

---

### `data_clustering` *

:material-chart-bell-curve-cumulative: **Mode 1:** :material-close:{ .no } **•** 
:custom-neural-network:                **Mode 2:** :material-check:{ .yes } **•** 
:material-head-cog-outline:            **Mode 3:** :material-close:{ .no }

Performs an analysis of all symmetry function vectors of all atoms and groups
the atomic environments to clusters with a maximum distance of 
`#!runner-config a0` between the symmetry function vectors. If 
`#!runner-config a1` is larger than 1.0 the assignment of each atom will be 
printed.

!!! Abstract "**Format:** `#!runner-config data_clustering a0 a1`"
    
    **`#!runner-config a0`: (`real`, default: `1.0`)**
    :   Maximum distance between the symmetry function vectors of two clusters
        of atomic environments.
    **`#!runner-config a1`: (`real`, default: `0.0`)**
    :   If `#!runner-config a1 > 1.0`, print the group that each atom has been 
        assigned to. 
    
    :material-code-tags: **Source variables**
    [`ldataclustering`](https://gitlab.com/TheochemGoettingen/RuNNer/-/blob/master/src-devel/fittingoptions.f90#L183),
    [`dataclusteringthreshold1`](https://gitlab.com/TheochemGoettingen/RuNNer/-/blob/master/src-devel/fittingoptions.f90#L115) and [`dataclusteringthreshold2`](https://gitlab.com/TheochemGoettingen/RuNNer/-/blob/master/src-devel/fittingoptions.f90#L116)

---

### `debug_mode` *

:material-chart-bell-curve-cumulative: **Mode 1:** :material-check:{ .yes } **•** 
:custom-neural-network:                **Mode 2:** :material-check:{ .yes } **•** 
:material-head-cog-outline:            **Mode 3:** :material-check:{ .yes }

If switched on, this option can produce a lot of output and is meant for
debugging new developments only!!!

!!! Abstract "**Format:** `#!runner-config debug_mode` (`logical`, default: `False`)"
    
    This keyword does not have any further options.
    
    [:material-code-tags: **Source variable** `ldebug`](https://gitlab.com/TheochemGoettingen/RuNNer/-/blob/master/src-devel/globaloptions.f90#L133)

---

### `detailed_timing` -

:material-chart-bell-curve-cumulative: **Mode 1:** :material-check:{ .yes } **•** 
:custom-neural-network:                **Mode 2:** :material-check:{ .yes } **•** 
:material-head-cog-outline:            **Mode 3:** :material-check:{ .yes }

Write detailed timing information for the individual parts of `RuNNer`
at the end of the run. This feature has to be used with some care because often
the implementation of the time measurement lacks behind in code development.

!!! Abstract "**Format:** `#!runner-config detailed_timing` (`logical`, default: `False`)"
    
    This keyword does not have any further options.
    
    [:material-code-tags: **Source variable** `lfinetime`](https://gitlab.com/TheochemGoettingen/RuNNer/-/blob/master/src-devel/globaloptions.f90#L130)

---

### `detailed_timing_epoch` -

:material-chart-bell-curve-cumulative: **Mode 1:** :material-check:{ .yes } **•** 
:custom-neural-network:                **Mode 2:** :material-check:{ .yes } **•** 
:material-head-cog-outline:            **Mode 3:** :material-check:{ .yes }

Write detailed timing information in each epoch in 
[`#!runner-config runner_mode`](/runner/reference/keywords/#runner_mode) 2. 
This feature has to be used with some care because often the implementation of
the time measurement lacks behind in code development.

!!! Abstract "**Format:** `#!runner-config detailed_timing_epoch` (`logical`, default: `False`)"
    
    This keyword does not have any further options.
    
    [:material-code-tags: **Source variable** `lfinetimeepoch`](https://gitlab.com/TheochemGoettingen/RuNNer/-/blob/master/src-devel/globaloptions.f90#L131)

---

### `detect_saturation` 

:material-chart-bell-curve-cumulative: **Mode 1:** :material-close:{ .no } **•** 
:custom-neural-network:                **Mode 2:** :material-check:{ .yes } **•** 
:material-head-cog-outline:            **Mode 3:** :material-close:{ .no }

For each training epoch, checks whether the value of a node in any hidden layer 
exceeds 
[`#!runner-config saturation_threshold`](/runner/reference/keywords/#saturation_threshold)
and prints a warning.

!!! Abstract "**Format:** `#!runner-config detect_saturation` (`logical`, default: `False`)"
    
    This keyword does not have any further options.
    
    [:material-code-tags: **Source variable** `ldetect_saturation`](https://gitlab.com/TheochemGoettingen/RuNNer/-/blob/master/src-devel/globaloptions.f90#L167)

!!! warning "Hint"
    For this keyword to take effect, please also use
    [`#!runner-config saturation_threshold`](/runner/reference/keywords/#saturation_threshold).
    
!!! warning "Hint"
    Please note that this keyword will only work for three hidden layers or 
    less.

---

### `dynamic_force_grouping` 

:material-chart-bell-curve-cumulative: **Mode 1:** :material-close:{ .no } **•** 
:custom-neural-network:                **Mode 2:** :material-check:{ .yes } **•** 
:material-head-cog-outline:            **Mode 3:** :material-close:{ .no }

Do not update the short-range NN weights after the presentation of an 
individual atomic force vector, but average the derivatives with respect to the 
weights over the number of force vectors for each element specified by
[`#!runner-config short_force_group`](/runner/reference/keywords/#short_force_group)

!!! Abstract "**Format:** `#!runner-config dynamic_force_grouping` (`logical`, default: `False`)"
    
    This keyword does not have any further options.
    
    [:material-code-tags: **Source variable** `ldynforcegroup`](https://gitlab.com/TheochemGoettingen/RuNNer/-/blob/master/src-devel/fittingoptions.f90#L182)

!!! warning "Hint"
    For this keyword to take effect, please also use
    [`#!runner-config short_force_group`](/runner/reference/keywords/#short_force_group).
    
---

### `electrostatic_type` ++

:material-chart-bell-curve-cumulative: **Mode 1:** :material-check:{ .yes } **•** 
:custom-neural-network:                **Mode 2:** :material-check:{ .yes } **•** 
:material-head-cog-outline:            **Mode 3:** :material-check:{ .yes }

This keyword determines the cutoff function to be used for the symmetry
functions.

!!! Abstract "**Format:** `#!runner-config electrostatic_type i0`"
    
    **`#!runner-config i0`: (`integer`, default: `1`)**
    :   Threshold for the absolute value of the sum of all force vectors per 
        atom. Can be one of:

        * **`1`:**
        :   There is a separate set of atomic NNs to fit the atomic charges as a
            function of the chemical environment.
        * **`2`:**
        :    The atomic charges are obtained as a second output node of the
             short range atomic NNs. **This is not yet implemented.**
        * **`3`:**
        :   Element-specific fixed charges are used that are specified in the
            [`input.nn`](/runner/reference/files/#inputnn)
            file by the keyword 
            [`#!runner-config fixed_charge`](/runner/reference/keywords/#fixed_charge).
        * **`4`:**
        :   The charges are fixed but can be different for each atom in the
            system. They are specified in the file `charges.in`.

    [:material-code-tags: **Source variable** `nn_type_elec`](https://gitlab.com/TheochemGoettingen/RuNNer/-/blob/master/src-devel/nnflags.f90#L32)

---

### `element_activation_electrostatic` +

:material-chart-bell-curve-cumulative: **Mode 1:** :material-close:{ .no } **•** 
:custom-neural-network:                **Mode 2:** :material-check:{ .yes } **•** 
:material-head-cog-outline:            **Mode 3:** :material-check:{ .yes }

Set the activation function for a specified node of a specified element in the
electrostatic NN. The default is set by the keyword
[`#!runner-config global_activation_electrostatic`](/runner/reference/keywords/#global_activation_electrostatic).

!!! Abstract "**Format:** `#!runner-config element_activation_electrostatic element layer node type`"

    **`#!runner-config element`: (`character`)**
    :   The periodic table symbol of the element whose atomic NN the activation
        function shall be applied to. 
        
    **`#!runner-config layer`: (`integer`)**
    :   The number of the layer of the target node. 
    
    **`#!runner-config node`: (`integer`)**
    :   The number of the target node in layer `#!runner-config layer`.
    
    **`#!runner-config type`: (`character`)**
    :   The kind of activation function. Options are listed under
        [`#!runner-config global_activation_short`](/runner/reference/keywords/#global_activation_short).
        
    [:material-code-tags: **Source array** actfunc_elec(maxnodes_elec, maxnum_layers_elec, nelem)](https://gitlab.com/TheochemGoettingen/RuNNer/-/blob/master/src-devel/nnewald.f90#L36)

#### Example

The line

```runner-config
element_activation_electrostatic Zn 2 1 s
``` 

will set the activation function for the Zn electrostatic NN for node 1 in layer
2 to a sigmoid function.

---

### `element_activation_pair` 

:material-chart-bell-curve-cumulative: **Mode 1:** :material-close:{ .no } **•** 
:custom-neural-network:                **Mode 2:** :material-check:{ .yes } **•** 
:material-head-cog-outline:            **Mode 3:** :material-check:{ .yes }

Set the activation function for a specified node of a specified element pair in 
the pairwise NN. The default is set by the keyword
[`#!runner-config global_activation_pair`](/runner/reference/keywords/#global_activation_pair).

!!! Abstract "**Format:** `#!runner-config element_activation_pair element element layer node type`"

    **`#!runner-config element`: (`character`)**
    :   The periodic table symbol of the first pair element whose 
        short-range pair NN the activation function shall be applied to. 
        
    **`#!runner-config element`: (`character`)**
    :   The periodic table symbol of the second element in the pair whose 
        short-range pair NN the activation function shall be applied to. 
        
    **`#!runner-config layer`: (`integer`)**
    :   The number of the layer of the target node. 
    
    **`#!runner-config node`: (`integer`)**
    :   The number of the target node in layer `#!runner-config layer`.
    
    **`#!runner-config type`: (`character`)**
    :   The kind of activation function. Options are listed under
        [`#!runner-config global_activation_short`](/runner/reference/keywords/#global_activation_short).
        
    [:material-code-tags: **Source array** actfunc_short_pair(maxnodes_short_pair,maxnum_layers_short_pair,npairs)](https://gitlab.com/TheochemGoettingen/RuNNer/-/blob/master/src-devel/nnshort_pair.f90#L36)

#### Example

The line

```runner-config
element_activation_pair Zn Zn 2 1 s
``` 

will set the activation function for the Zn Zn short-range pair NN for node 
1 in layer 2 to a sigmoid function.

!!! warning "Hint"
    This keyword only takes effect if 
    [`#!runner-config nn_type_short`](/runner/reference/keywords/#nn_type_short) 
    is set to 2.

---

### `element_activation_short` +

:material-chart-bell-curve-cumulative: **Mode 1:** :material-close:{ .no } **•** 
:custom-neural-network:                **Mode 2:** :material-check:{ .yes } **•** 
:material-head-cog-outline:            **Mode 3:** :material-check:{ .yes }

Set the activation function for a specified node of a specified element in the
short range NN. The default is set by the keyword
[`#!runner-config global_activation_short`](/runner/reference/keywords/#global_activation_short).

!!! Abstract "**Format:** `#!runner-config element_activation_short element layer node type`"

    **`#!runner-config element`: (`character`)**
    :   The periodic table symbol of the element whose atomic NN the activation
        function shall be applied to. 
        
    **`#!runner-config layer`: (`integer`)**
    :   The number of the layer of the target node. 
    
    **`#!runner-config node`: (`integer`)**
    :   The number of the target node in layer `#!runner-config layer`.
    
    **`#!runner-config type`: (`character`)**
    :   The kind of activation function. Options are listed under
        [`#!runner-config global_activation_short`](/runner/reference/keywords/#global_activation_short).
        
    [:material-code-tags: **Source array** actfunc_short(maxnodes_short, maxnum_layers_short, nelem)](https://gitlab.com/TheochemGoettingen/RuNNer/-/blob/master/src-devel/nnshort_atomic.f90#L36)

#### Example

The line

```runner-config
element_activation_electrostatic Zn 2 1 s
``` 

will set the activation function for the Zn short-range NN for node 1 in layer
2 to a sigmoid function.

---

### `element_decoupled_forces_v2` +

:material-chart-bell-curve-cumulative: **Mode 1:** :material-close:{ .no } **•** 
:custom-neural-network:                **Mode 2:** :material-check:{ .yes } **•** 
:material-head-cog-outline:            **Mode 3:** :material-close:{ .no }

This is a more sophisticated version of the element decoupled Kalman filter for
force fitting (switched on by the keyword
[`#!runner-config element_decoupled_kalman`](/runner/reference/keywords/#element_decoupled_kalman)).

!!! Abstract "**Format:** `#!runner-config element_decoupled_forces_v2` (`logical`, default: `False`)"
    
    This keyword does not have any further options.
    
    [:material-code-tags: **Source variable** `ledforcesv2`](https://gitlab.com/TheochemGoettingen/RuNNer/-/blob/master/src-devel/fittingoptions.f90#L193)

---

### `element_decoupled_kalman` +

:material-chart-bell-curve-cumulative: **Mode 1:** :material-close:{ .no } **•** 
:custom-neural-network:                **Mode 2:** :material-check:{ .yes } **•** 
:material-head-cog-outline:            **Mode 3:** :material-close:{ .no }

Use the element decoupled Kalman filter for the short range energy and force
update (if [`#!runner-config use_short_forces`](/runner/reference/keywords/#use_short_forces)
is switched on). This is implemented only for the atomic energy case. A more
sophisticated algorithm for the force fitting can be activated by using
additionally the keyword 
[`#!runner-config element_decoupled_forces_v2`](/runner/reference/keywords/#element_decoupled_forces_v2). 
One important parameter for force fitting is 
[`#!runner-config force_update_scaling`](/runner/reference/keywords/#force_update_scaling), 
which determines the magnitude of the force update compared to the energy update.
Usually 1.0 is a good default value.

!!! Abstract "**Format:** `#!runner-config element_decoupled_kalman` (`logical`, default: `False`)"
    
    This keyword does not have any further options.
    
    [:material-code-tags: **Source variable** `luseedkalman`](https://gitlab.com/TheochemGoettingen/RuNNer/-/blob/master/src-devel/fittingoptions.f90#L191)

---

### `element_hidden_layers_electrostatic` +

:material-chart-bell-curve-cumulative: **Mode 1:** :material-close:{ .no } **•** 
:custom-neural-network:                **Mode 2:** :material-check:{ .yes } **•** 
:material-head-cog-outline:            **Mode 3:** :material-check:{ .yes }

Overwrite the global default number of hidden layers given by
[`#!runner-config global_hidden_layers_electrostatic`](/runner/reference/keywords/#global_hidden_layers_electrostatic)
for a specific element. Just a reduction of the number of hidden layers is
possible.
.

!!! Abstract "**Format:** `#!runner-config element_hidden_layers_electrostatic element layers`"

    **`#!runner-config element`: (`character`)**
    :   The periodic table symbol of the element whose atomic NN hidden layer
        number will be set.
        
    **`#!runner-config layers`: (`integer`)**
    :   The number of hidden layers for this element.
        
    [:material-code-tags: **Source array** `num_layers_elec(nelem)`](https://gitlab.com/TheochemGoettingen/RuNNer/-/blob/master/src-devel/nnewald.f90#L24)

#### Example

```runner-config
element_hidden_layers_electrostatic Zn 1
```

will reduce the number of hidden layers for the electrostatic NN of the element
Zn to 1.

---

### `element_hidden_layers_pair` 

:material-chart-bell-curve-cumulative: **Mode 1:** :material-close:{ .no } **•** 
:custom-neural-network:                **Mode 2:** :material-check:{ .yes } **•** 
:material-head-cog-outline:            **Mode 3:** :material-check:{ .yes }

Overwrite the global default number of hidden layers given by
[`#!runner-config global_hidden_layers_pair`](/runner/reference/keywords/#global_hidden_layers_pair)
for a specific element. Just a reduction of the number of hidden layers is
possible.
.

!!! Abstract "**Format:** `#!runner-config element_hidden_layers_pair element element layers`"

    **`#!runner-config element`: (`character`)**
    :   The periodic table symbol of the first pair element whose short-range 
    pair NN hidden layer number will be set.
    
    **`#!runner-config element`: (`character`)**
    :   The periodic table symbol of the second pair element whose short-range 
    pair NN hidden layer number will be set.
        
    **`#!runner-config layers`: (`integer`)**
    :   The number of hidden layers for this element pair.
        
    [:material-code-tags: **Source array** `num_layers_short_pair(npairs)`](https://gitlab.com/TheochemGoettingen/RuNNer/-/blob/master/src-devel/nnshort_pair.f90#L24)

#### Example

```runner-config
element_hidden_layers_pair Zn Zn 1
```

will reduce the number of hidden layers for the short-range pair NN of the 
element Zn Zn to 1.

---

### `element_hidden_layers_short` +

:material-chart-bell-curve-cumulative: **Mode 1:** :material-close:{ .no } **•** 
:custom-neural-network:                **Mode 2:** :material-check:{ .yes } **•** 
:material-head-cog-outline:            **Mode 3:** :material-check:{ .yes }

Overwrite the global default number of hidden layers given by
[`#!runner-config global_hidden_layers_short`](/runner/reference/keywords/#global_hidden_layers_short)
for a specific element. Just a reduction of the number of hidden layers is
possible.

!!! Abstract "**Format:** `#!runner-config element_hidden_layers_short element layers`"

    **`#!runner-config element`: (`character`)**
    :   The periodic table symbol of the element whose atomic NN hidden layer
        number will be set.
        
    **`#!runner-config layers`: (`integer`)**
    :   The number of hidden layers for this element.
        
    [:material-code-tags: **Source array** `num_layers_short_atomic(nelem)`](https://gitlab.com/TheochemGoettingen/RuNNer/-/blob/master/src-devel/nnshort_atomic.f90#L24)

#### Example

```runner-config
element_hidden_layers_short Zn 1
```

will reduce the number of hidden layers for the short-range NN of the element
Zn to 1.

---

### `element_nodes_electrostatic` +

:material-chart-bell-curve-cumulative: **Mode 1:** :material-close:{ .no } **•** 
:custom-neural-network:                **Mode 2:** :material-check:{ .yes } **•** 
:material-head-cog-outline:            **Mode 3:** :material-check:{ .yes }

Overwrite the global default number of nodes in the specified hidden layer of an
elecrostatic NN given by 
[`#!runner-config global_nodes_electrostatic`](/runner/reference/keywords/#global_nodes_electrostatic) 
for a specific element. 
Just a reduction of the number of nodes is possible. 

!!! Abstract "**Format:** `#!runner-config element_nodes_electrostatic element layer i0`"
    
    **`#!runner-config element`: (`character`)**
    :   The periodic table symbol of the element.
    
    **`#!runner-config layer`: (`integer`)**
    :   The number of the hidden layer for which the number of nodes is set.
    
    **`#!runner-config i0`: (`integer`, default: [`#!runner-config global_nodes_electrostatic`](/runner/reference/keywords/#global_nodes_electrostatic))**
    :   The number of nodes to be set.

    [:material-code-tags: **Source array** `nodes_elec(0:maxnum_layerselec, nelem)`](https://gitlab.com/TheochemGoettingen/RuNNer/-/blob/master/src-devel/nnewald.f90#L25)

#### Example

```runner-config
element_nodes_electrostatic Zn 1 9
``` 

will reduce the number of nodes in hidden layer 1 of the electrostatic NN for 
the element Zn to 9.

---

### `element_nodes_pair` 

:material-chart-bell-curve-cumulative: **Mode 1:** :material-close:{ .no } **•** 
:custom-neural-network:                **Mode 2:** :material-check:{ .yes } **•** 
:material-head-cog-outline:            **Mode 3:** :material-check:{ .yes }

Overwrite the global default number of nodes in the specified hidden layer of a
pair NN given by 
[`#!runner-config global_nodes_pair`](/runner/reference/keywords/#global_nodes_pair) 
for a specific element. 
Just a reduction of the number of nodes is possible. 

!!! Abstract "**Format:** `#!runner-config element_nodes_pair element element layer i0`"
    
    **`#!runner-config element`: (`character`)**
    :   The periodic table symbol of the first pair element.
    
    **`#!runner-config element`: (`character`)**
    :   The periodic table symbol of the second pair element.
    
    **`#!runner-config layer`: (`integer`)**
    :   The number of the hidden layer for which the number of nodes is set.
    
    **`#!runner-config i0`: (`integer`, default: [`#!runner-config global_nodes_pair`](/runner/reference/keywords/#global_nodes_pair))**
    :   The number of nodes to be set.

    [:material-code-tags: **Source array** `nodes_short_pair(0:maxnum_layerspair, nelem)`](https://gitlab.com/TheochemGoettingen/RuNNer/-/blob/master/src-devel/nnshort_pair.f90#L25)

#### Example

```runner-config
element_nodes_pair Zn Zn 1 9
``` 

will reduce the number of nodes in hidden layer 1 of the pair NN for 
the element pair Zn Zn to 9.

---

### `element_nodes_short` +

:material-chart-bell-curve-cumulative: **Mode 1:** :material-close:{ .no } **•** 
:custom-neural-network:                **Mode 2:** :material-check:{ .yes } **•** 
:material-head-cog-outline:            **Mode 3:** :material-check:{ .yes }

Overwrite the global default number of nodes in the specified hidden layer of an
short-range atomic NN given by 
[`#!runner-config global_nodes_short`](/runner/reference/keywords/#global_nodes_short) 
for a specific element. 
Just a reduction of the number of nodes is possible. 

!!! Abstract "**Format:** `#!runner-config element_nodes_short element layer i0`"
    
    **`#!runner-config element`: (`character`)**
    :   The periodic table symbol of the element.
    
    **`#!runner-config layer`: (`integer`)**
    :   The number of the hidden layer for which the number of nodes is set.
    
    **`#!runner-config i0`: (`integer`, default: [`#!runner-config global_nodes_short`](/runner/reference/keywords/#global_nodes_short))**
    :   The number of nodes to be set.

    [:material-code-tags: **Source array** `nodes_short(0:maxnum_layersshort, nelem)`](https://gitlab.com/TheochemGoettingen/RuNNer/-/blob/master/src-devel/nnshort_atomic.f90#L25)

#### Example

```runner-config
element_nodes_short Zn 1 9
``` 

will reduce the number of nodes in hidden layer 1 of the short-range atomic NN 
for the element Zn to 9.

---

### `element_pairsymfunction_short` 

:material-chart-bell-curve-cumulative: **Mode 1:** :material-check:{ .yes } **•** 
:custom-neural-network:                **Mode 2:** :material-check:{ .yes } **•** 
:material-head-cog-outline:            **Mode 3:** :material-check:{ .yes }

Set the symmetry functions for one element pair for the short-range pair NN. 
The variables are the same as for the keyword 
[`#!runner-config global_pairsymfunction_short`](/runner/reference/keywords/#global_pairsymfunction_short)
and are explained in more detail there.

!!! Abstract "**Format:** `#!runner-config element_pairsymfunction_short element element type [parameters] cutoff`"
    
    **`#!runner-config element`: (`character`)**
    :   The periodic table symbol of the first pair element.
    
    **`#!runner-config element`: (`character`)**
    :   The periodic table symbol of the second pair element.
    
    **`#!runner-config type [parameters]`: (`integer [reals]`)**
    :   The type of symmetry function to be used. Different `parameters` have to
        be set depending on the choice of `#!runner-config type`. They are 
        explained under
        [`#!runner-config global_symfunction_short`](/runner/reference/keywords/#global_symfunction_short).

    **`#!runner-config cutoff`: (`real`, default: `None`)**
    :   The symmetry function cutoff radius (unit: Bohr).

    [:material-code-tags: **Read in** `readsymfunctionelementpair(...)`](https://gitlab.com/TheochemGoettingen/RuNNer/-/blob/master/src-devel/readsymfunctionelementpair.f90)

---

### `elements` ++

:material-chart-bell-curve-cumulative: **Mode 1:** :material-check:{ .yes } **•** 
:custom-neural-network:                **Mode 2:** :material-check:{ .yes } **•** 
:material-head-cog-outline:            **Mode 3:** :material-check:{ .yes }

The element symbols of all elements in the system in arbitrary order. 
The number of specified elements must fit to the value of the keyword
[`#!runner-config number_of_elements`](/runner/reference/keywords/#number_of_elements).

!!! Abstract "**Format:** `#!runner-config elements element [element...]`"
    
    **`#!runner-config element [element...]`: (`character [characters]`)**
    :   The periodic table symbols of all the element in the system.

    [:material-code-tags: **Source array** `element(nelem)`](https://gitlab.com/TheochemGoettingen/RuNNer/-/blob/master/src-devel/globaloptions.f90#L174)

---

### `element_symfunction_electrostatic` +

:material-chart-bell-curve-cumulative: **Mode 1:** :material-check:{ .yes } **•** 
:custom-neural-network:                **Mode 2:** :material-check:{ .yes } **•** 
:material-head-cog-outline:            **Mode 3:** :material-check:{ .yes }

Set the symmetry functions for one element with all possible neighbor element
combinations for the electrostatics NN. The variables are the same as for the 
keyword 
[`#!runner-config global_symfunction_electrostatic`](/runner/reference/keywords/#global_symfunction_electrostatic)
and are explained in more detail there.

!!! Abstract "**Format:** `#!runner-config element_symfunction_electrostatic element type [parameters] cutoff`"
    
    **`#!runner-config element`: (`character`)**
    :   The periodic table symbol of the element.
    
    **`#!runner-config type [parameters]`: (`integer [reals]`)**
    :   The type of symmetry function to be used. Different `parameters` have to
        be set depending on the choice of `#!runner-config type`. They are 
        explained under
        [`#!runner-config global_symfunction_short`](/runner/reference/keywords/#global_symfunction_short).

    **`#!runner-config cutoff`: (`real`, default: `None`)**
    :   The symmetry function cutoff radius (unit: Bohr).

    [:material-code-tags: **Read in** `readsymfunctionelementatomic(...)`](https://gitlab.com/TheochemGoettingen/RuNNer/-/blob/master/src-devel/readsymfunctionelementatomic.f90)

---

### `element_symfunction_short` +

:material-chart-bell-curve-cumulative: **Mode 1:** :material-check:{ .yes } **•** 
:custom-neural-network:                **Mode 2:** :material-check:{ .yes } **•** 
:material-head-cog-outline:            **Mode 3:** :material-check:{ .yes }

Set the symmetry functions for one element with all possible neighbor element
combinations for the short-range NN. The variables are the same as for the 
keyword 
[`#!runner-config global_symfunction_short`](/runner/reference/keywords/#global_symfunction_short)
and are explained in more detail there.

!!! Abstract "**Format:** `#!runner-config element_symfunction_short element type [parameters] cutoff`"
    
    **`#!runner-config element`: (`character`)**
    :   The periodic table symbol of the element.
    
    **`#!runner-config type [parameters]`: (`integer [reals]`)**
    :   The type of symmetry function to be used. Different `parameters` have to
        be set depending on the choice of `#!runner-config type`. They are 
        explained under
        [`#!runner-config global_symfunction_short`](/runner/reference/keywords/#global_symfunction_short).
                  
    **`#!runner-config cutoff`: (`real`, default: `None`)**
    :   The symmetry function cutoff radius (unit: Bohr).

    [:material-code-tags: **Read in** `readsymfunctionelementatomic(...)`](https://gitlab.com/TheochemGoettingen/RuNNer/-/blob/master/src-devel/readsymfunctionelementatomic.f90)

---

### `enable_on_the_fly_input` 

:material-chart-bell-curve-cumulative: **Mode 1:** :material-close:{ .no } **•** 
:custom-neural-network:                **Mode 2:** :material-check:{ .yes } **•** 
:material-head-cog-outline:            **Mode 3:** :material-close:{ .no }

Read modified [`input.nn`](/runner/reference/files/#inputnn) parameters during
the fitting procedure from a file labeled `input.otf`.

!!! Abstract "**Format:** `#!runner-config enable_on_the_fly_input` (`logical`, default: `False`)"
    
    This keyword does not have any further options.
    
    [:material-code-tags: **Source variable** `lenableontheflyinput`](https://gitlab.com/TheochemGoettingen/RuNNer/-/blob/master/src-devel/fittingoptions.f90#L186)

---

### `energy_threshold` ++

:material-chart-bell-curve-cumulative: **Mode 1:** :material-check:{ .yes } **•** 
:custom-neural-network:                **Mode 2:** :material-close:{ .no } **•** 
:material-head-cog-outline:            **Mode 3:** :material-close:{ .no }

Set an energy threshold for fitting data. This keyword is only used in 
[`#!runner-config runner_mode`](/runner/reference/keywords/#runner_mode) 1
for the decision if a point should be used in the training or 
test set or if it should be eliminated because of its high energy.

!!! Abstract "**Format:** `#!runner-config energy_threshold a0`"
    
    **`#!runner-config a0`: (`real`, default: `0.0`)**
    :   Threshold for the total energy of a structure (unit: Hartree per atom).

    :material-code-tags: **Source variables** [`lfitethres`](https://gitlab.com/TheochemGoettingen/RuNNer/-/blob/master/src-devel/mode1options.f90#L29) and [`fitethres`](https://gitlab.com/TheochemGoettingen/RuNNer/-/blob/master/src-devel/mode1options.f90#L25)

---

### `enforce_max_num_neighbors_atomic` 

:material-chart-bell-curve-cumulative: **Mode 1:** :material-check:{ .yes } **•** 
:custom-neural-network:                **Mode 2:** :material-check:{ .yes } **•** 
:material-head-cog-outline:            **Mode 3:** :material-check:{ .yes }

Set an upper threshold for the number of neighbors an atom can have.

!!! Abstract "**Format:** `#!runner-config enforce_max_num_neighbors_atomic i0`"
    
    **`#!runner-config i0`: (`integer`, default: `None`)**
    :   Maximum number of neighbors for one atom.

    :material-code-tags: **Source variables** [`lenforcemaxnumneighborsatomic`](https://gitlab.com/TheochemGoettingen/RuNNer/-/blob/master/src-devel/globaloptions.f90#L140) and [`max_num_neighbors_atomic_input`](https://gitlab.com/TheochemGoettingen/RuNNer/-/blob/master/src-devel/globaloptions.f90#L73)

---

### `enforce_totcharge` ++

:material-chart-bell-curve-cumulative: **Mode 1:** :material-close:{ .no } **•** 
:custom-neural-network:                **Mode 2:** :material-close:{ .no } **•** 
:material-head-cog-outline:            **Mode 3:** :material-check:{ .yes }

Rescale the NN atomic charges to get a neutral system. An overall neutral system
is required for a correct calculation of the Ewald sum for periodic systems. 
The additional error introduced by rescaling the NN charges is typically much
smaller than the fitting error, but this should be checked.

!!! Abstract "**Format:** `#!runner-config enforce_totcharge i0`"
    
    **`#!runner-config i0`: (`integer`, default: `0`)**
    :   Switch charge rescaling on (`1`) or off (`0`).

    [:material-code-tags: **Source [variable]** `enforcetotcharge`](https://gitlab.com/TheochemGoettingen/RuNNer/-/blob/master/src-devel/predictionoptions.f90#L24) 

---

### `environment_analysis` 

:material-chart-bell-curve-cumulative: **Mode 1:** :material-close:{ .no } **•** 
:custom-neural-network:                **Mode 2:** :material-check:{ .yes } **•** 
:material-head-cog-outline:            **Mode 3:** :material-close:{ .no }

Print a detailed analysis of the atomic environments in 
`trainstruct.data` and `teststruct.data`.

!!! Abstract "**Format:** `#!runner-config environment_analysis` (`logical`, default: `False`)"
    
    This keyword does not have any further options.
    
    [:material-code-tags: **Source variable** `lenvironmentanalysis`](https://gitlab.com/TheochemGoettingen/RuNNer/-/blob/master/src-devel/globaloptions.f90#L171)

---

### `epochs` ++

:material-chart-bell-curve-cumulative: **Mode 1:** :material-close:{ .no } **•** 
:custom-neural-network:                **Mode 2:** :material-check:{ .yes } **•** 
:material-head-cog-outline:            **Mode 3:** :material-close:{ .no }

The number of epochs for fitting. If `0` is specified, `RuNNer` will calculate
the error and terminate without adjusting weights.

!!! Abstract "**Format:** `#!runner-config epochs i0`"
    
    **`#!runner-config i0`: (`integer`, default: `0`)**
    :   Number of epochs.

    [:material-code-tags: **Source variable** `nepochs`](https://gitlab.com/TheochemGoettingen/RuNNer/-/blob/master/src-devel/fittingoptions.f90#L32) 

---

### `ewald_alpha` ++

:material-chart-bell-curve-cumulative: **Mode 1:** :material-close:{ .no } **•** 
:custom-neural-network:                **Mode 2:** :material-check:{ .yes } **•** 
:material-head-cog-outline:            **Mode 3:** :material-check:{ .yes }

Parameter $\alpha$ for the Ewald summation. Determines the accuracy of the 
electrostatic energy and force evaluation for periodic systems together with 
[`#!runner-config ewald_kmax`](/runner/reference/keywords/#ewald_kmax) and 
[`#!runner-config ewald_cutoff`](/runner/reference/keywords/#ewald_cutoff).
Recommended settings are 
([`#!runner-config ewald_alpha`](/runner/reference/keywords/#ewald_alpha) = 0.2 and 
[`#!runner-config ewald_kmax`](/runner/reference/keywords/#ewald_kmax) = 10) or
([`#!runner-config ewald_alpha`](/runner/reference/keywords/#ewald_alpha) = 0.5 and 
[`#!runner-config ewald_kmax`](/runner/reference/keywords/#ewald_kmax) > 20 ) and a 
sufficiently large
[`#!runner-config ewald_cutoff`](/runner/reference/keywords/#ewald_cutoff).

!!! Abstract "**Format:** `#!runner-config ewald_alpha a0`"
    
    **`#!runner-config a0`: (`real`, default: `0.0`)**
    :   The value of the parameter $\alpha$ for the Ewald summation.

    [:material-code-tags: **Source variable** `ewaldalpha`](https://gitlab.com/TheochemGoettingen/RuNNer/-/blob/master/src-devel/globaloptions.f90#L84) 

---

### `ewald_cutoff` ++

:material-chart-bell-curve-cumulative: **Mode 1:** :material-close:{ .no } **•** 
:custom-neural-network:                **Mode 2:** :material-check:{ .yes } **•** 
:material-head-cog-outline:            **Mode 3:** :material-check:{ .yes }

Parameter for the Ewald summation. Determines the accuracy of the 
electrostatic energy and force evaluation for periodic systems together with 
[`#!runner-config ewald_kmax`](/runner/reference/keywords/#ewald_kmax) and 
[`#!runner-config ewald_alpha`](/runner/reference/keywords/#ewald_alpha).
Must be chosen sufficiently large because it determines the number of neighbors
taken into account in the real space part of the Ewald summation (e.g. 15.0 Bohr)

!!! Abstract "**Format:** `#!runner-config ewald_cutoff a0`"
    
    **`#!runner-config a0`: (`real`, default: `0.0`)**
    :   The cutoff radius if the Ewald summation (unit: Bohr).

    [:material-code-tags: **Source variable** `ewaldcutoff`](https://gitlab.com/TheochemGoettingen/RuNNer/-/blob/master/src-devel/globaloptions.f90#L85) 

---

### `ewald_kmax` ++

:material-chart-bell-curve-cumulative: **Mode 1:** :material-close:{ .no } **•** 
:custom-neural-network:                **Mode 2:** :material-check:{ .yes } **•** 
:material-head-cog-outline:            **Mode 3:** :material-check:{ .yes }

Parameter for the reciprocal space part of the Ewald summation.
Determines the accuracy of the electrostatic energy and force evaluation for 
periodic systems together with 
[`#!runner-config ewald_alpha`](/runner/reference/keywords/#ewald_alpha) and 
[`#!runner-config ewald_cutoff`](/runner/reference/keywords/#ewald_cutoff).
Recommended settings are 
([`#!runner-config ewald_alpha`](/runner/reference/keywords/#ewald_alpha) = 0.2 and 
[`#!runner-config ewald_kmax`](/runner/reference/keywords/#ewald_kmax) = 10) or
([`#!runner-config ewald_alpha`](/runner/reference/keywords/#ewald_alpha) = 0.5 and 
[`#!runner-config ewald_kmax`](/runner/reference/keywords/#ewald_kmax) > 20 ) and a 
sufficiently large
[`#!runner-config ewald_cutoff`](/runner/reference/keywords/#ewald_cutoff).

!!! Abstract "**Format:** `#!runner-config ewald_kmax i0`"
    
    **`#!runner-config i0`: (`integer`, default: `0`)**
    :   k-space cutoff for the Ewald summation.

    [:material-code-tags: **Source variable** `ewaldkmax`](https://gitlab.com/TheochemGoettingen/RuNNer/-/blob/master/src-devel/globaloptions.f90#L65) 

---

### `ewald_prec` ++

:material-chart-bell-curve-cumulative: **Mode 1:** :material-close:{ .no } **•** 
:custom-neural-network:                **Mode 2:** :material-check:{ .yes } **•** 
:material-head-cog-outline:            **Mode 3:** :material-check:{ .yes }

This parameter determines the error tolerance in electrostatic energy and force 
evaluation for periodic systems when Ewald Summation is used. `RuNNer` will 
automatically choose the optimized 
[`#!runner-config ewald_alpha`](/runner/reference/keywords/#ewald_alpha),
[`#!runner-config ewald_kmax`](/runner/reference/keywords/#ewald_kmax), and 
[`#!runner-config ewald_cutoff`](/runner/reference/keywords/#ewald_cutoff).

!!! Abstract "**Format:** `#!runner-config ewald_prec a0`"
    
    **`#!runner-config a0`: (`real`, default: `-1.0`)**
    :   The desired precision of the Ewald summation. Recommended values are 
        $10^{-5}$ to $10^{-6}$.

    [:material-code-tags: **Source variable** `ewaldprec`](https://gitlab.com/TheochemGoettingen/RuNNer/-/blob/master/src-devel/globaloptions.f90#L87)

!!! warning "Hint"
    This keyword is mandatory if 3G- and 4G-HDNNPs are used.

---

### `find_contradictions` +

:material-chart-bell-curve-cumulative: **Mode 1:** :material-close:{ .no } **•** 
:custom-neural-network:                **Mode 2:** :material-check:{ .yes } **•** 
:material-head-cog-outline:            **Mode 3:** :material-close:{ .no }

This keyword can be used in 
[`#!runner-config runner_mode`](/runner/reference/keywords/#runner_mode) 2
to test if the symmetry functions are able to distinguish different atomic
environments sufficiently. If two atomic environments of a given element are
very similar, they will result in very similar symmetry function vectors.
Therefore, the length of the difference vector

$$
\Delta G = \sqrt{\sum_{i=1}^N (G_{i,1}-G_{i,2})^2} \,,\notag
$$

($N$ runs over all individual symmetry functions) will be close to zero. If the
environments are really similar, the absolute forces acting on the atom
should be similar as well, which is measured by

$$
\begin{align}
\Delta F &= |\sqrt{F_{1,x}^2+F_{1,y}^2+F_{1,z}^2}
           -\sqrt{F_{2,x}^2+F_{2,y}^2+F_{2,z}^2}|\,,\notag\\
         &= |F_1-F_2| \notag\,.
\end{align}
$$

If the forces are different ($\Delta F >$ `a1`) but the symmetry
functions similar ($\Delta G <$ `a0`) for an atom pair, a
message will be printed in the output file. The optimal choices for
`a0` and `a1` are system dependent and should be selected such that only the
most contradictory data is found. It is not recommended to keep this keyword
switched on routinely, because it requires substantial CPU time.

!!! Abstract "**Format:** `#!runner-config find_contradictions a0 a1`"
    
    **`#!runner-config a0`: (`real`, default: `0.0`)**
    :   Symmetry function threshold $\Delta G$.
    
    **`#!runner-config a1`: (`real`, default: `0.0`)**
    :   Force threshold $\Delta F$.    

    :material-code-tags: **Source variables** [`lfindcontradiction`](https://gitlab.com/TheochemGoettingen/RuNNer/-/blob/master/src-devel/fittingoptions.f90#L181),
    [`deltagthres`](https://gitlab.com/TheochemGoettingen/RuNNer/-/blob/master/src-devel/fittingoptions.f90#L111), and
    [`deltafthres`](https://gitlab.com/TheochemGoettingen/RuNNer/-/blob/master/src-devel/fittingoptions.f90#L112)

---

### `fitmode` --

Request batch fitting for the short-range atomic NN. This keyword is not well
tested and has been deprecated.

---

### `fitting_unit` ++

:material-chart-bell-curve-cumulative: **Mode 1:** :material-close:{ .no } **•** 
:custom-neural-network:                **Mode 2:** :material-check:{ .yes } **•** 
:material-head-cog-outline:            **Mode 3:** :material-close:{ .no }

Set the energy unit that is printed to the output files during training in 
[`#!runner-config runner_mode`](/runner/reference/keywords/#runner_mode) 2.

!!! Abstract "**Format:** `#!runner-config fitting_unit i0`"
    
    **`#!runner-config i0`: (`integer`, default: `1`)**
    :   Switch for different energy units. Can be one of:
    
        * **`1`:** 
        :   Unit: `eV`. The energy RMSE and MAD in the output file are given in
            eV/atom, the force error is given in eV/Bohr.
        * **`2`:**
        :   Unit: `Ha`. The energy RMSE and MAD in the output file are given in
            Ha/atom, the force error is given in Ha/Bohr.

    [:material-code-tags: **Source variable** `fitting_unit`](https://gitlab.com/TheochemGoettingen/RuNNer/-/blob/master/src-devel/fittingoptions.f90#L35) 

---

### `fix_weights` *

:material-chart-bell-curve-cumulative: **Mode 1:** :material-close:{ .no } **•** 
:custom-neural-network:                **Mode 2:** :material-check:{ .yes } **•** 
:material-head-cog-outline:            **Mode 3:** :material-close:{ .no }

Do not optimize all weights, but freeze some weights, which are
specified by the keywords 
[`#!runner-config weight_constraint`](/runner/reference/keywords/#weight_constraint)
and 
[`#!runner-config weighte_constraint`](/runner/reference/keywords/#weighte_constraint).

!!! Abstract "**Format:** `#!runner-config fix_weights` (`logical`, default: `False`)"
    
    This keyword does not have any further options.
    
    [:material-code-tags: **Source variable** `lfixweights`](https://gitlab.com/TheochemGoettingen/RuNNer/-/blob/master/src-devel/fittingoptions.f90#L174)

!!! warning "Hint"
    See also 
    [`#!runner-config weight_constraint`](/runner/reference/keywords/#weight_constraint)
    and 
    [`#!runner-config weighte_constraint`](/runner/reference/keywords/#weighte_constraint).

---

### `fixed_charge` *

:material-chart-bell-curve-cumulative: **Mode 1:** :material-close:{ .no } **•** 
:custom-neural-network:                **Mode 2:** :material-check:{ .yes } **•** 
:material-head-cog-outline:            **Mode 3:** :material-check:{ .yes }

Use a fixed charge for all atoms of the specified element independent of
the chemical environment.

!!! Abstract "**Format:** `#!runner-config fixed_charge element a0`"
    
    **`#!runner-config element`: (`character`)**
    :   The periodic table symbol of the element.
    
    **`#!runner-config a0`: (`real`, default: `0.0`)**
    :   The fixed charge of all atoms of this element (unit: electron charge).

    [:material-code-tags: **Source array** `fixedcharge(nelem)`](https://gitlab.com/TheochemGoettingen/RuNNer/-/blob/master/src-devel/readsymfunctionelementpair.f90)

#### Example

```runner-config
fixed_charge Zn 2.0
```
will set the charge on all Zn atoms to 2.0. 

!!! warning "Hint"
    For using fixed charges the keyword 
    [`#!runner-config use_fixed_charges`](/runner/reference/keywords/#use_fixed_charges)
    must be set. 

---

### `fixed_gausswidth` ++

:material-chart-bell-curve-cumulative: **Mode 1:** :material-check:{ .yes } **•** 
:custom-neural-network:                **Mode 2:** :material-check:{ .yes } **•** 
:material-head-cog-outline:            **Mode 3:** :material-check:{ .yes }

This keyword specifies the Gaussian width for calculating the charges and 
electrostatic energy and forces in 4G-HDNNPs. 

!!! Abstract "**Format:** `#!runner-config fixed_gausswidth element a0`"
    
    **`#!runner-config element`: (`character`)**
    :   The periodic table symbol of the element.
    
    **`#!runner-config a0`: (`real`, default: `-99.0`)**
    :   The Gaussian width for all atoms of this element (unit: Bohr).

    [:material-code-tags: **Source array** `fixedgausswidth(nelem)`](https://gitlab.com/TheochemGoettingen/RuNNer/-/blob/master/src-devel/globaloptions.f90#L92)

#### Example

```runner-config
fixed_gausswidth Zn 2.0
```

will set the Gaussian width of all Zn atoms to 2.0 Bohr. 

### `fixed_short_energy_error_threshold` 

:material-chart-bell-curve-cumulative: **Mode 1:** :material-close:{ .no } **•** 
:custom-neural-network:                **Mode 2:** :material-check:{ .yes } **•** 
:material-head-cog-outline:            **Mode 3:** :material-close:{ .no }

Only consider points in the weight update during 
[`#!runner-config runner_mode`](/runner/reference/keywords/#runner_mode) 2 for which the 
absolute error of the total energy is higher than 
[`#!runner-config fixed_short_energy_error_threshold`](/runner/reference/keywords/#fixed_short_energy_error_threshold).

!!! Abstract "**Format:** `#!runner-config fixed_short_energy_error_threshold a0`"
    
    **`#!runner-config a0`: (`real`, default: `0.0`)**
    :   The lower threshold for the absolute error of the total energy. 

    :material-code-tags: **Source variables** [`lfixederrore`](https://gitlab.com/TheochemGoettingen/RuNNer/-/blob/master/src-devel/fittingoptions.f90#L149)
    and
    [`fixederrore`](https://gitlab.com/TheochemGoettingen/RuNNer/-/blob/master/src-devel/fittingoptions.f90#L77)

---

### `fixed_short_force_error_threshold` 

:material-chart-bell-curve-cumulative: **Mode 1:** :material-close:{ .no } **•** 
:custom-neural-network:                **Mode 2:** :material-check:{ .yes } **•** 
:material-head-cog-outline:            **Mode 3:** :material-close:{ .no }

Only consider points in the weight update during 
[`#!runner-config runner_mode`](/runner/reference/keywords/#runner_mode) 2 for which the 
absolute error of the total force is higher than 
[`#!runner-config fixed_short_force_error_threshold`](/runner/reference/keywords/#fixed_short_force_error_threshold).

!!! Abstract "**Format:** `#!runner-config fixed_short_force_error_threshold a0`"
    
    **`#!runner-config a0`: (`real`, default: `0.0`)**
    :   The lower threshold for the absolute error of the total force. 

    :material-code-tags: **Source variables** [`lfixederrorf`](https://gitlab.com/TheochemGoettingen/RuNNer/-/blob/master/src-devel/fittingoptions.f90#L150)
    and
    [`fixederrorf`](https://gitlab.com/TheochemGoettingen/RuNNer/-/blob/master/src-devel/fittingoptions.f90#L78)

---

### `force_grouping_by_structure` 

:material-chart-bell-curve-cumulative: **Mode 1:** :material-close:{ .no } **•** 
:custom-neural-network:                **Mode 2:** :material-check:{ .yes } **•** 
:material-head-cog-outline:            **Mode 3:** :material-close:{ .no }

Do not update the short-range NN weights after the presentation of an 
individual atomic force vector, but average the derivatives with respect to the 
weights over the number of force vectors per structure.

!!! Abstract "**Format:** `#!runner-config force_grouping_by_structure` (`logical`, default: `False`)"
    
    This keyword does not have any further options.
    
    [:material-code-tags: **Source variable** `lfgroupbystruct`](https://gitlab.com/TheochemGoettingen/RuNNer/-/blob/master/src-devel/fittingoptions.f90#L151)
    
---

### `force_threshold` ++

:material-chart-bell-curve-cumulative: **Mode 1:** :material-check:{ .yes } **•** 
:custom-neural-network:                **Mode 2:** :material-close:{ .no } **•** 
:material-head-cog-outline:            **Mode 3:** :material-close:{ .no }

Set a force threshold for fitting data. If
any force component of a structure in the reference set is larger than
`a0` then the point is not used and eliminated from the data set. 


!!! Abstract "**Format:** `#!runner-config force_threshold a0`"
    
    **`#!runner-config a0`: (`real`, default: `0.0`)**
    :   The upper threshold for force components (unit: Ha/Bohr) 

    :material-code-tags: **Source variables** [`lfitfthres`](https://gitlab.com/TheochemGoettingen/RuNNer/-/blob/master/src-devel/mode1options.f90#L30)
    and
    [`fitfthres`](https://gitlab.com/TheochemGoettingen/RuNNer/-/blob/master/src-devel/mode1options.f90#L26)

---

### `force_update_scaling` +

:material-chart-bell-curve-cumulative: **Mode 1:** :material-close:{ .no } **•** 
:custom-neural-network:                **Mode 2:** :material-check:{ .yes } **•** 
:material-head-cog-outline:            **Mode 3:** :material-close:{ .no }

Since for a given structure the number of forces is much larger than the number
of energies, the force updates can have a dominant influence on the fits. This
can result in poor energy errors.
Using this option the relative strength of the energy and the forces can be 
adjusted. A value of 0.1 means that the influence of the energy is 10 times
stronger than of a single force. A negative value will automatically balance the
strength of the energy and of the forces by taking into account the actual
number of atoms of each structures.

!!! Abstract "**Format:** `#!runner-config force_update_scaling a0`"
    
    **`#!runner-config a0`: (`real`, default: `1.0`)**
    :   The relative strength of the energy and forces for a weight update.

    [:material-code-tags: **Source variable** `scalefactorf`](https://gitlab.com/TheochemGoettingen/RuNNer/-/blob/master/src-devel/fittingoptions.f90#L88)

---

### `global_activation_electrostatic` ++

:material-chart-bell-curve-cumulative: **Mode 1:** :material-close:{ .no } **•** 
:custom-neural-network:                **Mode 2:** :material-check:{ .yes } **•** 
:material-head-cog-outline:            **Mode 3:** :material-check:{ .yes }

Set the default activation function for each hidden layer and the output layer
in the electrostatic NNs of all elements. 

!!! Abstract "**Format:** `#!runner-config global_activation_electrostatic type [type...]`"

    **`#!runner-config type [type...]`: (`character [characters]`)**
    :   The kind of activation function. One `#!runner-config type` has to 
        be given for each layer in the NN. Options are listed under
        [`#!runner-config global_activation_short`](/runner/reference/keywords/#global_activation_short).

    [:material-code-tags: **Source array** actfunc_elec(maxnodes_elec, maxnum_layers_elec, nelem)](https://gitlab.com/TheochemGoettingen/RuNNer/-/blob/master/src-devel/nnewald.f90#L36)

!!! warning "Hint"
    Different activation functions for individual nodes can be specified using 
    the keyword 
    [`#!runner-config element_activation_electrostatic`](/runner/reference/keywords/#element_activation_electrostatic).

---

### `global_activation_pair` 

:material-chart-bell-curve-cumulative: **Mode 1:** :material-close:{ .no } **•** 
:custom-neural-network:                **Mode 2:** :material-check:{ .yes } **•** 
:material-head-cog-outline:            **Mode 3:** :material-check:{ .yes }

Set the default activation function for each hidden layer and the output layer
in the NNs of all element pairs. 

!!! Abstract "**Format:** `#!runner-config global_activation_pair type [type...]`"

    **`#!runner-config type [type...]`: (`character [characters]`)**
    :   The kind of activation function. One `#!runner-config type` has to 
        be given for each layer in the NN. Options are listed under
        [`#!runner-config global_activation_short`](/runner/reference/keywords/#global_activation_short).

    [:material-code-tags: **Source array** actfunc_short_pair(maxnodes_short_pair,maxnum_layers_short_pair,npairs)](https://gitlab.com/TheochemGoettingen/RuNNer/-/blob/master/src-devel/nnshort_pair.f90#L36)

!!! warning "Hint"
    This keyword is mandatory if 
    [`#!runner-config nn_type_short`](/runner/reference/keywords/#nn_type_short) 
    is set to 2.

---

### `global_activation_short` ++

:material-chart-bell-curve-cumulative: **Mode 1:** :material-close:{ .no } **•** 
:custom-neural-network:                **Mode 2:** :material-check:{ .yes } **•** 
:material-head-cog-outline:            **Mode 3:** :material-check:{ .yes }

Set the activation function for each hidden layer and the output layer
in the short range NNs of all elements. 

!!! Abstract "**Format:** `#!runner-config global_activation_short type [type...]`"

    **`#!runner-config type [type...]`: (`character [characters]`)**
    :   The kind of activation function. One `#!runner-config type` has to 
        be given for each layer in the NN. Can be one of:

        * **`c`:** 
        :   Cosine function: $\cos(x)$
        * **`e`:**
        :   Exponential function: $\exp(-x)$
        * **`g`:**
        :   Gaussian function: $\exp(-\alpha x^2)$
        * **`h`:**
        :   Harmonic function: $x^2$.
        * **`l`:**
        :   Linear function: $x$
        * **`p`:**
        :   Softplus function: $\ln(1+\exp(x))$
        * **`s`:**
        :   Sigmoid function v1: $(1-\exp(-x))^{-1}$
        * **`S`:**
        :   Sigmoid function v2: $1-(1-\exp(-x))^{-1}$
        * **`t`:**
        :   Hyperbolic tangent function: $\tanh(x)$
        
    [:material-code-tags: **Source array** actfunc_short(maxnodes_short, maxnum_layers_short, nelem)](https://gitlab.com/TheochemGoettingen/RuNNer/-/blob/master/src-devel/nnshort_atomic.f90#L36)

#### Example

For a NN with two hidden layers and one output layer three letters have to be 
given. The line

```runner-config
global_activation_short t t l
``` 

will set a hyperbolic tangent activation function for the two hidden layers and
a linear activation function for the output node.

!!! warning "Hint"
    Different activation functions for individual nodes can be specified using 
    the keyword 
    [`#!runner-config element_activation_short`](/runner/reference/keywords/#element_activation_short).

---

### `global_hidden_layers_electrostatic` ++

:material-chart-bell-curve-cumulative: **Mode 1:** :material-close:{ .no } **•** 
:custom-neural-network:                **Mode 2:** :material-check:{ .yes } **•** 
:material-head-cog-outline:            **Mode 3:** :material-check:{ .yes }

Set the default number of hidden layers in the electrostatic NNs of all 
elements. Internally 1 is added to `maxnum_layers_elec`, which also includes 
the output layer.

!!! Abstract "**Format:** `#!runner-config global_hidden_layers_electrostatic layers`"
        
    **`#!runner-config layers`: (`integer`)**
    :   The number of hidden layers.

    :material-code-tags: **Source variables** [`maxnum_layers_elec - 1`](https://gitlab.com/TheochemGoettingen/RuNNer/-/blob/master/src-devel/globaloptions.f90#L53)
    and
    [`num_layers_elec(nelem)`](https://gitlab.com/TheochemGoettingen/RuNNer/-/blob/master/src-devel/nnewald.f90#L24)

!!! warning "Hint"
    The number of hidden layers can be further modified for individual elements by the
    keyword
    [`#!runner-config element_hidden_layers_elec`](/runner/reference/keywords/#element_hidden_layers_elec).

---

### `global_hidden_layers_pair` 

:material-chart-bell-curve-cumulative: **Mode 1:** :material-close:{ .no } **•** 
:custom-neural-network:                **Mode 2:** :material-check:{ .yes } **•** 
:material-head-cog-outline:            **Mode 3:** :material-check:{ .yes }

Set the default number of hidden layers in the NNs of all element pairs. 
Internally 1 is added to `maxnum_layers_short_pair`, which also includes 
the output layer.

!!! Abstract "**Format:** `#!runner-config global_hidden_layers_pair layers`"
        
    **`#!runner-config layers`: (`integer`)**
    :   The number of hidden layers.

    :material-code-tags: **Source variables** [`maxnum_layers_short_pair - 1`](https://gitlab.com/TheochemGoettingen/RuNNer/-/blob/master/src-devel/globaloptions.f90#L57)
    and
    [`num_layers_short_pair(npairs)`](https://gitlab.com/TheochemGoettingen/RuNNer/-/blob/master/src-devel/nnshort_pair.f90#L24)

!!! warning "Hint"
    The number of hidden layers can be further modified for individual elements by the
    keyword
    [`#!runner-config element_hidden_layers_pair`](/runner/reference/keywords/#element_hidden_layers_pair).

!!! warning "Hint"
    This keyword is mandatory if 
    [`#!runner-config nn_type_short`](/runner/reference/keywords/#nn_type_short) 
    is set to 2.

---

### `global_hidden_layers_short` ++

:material-chart-bell-curve-cumulative: **Mode 1:** :material-close:{ .no } **•** 
:custom-neural-network:                **Mode 2:** :material-check:{ .yes } **•** 
:material-head-cog-outline:            **Mode 3:** :material-check:{ .yes }

Set the default number of hidden layers in the short-range NNs of all 
elements. Internally 1 is added to `maxnum_layers_short`, which also includes 
the output layer.

!!! Abstract "**Format:** `#!runner-config global_hidden_layers_short layers`"
        
    **`#!runner-config layers`: (`integer`)**
    :   The number of hidden layers.

    :material-code-tags: **Source variables** [`maxnum_layers_short - 1`](https://gitlab.com/TheochemGoettingen/RuNNer/-/blob/master/src-devel/globaloptions.f90#L49)
    and
    [`num_layers_short_atomic(nelem)`](https://gitlab.com/TheochemGoettingen/RuNNer/-/blob/master/src-devel/nnshort_atomic.f90#L24)

!!! warning "Hint"
    The number of hidden layers can be further modified for individual elements by the
    keyword
    [`#!runner-config element_hidden_layers_short`](/runner/reference/keywords/#element_hidden_layers_short).

---

### `global_nodes_electrostatic` ++

:material-chart-bell-curve-cumulative: **Mode 1:** :material-close:{ .no } **•** 
:custom-neural-network:                **Mode 2:** :material-check:{ .yes } **•** 
:material-head-cog-outline:            **Mode 3:** :material-check:{ .yes }

Set the default number of nodes in the hidden layers of the electrostatic NNs
in case of 
[`#!runner-config electrostatic_type`](/runner/reference/keywords/#electrostatic_type)
1.
In the array, the entries `1 - (maxnum_layerseelec - 1)` refer to the hidden 
layers. The first entry (0) refers to the nodes in the input layer and is 
determined automatically from the symmetry functions.

!!! Abstract "**Format:** `#!runner-config global_nodes_electrostatic i0 [i1...]`"
        
    **`#!runner-config i0 [i1...]`: (`integer [integers]`)**
    :   The number of nodes to be set in each layer.

    [:material-code-tags: **Source array** `nodes_elec(0:maxnum_layerselec, nelem)`](https://gitlab.com/TheochemGoettingen/RuNNer/-/blob/master/src-devel/nnewald.f90#L25)

---

### `global_nodes_pair` 

:material-chart-bell-curve-cumulative: **Mode 1:** :material-close:{ .no } **•** 
:custom-neural-network:                **Mode 2:** :material-check:{ .yes } **•** 
:material-head-cog-outline:            **Mode 3:** :material-check:{ .yes }

Set the default number of nodes in the hidden layers of the pairwise NNs
in case of 
[`#!runner-config nn_type_short`](/runner/reference/keywords/#nn_type_short) 2.

!!! Abstract "**Format:** `#!runner-config global_nodes_pair i0 [i1...]`"
        
    **`#!runner-config i0 [i1...]`: (`integer [integers]`)**
    :   The number of nodes to be set in each layer.

    [:material-code-tags: **Source array** `nodes_short_pair(0:maxnum_layerspair, nelem)`](https://gitlab.com/TheochemGoettingen/RuNNer/-/blob/master/src-devel/nnshort_pair.f90#L25)

---

### `global_nodes_short` ++

:material-chart-bell-curve-cumulative: **Mode 1:** :material-close:{ .no } **•** 
:custom-neural-network:                **Mode 2:** :material-check:{ .yes } **•** 
:material-head-cog-outline:            **Mode 3:** :material-check:{ .yes }

Set the default number of nodes in the hidden layers of the short-range NNs
in case of 
[`#!runner-config nn_type_short`](/runner/reference/keywords/#nn_type_short) 1.
In the array, the entries `1 - maxnum_layersshort - 1` refer to the hidden 
layers. The first entry (0) refers to the nodes in the input layer and is 
determined automatically from the symmetry functions.

!!! Abstract "**Format:** `#!runner-config global_nodes_short i0 [i1...]`"
        
    **`#!runner-config i0 [i1...]`: (`integer [integers]`)**
    :   The number of nodes to be set in each layer.

    [:material-code-tags: **Source array** `nodes_short(0:maxnum_layersshort, nelem)`](https://gitlab.com/TheochemGoettingen/RuNNer/-/blob/master/src-devel/nnshort_atomic.f90#L25)

---

### `global_output_nodes_electrostatic` 

This is an outdated keyword, which is no longer supported.

---

### `global_output_nodes_pair` 

This is an outdated keyword, which is no longer supported.

---

### `global_output_nodes_short` 

This is an outdated keyword, which is no longer supported.

---

### `global_pairsymfunction_short` 

:material-chart-bell-curve-cumulative: **Mode 1:** :material-check:{ .yes } **•** 
:custom-neural-network:                **Mode 2:** :material-check:{ .yes } **•** 
:material-head-cog-outline:            **Mode 3:** :material-check:{ .yes }

Specification of the global symmetry functions for all element pairs in the 
pairwise NN.

!!! Abstract "**Format:** `#!runner-config global_pairsymfunction_short element element type [parameters] cutoff`"
    
    **`#!runner-config element`: (`character`)**
    :   The periodic table symbol of the first pair element.

    **`#!runner-config element`: (`character`)**
    :   The periodic table symbol of the second pair element.
    
    **`#!runner-config type [parameters]`: (`integer [reals]`)**
    :   The type of symmetry function to be used. Different `parameters` have to
        be set depending on the choice of `#!runner-config type`. They are 
        explained under
        [`#!runner-config global_symfunction_short`](/runner/reference/keywords/#global_symfunction_short).

    **`#!runner-config cutoff`: (`real`, default: `None`)**
    :   The symmetry function cutoff radius (unit: Bohr).

    [:material-code-tags: **Read in** `readsymfunctionelementpair(...)`](https://gitlab.com/TheochemGoettingen/RuNNer/-/blob/master/src-devel/readsymfunctionelementpair.f90)

---

### `global_symfunction_electrostatic` ++

:material-chart-bell-curve-cumulative: **Mode 1:** :material-check:{ .yes } **•** 
:custom-neural-network:                **Mode 2:** :material-check:{ .yes } **•** 
:material-head-cog-outline:            **Mode 3:** :material-check:{ .yes }

Specification of global symmetry functions for all elements and all element 
combinations for the electrostatic NN.

!!! Abstract "**Format:** `#!runner-config global_symfunction_electrostatic type [parameters] cutoff`"
    
    **`#!runner-config type [parameters]`: (`integer [reals]`)**
    :   The type of symmetry function to be used. Different `parameters` have to
        be set depending on the choice of `#!runner-config type`. They are 
        explained under
        [`#!runner-config global_symfunction_short`](/runner/reference/keywords/#global_symfunction_short).

    **`#!runner-config cutoff`: (`real`, default: `None`)**
    :   The symmetry function cutoff radius (unit: Bohr).

    [:material-code-tags: **Read in** `readsymfunctionelementatomic(...)`](https://gitlab.com/TheochemGoettingen/RuNNer/-/blob/master/src-devel/readsymfunctionelementatomic.f90)

---

### `global_symfunction_short` ++

:material-chart-bell-curve-cumulative: **Mode 1:** :material-check:{ .yes } **•** 
:custom-neural-network:                **Mode 2:** :material-check:{ .yes } **•** 
:material-head-cog-outline:            **Mode 3:** :material-check:{ .yes }

Specification of global symmetry functions for all elements and all element 
combinations for the short-range atomic NN.

!!! Abstract "**Format:** `#!runner-config global_symfunction_short type [parameters] cutoff`"
    
    **`#!runner-config type [parameters]`: (`integer [reals]`)**
    :   The type of symmetry function to be used. The functional forms can be found [here](/runner/theory/symmetryfunctions/#code-implementation).
        Different `parameters` have to be set depending on the choice of `#!runner-config type`:
        
        * **`1`:** 
        :   Radial function. Requires no further `#!runner-config parameters`.
        * **`2`:**
        :    Radial function. Requires parameters `#!runner-config eta` and 
             `#!runner-config rshift`.
             ```runner-config
             global_symfunction_short 2 eta rshift cutoff
             ```
        * **`3`:**
        :    Angular function. Requires `#!runner-config parameters` 
             `#!runner-config eta`, `#!runner-config lambda`, and 
             `#!runner-config zeta`.
             ```runner-config
             global_symfunction_short 3 eta lambda zeta cutoff
             ```
        * **`4`:**
        :    Radial function. Requires parameter `#!runner-config eta`.
             ```runner-config
             global_symfunction_short 4 eta cutoff
             ```
        * **`5`:**
        :    Cartesian coordinate function. The parameter `#!runner-config eta` 
             will determine the coordinate axis: 
             `eta=1.0: X, eta=2.0: Y, eta=3.0: Z`.
             No `cutoff` required.
             ```runner-config
             global_symfunction_short 5 eta
             ```        
        * **`6`:**
        :    Bond length function. Requires no further `#!runner-config parameters`.
             ```runner-config
             global_symfunction_short 6 cutoff
             ```
        * **`7`:**
        :    Not implemented. 
        * **`8`:**
        :    Angular function. Requires `#!runner-config parameters` 
             `#!runner-config eta`, and `#!runner-config rshift`.
             ```runner-config
             global_symfunction_short 8 eta rshift cutoff
             ```   
        * **`9`:**
        :    Angular function. Requires `#!runner-config parameters` 
             `#!runner-config eta`.
             ```runner-config
             global_symfunction_short 9 eta cutoff
             ```                     
        * **`21`:**
        :    Radial spin-dependent function using the spin augmentation function $M^\mathrm{0^*}$. Requires parameters `#!runner-config eta` and 
             `#!runner-config rshift`.
             ```runner-config
             global_symfunction_short 21 eta rshift cutoff
             ```
        * **`22`:**
        :    Radial spin-dependent function using the spin augmentation function $M^\mathrm{+}$. Requires parameters `#!runner-config eta` and 
             `#!runner-config rshift`.
             ```runner-config
             global_symfunction_short 22 eta rshift cutoff
             ```
        * **`23`:**
        :    Radial spin-dependent function using the spin augmentation function $M^\mathrm{-}$. Requires parameters `#!runner-config eta` and 
             `#!runner-config rshift`.
             ```runner-config
             global_symfunction_short 23 eta rshift cutoff
             ```
        * **`24`:**
        :    Angular spin-dependent function using the spin augmentation function $M^\mathrm{00^*}$. Requires `#!runner-config parameters` 
             `#!runner-config eta`, `#!runner-config lambda`, and 
             `#!runner-config zeta`.
             ```runner-config
             global_symfunction_short 24 eta lambda zeta cutoff
             ```
        * **`25`:**
        :    Angular spin-dependent function using the spin augmentation function $M^\mathrm{++}$. Requires `#!runner-config parameters` 
             `#!runner-config eta`, `#!runner-config lambda`, and 
             `#!runner-config zeta`.
             ```runner-config
             global_symfunction_short 25 eta lambda zeta cutoff
             ```
        * **`26`:**
        :    Angular spin-dependent function using the spin augmentation function $M^\mathrm{--}$. Requires `#!runner-config parameters` 
             `#!runner-config eta`, `#!runner-config lambda`, and 
             `#!runner-config zeta`.
             ```runner-config
             global_symfunction_short 26 eta lambda zeta cutoff
             ```
        * **`27`:**
        :    Angular spin-dependent function using the spin augmentation function $M^\mathrm{+-}$. Requires `#!runner-config parameters` 
             `#!runner-config eta`, `#!runner-config lambda`, and 
             `#!runner-config zeta`.
             ```runner-config
             global_symfunction_short 27 eta lambda zeta cutoff
             ```
        * **`28`:**
        :    Angular spin-dependent function using the spin augmentation function $M^\mathrm{++2}$. Requires `#!runner-config parameters` 
             `#!runner-config eta`, `#!runner-config lambda`, and 
             `#!runner-config zeta`.
             ```runner-config
             global_symfunction_short 28 eta lambda zeta cutoff
             ```
        * **`29`:**
        :    Angular spin-dependent function using the spin augmentation function $M^\mathrm{++3}$. Requires `#!runner-config parameters` 
             `#!runner-config eta`, `#!runner-config lambda`, and 
             `#!runner-config zeta`.
             ```runner-config
             global_symfunction_short 29 eta lambda zeta cutoff
             ```
        * **`30`:**
        :    Angular spin-dependent function using the spin augmentation function $M^\mathrm{--2}$. Requires `#!runner-config parameters` 
             `#!runner-config eta`, `#!runner-config lambda`, and 
             `#!runner-config zeta`.
             ```runner-config
             global_symfunction_short 30 eta lambda zeta cutoff
             ```
        * **`31`:**
        :    Angular spin-dependent function using the spin augmentation function $M^\mathrm{--3}$. Requires `#!runner-config parameters` 
             `#!runner-config eta`, `#!runner-config lambda`, and 
             `#!runner-config zeta`.
             ```runner-config
             global_symfunction_short 31 eta lambda zeta cutoff
             ```
    **`#!runner-config cutoff`: (`real`, default: `None`)**
    :   The symmetry function cutoff radius (unit: Bohr).

    [:material-code-tags: **Read in** `readsymfunctionelementatomic(...)`](https://gitlab.com/TheochemGoettingen/RuNNer/-/blob/master/src-devel/readsymfunctionelementatomic.f90)

---

### `growth_mode` *

:material-chart-bell-curve-cumulative: **Mode 1:** :material-close:{ .no } **•** 
:custom-neural-network:                **Mode 2:** :material-check:{ .yes } **•** 
:material-head-cog-outline:            **Mode 3:** :material-close:{ .no }

If this keyword is used, not the full training set will be used in each epoch. 
First, only a few points will be used, and after a specified number of epochs
further points will be included and so on. 

!!! Abstract "**Format:** `#!runner-config growth_mode i0 i1`"
    
    **`#!runner-config i0`: (`integer`, default: `0`)**
    :   Number of points that will be added to the training set every 
        `#!runner-config i1` steps.
        
    **`#!runner-config i1`: (`integer`, default: `0`)**
    :   Number of steps to wait before increasing the number of training points.   

    :material-code-tags: **Source variables** [`lgrowth`](https://gitlab.com/TheochemGoettingen/RuNNer/-/blob/master/src-devel/fittingoptions.f90#L145),
    [`ngrowth`](https://gitlab.com/TheochemGoettingen/RuNNer/-/blob/master/src-devel/fittingoptions.f90#L24), and
    [`growthstep`](https://gitlab.com/TheochemGoettingen/RuNNer/-/blob/master/src-devel/fittingoptions.f90#L25),

#### Example

The line

```runner-config
growth_mode 11 6
```

will add 11 more points to the effective training set every 6 epochs until the 
full training set is used.

---

### `initialization_only` 

:material-chart-bell-curve-cumulative: **Mode 1:** :material-close:{ .no } **•** 
:custom-neural-network:                **Mode 2:** :material-check:{ .yes } **•** 
:material-head-cog-outline:            **Mode 3:** :material-close:{ .no }

With this keyword, which is active only in 
[`#!runner-config runner_mode`](/runner/reference/keywords/#runner_mode) 2,
`RuNNer` will stop 
after the initialization of the run before epoch 0, i.e. no fit will be done. 
This is meant as an automatic stop of the program in case only the analysis
carried out in the initialization of 
[`#!runner-config runner_mode`](/runner/reference/keywords/#runner_mode) 2 
is of interest.

!!! Abstract "**Format:** `#!runner-config initialization_only` (`logical`, default: `False`)"
    
    This keyword does not have any further options.
    
    [:material-code-tags: **Source variable** `linionly`](https://gitlab.com/TheochemGoettingen/RuNNer/-/blob/master/src-devel/fittingoptions.f90#L162)

---

### `ion_forces_only` -

:material-chart-bell-curve-cumulative: **Mode 1:** :material-close:{ .no } **•** 
:custom-neural-network:                **Mode 2:** :material-check:{ .yes } **•** 
:material-head-cog-outline:            **Mode 3:** :material-close:{ .no }

If this keyword is set, for structures with a nonzero net charge only the forces
will be used for fitting, the energies will be omitted. This keyword is 
currently implemented only for the atomic short range part.

!!! Abstract "**Format:** `#!runner-config ion_forces_only` (`logical`, default: `False`)"
    
    This keyword does not have any further options.
    
    [:material-code-tags: **Source variable** `lionforcesonly`](https://gitlab.com/TheochemGoettingen/RuNNer/-/blob/master/src-devel/fittingoptions.f90#L187)

---

### `joint_energy_force_update` *

:material-chart-bell-curve-cumulative: **Mode 1:** :material-close:{ .no } **•** 
:custom-neural-network:                **Mode 2:** :material-check:{ .yes } **•** 
:material-head-cog-outline:            **Mode 3:** :material-close:{ .no }

This is an experimental keyword not fully tested. For each atom only one weight
update is done for an averaged set of gradients calculated from the energy and
all forces (not yet working well).

!!! Abstract "**Format:** `#!runner-config joint_energy_force_update` (`logical`, default: `False`)"
    
    This keyword does not have any further options.
    
    [:material-code-tags: **Source variable** `ljointefupdate`](https://gitlab.com/TheochemGoettingen/RuNNer/-/blob/master/src-devel/fittingoptions.f90#L156)

---

### `kalman_damp_charge` 

:material-chart-bell-curve-cumulative: **Mode 1:** :material-close:{ .no } **•** 
:custom-neural-network:                **Mode 2:** :material-check:{ .yes } **•** 
:material-head-cog-outline:            **Mode 3:** :material-close:{ .no }

Reduce the effective RMSE on the charges for the Kalman filter update of the
weights in the electrostatic NN.

!!! Abstract "**Format:** `#!runner-config kalman_damp_charge a0`"
    
    **`#!runner-config a0`: (`real`, default: `1.0`)**
    :   Fraction of charge RMSE that is considered for the weight update.
        100% = 1.0.
    
    [:material-code-tags: **Source variable** `kalman_dampq`](https://gitlab.com/TheochemGoettingen/RuNNer/-/blob/master/src-devel/fittingoptions.f90#L87)

!!! warning "Hint"
    This keyword is used in case of 
    [`#!runner-config electrostatic_type`](/runner/reference/keywords/#electrostatic_type) 1
    and
    [`#!runner-config optmode_charge`](/runner/reference/keywords/#optmode_charge) 1
    only.

---

### `kalman_damp_force` 

:material-chart-bell-curve-cumulative: **Mode 1:** :material-close:{ .no } **•** 
:custom-neural-network:                **Mode 2:** :material-check:{ .yes } **•** 
:material-head-cog-outline:            **Mode 3:** :material-close:{ .no }

Reduce the effective RMSE on the forces for the Kalman filter update of the
weights in the short-range NN.

!!! Abstract "**Format:** `#!runner-config kalman_damp_force a0`"
    
    **`#!runner-config a0`: (`real`, default: `1.0`)**
    :   Fraction of force RMSE that is considered for the weight update.
        100% = 1.0.
    
    [:material-code-tags: **Source variable** `kalman_dampf`](https://gitlab.com/TheochemGoettingen/RuNNer/-/blob/master/src-devel/fittingoptions.f90#L86)

!!! warning "Hint"
    This keyword is used in case for 
    [`#!runner-config optmode_short_force`](/runner/reference/keywords/#optmode_short_force) 1
    only.

---

### `kalman_damp_short` 

:material-chart-bell-curve-cumulative: **Mode 1:** :material-close:{ .no } **•** 
:custom-neural-network:                **Mode 2:** :material-check:{ .yes } **•** 
:material-head-cog-outline:            **Mode 3:** :material-close:{ .no }

Reduce the effective RMSE on the energies for the Kalman filter update of the
weights in the short-range NN.

!!! Abstract "**Format:** `#!runner-config kalman_damp_short a0`"
    
    **`#!runner-config a0`: (`real`, default: `1.0`)**
    :   Fraction of energy RMSE that is considered for the weight update.
        100% = 1.0.
    
    [:material-code-tags: **Source variable** `kalman_dampe`](https://gitlab.com/TheochemGoettingen/RuNNer/-/blob/master/src-devel/fittingoptions.f90#L85)

!!! warning "Hint"
    This keyword is used for
    [`#!runner-config optmode_short_energy`](/runner/reference/keywords/#optmode_short_energy) 1
    only.

---

### `kalman_epsilon` 

:material-chart-bell-curve-cumulative: **Mode 1:** :material-close:{ .no } **•** 
:custom-neural-network:                **Mode 2:** :material-check:{ .yes } **•** 
:material-head-cog-outline:            **Mode 3:** :material-close:{ .no }

Set the initialization parameter for the correlation matrix of the Kalman filter
according to

$$
P(0)=\epsilon^{-1} \mathcal{I}.
$$

$\epsilon$ is often set to the order of $10^{-3}$ to $10^{-2}$.

!!! Abstract "**Format:** `#!runner-config kalman_epsilon a0`"
    
    **`#!runner-config a0`: (`real`, default: `1.0`)**
    :   Value of $\epsilon$.
    
    [:material-code-tags: **Source variable** `kalman_epsilon`](https://gitlab.com/TheochemGoettingen/RuNNer/-/blob/master/src-devel/globaloptions.f90#L97)

!!! warning "Hint"
    This keyword is mandatory if
    [`#!runner-config use_noisematrix`](/runner/reference/keywords/#use_noisematrix)
    is used.

---

### `kalman_lambda_charge` +

:material-chart-bell-curve-cumulative: **Mode 1:** :material-close:{ .no } **•** 
:custom-neural-network:                **Mode 2:** :material-check:{ .yes } **•** 
:material-head-cog-outline:            **Mode 3:** :material-close:{ .no }

Kalman filter parameter $\lambda$ for the electrostatic NN weight updates.

!!! Abstract "**Format:** `#!runner-config kalman_lambda_charge a0`"
    
    **`#!runner-config a0`: (`real`, default: `1.0`)**
    :   Value of $\lambda$.
    
    [:material-code-tags: **Source array** `kalmanlambdae(nelem)`](https://gitlab.com/TheochemGoettingen/RuNNer/-/blob/master/src-devel/fittingoptions.f90#L110)

!!! warning "Hint"
    This keyword is mandatory for
    [`#!runner-config optmode_charge`](/runner/reference/keywords/#optmode_charge) 1.
---

### `kalman_lambda_charge_constraint` --

This keyword has been deprecated and is unlikely to work.

---

### `kalman_lambda_short` +

:material-chart-bell-curve-cumulative: **Mode 1:** :material-close:{ .no } **•** 
:custom-neural-network:                **Mode 2:** :material-check:{ .yes } **•** 
:material-head-cog-outline:            **Mode 3:** :material-close:{ .no }

Kalman filter parameter $\lambda$ for the short range NN weight updates.

!!! Abstract "**Format:** `#!runner-config kalman_lambda_short a0`"
    
    **`#!runner-config a0`: (`real`, default: `1.0`)**
    :   Value of $\lambda$.
    
    [:material-code-tags: **Source array** `kalmanlambda(nelem)`](https://gitlab.com/TheochemGoettingen/RuNNer/-/blob/master/src-devel/fittingoptions.f90#L108)

!!! warning "Hint"
    This keyword is mandatory for
    [`#!runner-config optmode_short_energy`](/runner/reference/keywords/#optmode_short_energy) 1.

---

### `kalman_nue_charge` +

:material-chart-bell-curve-cumulative: **Mode 1:** :material-close:{ .no } **•** 
:custom-neural-network:                **Mode 2:** :material-check:{ .yes } **•** 
:material-head-cog-outline:            **Mode 3:** :material-close:{ .no }

Kalman filter parameter $\lambda_0$ for the electrostatic NN weight updates.

!!! Abstract "**Format:** `#!runner-config kalman_nue_charge a0`"
    
    **`#!runner-config a0`: (`real`, default: `None`)**
    :   Value of $\lambda_0$.
    
    [:material-code-tags: **Source variable** `kalmannuee`](https://gitlab.com/TheochemGoettingen/RuNNer/-/blob/master/src-devel/fittingoptions.f90#L95)

!!! warning "Hint"
    This keyword is mandatory for
    [`#!runner-config optmode_charge`](/runner/reference/keywords/#optmode_short_energy) 1.

---

### `kalman_nue_charge_constraint` --

This keyword has been deprecated and is unlikely to work.

---

### `kalman_nue_short` +

:material-chart-bell-curve-cumulative: **Mode 1:** :material-close:{ .no } **•** 
:custom-neural-network:                **Mode 2:** :material-check:{ .yes } **•** 
:material-head-cog-outline:            **Mode 3:** :material-close:{ .no }

Kalman filter parameter $\lambda_0$ for the short range weight updates.

!!! Abstract "**Format:** `#!runner-config kalman_nue_short a0`"
    
    **`#!runner-config a0`: (`real`, default: `None`)**
    :   Value of $\lambda_0$.
    
    [:material-code-tags: **Source variable** `kalmannue`](https://gitlab.com/TheochemGoettingen/RuNNer/-/blob/master/src-devel/fittingoptions.f90#L94)

!!! warning "Hint"
    This keyword is mandatory for
    [`#!runner-config optmode_short_energy`](/runner/reference/keywords/#optmode_short_energy) 1
    and
    [`#!runner-config optmode_short_force`](/runner/reference/keywords/#optmode_short_force) 1

---

### `kalman_q0`

:material-chart-bell-curve-cumulative: **Mode 1:** :material-close:{ .no } **•** 
:custom-neural-network:                **Mode 2:** :material-check:{ .yes } **•** 
:material-head-cog-outline:            **Mode 3:** :material-close:{ .no }

It is possible to add artificial process noise for the Kalman filter in the form
of 

$$
Q(t) =q(t)\mathcal{I},
$$ 

with either a fixed $q(t)=q(0)$ or annealing from a higher $q(0)$ to 
$q_{\mathrm{min}}$ following a scheme like

$$
q(t) = \max(q_{0}e^{-t/\tau_{q}}, q_{\mathrm{min}}).
$$

The value of $q(0)$ is usually set between $10^{-6}$ and $10^{-2}$.
It is recommended for the user to do some test for each new system, altering
[`#!runner-config kalman_q0`](/runner/reference/keywords/#kalman_q0), 
[`#!runner-config kalman_qmin`](/runner/reference/keywords/#kalman_qmin) and 
[`#!runner-config kalman_qtau`](/runner/reference/keywords/#kalman_qtau) to obtain the optimal
performance for minimizing the root mean square error.

!!! Abstract "**Format:** `#!runner-config kalman_q0 a0`"
    
    **`#!runner-config a0`: (`real`, default: `0.0`)**
    :   Value of $q(0)$.
    
    [:material-code-tags: **Source variable** `kalman_q0`](https://gitlab.com/TheochemGoettingen/RuNNer/-/blob/master/src-devel/globaloptions.f90#L104)

!!! warning "Hint"
    This keyword is mandatory if
    [`#!runner-config use_noisematrix`](/runner/reference/keywords/#use_noisematrix)
    is used.

---

### `kalman_qmin`

:material-chart-bell-curve-cumulative: **Mode 1:** :material-close:{ .no } **•** 
:custom-neural-network:                **Mode 2:** :material-check:{ .yes } **•** 
:material-head-cog-outline:            **Mode 3:** :material-close:{ .no }

Parameter $q_{\mathrm{min}}$ for adding artificial process noise to the Kalman
filter noise matrix. See [`#!runner-config kalman_q0`](/runner/reference/keywords/#kalman_q0)
for a more detailed explanation.

!!! Abstract "**Format:** `#!runner-config kalman_qmin a0`"
    
    **`#!runner-config a0`: (`real`, default: `0.0`)**
    :   Value of $q_{\mathrm{min}}$.
    
    [:material-code-tags: **Source variable** `kalman_qmin`](https://gitlab.com/TheochemGoettingen/RuNNer/-/blob/master/src-devel/globaloptions.f90#L103)

!!! warning "Hint"
    This keyword is mandatory if
    [`#!runner-config use_noisematrix`](/runner/reference/keywords/#use_noisematrix)
    is used.

---

### `kalman_qtau`

:material-chart-bell-curve-cumulative: **Mode 1:** :material-close:{ .no } **•** 
:custom-neural-network:                **Mode 2:** :material-check:{ .yes } **•** 
:material-head-cog-outline:            **Mode 3:** :material-close:{ .no }

Parameter $\tau_q$ for adding artificial process noise to the Kalman
filter noise matrix. See [`#!runner-config kalman_q0`](/runner/reference/keywords/#kalman_q0)
for a more detailed explanation.

!!! Abstract "**Format:** `#!runner-config kalman_qtau a0`"
    
    **`#!runner-config a0`: (`real`, default: `0.0`)**
    :   Value of $\tau_q$.
    
    [:material-code-tags: **Source variable** `kalman_qtau`](https://gitlab.com/TheochemGoettingen/RuNNer/-/blob/master/src-devel/globaloptions.f90#L102)

!!! warning "Hint"
    This keyword is mandatory if
    [`#!runner-config use_noisematrix`](/runner/reference/keywords/#use_noisematrix)
    is used.

---

### `max_energy`

:material-chart-bell-curve-cumulative: **Mode 1:** :material-close:{ .no } **•** 
:custom-neural-network:                **Mode 2:** :material-check:{ .yes } **•** 
:material-head-cog-outline:            **Mode 3:** :material-close:{ .no }

Set an upper threshold for the consideration of a structure during the weight 
update. If the total energy is above 
[`#!runner-config max_energy`](/runner/reference/keywords/#max_energy)]
the data point will be ignored.

!!! Abstract "**Format:** `#!runner-config max_energy a0`"
    
    **`#!runner-config a0`: (`real`, default: `10000.0`)**
    :   Maximum energy of a structure to be considered for the weight update
        (unit: Hartree).
    
    [:material-code-tags: **Source variable** `maxenergy`](https://gitlab.com/TheochemGoettingen/RuNNer/-/blob/master/src-devel/globaloptions.f90#L102)

---

### `max_force` 

:material-chart-bell-curve-cumulative: **Mode 1:** :material-close:{ .no } **•** 
:custom-neural-network:                **Mode 2:** :material-check:{ .yes } **•** 
:material-head-cog-outline:            **Mode 3:** :material-close:{ .no }

Set an upper threshold for the consideration of a structure during the weight 
update. If any force component is above 
[`#!runner-config max_force`](/runner/reference/keywords/#max_force)]
the data point will be ignored.

!!! Abstract "**Format:** `#!runner-config max_force a0`"
    
    **`#!runner-config a0`: (`real`, default: `10000.0`)**
    :   Maximum force component of a structure to be considered for the weight 
        update (unit: Hartree/Bohr).
    
    [:material-code-tags: **Source variable** `maxforce`](https://gitlab.com/TheochemGoettingen/RuNNer/-/blob/master/src-devel/globaloptions.f90#L105)

---

### `md_mode` --

:material-chart-bell-curve-cumulative: **Mode 1:** :material-close:{ .no } **•** 
:custom-neural-network:                **Mode 2:** :material-close:{ .no } **•** 
:material-head-cog-outline:            **Mode 3:** :material-check:{ .yes }

The purpose of this keyword is to reduce the output to enable the incorporation
of `RuNNer` into a MD code.

!!! Abstract "**Format:** `#!runner-config md_mode`  (`logical`, default: `False`)"
    
    This keyword does not have any further options.
    
    [:material-code-tags: **Source variable** `lmd`](https://gitlab.com/TheochemGoettingen/RuNNer/-/blob/master/src-devel/globaloptions.f90#L149)

---

### `mix_all_points` ++

:material-chart-bell-curve-cumulative: **Mode 1:** :material-close:{ .no } **•** 
:custom-neural-network:                **Mode 2:** :material-check:{ .yes } **•** 
:material-head-cog-outline:            **Mode 3:** :material-close:{ .no }

Randomly reorder the data points in the data set at the beginning of each new
epoch.

!!! Abstract "**Format:** `#!runner-config mix_all_points`  (`logical`, default: `False`)"
    
    This keyword does not have any further options.
    
    [:material-code-tags: **Source variable** `lmd`](https://gitlab.com/TheochemGoettingen/RuNNer/-/blob/master/src-devel/fittingoptions.f90#L153)

---

### `nguyen_widrow_weights_electrostatic` 

:material-chart-bell-curve-cumulative: **Mode 1:** :material-close:{ .no } **•** 
:custom-neural-network:                **Mode 2:** :material-check:{ .yes } **•** 
:material-head-cog-outline:            **Mode 3:** :material-close:{ .no }

Initialize the electrostatic NN weights according to the scheme proposed by
Nguyen and Widrow. The initial weights and bias values in the hidden layer are
chosen such that the input space is evenly distributed over the nodes.
This may speed up the training process. nguyen_widrow_weights_ewald is an
alternative name of the keyword.

!!! Abstract "**Format:** `#!runner-config nguyen_widrow_weights_electrostatic`  (`logical`, default: `False`)"
    
    This keyword does not have any further options.
    
    [:material-code-tags: **Source variable** `lnwweightse`](https://gitlab.com/TheochemGoettingen/RuNNer/-/blob/master/src-devel/fittingoptions.f90#L168)

---

### `nguyen_widrow_weights_short`

:material-chart-bell-curve-cumulative: **Mode 1:** :material-close:{ .no } **•** 
:custom-neural-network:                **Mode 2:** :material-check:{ .yes } **•** 
:material-head-cog-outline:            **Mode 3:** :material-close:{ .no }

Initialize the short-range NN weights according to the scheme proposed by
Nguyen and Widrow. The initial weights and bias values in the hidden layer are
chosen such that the input space is evenly distributed over the nodes.
This may speed up the training process.

!!! Abstract "**Format:** `#!runner-config nguyen_widrow_weights_short`  (`logical`, default: `False`)"
    
    This keyword does not have any further options.
    
    [:material-code-tags: **Source variable** `lnwweights`](https://gitlab.com/TheochemGoettingen/RuNNer/-/blob/master/src-devel/fittingoptions.f90#L167)

---

### `nn_type_elec` 

This keyword has the same meaning as 
[`#!runner-config electrostatic_type`](/runner/reference/keywords/#electrostatic_type)
and the same options. Only one of these keywords can be used.

---

### `nn_type_short` ++

:material-chart-bell-curve-cumulative: **Mode 1:** :material-check:{ .yes } **•** 
:custom-neural-network:                **Mode 2:** :material-check:{ .yes } **•** 
:material-head-cog-outline:            **Mode 3:** :material-check:{ .yes }

Specify the NN type of the short-range part.

!!! Abstract "**Format:** `#!runner-config nn_type_short i0`"
    
    **`#!runner-config i0`: (`integer`, default: `None`)**
    :   Set the short-range NN type. Can be one of:
    
        * **`1`:** 
        :   Behler-Parrinello atomic NNs. The short range energy is constructed as a
            sum of environment-dependent atomic energies.
        * **`2`:**
        :    Pair NNs. The short range energy is constructed as a sum of 
             environment-dependent pair energies.
    
    [:material-code-tags: **Source variable** `nn_type_short`](https://gitlab.com/TheochemGoettingen/RuNNer/-/blob/master/src-devel/nnflags.f90#L27)

!!! warning "Hint"
    This keyword is mandatory.

---

### `nnp_gen` ++

:material-chart-bell-curve-cumulative: **Mode 1:** :material-check:{ .yes } **•** 
:custom-neural-network:                **Mode 2:** :material-check:{ .yes } **•** 
:material-head-cog-outline:            **Mode 3:** :material-check:{ .yes }

This keyword specifies the generation of HDNNP that will be constructed.

!!! Abstract "**Format:** `#!runner-config nnp_gen i0`"
    
    **`#!runner-config i0`: (`integer`, default: `None`)**
    :   Set the short-range and electrostatics NN type. Can be one of:
    
        * **`2`:** 
        :   2G-HDNNPs only include the short-range part (Behler-Parrinello 
            atomic NNs). Users should also specify 
            [`#!runner-config use_short_nn`](/runner/reference/keywords/#use_short_nn).

        * **`3`:**
        :    3G-HDNNPs include both the short-range part and the long-range 
             electrostatic part. 
             Users are advised to first construct a representation for the 
             electrostatic part by specifying 
             [`#!runner-config use_electrostatics`](/runner/reference/keywords/#use_electrostatics)
             and then switch to the short range part by setting both 
             [`#!runner-config use_short_nn`](/runner/reference/keywords/#use_short_nn) and 
             [`#!runner-config use_electrostatics`](/runner/reference/keywords/#use_electrostatics).

         * **`4`:**
         :    4G-HDNNPs include both the short-range part and the long-range 
              electrostatic part. 
              Users are advised to first construct a representation for the 
              electrostatic part by specifying 
              [`#!runner-config use_electrostatics`](/runner/reference/keywords/#use_electrostatics)
              and then switch to the short range part by setting both 
              [`#!runner-config use_short_nn`](/runner/reference/keywords/#use_short_nn) and 
              [`#!runner-config use_electrostatics`](/runner/reference/keywords/#use_electrostatics).

     :material-code-tags: **Source variables** [`nn_type_short`](https://gitlab.com/TheochemGoettingen/RuNNer/-/blob/master/src-devel/nnflags.f90#L27) and
     [`nn_type_elec`](https://gitlab.com/TheochemGoettingen/RuNNer/-/blob/master/src-devel/nnflags.f90#L32).

!!! warning "Hint"
    This keyword is mandatory. Please also consider reading the 
    [workflow recommendation](/runner/getting_started/workflow).

---

### `noise_charge` 

:material-chart-bell-curve-cumulative: **Mode 1:** :material-close:{ .no } **•** 
:custom-neural-network:                **Mode 2:** :material-check:{ .yes } **•** 
:material-head-cog-outline:            **Mode 3:** :material-close:{ .no }

Introduce artificial noise on the atomic charges in the training process by
setting a lower threshold that the absolute charge error of a data point has to 
surpass before being considered for the weight update.   

!!! Abstract "**Format:** `#!runner-config noise_charge a0`"
    
    **`#!runner-config a0`: (`real`, default: `0.0`)**
    :   Noise charge threshold (unit: Hartree per atom). Must be positive.

    [:material-code-tags: **Source variable** `noiseq`](https://gitlab.com/TheochemGoettingen/RuNNer/-/blob/master/src-devel/fittingoptions.f90#L81)

---

### `noise_energy` 

:material-chart-bell-curve-cumulative: **Mode 1:** :material-close:{ .no } **•** 
:custom-neural-network:                **Mode 2:** :material-check:{ .yes } **•** 
:material-head-cog-outline:            **Mode 3:** :material-close:{ .no }

Introduce artificial noise on the atomic energies in the training process by
setting a lower threshold that the absolute energy error of a data point has to 
surpass before being considered for the weight update.   

!!! Abstract "**Format:** `#!runner-config noise_energy a0`"
    
    **`#!runner-config a0`: (`real`, default: `0.0`)**
    :   Noise energy threshold (unit: electron charge). Must be positive.

    [:material-code-tags: **Source variable** `noisee`](https://gitlab.com/TheochemGoettingen/RuNNer/-/blob/master/src-devel/fittingoptions.f90#L79)

---

### `noise_force` 

:material-chart-bell-curve-cumulative: **Mode 1:** :material-close:{ .no } **•** 
:custom-neural-network:                **Mode 2:** :material-check:{ .yes } **•** 
:material-head-cog-outline:            **Mode 3:** :material-close:{ .no }

Introduce artificial noise on the atomic forces in the training process by
setting a lower threshold that the absolute force error of a data point has to 
surpass before being considered for the weight update.   

!!! Abstract "**Format:** `#!runner-config noise_force a0`"
    
    **`#!runner-config a0`: (`real`, default: `0.0`)**
    :   Noise force threshold (unit: Hartree per Bohr). Must be positive.

    [:material-code-tags: **Source variable** `noisef`](https://gitlab.com/TheochemGoettingen/RuNNer/-/blob/master/src-devel/fittingoptions.f90#L80)

---

### `normalize_nodes` +

:material-chart-bell-curve-cumulative: **Mode 1:** :material-close:{ .no } **•** 
:custom-neural-network:                **Mode 2:** :material-check:{ .yes } **•** 
:material-head-cog-outline:            **Mode 3:** :material-check:{ .yes }

Divide the accumulated sum at each node by the number of nodes in the
previous layer before the activation function is applied. This may help
to activate the activation functions in their non-linear regions.

!!! Abstract "**Format:** `#!runner-config normalize_nodes`  (`logical`, default: `False`)"
    
    This keyword does not have any further options.
    
    [:material-code-tags: **Source variable** `lnormnodes`](https://gitlab.com/TheochemGoettingen/RuNNer/-/blob/master/src-devel/globaloptions.f90#L134)

---

### `number_of_elements` ++

:material-chart-bell-curve-cumulative: **Mode 1:** :material-check:{ .yes } **•** 
:custom-neural-network:                **Mode 2:** :material-check:{ .yes } **•** 
:material-head-cog-outline:            **Mode 3:** :material-check:{ .yes }

Specify the number of chemical elements in the system.

!!! Abstract "**Format:** `#!runner-config number_of_elements i0`"
    
    **`#!runner-config i0`: (`integer`, default: `None`)**
    :   Number of elements.
    
    [:material-code-tags: **Source variable** `nelem`](https://gitlab.com/TheochemGoettingen/RuNNer/-/blob/master/src-devel/globaloptions.f90#L76)

!!! warning "Hint"
    This keyword is mandatory. For each element there must be an entry for the
    keyword [`#!runner-config elements`](/runner/reference/keywords/#elements).

---

### `optmode_charge` ++

:material-chart-bell-curve-cumulative: **Mode 1:** :material-check:{ .yes } **•** 
:custom-neural-network:                **Mode 2:** :material-check:{ .yes } **•** 
:material-head-cog-outline:            **Mode 3:** :material-check:{ .yes }

Specify the optimization algorithm for the atomic charges in case of 
[`#!runner-config electrostatic_type`](/runner/reference/keywords/#electrostatic_type) 1.

!!! Abstract "**Format:** `#!runner-config optmode_charge i0`"
    
    **`#!runner-config i0`: (`integer`, default: `1`)**
    :   Set the atomic charge optimization algorithm. Can be one of:
    
        * **`1`:** 
        :    Kalman filter.
        * **`2`:**
        :    Reserved for conjugate gradient, not implemented.
        * **`3`:**
        :    Steepest descent. Not recommended.
    
    [:material-code-tags: **Source variable** `optmodeq`](https://gitlab.com/TheochemGoettingen/RuNNer/-/blob/master/src-devel/fittingoptions.f90#L31)

---

### `optmode_short_energy` ++

:material-chart-bell-curve-cumulative: **Mode 1:** :material-check:{ .yes } **•** 
:custom-neural-network:                **Mode 2:** :material-check:{ .yes } **•** 
:material-head-cog-outline:            **Mode 3:** :material-check:{ .yes }

Specify the optimization algorithm for the short-range energy contributions.

!!! Abstract "**Format:** `#!runner-config optmode_short_energy i0`"
    
    **`#!runner-config i0`: (`integer`, default: `1`)**
    :   Set the short-range energy optimization algorithm. Can be one of:
    
        * **`1`:** 
        :    Kalman filter.
        * **`2`:**
        :    Reserved for conjugate gradient, not implemented.
        * **`3`:**
        :    Steepest descent. Not recommended.
    
    [:material-code-tags: **Source variable** `optmodee`](https://gitlab.com/TheochemGoettingen/RuNNer/-/blob/master/src-devel/fittingoptions.f90#L29)

---

### `optmode_short_force` ++

:material-chart-bell-curve-cumulative: **Mode 1:** :material-check:{ .yes } **•** 
:custom-neural-network:                **Mode 2:** :material-check:{ .yes } **•** 
:material-head-cog-outline:            **Mode 3:** :material-check:{ .yes }

Specify the optimization algorithm for the short-range forces.

!!! Abstract "**Format:** `#!runner-config optmode_short_force i0`"
    
    **`#!runner-config i0`: (`integer`, default: `1`)**
    :   Set the short-range force optimization algorithm. Can be one of:
    
        * **`1`:** 
        :    Kalman filter.
        * **`2`:**
        :    Reserved for conjugate gradient, not implemented.
        * **`3`:**
        :    Steepest descent. Not recommended.
    
    [:material-code-tags: **Source variable** `optmodef`](https://gitlab.com/TheochemGoettingen/RuNNer/-/blob/master/src-devel/fittingoptions.f90#L30)

---

### `pairsymfunction_short` 

---

### `parallel_mode` -

:material-chart-bell-curve-cumulative: **Mode 1:** :material-check:{ .yes } **•** 
:custom-neural-network:                **Mode 2:** :material-check:{ .yes } **•** 
:material-head-cog-outline:            **Mode 3:** :material-check:{ .yes }

This flag controls the parallelization of some subroutines. 

!!! Abstract "**Format:** `#!runner-config parallel_mode i0`"
    
    **`#!runner-config i0`: (`integer`, default: `1`)**
    :   Set the short-range force optimization algorithm. Can be one of:
    
        * **`1`:** 
        :    Serial version. 
        * **`2`:**
        :    Parallel version.
    
    [:material-code-tags: **Source variable** `paramode`](https://gitlab.com/TheochemGoettingen/RuNNer/-/blob/master/src-devel/globaloptions.f90#L63)

!!! Warning "Hint"
    This feature is not fully implemented yet and should be used only by 
    developers.

---

### `points_in_memory` ++

:material-chart-bell-curve-cumulative: **Mode 1:** :material-check:{ .yes } **•** 
:custom-neural-network:                **Mode 2:** :material-check:{ .yes } **•** 
:material-head-cog-outline:            **Mode 3:** :material-check:{ .yes }

This keyword controls memory consumption and IO and is therefore important to
achieve an optimum performance of `RuNNer`. Has a different meaning depending on
the current 
[`#!runner-config runner_mode`](/runner/reference/keywords/#runner_mode).

!!! Abstract "**Format:** `#!runner-config points_in_memory i0`"
    
    **`#!runner-config i0`: (`integer`, default: `200`)**
    :   In 
        [`#!runner-config runner_mode`](/runner/reference/keywords/#runner_mode)
        1 this is the maximum number of structures in memory at a time.  
        In 
        [`#!runner-config runner_mode`](/runner/reference/keywords/#runner_mode)
        3 this is the number of atoms for which the symmetry functions are in
        memory at once.
        In parallel runs these atoms are further split between the processes.
    
    [:material-code-tags: **Source variable** `nblock`](https://gitlab.com/TheochemGoettingen/RuNNer/-/blob/master/src-devel/globaloptions.f90#L31)
    
---

### `precondition_weights` 

:material-chart-bell-curve-cumulative: **Mode 1:** :material-close:{ .no } **•** 
:custom-neural-network:                **Mode 2:** :material-check:{ .yes } **•** 
:material-head-cog-outline:            **Mode 3:** :material-check:{ .yes }

Shift the weights of the atomic NNs right after the initialization so that 
the standard deviation of the NN energies is the same as the standard deviation
of the reference energies.

!!! Abstract "**Format:** `#!runner-config precondition_weights`  (`logical`, default: `False`)"
    
    This keyword does not have any further options.
    
    [:material-code-tags: **Source variable** `lprecond`](https://gitlab.com/TheochemGoettingen/RuNNer/-/blob/master/src-devel/fittingoptions.f90#L163)

!!! Warning "Hint"
    This keyword effects short-range, pairwise, and electrostatic NNs alike.

---

### `prepare_md` --

This keyword is not fully implemented yet. The purpose is to write the full 
configuration of RuNNer to an output file for interfacing with an external MD 
program. 

---

### `print_all_deshortdw` *

:material-chart-bell-curve-cumulative: **Mode 1:** :material-close:{ .no } **•** 
:custom-neural-network:                **Mode 2:** :material-check:{ .yes } **•** 
:material-head-cog-outline:            **Mode 3:** :material-close:{ .no }

For debugging only. Prints the derivatives of the short range energy with 
respect to the short range NN weight parameters after each update.
This derivative array is responsible for the weight update. The
derivatives (the array `deshortdw`) are written to the file `debug.out`.

!!! Abstract "**Format:** `#!runner-config print_all_deshortdw` (`logical`, default: `False`)"
    
    This keyword does not have any further options.

    [:material-code-tags: **Source variable** `pstring(3)`](https://gitlab.com/TheochemGoettingen/RuNNer/-/blob/master/src-devel/globaloptions.f90#L182)
      
!!! warning 
    **Caution:** Use this only for small data sets and few epochs, otherwise
    your hard disk will be immediately full.  
    
---

### `print_all_dfshortdw` *

:material-chart-bell-curve-cumulative: **Mode 1:** :material-close:{ .no } **•** 
:custom-neural-network:                **Mode 2:** :material-check:{ .yes } **•** 
:material-head-cog-outline:            **Mode 3:** :material-close:{ .no }

For debugging only. Prints the derivatives of the short range forces
with respect to the short range NN weight parameters after each update.
This derivative array is responsible for the weight update. The
derivatives (the array `dfshortdw(maxnum_weightsshort)`) are written to
the file `debug.out`.

!!! Abstract "**Format:** `#!runner-config print_all_dfshortdw` (`logical`, default: `False`)"
    
    This keyword does not have any further options.

    [:material-code-tags: **Source variable** `pstring(4)`](https://gitlab.com/TheochemGoettingen/RuNNer/-/blob/master/src-devel/globaloptions.f90#L182)
      
!!! warning 
    **Caution:** Use this only for small data sets and few epochs, otherwise
    your hard disk will be immediately full.  
    
---

### `print_all_electrostatic_weights` *

:material-chart-bell-curve-cumulative: **Mode 1:** :material-close:{ .no } **•** 
:custom-neural-network:                **Mode 2:** :material-check:{ .yes } **•** 
:material-head-cog-outline:            **Mode 3:** :material-close:{ .no }

For debugging only. Print the electrostatic NN weight parameters after each 
update, not only once per epoch to a file. The weights (the array 
`weights_ewald()`) are written to the file `debug.out`.

!!! Abstract "**Format:** `#!runner-config print_all_electrostatic_weights` (`logical`, default: `False`)"
    
    This keyword does not have any further options.

    [:material-code-tags: **Source variable** `pstring(2)`](https://gitlab.com/TheochemGoettingen/RuNNer/-/blob/master/src-devel/globaloptions.f90#L182)
      
!!! warning 
    **Caution:** Use this only for small data sets and few epochs, otherwise
    your hard disk will be immediately full.  

---

### `print_all_short_weights` 

:material-chart-bell-curve-cumulative: **Mode 1:** :material-close:{ .no } **•** 
:custom-neural-network:                **Mode 2:** :material-check:{ .yes } **•** 
:material-head-cog-outline:            **Mode 3:** :material-close:{ .no }

For debugging only. Print the short range NN weight parameters after each 
update, not only once per epoch to a file. The weights (the array
`weights_short()`) are written to the file `debug.out`. 

!!! Abstract "**Format:** `#!runner-config print_all_short_weights` (`logical`, default: `False`)"
    
    This keyword does not have any further options.

    [:material-code-tags: **Source variable** `pstring(1)`](https://gitlab.com/TheochemGoettingen/RuNNer/-/blob/master/src-devel/globaloptions.f90#L182)
      
!!! warning 
    **Caution:** Use this only for small data sets and few epochs, otherwise
    your hard disk will be immediately full.  

---

### `print_convergence_vector` 

:material-chart-bell-curve-cumulative: **Mode 1:** :material-close:{ .no } **•** 
:custom-neural-network:                **Mode 2:** :material-check:{ .yes } **•** 
:material-head-cog-outline:            **Mode 3:** :material-close:{ .no }

During training, print a measure for the convergence of the weight vector.
The output is:

```runner-data
CONVVEC element epoch C1 C2 wshift wshift2
```

`C1` and `C2` are two-dimensional coordinates of projections of the
weight vectors for plotting qualitatively the convergence of the
weights. `wshift` is the length (normalized by the number of weights) of
the difference vector of the weights between two epochs. `wshift2` is
the length (normalized by the number of weights) of the difference
vector between the current weights and the weights two epochs ago.

!!! Abstract "**Format:** `#!runner-config print_convergence_vector` (`logical`, default: `False`)"
    
    This keyword does not have any further options.

    [:material-code-tags: **Source variable** `lprintconv`](https://gitlab.com/TheochemGoettingen/RuNNer/-/blob/master/src-devel/fittingoptions.f90#L164)

---

### `print_date_and_time` 

:material-chart-bell-curve-cumulative: **Mode 1:** :material-close:{ .no } **•** 
:custom-neural-network:                **Mode 2:** :material-check:{ .yes } **•** 
:material-head-cog-outline:            **Mode 3:** :material-close:{ .no }

Print in each training epoch the date and the real time in an extra line.

!!! Abstract "**Format:** `#!runner-config print_date_and_time` (`logical`, default: `False`)"
    
    This keyword does not have any further options.

    [:material-code-tags: **Source variable** `lprintdateandtime`](https://gitlab.com/TheochemGoettingen/RuNNer/-/blob/master/src-devel/fittingoptions.f90#L185)

---

### `print_force_components` *

:material-chart-bell-curve-cumulative: **Mode 1:** :material-close:{ .no } **•** 
:custom-neural-network:                **Mode 2:** :material-close:{ .no } **•** 
:material-head-cog-outline:            **Mode 3:** :material-check:{ .yes }

For debugging only. Prints in 
[`#!runner-config runner_mode`](/runner/reference/keywords/#runner_mode) 3 
the contributions of all atomic energies to the force components of each atom.

!!! Abstract "**Format:** `#!runner-config print_force_components` (`logical`, default: `False`)"
    
    This keyword does not have any further options.

    [:material-code-tags: **Source variable** `lprintforcecomponents`](https://gitlab.com/TheochemGoettingen/RuNNer/-/blob/master/src-devel/predictionoptions.f90#L37)

---

### `print_mad` +

:material-chart-bell-curve-cumulative: **Mode 1:** :material-close:{ .no } **•** 
:custom-neural-network:                **Mode 2:** :material-check:{ .yes } **•** 
:material-head-cog-outline:            **Mode 3:** :material-close:{ .no }

Print a line with the mean absolute deviation as an additional output line next
to the RMSE in 
[`#!runner-config runner_mode`](/runner/reference/keywords/#runner_mode) 2. 
Usually the MAD is smaller than the RMSE as outliers do not have such a large 
impact.

!!! Abstract "**Format:** `#!runner-config print_mad` (`logical`, default: `False`)"
    
    This keyword does not have any further options.

    [:material-code-tags: **Source variable** `lprintmad`](https://gitlab.com/TheochemGoettingen/RuNNer/-/blob/master/src-devel/fittingoptions.f90#L165)

---

### `print_sensitivity` 

:material-chart-bell-curve-cumulative: **Mode 1:** :material-close:{ .no } **•** 
:custom-neural-network:                **Mode 2:** :material-check:{ .yes } **•** 
:material-head-cog-outline:            **Mode 3:** :material-check:{ .yes }

Perform sensitivity analysis on the symmetry functions of the neural network. The 
sensitivity is a measure of how much the NN output changes with the symmetry
functions, i.e. the derivative. It will be analyzed upon weight initialization
and for each training epoch in all short-range, pair, and electrostatic NNs
there are. 

!!! Abstract "**Format:** `#!runner-config print_sensitivity` (`logical`, default: `False`)"
    
    This keyword does not have any further options.

    [:material-code-tags: **Source variable** `lsens`](https://gitlab.com/TheochemGoettingen/RuNNer/-/blob/master/src-devel/globaloptions.f90#L171)

---

### `random_number_type` ++

:material-chart-bell-curve-cumulative: **Mode 1:** :material-check:{ .yes } **•** 
:custom-neural-network:                **Mode 2:** :material-check:{ .yes } **•** 
:material-head-cog-outline:            **Mode 3:** :material-close:{ .no }

Specify the type of random number generator used in `RuNNer`. The seed can be
given with the keyword 
[`#!runner-config random_seed`](/runner/reference/keywords/#random_seed).

!!! Abstract "**Format:** `#!runner-config random_number_type i0`"
    
    **`#!runner-config i0`: (`integer`, default: `5`)**
    :   Set the random number generator type. Can be one of:
      
        * **`1-4`:** 
        :    Deprecated.
        * **`5`:** 
        :    Normal distribution of random numbers.
        * **`6`:**
        :    Normal distribution of random numbers with the `xorshift` algorithm.
             **This is the recommended option.**
    
    [:material-code-tags: **Source variable** `nran`](https://gitlab.com/TheochemGoettingen/RuNNer/-/blob/master/src-devel/globaloptions.f90#L28)

---

### `random_order_training` --

This keyword has been deprecated in favor of 
[`#!runner-config mix_all_points`](/runner/reference/keywords/#mix_all_points) 

---

### `random_seed` ++

:material-chart-bell-curve-cumulative: **Mode 1:** :material-check:{ .yes } **•** 
:custom-neural-network:                **Mode 2:** :material-check:{ .yes } **•** 
:material-head-cog-outline:            **Mode 3:** :material-close:{ .no }

Set the integer seed for the random number generator used at many places in 
`RuNNer`.
In order to ensure that all results are reproducible, the same seed will result
in exactly the same output at all times (machine and compiler dependence cannot
be excluded).

This seed value is used for all random number generator in `RuNNer`, but
internally for each purpose a local copy is made first to avoid interactions
between the different random number generators. 
Please see also the keyword 
[`#!runner-config random_number_type`](/runner/reference/keywords/#random_number_type).

!!! Abstract "**Format:** `#!runner-config random_seed i0`"
    
    **`#!runner-config i0`: (`integer`, default: `200`)**
    :   Seed value.
    
    [:material-code-tags: **Source variable** `iseed`](https://gitlab.com/TheochemGoettingen/RuNNer/-/blob/master/src-devel/main.f90#L31)

---

### `read_kalman_matrices` ++

:material-chart-bell-curve-cumulative: **Mode 1:** :material-close:{ .no } **•** 
:custom-neural-network:                **Mode 2:** :material-check:{ .yes } **•** 
:material-head-cog-outline:            **Mode 3:** :material-close:{ .no }

Restart a fit using old Kalman filter matrices from the files
`kalman.short.XXX.data` and `kalman.elec.XXX.data`. `XXX` is the nuclear
charge of the respective element. Using old Kalman matrices will reduce
the oscillations of the errors when a fit is restarted with the Kalman
filter. The Kalman matrices are written to the files using the keyword
[`#!runner-config save_kalman_matrices`](/runner/reference/keywords/#save_kalman_matrices)

!!! Abstract "**Format:** `#!runner-config read_kalman_matrices` (`logical`, default: `False`)"
    
    This keyword does not have any further options.

    [:material-code-tags: **Source variable** `lrestkalman`](https://gitlab.com/TheochemGoettingen/RuNNer/-/blob/master/src-devel/fittingoptions.f90#L144)

!!! warning
    Caution: When using an old Kalman matrix for restart, don't change the 
    number of weight parameters to be optimized, e.g. by fixing some weights.

---

### `read_unformatted` 

:material-chart-bell-curve-cumulative: **Mode 1:** :material-close:{ .no } **•** 
:custom-neural-network:                **Mode 2:** :material-check:{ .yes } **•** 
:material-head-cog-outline:            **Mode 3:** :material-check:{ .yes }

Read old NN weights and/or an old Kalman matrix from an unformatted input file.

!!! Abstract "**Format:** `#!runner-config read_unformatted` (`logical`, default: `False`)"
    
    This keyword does not have any further options.

    [:material-code-tags: **Source variable** `lreadunformatted`](https://gitlab.com/TheochemGoettingen/RuNNer/-/blob/master/src-devel/globaloptions.f90#L136)

### `regularize_fit_param`

:material-chart-bell-curve-cumulative: **Mode 1:** :material-close:{ .no } **•** 
:custom-neural-network:                **Mode 2:** :material-check:{ .yes } **•** 
:material-head-cog-outline:            **Mode 3:** :material-close:{ .no }

This keyword switches on L2 regularization, mainly for the electrostatic part in
4G-HDNNPs.

!!! Abstract "**Format:** `#!runner-config regularize_fit_param a0`"
    
    **`#!runner-config a0`: (`real`, default: `0.0`)**
    :   Regularization parameter. Recommended setting is $10^{-6}$.
    
    [:material-code-tags: **Source variable** `restrictw`](https://gitlab.com/TheochemGoettingen/RuNNer/-/blob/master/src-devel/globaloptions.f90#L98)

---

### `remove_atom_energies` ++

:material-chart-bell-curve-cumulative: **Mode 1:** :material-check:{ .yes } **•** 
:custom-neural-network:                **Mode 2:** :material-close:{ .no } **•** 
:material-head-cog-outline:            **Mode 3:** :material-check:{ .yes }

Remove the energies of the free atoms from the total energies per atom to reduce
the absolute values of the target energies. This means that when this keyword is
used, `RuNNer` will fit binding energies instead of total energies. This is
expected to facilitate the fitting process because binding energies are closer
to zero. 

!!! Abstract "**Format:** `#!runner-config remove_atom_energies` (`logical`, default: `False`)"
    
    This keyword does not have any further options.

    [:material-code-tags: **Source variable** `lremoveatomenergies`](https://gitlab.com/TheochemGoettingen/RuNNer/-/blob/master/src-devel/globaloptions.f90#L132)

!!! Warning "Hint"
    If set, the atomic energies of all elements need to be specified by a
    set of 
    [`#!runner-config atom_energy`](/runner/reference/keywords/#atom_energy)
    keywords.

### `remove_vdw_energies` *

:material-chart-bell-curve-cumulative: **Mode 1:** :material-close:{ .no } **•** 
:custom-neural-network:                **Mode 2:** :material-check:{ .yes } **•** 
:material-head-cog-outline:            **Mode 3:** :material-close:{ .no }

Subtract van-der-Waals dispersion energy and forces from the reference data
before fitting a neural network potential. 

!!! Abstract "**Format:** `#!runner-config remove_vdw_energies` (`logical`, default: `False`)"
    
    This keyword does not have any further options.

    [:material-code-tags: **Source variable** `lremovevdwenergies`](https://gitlab.com/TheochemGoettingen/RuNNer/-/blob/master/src-devel/globaloptions.f90#L133)

!!! Warning "Hint"
    If this keyword is used the following keywords need to be given as well:

    * [`#!runner-config vdw_type`](/runner/reference/keywords/#vdw_type)  
    * [`#!runner-config vdw_cutoff`](/runner/reference/keywords/#vdw_cutoff),  
    * [`#!runner-config vdw_coefficient`](/runner/reference/keywords/#vdw_coefficient) 
      for each unique pair of elements in the data set,
    * [`#!runner-config vdw_radius`](/runner/reference/keywords/#vdw_radius) 
      for each element in the data set,
    * [`#!runner-config vdw_screening`](/runner/reference/keywords/#vdw_screening),

---

### `repeated_energy_update` ++

:material-chart-bell-curve-cumulative: **Mode 1:** :material-close:{ .no } **•** 
:custom-neural-network:                **Mode 2:** :material-check:{ .yes } **•** 
:material-head-cog-outline:            **Mode 3:** :material-close:{ .no }

If this keyword is set, the weights of the short-range NN are updated a second
time after the force update with respect to the total energies in the data set. 
This usually results in a more accurate potential energy fitting at the cost of
slightly detiorated forces.

!!! Abstract "**Format:** `#!runner-config repeated_energy_update` (`logical`, default: `False`)"
    
    This keyword does not have any further options.

    [:material-code-tags: **Source variable** `lrepeate`](https://gitlab.com/TheochemGoettingen/RuNNer/-/blob/master/src-devel/fittingoptions.f90#L175)

---

### `reset_kalman` 

:material-chart-bell-curve-cumulative: **Mode 1:** :material-close:{ .no } **•** 
:custom-neural-network:                **Mode 2:** :material-check:{ .yes } **•** 
:material-head-cog-outline:            **Mode 3:** :material-close:{ .no }

Re-initialize the correlation matrix of the Kalman filter at each new training
epoch.

!!! Abstract "**Format:** `#!runner-config reset_kalman` (`logical`, default: `False`)"
    
    This keyword does not have any further options.

    [:material-code-tags: **Source variable** `lresetkalman`](https://gitlab.com/TheochemGoettingen/RuNNer/-/blob/master/src-devel/fittingoptions.f90#L179)

---

### `restrict_weights`

:material-chart-bell-curve-cumulative: **Mode 1:** :material-close:{ .no } **•** 
:custom-neural-network:                **Mode 2:** :material-check:{ .yes } **•** 
:material-head-cog-outline:            **Mode 3:** :material-close:{ .no }

Restrict the weights of the NN to the interval 
[`-restrictw +1.0`, `restrictw - 1.0`].

!!! Abstract "**Format:** `#!runner-config restrict_weights a0`"
    
    **`#!runner-config a0`: (`real`, default: `-100000`)**
    :   Boundary value for neural network weights. Must be positive.
    
    [:material-code-tags: **Source variable** `restrictw`](https://gitlab.com/TheochemGoettingen/RuNNer/-/blob/master/src-devel/fittingoptions.f90#L104)

---

### `runner_mode` ++

:material-chart-bell-curve-cumulative: **Mode 1:** :material-check:{ .yes } **•** 
:custom-neural-network:                **Mode 2:** :material-check:{ .yes } **•** 
:material-head-cog-outline:            **Mode 3:** :material-check:{ .yes }

Choose the operating mode of `RuNNer`.

!!! Abstract "**Format:** `#!runner-config runner_mode i0`"
    
    **`#!runner-config a0`: (`integer`, default: `None`)**
    :   The chosen mode of `RuNNer`. Can be one of:

        * **`1`:**
        :    Preparation mode. Generate the symmetry functions from structures 
             in the `input.data` file.
            
        * **`2`:** 
        :    Fitting mode. Determine the NN weight parameters.
        
        * **`3`:** 
        :    Production mode. Application of the NN potential, prediction of the 
             energy and forces of all structures in the `input.data` file.
    
    [:material-code-tags: **Source variable** `mode`](https://gitlab.com/TheochemGoettingen/RuNNer/-/blob/master/src-devel/nnflags.f90#L24)

!!! warning "Hint"
    This keyword is mandatory in all `RuNNer` modes.

---

### `save_kalman_matrices` *

:material-chart-bell-curve-cumulative: **Mode 1:** :material-close:{ .no } **•** 
:custom-neural-network:                **Mode 2:** :material-check:{ .yes } **•** 
:material-head-cog-outline:            **Mode 3:** :material-close:{ .no }

Save the Kalman filter matrices to the files `kalman.short.XXX.data` and
`kalman.elec.XXX.data`. `XXX` is the nuclear charge of the respective
element. The Kalman matrices are read from the files using the keyword 
[`#!runner-config read_kalman_matrices`](/runner/reference/keywords/#read_kalman_matrices).

!!! Abstract "**Format:** `#!runner-config save_kalman_matrices` (`logical`, default: `False`)"
    
    This keyword does not have any further options.

    [:material-code-tags: **Source variable** `lsavekalman`](https://gitlab.com/TheochemGoettingen/RuNNer/-/blob/master/src-devel/fittingoptions.f90#L143)

!!! warning "Hint"
    This keyword is only active if the Kalman filter is used as optimization 
    algorithm.
    
!!! warning "Hint"
    **Caution:** The files can be large. 

---

### `scale_max_elec` +

:material-chart-bell-curve-cumulative: **Mode 1:** :material-check:{ .yes } **•** 
:custom-neural-network:                **Mode 2:** :material-check:{ .yes } **•** 
:material-head-cog-outline:            **Mode 3:** :material-check:{ .yes }

Rescale the electrostatic symmetry functions to an interval given by

* [`#!runner-config scale_min_elec`](/runner/reference/keywords/#scale_min_elec) and 
* [`#!runner-config scale_max_elec`](/runner/reference/keywords/#scale_max_elec) 

For further details please see 
[`#!runner-config scale_symmetry_functions`](/runner/reference/keywords/#scale_symmetry_functions).

!!! Abstract "**Format:** `#!runner-config scale_max_elec a0`"
    
    **`#!runner-config a0`: (`real`, default: `1.0`)**
    :   Upper boundary value for rescaling the electrostatic symmetry functions.
    
    [:material-code-tags: **Source variable** `scmax_elec`](https://gitlab.com/TheochemGoettingen/RuNNer/-/blob/master/src-devel/nnewald.f90#L34)

---

### `scale_max_short` 

:material-chart-bell-curve-cumulative: **Mode 1:** :material-check:{ .yes } **•** 
:custom-neural-network:                **Mode 2:** :material-check:{ .yes } **•** 
:material-head-cog-outline:            **Mode 3:** :material-check:{ .yes }

Rescale the electrostatic symmetry functions to an interval given by

* [`#!runner-config scale_min_elec`](/runner/reference/keywords/#scale_min_elec) and 
* [`#!runner-config scale_max_elec`](/runner/reference/keywords/#scale_max_elec) 

For further details please see 
[`#!runner-config scale_symmetry_functions`](/runner/reference/keywords/#scale_symmetry_functions).

!!! Abstract "**Format:** `#!runner-config scale_max_short a0`"
    
    **`#!runner-config a0`: (`real`, default: `1.0`)**
    :   Upper boundary value for rescaling the electrostatic symmetry functions.
    
    [:material-code-tags: **Source variable** `scmax_elec`](https://gitlab.com/TheochemGoettingen/RuNNer/-/blob/master/src-devel/nnewald.f90#L34)

---

### `scale_max_short_atomic` +

:material-chart-bell-curve-cumulative: **Mode 1:** :material-check:{ .yes } **•** 
:custom-neural-network:                **Mode 2:** :material-check:{ .yes } **•** 
:material-head-cog-outline:            **Mode 3:** :material-check:{ .yes }

Rescale the short-range symmetry functions to an interval given by

* [`#!runner-config scale_min_short_atomic`](/runner/reference/keywords/#scale_min_short_atomic) and 
* [`#!runner-config scale_max_short_atomic`](/runner/reference/keywords/#scale_max_short_atomic) 

For further details please see 
[`#!runner-config scale_symmetry_functions`](/runner/reference/keywords/#scale_symmetry_functions).

!!! Abstract "**Format:** `#!runner-config scale_max_short_atomic a0`"
    
    **`#!runner-config a0`: (`real`, default: `1.0`)**
    :   Upper boundary value for rescaling the short-range symmetry functions.
    
    [:material-code-tags: **Source variable** `scmax_short_atomic`](https://gitlab.com/TheochemGoettingen/RuNNer/-/blob/master/src-devel/nnshort_atomic.f90#L34)

---

### `scale_max_short_pair` 

:material-chart-bell-curve-cumulative: **Mode 1:** :material-check:{ .yes } **•** 
:custom-neural-network:                **Mode 2:** :material-check:{ .yes } **•** 
:material-head-cog-outline:            **Mode 3:** :material-check:{ .yes }

Rescale the short-range pairwise symmetry functions to an interval given by

* [`#!runner-config scale_min_short_pair`](/runner/reference/keywords/#scale_min_short_pair) and 
* [`#!runner-config scale_max_short_pair`](/runner/reference/keywords/#scale_max_short_pair) 

For further details please see 
[`#!runner-config scale_symmetry_functions`](/runner/reference/keywords/#scale_symmetry_functions).

!!! Abstract "**Format:** `#!runner-config scale_max_short_pair a0`"
    
    **`#!runner-config a0`: (`real`, default: `1.0`)**
    :   Upper boundary value for rescaling the short-range pairwise symmetry functions.
    
    [:material-code-tags: **Source variable** `scmax_short_pair`](https://gitlab.com/TheochemGoettingen/RuNNer/-/blob/master/src-devel/nnshort_atomic.f90#L34)

---

### `scale_min_elec` +

:material-chart-bell-curve-cumulative: **Mode 1:** :material-check:{ .yes } **•** 
:custom-neural-network:                **Mode 2:** :material-check:{ .yes } **•** 
:material-head-cog-outline:            **Mode 3:** :material-check:{ .yes }

Rescale the electrostatic symmetry functions to an interval given by

* [`#!runner-config scale_min_elec`](/runner/reference/keywords/#scale_min_elec) and 
* [`#!runner-config scale_max_elec`](/runner/reference/keywords/#scale_max_elec) 

For further details please see 
[`#!runner-config scale_symmetry_functions`](/runner/reference/keywords/#scale_symmetry_functions).

!!! Abstract "**Format:** `#!runner-config scale_min_elec a0`"
    
    **`#!runner-config a0`: (`real`, default: `0.0`)**
    :   Lower boundary value for rescaling the electrostatic symmetry functions.
    
    [:material-code-tags: **Source variable** `scmin_elec`](https://gitlab.com/TheochemGoettingen/RuNNer/-/blob/master/src-devel/nnewald.f90#L33)

---

### `scale_min_short_atomic` +

:material-chart-bell-curve-cumulative: **Mode 1:** :material-check:{ .yes } **•** 
:custom-neural-network:                **Mode 2:** :material-check:{ .yes } **•** 
:material-head-cog-outline:            **Mode 3:** :material-check:{ .yes }

Rescale the short-range symmetry functions to an interval given by

* [`#!runner-config scale_min_short_atomic`](/runner/reference/keywords/#scale_min_short_atomic) and 
* [`#!runner-config scale_max_short_atomic`](/runner/reference/keywords/#scale_max_short_atomic) 

For further details please see 
[`#!runner-config scale_symmetry_functions`](/runner/reference/keywords/#scale_symmetry_functions).

!!! Abstract "**Format:** `#!runner-config scale_min_short_atomic a0`"
    
    **`#!runner-config a0`: (`real`, default: `0.0`)**
    :   Lower boundary value for rescaling the short-range symmetry functions.
    
    [:material-code-tags: **Source variable** `scmin_short_atomic`](https://gitlab.com/TheochemGoettingen/RuNNer/-/blob/master/src-devel/nnshort_atomic.f90#L33)

---

### `scale_min_short_pair` +

:material-chart-bell-curve-cumulative: **Mode 1:** :material-check:{ .yes } **•** 
:custom-neural-network:                **Mode 2:** :material-check:{ .yes } **•** 
:material-head-cog-outline:            **Mode 3:** :material-check:{ .yes }

Rescale the short-range pairwise symmetry functions to an interval given by

* [`#!runner-config scale_min_short_pair`](/runner/reference/keywords/#scale_min_short_pair) and 
* [`#!runner-config scale_max_short_pair`](/runner/reference/keywords/#scale_max_short_pair) 

For further details please see 
[`#!runner-config scale_symmetry_functions`](/runner/reference/keywords/#scale_symmetry_functions).

!!! Abstract "**Format:** `#!runner-config scale_min_short_pair a0`"
    
    **`#!runner-config a0`: (`real`, default: `0.0`)**
    :   Lower boundary value for rescaling the short-range pairwise symmetry functions.
    
    [:material-code-tags: **Source variable** `scmin_short_pair`](https://gitlab.com/TheochemGoettingen/RuNNer/-/blob/master/src-devel/nnshort_atomic.f90#L33)

---

### `scale_symmetry_functions` ++

:material-chart-bell-curve-cumulative: **Mode 1:** :material-check:{ .yes } **•** 
:custom-neural-network:                **Mode 2:** :material-check:{ .yes } **•** 
:material-head-cog-outline:            **Mode 3:** :material-check:{ .yes }

Rescale symmetry functions to a certain interval (the default interval is 0 to 
1). This has numerical advantages if the orders of magnitudes of different 
symmetry functions are very different. If the minimum and maximum value for a
symmetry function is the same for all structures, rescaling is not possible and
`RuNNer` will terminate with an error. The interval can be specified by the 
keywords 

* [`#!runner-config scale_min_short_atomic`](/runner/reference/keywords/#scale_min_short_atomic),
* [`#!runner-config scale_max_short_atomic`](/runner/reference/keywords/#scale_max_short_atomic), 
* [`#!runner-config scale_min_short_pair`](/runner/reference/keywords/#scale_min_short_pair), and
* [`#!runner-config scale_max_short_pair`](/runner/reference/keywords/#scale_max_short_pair) 

for the short range / pairwise NN and by 

* [`#!runner-config scale_min_elec`](/runner/reference/keywords/#scale_min_elec) and 
* [`#!runner-config scale_max_elec`](/runner/reference/keywords/#scale_max_elec) 

for the electrostatic NN. 

!!! Abstract "**Format:** `#!runner-config scale_symmetry_functions` (`logical`, default: `False`)"
    
    This keyword does not have any further options.

    [:material-code-tags: **Source variable** `lscalesym`](https://gitlab.com/TheochemGoettingen/RuNNer/-/blob/master/src-devel/fittingoptions.f90#L143)

!!! warning "Hint"
    If non-default boundaries are given, a combination with the keyword 
    [`#!runner-config center_symmetry_functions`](/runner/reference/keywords/#center_symmetry_functions) 
    is not possible.

---

### `screen_electrostatics` 

---

### `separate_bias_ini_short` 

:material-chart-bell-curve-cumulative: **Mode 1:** :material-close:{ .no } **•** 
:custom-neural-network:                **Mode 2:** :material-check:{ .yes } **•** 
:material-head-cog-outline:            **Mode 3:** :material-close:{ .no }

Request a separate random initialization of the bias weights at the beginning of
`#!runner-config runner_mode 2` on an interval between 
`#!runner-config biasweights_min` and `#!runner-config biasweights_max`. 

!!! Abstract "**Format:** `#!runner-config separate_bias_ini_short` (`logical`, default: `False`)"
    
    This keyword does not have any further options.
    
    [:material-code-tags: **Source variable** `lseparatebiasini`](https://gitlab.com/TheochemGoettingen/RuNNer/-/blob/master/src-devel/fittingoptions.f90#L180)

!!! warning
    This keyword requires the use of keywords
    [`#!runner-config biasweights_max`](/runner/reference/keywords/#biasweights_max)  and
    [`#!runner-config biasweights_min`](/runner/reference/keywords/#biasweights_min) 

---

### `separate_kalman_short` 

:material-chart-bell-curve-cumulative: **Mode 1:** :material-close:{ .no } **•** 
:custom-neural-network:                **Mode 2:** :material-check:{ .yes } **•** 
:material-head-cog-outline:            **Mode 3:** :material-close:{ .no }

Use a different Kalman filter correlation matrix for the energy and force 
update. 

!!! Abstract "**Format:** `#!runner-config separate_kalman_short` (`logical`, default: `False`)"
    
    This keyword does not have any further options.

    [:material-code-tags: **Source variable** `lsepkalman`](https://gitlab.com/TheochemGoettingen/RuNNer/-/blob/master/src-devel/fittingoptions.f90#L176)

!!! warning "Hint"
    The keyword [`#!runner-config element_decoupled_kalman`](/runner/reference/keywords/#element_decoupled_kalman)
    will enforce a similar behaviour.

---

### `short_energy_error_threshold` ++

:material-chart-bell-curve-cumulative: **Mode 1:** :material-close:{ .no } **•** 
:custom-neural-network:                **Mode 2:** :material-check:{ .yes } **•** 
:material-head-cog-outline:            **Mode 3:** :material-close:{ .no }

Threshold value for the error of the energies in units of the RMSE of the
previous epoch. A value of 0.3 means that only charges with an error larger than
0.3*RMSE will be used for the weight update. Large values (about 1.0) will speed
up the first epochs, because only a few points will be used. 

!!! Abstract "**Format:** `#!runner-config short_energy_error_threshold a0`"
    
    **`#!runner-config a0`: (`real`, default: `0.0`)**
    :   Fraction of energy RMSE that a point needs to reach to be used in the
        weight update.
    
        [:material-code-tags: **Source variable** `kalmanthreshold`](https://gitlab.com/TheochemGoettingen/RuNNer/-/blob/master/src-devel/fittingoptions.f90#L90)

!!! warning "Hint"
    For this keyword to take effect, you need to use the Kalman filter algorithm.

!!! warning "Hint"
    If you, instead, want to set a fixed threshold, please refer to 
    [`#!runner-config fixed_short_energy_error_threshold`](/runner/reference/keywords/#fixed_short_energy_error_threshold).

---

### `short_energy_fraction` ++

:material-chart-bell-curve-cumulative: **Mode 1:** :material-close:{ .no } **•** 
:custom-neural-network:                **Mode 2:** :material-check:{ .yes } **•** 
:material-head-cog-outline:            **Mode 3:** :material-close:{ .no }

Defines the random fraction of energies used for fitting the short range
weights.

!!! Abstract "**Format:** `#!runner-config short_energy_fraction a0`"
    
    **`#!runner-config a0`: (`real`, default: `1.0`)**
    :   Fraction of energies used for short-range fitting. 100% = 1.0.
    
        [:material-code-tags: **Source variable** `energyrnd`](https://gitlab.com/TheochemGoettingen/RuNNer/-/blob/master/src-devel/fittingoptions.f90#L73)

---

### `short_energy_group` ++

:material-chart-bell-curve-cumulative: **Mode 1:** :material-close:{ .no } **•** 
:custom-neural-network:                **Mode 2:** :material-check:{ .yes } **•** 
:material-head-cog-outline:            **Mode 3:** :material-close:{ .no }

Do not update the short range NN weights after the presentation of an 
individual atomic charge, but average the derivatives with respect to the 
weights over the specified number of structures for each element.

!!! Abstract "**Format:** `#!runner-config short_energy_group i0`"
    
    **`#!runner-config i0`: (`integer`, default: `1`)**
    :   Number of structures per group. The maximum is given by
        [`#!runner-config points_in_memory`](/runner/reference/keywords/#points_in_memory).
    
    [:material-code-tags: **Source variable** `nenergygroup`](https://gitlab.com/TheochemGoettingen/RuNNer/-/blob/master/src-devel/fittingoptions.f90#L26)

---

### `short_force_error_threshold` ++

:material-chart-bell-curve-cumulative: **Mode 1:** :material-close:{ .no } **•** 
:custom-neural-network:                **Mode 2:** :material-check:{ .yes } **•** 
:material-head-cog-outline:            **Mode 3:** :material-close:{ .no }

Threshold value for the error of the atomic forces in units of the RMSE of the
previous epoch. A value of 0.3 means that only forces with an error larger than
0.3*RMSE will be used for the weight update. Large values (about 1.0) will speed
up the first epochs, because only a few points will be used. 

!!! Abstract "**Format:** `#!runner-config short_force_error_threshold a0`"
    
    **`#!runner-config a0`: (`real`, default: `0.0`)**
    :   Fraction of force RMSE that a point needs to reach to be used in the
        weight update.
    
        [:material-code-tags: **Source variable** `kalmanthresholdf`](https://gitlab.com/TheochemGoettingen/RuNNer/-/blob/master/src-devel/fittingoptions.f90#L91)

!!! warning "Hint"
    For this keyword to take effect, you need to use the Kalman filter algorithm.

---

### `short_force_fraction` ++

:material-chart-bell-curve-cumulative: **Mode 1:** :material-close:{ .no } **•** 
:custom-neural-network:                **Mode 2:** :material-check:{ .yes } **•** 
:material-head-cog-outline:            **Mode 3:** :material-close:{ .no }

Defines the random fraction of forces used for fitting the short range
weights.

!!! Abstract "**Format:** `#!runner-config short_force_fraction a0`"
    
    **`#!runner-config a0`: (`real`, default: `1.0`)**
    :   Fraction of force used for short-range fitting. 100% = 1.0.
    
        [:material-code-tags: **Source variable** `forcernd`](https://gitlab.com/TheochemGoettingen/RuNNer/-/blob/master/src-devel/fittingoptions.f90#L74)


---

### `short_force_group` ++

:material-chart-bell-curve-cumulative: **Mode 1:** :material-close:{ .no } **•** 
:custom-neural-network:                **Mode 2:** :material-check:{ .yes } **•** 
:material-head-cog-outline:            **Mode 3:** :material-close:{ .no }

Do not update the short range NN weights after the presentation of an 
individual atomic force, but average the derivatives with respect to the 
weights over the specified number of forces for each element.

!!! Abstract "**Format:** `#!runner-config short_force_group i0`"
    
    **`#!runner-config i0`: (`integer`, default: `1`)**
    :   Number of structures per group. The maximum is given by
        [`#!runner-config points_in_memory`](/runner/reference/keywords/#points_in_memory).
    
    [:material-code-tags: **Source variable** `nforcegroup`](https://gitlab.com/TheochemGoettingen/RuNNer/-/blob/master/src-devel/fittingoptions.f90#L27)

---

### `shuffle_weights_short_atomic` 

:material-chart-bell-curve-cumulative: **Mode 1:** :material-close:{ .no } **•** 
:custom-neural-network:                **Mode 2:** :material-check:{ .yes } **•** 
:material-head-cog-outline:            **Mode 3:** :material-close:{ .no }

Randomly shuffle some weights in the short-range atomic NN after a defined 
number of epochs.

!!! Abstract "**Format:** `#!runner-config shuffle_weights_short_atomic i0 a0`"
    
    **`#!runner-config i0`: (`integer`, default: `10`)**
    :   The weights will be shuffled every `#!runner-config i0` epochs.

    **`#!runner-config a0`: (`real`, default: `0.1`)**
    :   Treshold that a random number has to pass so that the weights at handled
        will be shuffled. This indirectly defines the number of weights that 
        will be shuffled. 

    :material-code-tags: **Source variables** [`lshuffle_weights_short_atomic`](https://gitlab.com/TheochemGoettingen/RuNNer/-/blob/master/src-devel/fittingoptions.f90#L66),
    [`nshuffle_weights_short_atomic`](https://gitlab.com/TheochemGoettingen/RuNNer/-/blob/master/src-devel/fittingoptions.f90#L64), and
    [`shuffle_weights_short_atomic`](https://gitlab.com/TheochemGoettingen/RuNNer/-/blob/master/src-devel/fittingoptions.f90#L65).

---

### `silent_mode` 

This keyword has not been implemented yet.

---

### `steepest_descent_step_charge` +

:material-chart-bell-curve-cumulative: **Mode 1:** :material-close:{ .no } **•** 
:custom-neural-network:                **Mode 2:** :material-check:{ .yes } **•** 
:material-head-cog-outline:            **Mode 3:** :material-close:{ .no }

Step size for steepest descent fitting of the atomic charges.

!!! Abstract "**Format:** `#!runner-config steepest_descent_step_charge a0`"
    
    **`#!runner-config a0`: (`real`, default: `0.01`)**
    :   Charge steepest descent step size.
    
    [:material-code-tags: **Source variable** `steepeststepq`](https://gitlab.com/TheochemGoettingen/RuNNer/-/blob/master/src-devel/fittingoptions.f90#L84)

!!! warning "Hint"
    This keyword is relevant only if the steepest descent optimization algorithm
    has been selected with 
    [`#!runner-config optmode_charge`](/runner/reference/keywords/#optmode_charge) 3.

---

### `steepest_descent_step_energy_short` +

:material-chart-bell-curve-cumulative: **Mode 1:** :material-close:{ .no } **•** 
:custom-neural-network:                **Mode 2:** :material-check:{ .yes } **•** 
:material-head-cog-outline:            **Mode 3:** :material-close:{ .no }

Step size for steepest descent fitting of the short-range energy.

!!! Abstract "**Format:** `#!runner-config steepest_descent_step_energy_short a0`"
    
    **`#!runner-config a0`: (`real`, default: `0.01`)**
    :   Short-range energy steepest descent step size.
    
    [:material-code-tags: **Source variable** `steepeststepe`](https://gitlab.com/TheochemGoettingen/RuNNer/-/blob/master/src-devel/fittingoptions.f90#L82)

!!! warning "Hint"
    This keyword is relevant only if the steepest descent optimization algorithm
    has been selected with 
    [`#!runner-config optmode_charge`](/runner/reference/keywords/#optmode_charge) 3.

---

### `steepest_descent_step_force_short` +

:material-chart-bell-curve-cumulative: **Mode 1:** :material-close:{ .no } **•** 
:custom-neural-network:                **Mode 2:** :material-check:{ .yes } **•** 
:material-head-cog-outline:            **Mode 3:** :material-close:{ .no }

Step size for steepest descent fitting of the short-range forces.

!!! Abstract "**Format:** `#!runner-config steepest_descent_step_force_short a0`"
    
    **`#!runner-config a0`: (`real`, default: `0.01`)**
    :   Short-range force steepest descent step size.
    
    [:material-code-tags: **Source variable** `steepeststepe`](https://gitlab.com/TheochemGoettingen/RuNNer/-/blob/master/src-devel/fittingoptions.f90#L83)

!!! warning "Hint"
    This keyword is relevant only if the steepest descent optimization algorithm
    has been selected with 
    [`#!runner-config optmode_charge`](/runner/reference/keywords/#optmode_charge) 3.

---

### `symfunction_correlation` 

:material-chart-bell-curve-cumulative: **Mode 1:** :material-close:{ .no } **•** 
:custom-neural-network:                **Mode 2:** :material-check:{ .yes } **•** 
:material-head-cog-outline:            **Mode 3:** :material-close:{ .no }

Determine and print Pearson's correlation of all pairs of symmetry functions.

!!! Abstract "**Format:** `#!runner-config symfunction_correlation` (`logical`, default: `False`)"
    
    This keyword does not have any further options.

    [:material-code-tags: **Source variable** `lpearson_correlation`](https://gitlab.com/TheochemGoettingen/RuNNer/-/blob/master/src-devel/globaloptions.f90#L164)

---

### `symfunction_electrostatic` ++

:material-chart-bell-curve-cumulative: **Mode 1:** :material-check:{ .yes } **•** 
:custom-neural-network:                **Mode 2:** :material-check:{ .yes } **•** 
:material-head-cog-outline:            **Mode 3:** :material-check:{ .yes }

Specification of the symmetry functions for a specific element with a specific 
neighbor element combination for the electrostatic NN.

!!! Abstract "**Format:** `#!runner-config symfunction_electrostatic element [element...] type [parameters] cutoff`"
    
    **`#!runner-config element [element...]`: (`character [characters]`)**
    :   The periodic table symbol of the elements. One neighbor 
        `#!runner-config [element]` has to be given for radial symmetry functions,
        two for angular symmetry functions.
    
    **`#!runner-config type [parameters]`: (`integer [reals]`)**
    :   The type of symmetry function to be used. Different `parameters` have to
        be set depending on the choice of `#!runner-config type`. They are 
        explained under
        [`#!runner-config global_symfunction_short`](/runner/reference/keywords/#global_symfunction_short).

    **`#!runner-config cutoff`: (`real`, default: `None`)**
    :   The symmetry function cutoff radius (unit: Bohr).

    [:material-code-tags: **Read in** `readsymfunctionelementatomic(...)`](https://gitlab.com/TheochemGoettingen/RuNNer/-/blob/master/src-devel/readsymfunctionelementatomic.f90)

---

### `symfunction_short` ++

:material-chart-bell-curve-cumulative: **Mode 1:** :material-check:{ .yes } **•** 
:custom-neural-network:                **Mode 2:** :material-check:{ .yes } **•** 
:material-head-cog-outline:            **Mode 3:** :material-check:{ .yes }

Specification of the symmetry functions for a specific element with a specific 
neighbor element combination for the short-range NN.

!!! Abstract "**Format:** `#!runner-config symfunction_short element [element...] type [parameters] cutoff`"
    
    **`#!runner-config element [element...]`: (`character [characters]`)**
    :   The periodic table symbol of the elements. One neighbor 
        `#!runner-config [element]` has to be given for radial symmetry functions,
        two for angular symmetry functions.
    
    **`#!runner-config type [parameters]`: (`integer [reals]`)**
    :   The type of symmetry function to be used. Different `parameters` have to
        be set depending on the choice of `#!runner-config type`. They are 
        explained under
        [`#!runner-config global_symfunction_short`](/runner/reference/keywords/#global_symfunction_short).

    **`#!runner-config cutoff`: (`real`, default: `None`)**
    :   The symmetry function cutoff radius (unit: Bohr).

    [:material-code-tags: **Read in** `readsymfunctionelementatomic(...)`](https://gitlab.com/TheochemGoettingen/RuNNer/-/blob/master/src-devel/readsymfunctionelementatomic.f90)

---

### `test_fraction` ++

:material-chart-bell-curve-cumulative: **Mode 1:** :material-check:{ .yes } **•** 
:custom-neural-network:                **Mode 2:** :material-close:{ .no } **•** 
:material-head-cog-outline:            **Mode 3:** :material-close:{ .no }

Threshold for splitting between training and testing set in 
[`#!runner-config runner_mode`](/runner/reference/keywords/runner_mode) 1.

!!! Abstract "**Format:** `#!runner-config test_fraction a0`"
    
    **`#!runner-config a0`: (`real`, default: `0.01`)**
    :   Splitting ratio. A value of e.g. 0.1 means that 10% of the structures 
        in the `input.data` file will be used as test set and 90% as training set.
    
    [:material-code-tags: **Source variable** `splitthres`](https://gitlab.com/TheochemGoettingen/RuNNer/-/blob/master/src-devel/mode1options.f90#L24)

!!! warning "Hint"
    The specific splitting can be modified by changing the value of 
    [`#!runner-config random_seed`](/runner/reference/keywords/#random_seed).

---

### `total_charge_error_threshold` *

This keyword has been deprecated and is unlikely to work.

---

### `update_single_element` *

:material-chart-bell-curve-cumulative: **Mode 1:** :material-close:{ .no } **•** 
:custom-neural-network:                **Mode 2:** :material-check:{ .yes } **•** 
:material-head-cog-outline:            **Mode 3:** :material-close:{ .no }

During training, only the NN weight parameters for the NNs of a
specified element will be updated. In this case the printed errors for the 
forces and the charges will refer only to this element. The total energy error 
will remain large since some NNs are not optimized. 

!!! Abstract "**Format:** `#!runner-config update_single_element i0`"
    
    **`#!runner-config i0`: (`integer`, default: `None`)**
    :   The nuclear charge of the element whose NN should be updated.

    :material-code-tags: **Source variables** [`lupdatebyelement`](https://gitlab.com/TheochemGoettingen/RuNNer/-/blob/master/src-devel/fittingoptions.f90#L154) and
    [`elemupdate`](https://gitlab.com/TheochemGoettingen/RuNNer/-/blob/master/src-devel/fittingoptions.f90#L33).

#### Example

```runner-config
update_single_element 30
```

will optimize only the weights for the NNs describing Zn.

---

### `update_worst_charges` *

:material-chart-bell-curve-cumulative: **Mode 1:** :material-close:{ .no } **•** 
:custom-neural-network:                **Mode 2:** :material-check:{ .yes } **•** 
:material-head-cog-outline:            **Mode 3:** :material-close:{ .no }

To speed up the fits for each block specified by
[`#!runner-config points_in_memory`](/runner/reference/keywords/#points_in_memory)
first the worst charges are determined. Only these charges are then used for the
weight update for this block of points, no matter if the fit would be reduced
during the update.

!!! Abstract "**Format:** `#!runner-config update_worst_charges a0`"
    
    **`#!runner-config a0`: (`real`, default: `None`)**
    : The percentage of worst charges to be considered for the weight update.
      A value of 0.1 here means to identify the worst 10%.  
    
    :material-code-tags: **Source variables** [`luseworstq`](https://gitlab.com/TheochemGoettingen/RuNNer/-/blob/master/src-devel/fittingoptions.f90#L148) and
    [`worstq`](https://gitlab.com/TheochemGoettingen/RuNNer/-/blob/master/src-devel/fittingoptions.f90#L76).

!!! warning "Hint"
    This keyword can be combined with 
    [`#!runner-config charge_error_threshold`](/runner/reference/keywords/#charge_error_threshold)
    to eliminate unnecessary updates. 

---

### `update_worst_short_energies` *

:material-chart-bell-curve-cumulative: **Mode 1:** :material-close:{ .no } **•** 
:custom-neural-network:                **Mode 2:** :material-check:{ .yes } **•** 
:material-head-cog-outline:            **Mode 3:** :material-close:{ .no }

To speed up the fits for each block specified by
[`#!runner-config points_in_memory`](/runner/reference/keywords/#points_in_memory)
first the worst energies are determined. Only these points are then used for the
weight update for this block of points, no matter if the fit would be reduced
during the update.

!!! Abstract "**Format:** `#!runner-config update_worst_short_energies a0`"
    
    **`#!runner-config a0`: (`real`, default: `None`)**
    : The percentage of worst energies to be considered for the weight update.
      A value of 0.1 here means to identify the worst 10%. 
    
    :material-code-tags: **Source variables** [`luseworste`](https://gitlab.com/TheochemGoettingen/RuNNer/-/blob/master/src-devel/fittingoptions.f90#L146) and
    [`worste`](https://gitlab.com/TheochemGoettingen/RuNNer/-/blob/master/src-devel/fittingoptions.f90#L74).

!!! warning "Hint"
    This keyword can be combined with 
    [`#!runner-config short_energy_error_threshold`](/runner/reference/keywords/#short_energy_error_threshold)
    to eliminate unnecessary updates. 

---

### `update_worst_short_forces` *

:material-chart-bell-curve-cumulative: **Mode 1:** :material-close:{ .no } **•** 
:custom-neural-network:                **Mode 2:** :material-check:{ .yes } **•** 
:material-head-cog-outline:            **Mode 3:** :material-close:{ .no }

To speed up the fits for each block specified by
[`#!runner-config points_in_memory`](/runner/reference/keywords/#points_in_memory)
first the worst forces are determined. Only these points are then used for the
weight update for this block of points, no matter if the fit would be reduced
during the update.

!!! Abstract "**Format:** `#!runner-config update_worst_short_forces a0`"
    
    **`#!runner-config a0`: (`real`, default: `None`)**
    : The percentage of worst forces to be considered for the weight update.
      A value of 0.1 here means to identify the worst 10%. 
    
    :material-code-tags: **Source variables** [`luseworstf`](https://gitlab.com/TheochemGoettingen/RuNNer/-/blob/master/src-devel/fittingoptions.f90#L147) and
    [`worstf`](https://gitlab.com/TheochemGoettingen/RuNNer/-/blob/master/src-devel/fittingoptions.f90#L75).

!!! warning "Hint"
    This keyword can be combined with 
    [`#!runner-config short_force_error_threshold`](/runner/reference/keywords/#short_energy_error_threshold)
    to eliminate unnecessary updates. 

---

### `use_atom_charges` ++

:material-chart-bell-curve-cumulative: **Mode 1:** :material-close:{ .yes } **•** 
:custom-neural-network:                **Mode 2:** :material-check:{ .yes } **•** 
:material-head-cog-outline:            **Mode 3:** :material-check:{ .yes }

Use atomic charges for fitting.
%At the moment this flag will be switched
%on automatically by `RuNNer` if electrostatic NNs are requested. In
%future versions of `RuNNer` this keyword will be used to control
%different origins of atomic charges.

!!! Abstract "**Format:** `#!runner-config use_atom_charges` (`logical`, default: `False`)"
    
    This keyword does not have any further options.

    [:material-code-tags: **Source variable** `luseatomcharges`](https://gitlab.com/TheochemGoettingen/RuNNer/-/blob/master/src-devel/globaloptions.f90#L144)

---

### `use_atom_spins` ++

:material-chart-bell-curve-cumulative: **Mode 1:** :material-close:{ .yes } **•** 
:custom-neural-network:                **Mode 2:** :material-check:{ .yes } **•** 
:material-head-cog-outline:            **Mode 3:** :material-check:{ .yes }

Use atomic spins for high-dimensional neural network spin prediction.

!!! Abstract "**Format:** `#!runner-config use_atom_spins` (`logical`, default: `False`)"
    
    This keyword does not have any further options.

    [:material-code-tags: **Source variable** `luseatomspins`](https://gitlab.com/TheochemGoettingen/RuNNer/-/blob/master/src-devel/globaloptions.f90#L144)

---

### `use_atom_energies`

:material-chart-bell-curve-cumulative: **Mode 1:** :material-close:{ .yes } **•** 
:custom-neural-network:                **Mode 2:** :material-check:{ .yes } **•** 
:material-head-cog-outline:            **Mode 3:** :material-check:{ .yes }

Check if sum of specified atomic energies is equal to the total energy of each
structure.

!!! Abstract "**Format:** `#!runner-config use_atom_energies` (`logical`, default: `False`)"
    
    This keyword does not have any further options.

    [:material-code-tags: **Source variable** `luseatomenergies`](https://gitlab.com/TheochemGoettingen/RuNNer/-/blob/master/src-devel/globaloptions.f90#L145)

---

### `use_charge_constraint` *

This keyword has been deprecated and is unlikely to work.

---

### `use_damping` *

:material-chart-bell-curve-cumulative: **Mode 1:** :material-close:{ .no } **•** 
:custom-neural-network:                **Mode 2:** :material-check:{ .yes } **•** 
:material-head-cog-outline:            **Mode 3:** :material-close:{ .no }

In order to avoid too large (too positive or
too negative) weight parameters, this damping scheme can be used. An
additional term is added to the absolute energy error.

* For the short range energy the modification is: 
  `errore=(1.d0-dampw)errore + (dampwsumwsquared) /dble(totnum_weightsshort)`
* For the short range forces the modification is:
  `errorf=(1.d0-dampw)errorf+(dampwsumwsquared) /dble(num_weightsshort(elementindex(zelem(i2))))`
* For the short range forces the modification is:
  `error=(1.d0-dampw)*error + (dampwsumwsquared) /dble(num_weightsewald(elementindex(zelem_list(idx(i1),i2))))`

!!! Abstract "**Format:** `#!runner-config use_damping a0`"
    
    **`#!runner-config a0`: (`real`, default: `None`)**
    : The damping parameter.
    
    :material-code-tags: **Source variables** [`ldampw`](https://gitlab.com/TheochemGoettingen/RuNNer/-/blob/master/src-devel/fittingoptions.f90#L155) and
    [`dampw`](https://gitlab.com/TheochemGoettingen/RuNNer/-/blob/master/src-devel/fittingoptions.f90#L97).

---

### `use_electrostatic_nn` ++

This keyword has been deprecated. Please use
[`#!runner-config electrostatic_type`](/runner/reference/keywords/#electrostatic_type) 
and 
[`#!runner-config use_electrostatics`](/runner/reference/keywords/#use_electrostatics)
from now on.

---

### `use_electrostatics` ++

:material-chart-bell-curve-cumulative: **Mode 1:** :material-close:{ .no } **•** 
:custom-neural-network:                **Mode 2:** :material-check:{ .yes } **•** 
:material-head-cog-outline:            **Mode 3:** :material-check:{ .yes }

Calculate long range electrostatic interactions explicitly. The type of
atomic charges is specified by the keyword 
[`#!runner-config electrostatic_type`](/runner/reference/keywords/#electrostatic_type)
.

!!! Abstract "**Format:** `#!runner-config use_electrostatics` (`logical`, default: `False`)"
    
    This keyword does not have any further options.

    [:material-code-tags: **Source variable** `lelec`](https://gitlab.com/TheochemGoettingen/RuNNer/-/blob/master/src-devel/nnflags.f90#L36)

---

### `use_fixed_charges` *

:material-chart-bell-curve-cumulative: **Mode 1:** :material-check:{ .yes } **•** 
:custom-neural-network:                **Mode 2:** :material-check:{ .yes } **•** 
:material-head-cog-outline:            **Mode 3:** :material-check:{ .yes }

Do not use a NN to calculate the atomic charges, but use fixed charges
for each element independent of the chemical environment.
[`#!runner-config electrostatic_type`](/runner/reference/keywords/#electrostatic_type).

!!! Abstract "**Format:** `#!runner-config use_fixed_charges` (`logical`, default: `False`)"
    
    This keyword does not have any further options.

    [:material-code-tags: **Source variable** `lusefixedcharge`](https://gitlab.com/TheochemGoettingen/RuNNer/-/blob/master/src-devel/globaloptions.f90#L161)

!!! warning "Hint"
    If this keyword is used, the fixed charges for all elements in the system must
    be specified by the keyword 
    [`#!runner-config fixed_charge`](/runner/reference/keywords/#fixed_charge).

---

### `use_gausswidth` ++

:material-chart-bell-curve-cumulative: **Mode 1:** :material-check:{ .yes } **•** 
:custom-neural-network:                **Mode 2:** :material-check:{ .yes } **•** 
:material-head-cog-outline:            **Mode 3:** :material-check:{ .yes }

Use Gaussian for modeling atomic charges during the construction of 4G-HDNNPs.

!!! Abstract "**Format:** `#!runner-config use_gausswidth` (`logical`, default: `False`)"
    
    This keyword does not have any further options.

    [:material-code-tags: **Source variable** `lusefixedcharge`](https://gitlab.com/TheochemGoettingen/RuNNer/-/blob/master/src-devel/globaloptions.f90#L161)

!!! warning "Hint"
    If this keyword is used, the Gaussian width for all elements in the system 
    must be specified by the keyword 
    [`#!runner-config fixed_gausswidth`](/runner/reference/keywords/#fixed_gausswidth).

---

### `use_noisematrix`

:material-chart-bell-curve-cumulative: **Mode 1:** :material-check:{ .yes } **•** 
:custom-neural-network:                **Mode 2:** :material-check:{ .yes } **•** 
:material-head-cog-outline:            **Mode 3:** :material-check:{ .yes }

Use noise matrix for fitting the short range weight with the short range NN 
weights with Kalman filter.

!!! Abstract "**Format:** `#!runner-config use_noisematrix` (`logical`, default: `False`)"
    
    This keyword does not have any further options.

    [:material-code-tags: **Source variable** `lusenoisematrix`](https://gitlab.com/TheochemGoettingen/RuNNer/-/blob/master/src-devel/globaloptions.f90#L150)

---

### `use_old_scaling` 

:material-chart-bell-curve-cumulative: **Mode 1:** :material-close:{ .no } **•** 
:custom-neural-network:                **Mode 2:** :material-check:{ .yes } **•** 
:material-head-cog-outline:            **Mode 3:** :material-close:{ .no }

Restart a fit with old scaling parameters for the short-range and electrostatic 
NNs. The symmetry function scaling factors are read from `scaling.data`.

!!! Abstract "**Format:** `#!runner-config use_old_scaling` (`logical`, default: `False`)"
    
    This keyword does not have any further options.

    [:material-code-tags: **Source variable** `luseoldscaling`](https://gitlab.com/TheochemGoettingen/RuNNer/-/blob/master/src-devel/fittingoptions.f90#L161)

---

### `use_old_weights_charge` ++

:material-chart-bell-curve-cumulative: **Mode 1:** :material-close:{ .no } **•** 
:custom-neural-network:                **Mode 2:** :material-check:{ .yes } **•** 
:material-head-cog-outline:            **Mode 3:** :material-close:{ .no }

Restart a fit with old weight parameters for the electrostatic NN.
The files `weightse.XXX.data` must be present.  If the training data set is 
unchanged, the error of epoch 0 must be the same as the error of the previous
fitting cycle.

!!! Abstract "**Format:** `#!runner-config use_old_weights_charge` (`logical`, default: `False`)"
    
    This keyword does not have any further options.

    [:material-code-tags: **Source variable** `luseoldweightscharge`](https://gitlab.com/TheochemGoettingen/RuNNer/-/blob/master/src-devel/fittingoptions.f90#L160)

---

### `use_old_weights_short` ++

:material-chart-bell-curve-cumulative: **Mode 1:** :material-close:{ .no } **•** 
:custom-neural-network:                **Mode 2:** :material-check:{ .yes } **•** 
:material-head-cog-outline:            **Mode 3:** :material-close:{ .no }

Restart a fit with old weight parameters for the short-range NN. This
keyword is active only in mode 2. The files `weights.XXX.data` must be present. 
If the training data set is unchanged, the error of epoch 0 must be the same as
the error of the previous fitting cycle. However, if the training data is 
different, the file `scaling.data` changes and either one of the keywords
[`#!runner-config scale_symmetry_functions`](/runner/reference/keywords/#scale_symmetry_functions)
or
[`#!runner-config center_symmetry_functions`](/runner/reference/keywords/#center_symmetry_functions)
is used, the RMSE will be different.

!!! Abstract "**Format:** `#!runner-config use_old_weights_short` (`logical`, default: `False`)"
    
    This keyword does not have any further options.

    [:material-code-tags: **Source variable** `luseoldweightsshort`](https://gitlab.com/TheochemGoettingen/RuNNer/-/blob/master/src-devel/fittingoptions.f90#L159)

---

### `use_omp_mkl` 

:material-chart-bell-curve-cumulative: **Mode 1:** :material-close:{ .no } **•** 
:custom-neural-network:                **Mode 2:** :material-check:{ .yes } **•** 
:material-head-cog-outline:            **Mode 3:** :material-close:{ .no }

Make use of the OpenMP threading in Intels MKL library. 

!!! Abstract "**Format:** `#!runner-config use_omp_mkl` (`logical`, default: `False`)"
    
    This keyword does not have any further options.

    [:material-code-tags: **Source variable** `lompmkl`](https://gitlab.com/TheochemGoettingen/RuNNer/-/blob/master/src-devel/globaloptions.f90#L142)

---

### `use_short_forces` ++

:material-chart-bell-curve-cumulative: **Mode 1:** :material-check:{ .yes } **•** 
:custom-neural-network:                **Mode 2:** :material-check:{ .yes } **•** 
:material-head-cog-outline:            **Mode 3:** :material-close:{ .no }

Use forces for fitting the short range NN weights. In 
[`#!runner-config runner_mode`](/runner/reference/keywords/#runner_mode) 1, the 
files `trainforces.data`, `testforces.data`, `trainforcese.data` and
`testforcese.data` are written. 
In 
[`#!runner-config runner_mode`](/runner/reference/keywords/#runner_mode) 2, 
these files are needed to use the short range forces for optimizing the 
short range weights. However, if the training data is different, the file 
`scaling.data` changes and either one of the keywords
[`#!runner-config scale_symmetry_functions`](/runner/reference/keywords/#scale_symmetry_functions)
or
[`#!runner-config center_symmetry_functions`](/runner/reference/keywords/#center_symmetry_functions)
is used, the RMSE will be different.

!!! Abstract "**Format:** `#!runner-config use_short_forces` (`logical`, default: `False`)"
    
    This keyword does not have any further options.

    [:material-code-tags: **Source variable** `luseforces`](https://gitlab.com/TheochemGoettingen/RuNNer/-/blob/master/src-devel/globaloptions.f90#L146)

---

### `use_short_nn` ++

:material-chart-bell-curve-cumulative: **Mode 1:** :material-check:{ .yes } **•** 
:custom-neural-network:                **Mode 2:** :material-check:{ .yes } **•** 
:material-head-cog-outline:            **Mode 3:** :material-check:{ .yes }

Use the a short range NN. Whether an atomic or pair-based energy expression is
used is determined via the keyword
[`#!runner-config nn_type_short`](/runner/reference/keywords/#nn_type_short).

!!! Abstract "**Format:** `#!runner-config use_short_nn` (`logical`, default: `False`)"
    
    This keyword does not have any further options.

    [:material-code-tags: **Source variable** `lshort`](https://gitlab.com/TheochemGoettingen/RuNNer/-/blob/master/src-devel/nnflags.f90#L34)

---

### `use_systematic_weights_electrostatic` 

:material-chart-bell-curve-cumulative: **Mode 1:** :material-close:{ .no } **•** 
:custom-neural-network:                **Mode 2:** :material-check:{ .yes } **•** 
:material-head-cog-outline:            **Mode 3:** :material-close:{ .no }

Overwrite the randomly initialized weights of the electrostatic NNs with 
systematically calculated weights. The weights are evenly distributed over
the interval between the minimum and maximum of the weights after the random
initialization.

!!! Abstract "**Format:** `#!runner-config use_systematic_weights_electrostatic` (`logical`, default: `False`)"
    
    This keyword does not have any further options.

    [:material-code-tags: **Source variable** `lsysweightse`](https://gitlab.com/TheochemGoettingen/RuNNer/-/blob/master/src-devel/fittingoptions.f90#L158)

---

### `use_systematic_weights_short` 

:material-chart-bell-curve-cumulative: **Mode 1:** :material-close:{ .no } **•** 
:custom-neural-network:                **Mode 2:** :material-check:{ .yes } **•** 
:material-head-cog-outline:            **Mode 3:** :material-close:{ .no }

Overwrite the randomly initialized weights of the short-range and pairwise NNs
with systematically calculated weights. The weights are evenly distributed over
the interval between the minimum and maximum of the weights after the random
initialization.

!!! Abstract "**Format:** `#!runner-config use_systematic_weights_short` (`logical`, default: `False`)"
    
    This keyword does not have any further options.

    [:material-code-tags: **Source variable** `lsysweightse`](https://gitlab.com/TheochemGoettingen/RuNNer/-/blob/master/src-devel/fittingoptions.f90#L157)

---

### `use_vdw` *

Switch dispersion corrections in RuNNer mode 3 on or off. If this
keyword is used the following keywords need to be given as well:

-   1: `vdw_coefficient` for each unique pair of elements in the data
    set.

-   2: `vdw_cutoff`,

-   3: `vdw_radius` for each element in the data set,

-   4: `vdw_screening`,

-   5: `vdw_type`.

Source variable: `lvdw` (logical, default = F)

---

### `vdw_coefficient` *

`vdw_coefficient` `element1` `element2` `value`

where `element1` and `element2` are character strings and `value` is a
real number.
Specification of the pairwise $C_{6ij}$ dispersion coefficients.
Mandatory keyword if the keyword `use_vdw` is used.

-   $\texttt{vdw\_type}=1$ (Tkatchenko-Scheffler method): a coefficient
    must be provided for each unique element pair in the data set.

-   $\texttt{vdw\_type}=2$ (Grimme DFT-D2 correction): a coefficient
    must be provided for each same-element pair in the data set.

<!-- Format example: `vdw_coefficient C H 22.54886`. The unit must be
$[\mathrm{E}_{\mathrm{h}}a\,_0^6\,\text{atom}^{-1}]$.\ -->
Source array: `vdw_coefficients(nelem, nelem)` (real, default = None)

---

### `vdw_cutoff` *

where `value` is a real number.
Specification of the cutoff radius to be used in the calculation of the
dispersion correction. This keyword must be used if the keyword
`use_vdw` is used. Format example: `vdw_cutoff 94.5`. The unit should be
$[a_0]$.\
Source variable: `vdw_cutoff` (real, default = 0.0)

---

### `vdw_radius` *

`vdw_radius element value`

where `element` is a character string and `value` is a real number.\
Specification of the van der Waals radius of `element`. This keyword
must be used for each element if the keyword `use_vdw` is used. Format
example: `vdw_radius C 0.76836936`. The unit should be $[a_0]$.\
Source array: `vdw_radius(nelem)` (real, default = None)

---

### `vdw_screening` *

:material-chart-bell-curve-cumulative: **Mode 1:** :material-check:{ .yes } **•** 
:custom-neural-network:                **Mode 2:** :material-close:{ .no } **•** 
:material-head-cog-outline:            **Mode 3:** :material-check:{ .yes }

Specification of the shape parameters of the Fermi-screening function
in the employed DFT-D2 dispersion correction expression.

!!! Abstract "**Format:** `#!runner-config vdw_screening s_6 d s_R`"

    **`#!runner-config s_6`: (`real`, `default` 0.0)**
    :   The global scaling parameter $s_6$ in the screening function.

    **`#!runner-config d`: (`real`, `default` 0.0)**
    :   The exchange-correlation functional dependent damping parameter. More 
        information can be found in the theory section.
    
    **`#!runner-config s_R`: (`real`, `default` 0.0)**
    :   Range separation parameter.

    [:material-code-tags: **Source array** `vdw_screening(3)`](https://gitlab.com/TheochemGoettingen/RuNNer/-/blob/master/src-devel/predictionoptions.f90#L27)

#### Example

```runner-config
vdw_screening 1.0 20.0 0.94
```

The parameters are dimensionless.

!!! Warning "Hint"
    This keyword must be used if the keyword 
    [`#!runner-config use_vdw`](/runner/reference/keywords/#use_vdw) is used.

---

### `vdw_type` *

:material-chart-bell-curve-cumulative: **Mode 1:** :material-check:{ .yes } **•** 
:custom-neural-network:                **Mode 2:** :material-check:{ .yes } **•** 
:material-head-cog-outline:            **Mode 3:** :material-check:{ .yes }

Specification of the type of dispersion correction to be employed.

!!! Abstract "**Format:** `#!runner-config vdw_type i0`"

    **`#!runner-config i0`: (`integer`, `default` 0)**
    :   Type of vdW correction scheme. Can be one of:
        
        * **`1`**: 
        :    Simple, environment-dependent dispersion correction inspired by
            the Tkatchenko-Scheffler scheme.
            
        * **`2`**: 
        :    Grimme DFT-D2 correction.
        
        * **`3`**: 
        :    Grimme DFT-D3 correction.

    [:material-code-tags: **Source variable** `nn_type_vdw`](https://gitlab.com/TheochemGoettingen/RuNNer/-/blob/master/src-devel/predictionoptions.f90#L25)

!!! Warning "Hint"
    This keyword must be used if the keyword 
    [`#!runner-config use_vdw`](/runner/reference/keywords/#use_vdw) is used.

---

### `weight_analysis` 

:material-chart-bell-curve-cumulative: **Mode 1:** :material-close:{ .no } **•** 
:custom-neural-network:                **Mode 2:** :material-check:{ .yes } **•** 
:material-head-cog-outline:            **Mode 3:** :material-close:{ .no }

Print analysis of weights in 
[`#!runner-config runner_mode`](/runner/reference/keywords/#runner_mode) 2.

!!! Abstract "**Format:** `#!runner-config weight_analysis` (`logical`, default: `False`)"
    
    This keyword does not have any further options.
        
    [:material-code-tags: **Source variable** `lweightanalysis`](https://gitlab.com/TheochemGoettingen/RuNNer/-/blob/master/src-devel/fittingoptions.f90#L196)

---

### `weight_constraint` +

:material-chart-bell-curve-cumulative: **Mode 1:** :material-close:{ .no } **•** 
:custom-neural-network:                **Mode 2:** :material-check:{ .yes } **•** 
:material-head-cog-outline:            **Mode 3:** :material-close:{ .no }

Specify, which short range NN weights shall not be optimized. This keyword can
be used several times to specify various groups of weights. The order is 
relevant in that each specification will be overwritten by the following one. 
Using the options `free` and `fixed` various constraints can be set. 

!!! Abstract "**Format:** `#!runner-config weight_constraint element selection [counts] type`"

    **`#!runner-config element`: (`string`)**
    :   The periodic table symbol of the element whose atomic NN the constraint 
        applies to. 
        
    **`#!runner-config selection [id1 id2 ...]`: (`string`, [`integers`])**
    :   Which part of the NN the constraint shall be applied to. Can be one of:
    
        * **`#!runner-config all`:**
        :   All short-range NN weights of the chosen `#!runner-config element`.
        * **`#!runner-config interlayer layer layer`: (`integers`)**
        :   All short-range NN weights between layers `layer` 1 and `layer` 2.
        * **`#!runner-config bias layer node`: (`integers`)**
        :   Bias weight at node `node` in layer `layer`.
        * **`#!runner-config weight layer node layer node`: (`integers`)**
        :   Weight connecting `node` 1 in `layer` 1 with `node` 
            2 in `layer` 2. 
        * **`#!runner-config node layer node`: (`integers`)**
        :   All short-range weights connected to node `node` in layer `layer`.
    
    **`#!runner-config type`: (`string`)**
    :   The kind of constraint that will be applied. Can be one of:
        
        * `free`:
        :   Fix all short-range NN weights of the chosen element _except for_ 
            the ones in `selection`. 
        
        * `fixed`:
        :   Keep `selection` fixed while optimizing all other weights of the 
            chosen `element`'s atomic electrostatic NN.
        
    [:material-code-tags: **Source array** wconstraint(maxnum_weights, nelem)](https://gitlab.com/TheochemGoettingen/RuNNer/-/blob/master/src-devel/getfixedweights.f90)

#### Examples

The following groups of weights are accepted:

1. Fix all short-range NN weights for the NN of oxygen:

    ```runner-config
    weight_constraint O all fixed 
    ```

2. Optimize all short-range NN weights for the NN of oxygen connecting
the nodes in layers 1 and 2:

    ```runner-config 
    weight_constraint O interlayer 1 2 free
    ```

3. Optimize the short-range bias weight at node 2 in layer 1 for the NN
of zinc:
    ```runner-config
    weight_constraint Zn bias 1 2 free
    ```

4. Optimize the short-range NN weight connecting node 3 in layer 1 with
node 4 in layer 2 for the NN of zinc:

    ```runner-config
    weight_constraint Zn weight 1 3 2 4 free
    ```

5. Optimize all short-range NN weights connecting node 1 in layer 2 with
all nodes in the previous and subsequent layers for the NN of zinc:

    ```runner-config
    weight_constraint Zn node 2 1 free
    ```

!!! Warning "Hint"
    This keyword requires the use of the keyword `fix_weights`.

---

### `weighte_constraint` +

:material-chart-bell-curve-cumulative: **Mode 1:** :material-close:{ .no } **•** 
:custom-neural-network:                **Mode 2:** :material-check:{ .yes } **•** 
:material-head-cog-outline:            **Mode 3:** :material-close:{ .no }

Specify, which electrostatic NN weights shall not be optimized. This keyword can
be used several times to specify various groups of weights. The order is 
relevant in that each specification will be overwritten by the following one. 
Using the options `free` and `fixed` various constraints can be set. 

!!! Abstract "**Format:** `#!runner-config weighte_constraint element selection [counts] type`"

    **`#!runner-config element`: (`string`)**
    :   The periodic table symbol of the element whose atomic NN the constraint 
        applies to. 
        
    **`#!runner-config selection [id1 id2 ...]`: (`string`, [`integers`])**
    :   Which part of the NN the constraint shall be applied to. Can be one of:
    
        * **`#!runner-config all`:**
        :   All electrostatic NN weights of the chosen `#!runner-config element`.
        * **`#!runner-config interlayer layer layer`: (`integers`)**
        :   All electrostatic NN weights between layers `layer` 1 and `layer` 2.
        * **`#!runner-config bias layer node`: (`integers`)**
        :   Bias weight at node `node` in layer `layer`.
        * **`#!runner-config weight layer node layer node`: (`integers`)**
        :   Weight connecting `node` 1 in `layer` 1 with `node` 
            2 in `layer` 2. 
        * **`#!runner-config node layer node`: (`integers`)**
        :   All electrostatic weights connected to node `node` in layer `layer`.
    
    **`#!runner-config type`: (`string`)**
    :   The kind of constraint that will be applied. Can be one of:
        
        * `free`:
        :   Fix all electrostatic NN weights of the chosen element _except for_ 
            the ones in `selection`. 
        
        * `fixed`:
        :   Keep `selection` fixed while optimizing all other weights of the 
            chosen `element`'s atomic electrostatic NN.
        
    [:material-code-tags: **Source array** wconstrainte(maxnum_weights_elec, nelem)](https://gitlab.com/TheochemGoettingen/RuNNer/-/blob/master/src-devel/getfixedweights.f90)

#### Examples

The following groups of weights are accepted:

1. Fix all electrostatic NN weights for the NN of oxygen:

    ```runner-config
    weighte_constraint O all fixed 
    ```

2. Optimize all electrostatic NN weights for the NN of oxygen connecting
the nodes in layers 1 and 2:

    ```runner-config 
    weighte_constraint O interlayer 1 2 free
    ```

3. Optimize the electrostatic bias weight at node 2 in layer 1 for the NN
of zinc:
    ```runner-config
    weighte_constraint Zn bias 1 2 free
    ```

4. Optimize the electrostatic NN weight connecting node 3 in layer 1 with
node 4 in layer 2 for the NN of zinc:

    ```runner-config
    weighte_constraint Zn weight 1 3 2 4 free
    ```

5. Optimize all electrostatic NN weights connecting node 1 in layer 2 with
all nodes in the previous and subsequent layers for the NN of zinc:

    ```runner-config
    weighte_constraint Zn node 2 1 free
    ```

!!! Warning "Hint"
    This keyword requires the use of the keyword 
    [`#!runner-config fix_weights`](/runner/reference/keywords/#fix_weights).

---

### `weights_max` ++

:material-chart-bell-curve-cumulative: **Mode 1:** :material-close:{ .no } **•** 
:custom-neural-network:                **Mode 2:** :material-check:{ .yes } **•** 
:material-head-cog-outline:            **Mode 3:** :material-close:{ .no }

Set an upper limit for the random initialization of the short-range NN weights. 

!!! Abstract "**Format:** `#!runner-config weights_max a0`"

    **`#!runner-config a0`: (`real`, default: `+1.0`)**
    :   This number defines the maximum value for initial random short range
        weights.

    [:material-code-tags: **Source variable** `weightse_max`](https://gitlab.com/TheochemGoettingen/RuNNer/-/blob/master/src-devel/fittingoptions.f90#L99)

!!! warning "Hint"
    This keyword is active only if an electrostatic NN is used, i.e. for 
    [`#!runner-config electrostatic_type`](/runner/reference/keywords/#electrostatic_type) 1.

---

### `weights_min` ++

:material-chart-bell-curve-cumulative: **Mode 1:** :material-close:{ .no } **•** 
:custom-neural-network:                **Mode 2:** :material-check:{ .yes } **•** 
:material-head-cog-outline:            **Mode 3:** :material-close:{ .no }

Set a lower limit for the random initialization of the short-range NN weights. 

!!! Abstract "**Format:** `#!runner-config weights_min a0`"

    **`#!runner-config a0`: (`real`, default: `-1.0`)**
    :   This number defines the minimum value for initial random short range
        weights.

    [:material-code-tags: **Source variable** `weights_min`](https://gitlab.com/TheochemGoettingen/RuNNer/-/blob/master/src-devel/fittingoptions.f90#L98)

---

### `weightse_max` ++

:material-chart-bell-curve-cumulative: **Mode 1:** :material-close:{ .no } **•** 
:custom-neural-network:                **Mode 2:** :material-check:{ .yes } **•** 
:material-head-cog-outline:            **Mode 3:** :material-close:{ .no }

Set an upper limit for the random initialization of the electrostatic NN weights. 

!!! Abstract "**Format:** `#!runner-config weightse_max a0`"

    **`#!runner-config a0`: (`real`, default: `+1.0`)**
    :   This number defines the maximum value for initial random electrostatic
        weights. This keyword is active only if an electrostatic
        NN is used, i.e. for `#!runner-config electrostatic_type` 1.

    [:material-code-tags: **Source variable** `weightse_max`](https://gitlab.com/TheochemGoettingen/RuNNer/-/blob/master/src-devel/fittingoptions.f90#L103)

---

### `weightse_min` ++

:material-chart-bell-curve-cumulative: **Mode 1:** :material-close:{ .no } **•** 
:custom-neural-network:                **Mode 2:** :material-check:{ .yes } **•** 
:material-head-cog-outline:            **Mode 3:** :material-close:{ .no }

Set a lower limit for the random initialization of the electrostatic NN weights. 

!!! Abstract "**Format:** `#!runner-config weightse_min a0`"

    **`#!runner-config a0`: (`real`, default: `-1.0`)**
    :   This number defines the minimum value for initial random electrostatic
        weights. This keyword is active only if an electrostatic
        NN is used, i.e. for `#!runner-config electrostatic_type` 1.

    [:material-code-tags: **Source variable** `weightse_min`](https://gitlab.com/TheochemGoettingen/RuNNer/-/blob/master/src-devel/fittingoptions.f90#L102)

---

### `write_fit_statistics` 

:material-chart-bell-curve-cumulative: **Mode 1:** :material-close:{ .no } **•** 
:custom-neural-network:                **Mode 2:** :material-check:{ .yes } **•** 
:material-head-cog-outline:            **Mode 3:** :material-close:{ .no }

!!! Abstract "**Format:** `#!runner-config write_fit_statistics` (`logical`, default: `False`)"
    
    This keyword does not have any further options.
    
    [:material-code-tags: **Source variable** `lfitstats`](https://gitlab.com/TheochemGoettingen/RuNNer/-/blob/master/src-devel/fittingoptions.f90#L177)

---

### `write_pdb` -- {#write_pdb} 

This is an outdated keyword, which is no longer supported.

---

### `write_pov` -- {#write_pov} 

This is an outdated keyword, which is no longer supported.

---

### `write_pwscf` -- {#write_pwscf} 

This is an outdated keyword, which is no longer supported.

---

### `write_temporary_weights` *

:material-chart-bell-curve-cumulative: **Mode 1:** :material-close:{ .no } **•** 
:custom-neural-network:                **Mode 2:** :material-check:{ .yes } **•** 
:material-head-cog-outline:            **Mode 3:** :material-close:{ .no }

Write temporary weights after each data block defined by `points_in_memory` to
files `tmpweights.XXX.out` and `tmpweightse.XXX.out`. XXX is the nuclear charge.
This option is active only in `#!runner-config runner_mode` 2 and meant to store 
intermediate weights in very long epochs.

!!! Abstract "**Format:** `#!runner-config write_temporary_weights` (`logical`, default: `False`)"
    
    This keyword does not have any further options.
    
    [:material-code-tags: **Source variable** `lwritetmpweights`](https://gitlab.com/TheochemGoettingen/RuNNer/-/blob/master/src-devel/fittingoptions.f90#L142)

---

### `write_traincharges` ++

:material-chart-bell-curve-cumulative: **Mode 1:** :material-close:{ .no } **•** 
:custom-neural-network:                **Mode 2:** :material-check:{ .yes } **•** 
:material-head-cog-outline:            **Mode 3:** :material-close:{ .no }

In [`#!runner-config runner_mode`](/runner/reference/keywords/#runner_mode) 2 
write the files `traincharges.YYYYYY.out` and `testcharges.YYYYYY.out` for each
epoch. `YYYYYY` is the number of the epoch. 
The files are written only if the electrostatic NN is used in case of 
[`#!runner-config electrostatic_type`](/runner/reference/keywords/#electrostatic_type)
1. 
This can generate many large files and is intended for a detailed analysis of 
the fits.

!!! Abstract "**Format:** `#!runner-config write_traincharges` (`logical`, default: `False`)"
    
    This keyword does not have any further options.
    
    [:material-code-tags: **Source variable** `lwritetraincharges`](https://gitlab.com/TheochemGoettingen/RuNNer/-/blob/master/src-devel/fittingoptions.f90#L172)

---

### `write_trainforces` ++

:material-chart-bell-curve-cumulative: **Mode 1:** :material-close:{ .no } **•** 
:custom-neural-network:                **Mode 2:** :material-check:{ .yes } **•** 
:material-head-cog-outline:            **Mode 3:** :material-close:{ .no }

In [`#!runner-config runner_mode`](/runner/reference/keywords/#runner_mode) 2 
write the files 
`trainforces.YYYYYY.out` 
and 
`testforces.YYYYYY.out` 
for each epoch. `YYYYYY` is the number of the epoch. The
files are written only if the short range NN is used and if the forces are used
for training (keyword 
[`#!runner-config use_short_forces`](/runner/reference/keywords/#use_short_forces)). 
This can generate  many large files and is intended for a detailed analysis of
the fits.

!!! Abstract "**Format:** `#!runner-config write_trainforces` (`logical`, default: `False`)"
    
    This keyword does not have any further options.
    
    [:material-code-tags: **Source variable** `lwritetrainforces`](https://gitlab.com/TheochemGoettingen/RuNNer/-/blob/master/src-devel/fittingoptions.f90#L173)

---

### `write_symfunctions` -

:material-chart-bell-curve-cumulative: **Mode 1:** :material-close:{ .no } **•** 
:custom-neural-network:                **Mode 2:** :material-close:{ .no } **•** 
:material-head-cog-outline:            **Mode 3:** :material-check:{ .yes }

In [`#!runner-config runner_mode`](/runner/reference/keywords/#runner_mode) 
3 write the file `symfunctions.out` containing 
the unscaled and non-centered symmetry functions values of all atoms in the 
predicted structure.
The format is the same as for the files 
[`function.data`](/runner/reference/files/#functiondata) 
and 
[`testing.data`](/runner/reference/files/#testingdata)
with the exception that no energies are given. The purpose of this file is code
debugging.

!!! Abstract "**Format:** `#!runner-config write_symfunctions` (`logical`, default: `False`)"
    
    This keyword does not have any further options.
    
    [:material-code-tags: **Source variable** `lwritesymfunctions`](https://gitlab.com/TheochemGoettingen/RuNNer/-/blob/master/src-devel/predictionoptions.f90#L40)

---

### `write_binding_energy_only` +

:material-chart-bell-curve-cumulative: **Mode 1:** :material-close:{ .no } **•** 
:custom-neural-network:                **Mode 2:** :material-check:{ .yes } **•** 
:material-head-cog-outline:            **Mode 3:** :material-close:{ .no }

In 
[`#!runner-config runner_mode`](/runner/reference/keywords/#runner_mode) 2 
write only the binding energy instead of total energies in 
the files 
[`trainpoints.XXXXXX.out`](/runner/reference/files/#trainpointsxxxxxxout)
and
[`testpoints.XXXXXX.out`](/runner/reference/files/#testpointsxxxxxxout) 
for each epoch.

!!! Abstract "**Format:** `#!runner-config write_binding_energy_only` (`logical`, default: `False`)"
    
    This keyword does not have any further options.
    
    [:material-code-tags: **Source variable** `lbindingenergyonly`](https://gitlab.com/TheochemGoettingen/RuNNer/-/blob/master/src-devel/globaloptions.f90#L150)

---

### `write_trainpoints` ++

:material-chart-bell-curve-cumulative: **Mode 1:** :material-close:{ .no } **•** 
:custom-neural-network:                **Mode 2:** :material-check:{ .yes } **•** 
:material-head-cog-outline:            **Mode 3:** :material-close:{ .no }

In 
[`#!runner-config runner_mode`](/runner/reference/keywords/#runner_mode) 2 
write the files 
[`trainpoints.XXXXXX.out`](/runner/reference/files/#trainpointsxxxxxxout)
and
[`testpoints.XXXXXX.out`](/runner/reference/files/#testpointsxxxxxxout) 
for each epoch. `XXXXXX` is the number of the
epoch. The files are written only if the short range NN is used. This
can generate many large files and is intended for a detailed analysis of
the fits.

!!! Abstract "**Format:** `#!runner-config write_trainpoints` (`logical`, default: `False`)"
    
    This keyword does not have any further options.
    
    [:material-code-tags: **Source variable** `lwritetrainpoints`](https://gitlab.com/TheochemGoettingen/RuNNer/-/blob/master/src-devel/fittingoptions.f90#L171)

---

### `write_unformatted` 

:material-chart-bell-curve-cumulative: **Mode 1:** :material-check:{ .yes } **•** 
:custom-neural-network:                **Mode 2:** :material-check:{ .yes } **•** 
:material-head-cog-outline:            **Mode 3:** :material-check:{ .yes }

Write output without any formatting.

!!! Abstract "**Format:** `#!runner-config write_unformatted` (`logical`, default: `False`)"
    
    This keyword does not have any further options.
    
    [:material-code-tags: **Source variable** `lwriteunformatted`](https://gitlab.com/TheochemGoettingen/RuNNer/-/blob/master/src-devel/globaloptions.f90#L129)

---

### `write_weights_epoch` ++

:material-chart-bell-curve-cumulative: **Mode 1:** :material-close:{ .no } **•** 
:custom-neural-network:                **Mode 2:** :material-check:{ .yes } **•** 
:material-head-cog-outline:            **Mode 3:** :material-close:{ .no }

Determine in which epochs the files `YYYYYY.short.XXX.out` and 
`YYYYYY.elec.XXX.out` are written. 

!!! Abstract "**Format:** `#!runner-config write_weights_epoch i1`"

    **`#!runner-config i1`: (`integer`, default: `1`)**
    :   Frequency of weight output.

    [:material-code-tags: **Source variable** `iwriteweight`](https://gitlab.com/TheochemGoettingen/RuNNer/-/blob/master/src-devel/fittingoptions.f90#L36)

#### Example

With 

```runner-config 
write_weights_epoch 5
``` 

the weight files will be written for every 5th epoch only.

---

<!-- The additional anchor tag is necessary so that the -- is not interpreted as 
oart of the header -->

### `write_xyz` -- {#write_xyz} 

This is an outdated keyword, which is no longer supported.
