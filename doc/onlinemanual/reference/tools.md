### Overview

The purpose of the `RuNNer tools` is manifold, from manipulating
structures in the `input.data` format, via the analysis of different
properties and quantities (like the composition of data sets and
plotting symmetry functions) to file conversion. For each tool there is
a separate folder containing a `Readme.txt` file describing the purpose
of the tool, its input and output as well as its usage and input flags.
Often the tools are written in a rather "quick and dirty" style, so a
certain amount of caution in using them is appropriate. Users should
also be fully aware of the purpose of a given tool, e.g., some tools for
structure manipulation are not intended to generate physically
meaningful structures, but are meant to be used in extended scripts for
preparing structures for visualization only.

### RuNNerUC

The RuNNer Universal Converter can convert different structure file formats.
It can read RuNNer input.data, LAMMPS input and trajectory files, FHIaims
geometry.in, output, and trajectories, VASP poscar and outcar, and xyz files.
It can write RuNNer input.data, LAMMPS input and trajectory files, FHIaims
geometry.in, VASP poscar, and xyz files.

### RuNNerActiveLearn

RuNNerActiveLearn sets up and analyzes LAMMPS simulations to find structures
for which the underlying HDNNP shows significant inter- and extrapolation errors.
The aim of the algorithm is to create a selection of uncorrelated structures which are missing
in the current reference data set of the HDNNP. These structures are written to the
input.data-add file and have to be recalculated by the reference method to improve the
reliability of the HDNNP. RuNNerActiveLearn automatically adjusts some required thresholds in
this self-learning procedure and provides reasonable recommendations for the others to sample
the configuration space efficiently. Interpolation errors are detected by a comparison of the
results of two different HDNNPs. Statistics of the extrapolated structures are provided as
well. The program package requires binaries of LAMMPS including the n2p2 libraries and RuNNer.
                                                                                              
When using RuNNerActiveLearn, please cite:

* [M. Eckhoff, J. Behler, _npj Comput. Mater._ **2021**, _7_, 170](https://doi.org/10.1038/s41524-021-00636-z).
* [M. Eckhoff, J. Behler, _J. Chem. Theory Comput._ **2019**, _15_, 3793--3809](https://doi.org/10.1021/acs.jctc.8b01288).

