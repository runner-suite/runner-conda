`RuNNer` uses atomic units internally.

* **Length**: $a_0$ (Bohr radius)
* **Charge**: $e$ (charge of an electron $\rightarrow 1$ in RuNNer units)
* **Spin**: $\hbar$ (spin of an electron $\rightarrow \tfrac{1}{2}$ in RuNNer units)
* **Energy**: $E_\mathrm{h}$ (Hartree)

$$
    1\,E_\mathrm{h} 
    = \frac{\hbar^2}{m_e a_0^2}
    = \frac{e^2}{4\pi \epsilon_0 a_0}
$$

* **Force**: $E_\mathrm{h}/a_0$
* **Stress**: $E_\mathrm{h}/a_0^3$

While in the standard output file of `#!runner-config runner_mode` 2 the unit 
can be switched to eV for energies and eV/$E_\mathrm{h}$ for forces by an input keyword
([`#!runner-config fitting_unit`](/runner/reference/keywords/#fitting_unit)),
please note that all other input and output files strictly use these atomic
units, e.g. for the atomic positions, forces, and lattice vectors. According to
experience, unit errors belong to the most frequent errors made by beginners
using `RuNNer`.

Please also note that internally `RuNNer` uses energies normalized per atom and
depending on the setting of the 
[`#!runner-config remove_atom_energies`](/runner/reference/keywords/#remove_atom_energies) 
keyword these energies may not be total energies but atomic "binding energies".
