## Overview

The typical use of `RuNNer` consists of three steps called modes:

* `mode 1`: Transformation of the Cartesian coordinates to symmetry functions,
    splitting of the reference data into a training and a test set, and the
    conversion of total energies in energies per atom (and removing the free
        atom energies, if requested).

* `mode 2`: Analysis of the training and test sets and determination of the NN
    parameters in an iterative optimization process.

* `mode 3`: Application of the potential to predict energies, forces, stress
    tensors and charges. Currently `RuNNer` is mainly used for calculating these
    properties for multiple given structures. `RuNNer` is not a molecular
    dynamics code. For MD, a compatible implementation in LAMMPS is available,
    which can use NN potentials constructed with `RuNNer`.

## Mode 1: Generation of the Symmetry Functions

In mode 1 the symmetry functions are generated from the structural information
provided in the `input.data` file. The generation of the symmetry functions is
not parallelized as even for different fits the generation has to be done only
once with reasonable CPU time requirements. It needs to be repeated only if the
symmetry functions or the splitting into the training and test sets (cf. keyword
[`#!runner-config test_fraction`](/runner/reference/keywords/#test_fraction)) 
changes.

!!! tip "Relevant files to get you started"
    Only two files are needed by RuNNer in order to start calculating symmetry 
    functions:
    
    * [`input.nn`](/runner/reference/files/#inputnn)
    * [`input.data`](/runner/reference/files/#inputdata)

## Mode 2: Fitting

In mode 2 the weight parameters of the atomic neural networks are determined.

## Mode 3: Application of the Neural Network Potential

In mode 3 the finalized NNP can be used to predict the energies and forces of a
single structure given in the `input.data` file. If there is more than one
structure included in the `input.data` file, all structures in `input.data` are
used.
