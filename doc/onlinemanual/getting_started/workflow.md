# The work flow of constructing HDNNPs

This chapter gives an overview about the construction of HDNNPs in different 
generations. Starting in `version 2.0` or later, a new keyword 
[`#!runner-config nnp_generation i0`](/runner/reference/keywords/#nnp_generation)
or [`#!runner-config nnp_gen i0`](/runner/reference/keywords/#nnp_gen)
can be used to specify which generation of HDNNPs is constructed.
Here, `#!runner-config i0` can be either `2` for 2G-, `3` for 3G-, or `4` for 
4G-HDNNPs. RuNNer is still fully backward compatible 
by specifying 
[`#!runner-config nn_type_short`](/runner/reference/keywords/#nn_type_short) 
for 2G-HDNNPs. However, using new keywords is recommended. 

The construction of 3G- and 4G-HDNNPs is a two-step procedure and users have to
construct the charge fit first by specifying a keyword 
[`#!runner-config use_electrostatics`](/runner/reference/keywords/#use_electrostatics). 
After finishing the charge fit and renaming the weight files and possible 
hardness files for different elements, users can then perform the short-range
fit by specifying both
[`#!runner-config use_electrostatics`](/runner/reference/keywords/#use_electrostatics)
and [`#!runner-config use_short_nn`](/runner/reference/keywords/#use_short_nn)
in the [`input.nn`](/runner/reference/files/#inputnn) file. Examples including 
[`input.nn`](/runner/reference/files/#inputnn) and 
[`input.data`](/runner/reference/files/#inputdata) for constructing 3G and 
4G-HDNNP are provided in the 
[`Examples/`](https://gitlab.gwdg.de/runner/runner/Examples) directory of the 
RuNNer source code.

## 2G-HDNNP

!!! tip
    2G-HDNNPs are constructed by specifying 
    [`#!runner-config nnp_generation 2`](/runner/reference/keywords/#nnp_generation)
    or
    [`#!runner-config nnp_gen 2`](/runner/reference/keywords/#nnp_gen)
    with other necessary keywords in 
    [`input.nn`](/runner/reference/files/#inputnn).

1. First, users have to perform mode 1 for calculating the symmetry function 
   values and splitting the reference data set into a training and a testing set.
2. Next, users should perform mode 2 to train the short-range 
   atomic neural networks.
3. After training the atomic neural networks, users can utilize the novel NNP
   for the prediction of the total energy, forces and stress tensor of 
   configurations from [`input.data`](/runner/reference/files/#inputdata) by 
   renaming [`optweights.XXX.out`](/runner/reference/files/#optweightsxxxout) to 
   [`weights.XXX.data`](/runner/reference/files/#weightsxxxdata).

!!! note
    Magnetic high-dimensional neural network potentials (mHDNNPs) as constructed in
    [M. Eckhoff, J. Behler, _npj Comput. Mater._ **2021**, _7_, 170](https://doi.org/10.1038/s41524-021-00636-z)
    are 2G-HDNNPs which apply spin-dependent atom-centered symmetry functions (sACSFs).
    These sACSFs distinguish different types of magnetic as well as non-magnetic
    interactions. The aforementioned publication explains how to set up these sACSFs
    for a magnetic system. The atomic spins can be specified in the flexible format of
    the input.data file. Therefore, the information provided in each column needs to be
    specified explicitly for each structure after the keyword `begin`, for example,
    `begin x y z elem_symbol atom_charge atom_energy fx fy fz atom_spin`.

!!! note
    To construct a high-dimensional neural network for spin prediction (HDNNS) as in
    [M. Eckhoff, K. N. Lausch, P. E. Blöchl, J. Behler, _J. Chem. Phys._ **2020**, _153_, 164107](https://doi.org/10.1063/5.0021452),
    [`#!runner-config nnp_gen 2`](/runner/reference/keywords/#nnp_gen),
    [`#!runner-config use_electrostatics`](/runner/reference/keywords/#use_electrostatics),
    [`#!runner-config electrostatic_type 1`](/runner/reference/keywords/#electrostatic_type),
    and [`#!runner-config use_atom_spins`](/runner/reference/keywords/#use_atom_spins)
    have to be specified in [`input.nn`](/runner/reference/files/#inputnn), while
    [`#!runner-config use_short_nn`](/runner/reference/keywords/#use_short_nn)
    must not be activated. Further options need to be specified for the electrostatic
    NN instead of the short NN. The atomic spins can be specified in the flexible
    format of the input.data file. Only the absolute value of the atomic spins is used
    in the construction of the HDNNS.

## 3G-HDNNP

!!! tip 
    3G-HDNNPs are constructed by specifying 
    [`#!runner-config nnp_generation 3`](/runner/reference/keywords/#nnp_generation)
    or
    [`#!runner-config nnp_type_gen 3`](/runner/reference/keywords/#nnp_type_gen)
    with other necessary keywords in 
    [`input.nn`](/runner/reference/files/#inputnn).

1. First, the electrostatic NNP is constructed. To that end,
   users should perform mode 1 by setting 
   [`#!runner-config use_electrostatics`](/runner/reference/keywords/#use_electrostatics)
   for calculating the electrostatic symmetry function values and
   splitting the reference data set into a training and a testing set.  
2. Next, users should perform mode 2 to train the long-range electrostatics
   atomic neural networks.
3. After renaming [`optweightse.XXX.out`](/runner/reference/files/#optweightsexxxout) 
   to [`weightse.XXX.data`](/runner/reference/files/#weightsexxxdata), the 
   short-range NNP is trained. The keywords 
   [`#!runner-config use_short_nn`](/runner/reference/keywords/#use_short_nn) 
   and 
   [`#!runner-config use_electrostatics`](/runner/reference/keywords/#use_electrostatics)
   are switched on simultaneously and mode 1 is run again to obtain new training and testing
   data sets.
4. Then, users can perform mode 2 again to obtain another set of optimized
   atomic neural network weights for the short-range part.
5. Finally, users can predict total energy, forces and stress tensor including 
   short-range and electrostatic interactions of configurations from input.data
   by renaming [`optweights.XXX.out`](/runner/reference/files/#optweightsxxxout) 
   to [`weights.XXX.data`](/runner/reference/files/#weightsxxxdata).

## 4G-HDNNP

!!! tip 
    3G-HDNNPs are constructed by specifying 
    [`#!runner-config nnp_generation 4`](/runner/reference/keywords/#nnp_generation)
    or
    [`#!runner-config nnp_type_gen 4`](/runner/reference/keywords/#nnp_type_gen)
    and 
    [`#!runner-config use_electrostatics`](/runner/reference/keywords/#use_electrostatics) 
    together with other necessary keywords in 
    [`input.nn`](/runner/reference/files/#inputnn).

1. First, the electrostatic NNP is constructed. To that end,
   users should perform mode 1 for calculating the symmetry function values and
   splitting the reference data set into a training and a testing set.
2. Next, users should perform mode 2 to train the long-range electrostatics
   atomic neural networks.
3. After renaming [`optweightse.XXX.out`](/runner/reference/files/#optweightsexxxout) 
   to [`weightse.XXX.data`](/runner/reference/files/#weightsexxxdata), the 
   short-range NNP is trained. The keyword 
   [`#!runner-config use_short_nn`](/runner/reference/keywords/#use_short_nn) is
   switched on and mode 1 is performed again to obtain new training and testing
   data sets.
4. Then, users can perform mode 2 again to obtain another set of optimized
   atomic neural network weights for the short-range part.
5. Finally, users can predict total energy, forces and stress tensor including 
   short-range and electrostatic interactions of configurations from input.data
   by renaming [`optweights.XXX.out`](/runner/reference/files/#optweightsxxxout) 
   to [`weights.XXX.data`](/runner/reference/files/#weightsxxxdata).
