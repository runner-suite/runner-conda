RuNNer requires LAPACK libraries, which can be downloaded for free e.g. from
[www.netlib.org](http://www.netlib.org). After downloading and compiling LAPACK,
please proceed as follows:

## Compilation with **make**

!!! tip inline end "Friendly reminder of the setting for ifort"
    Please refer to FAQ `7.1.9` in the documentation.

1. Go to the source folder and edit the Makefile by inserting your compiler, e.g.
`ifort` or `gfortran`, (`g95` should not be used), your compiler options and the
library path to LAPACK (e.g. the `mkl` path or the path of the downloaded netlib
libraries). For the mpi version of `RuNNer` also some MPI settings are required
(only mode 3 of `RuNNer` is currently parallelized in a meaningful way). The
serial version should be the first to try. The fitting part of RuNNer is
currently parallelized by using openmp, just add `-f openmp` in FFLAGS of RuNNer
Makefile and compile with serial version. Before executing the executable of
`RuNNer`, type the command `export OMP_NUM_THREADS=N` for N cores. Since the
openmp parallelization is for the symmetry function calculation only, a
speed-up factor of 2 can be expected at most.

2. For compiling the serial version of `RuNNer` please type

    ``` bash
    make serial
    ```
    
    You should obtain a binary called `RuNNer.serial.x`.

3. For compiling the parallel version of `RuNNer` please type

    ``` bash
    make mpi
    ```
    
    You should obtain a binary called `RuNNer.mpi.x`.

## Compilation using **CMake**

For the version 2.0 or above, `RuNNer` uses **CMake** for the compilation. The 
procedure is the following:

1. Create the build subdirectory in the RuNNer main directory: 
    ```bash
    mkdir build
    ```
2. Move to the build directory and run CMake: 
    ```bash
    cd build
    cmake .. -DOPENMP=ON -DINTEL=ON -DDEBUG=OFF
    ```
3. run make: 
    ```bash
    make
    ```

CMake takes three parameter which can be either set to `ON` or `OFF`:

* **OPENMP**: Enables OpenMP parallelization.
* **INTEL** Uses ifort to compile RuNNer. If disabled, gfortran is used.
* ** DEBUG** Compile with debugging flags. Do not forget to disable this for
production runs, since performance is significantly reduced.

Now `RuNNer` is ready for use.
