# Welcome to RuNNer!

The RuNNer Neural Network Energy Representation is a Fortran-based framework for
the construction of Behler-Parrinello-type high-dimensional neural network
potentials.

!!! bug "Version breaking changes in RuNNer 1.3"
    Be careful, with the new version we have introduced some breaking changes:
    Some keywords are now deprecated and need to be removed from existing
      `input.nn` files:
      
      * `#!runner-config use_atom_charges`
      * `#!runner-config fixed_atom_charges`

!!! tip "Features"
    * **Construction** of very flexible neural network potentials for the
      representation of potential energies in high-dimensional systems:
        * unlimited number of degrees of freedom (atoms).
        * training data can be obtained from arbitrary electronic structure
          methods and codes.
        * training using energies and forces.
        * periodic and non-periodic systems.
        * several types of descriptors for the atomic environments, including
          atom-centered symmetry functions, are available.
        * several types of activation functions are available
        * arbitrary topology of the atomic neural networks.
        * provides energies and analytic derivatives (forces and stress tensor).
    * **Evaluation** of the NNPs in junction with a MD or MC run.
    
[Quickstart :material-rocket-launch:](getting_started/quickstart/){ .md-button .md-button--primary }
<!-- [Tutorials :fontawesome-solid-graduation-cap:](tutorials/){ .md-button .md-button--primary } -->
