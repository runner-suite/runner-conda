
We are currently in the process of transitioning to an online manual. Key
differences to the previous LaTeX-based manual are:

  * Markdown source files instead of LaTeX (equations are still written in
    LaTeX).
  * Automatic build upon pushing a contribution to the Git repository.
  * No missing keywords (as of Sep 2021), hopefully up-to-date information.
    Obsolete keywords have been removed. 
  * Interactive features of a modern web page, including a search bar.
  * _Coming soon:_ A versioning system to access the manual for a previous
    release version of RuNNer. 

### A Note on the Search Function 

When searching for a partial group of words in a RuNNer keyword, try putting
asterisks at the beginning and end of the search string, e.g.

```
*symfun_short*
```

Otherwise, the search will only pick up keywords that start with the current
search string.

## Technical overview 

The documentation is built with [Mkdocs](https://www.mkdocs.org) with a theme
provided through
[Material for Mkdocs](https://squidfunk.github.io/mkdocs-material). These
projects are Python packages which take Markdown files as input and produce a
ready-to-publish static web page. For convenience, this process has been
automated for the RuNNer manual.

### CI Pipeline

Whenever a user pushes a commit to the
[Gitlab repository of the project](https://gitlab.gwdg.de/runner/runner), an
automated
[Continuous Integration pipeline](https://docs.gitlab.com/ee/ci/introduction/)
(CI pipeline) is triggered. CI pipelines serve the purpose of checking and
testing code for correctness, formatting, and functionality before a new commit is
permanently added to the project's source code. **At the moment**, however, the
RuNNer CI pipeline does only build the documentation.

The CI pipeline is configured in the
[.gitlab-ci.yml](https://gitlab.gwdg.de/runner/runner/-/blob/master/.gitlab-ci.yml)
file in the project's root directory. Here are the relevant pieces:

```yaml
pages:
  stage: deploy
  image: python:3.8-buster
  before_script:
    - pip3 install mkdocs-material
    - python3 -m pip install git+https://gitlab.gwdg.de/aknoll/pymdown-lexer-runner.git
  script:
    - mkdocs build --strict --verbose
  artifacts:
    paths:
      - public
  only:
    - master
```

Let us go through this line by line:


  * For readability, this part of the CI pipeline is given the name `pages`.
  * CI pipelines are structured in stages, which are triggered one after the
    other. Typical stages are _build_ (compile the code), _test_  (test the
    functionality of the code), and _deploy_ (publish the code and everything
    pertaining to it). Consequently, the documentation is created in
    
    ```yaml
    stage: deploy
    ```
    
  * Each stage of a CI pipeline needs a place where it can be executed.
    For this reason, a clean Docker image is loaded as the first action of a
    CI stage. A Docker image can be conceptualized as a light-weight,
    operating system in a box. In our case, we want to build the documentation
    with Python, thus, a stripped-down version of Debian-buster is loaded with
    Python3.8 installed:

    ```yaml
    image: python:3.8-buster
    ```
    
  - The _before_script_ invokes a few commands that are necessary for the core
    _script_ part of the CI stage to succeed. In our case, we first install the
    _mkdocs-material_ page through Python's package manager _pip_. Second, we
    use pip to install the custom package
    [_pymdown-lexer-runner_](https://gitlab.gwdg.de/aknoll/pymdown-lexer-runner).
    This package has been created to highlight _input.data_ and _input.nn_
    source code on the web page. Feel free to take a look at the coloring of
    the keywords in the
    [reference section of the manual](https://runner.pages.gwdg.de/runner/reference/keywords/)
    for a demonstration.


    ```yaml
    before_script: [...]
    ```

  * _Script_ contains the main directives for this stage of the pipeline. 
    _Mkdocs_ is executed to build the documentation. The _--strict_ flag will 
    cause the code to issue errors instead of warnings, so mistakes can be 
    spotted early on. 
    
    ```yaml
    script: [...]
    ```
    
  * The documentation is hosted on servers provided by the GWDG as part of 
    their Gitlab installation (the _pages_ functionality). In order for the 
    freshly-built documentation to be displayed there, we need to somehow copy
    it out of the Docker container. Otherwise, it would be destroyed after
    completion of the pipeline. In other words, we want to remember it after
    extinction by leaving behind an artifact. The _mkdocs_ command in the 
    _script_ stage will have created a folder named _public/_ where all the
    necessary .html, .css, and .js files are stored for the static manual web
    page. Thus, we tell the pipeline to leave the _public//_ folder behind as
    an artifact: 
    
    ```yaml
    artifacts: [...]
    ```
    
  * Finally, we inform Gitlab when this stage of the pipeline is supposed to
    be executed. At the moment, the documentation will only be built whenever
    a contributor pushes to the master branch of the Gitlab project.

#### Structure of the documentation source code

The master file containing all settings for the documentation is 
[mkdocs.yml](https://gitlab.gwdg.de/runner/runner/-/blob/master/mkdocs.yml). See
the comments in the file for a description of all options. Most importantly, 
this file contains a _nav_ section:

```yaml
nav: 
  - Home: index.md
  - Overview: 
    - About RuNNer: overview/about.md
    - Contributors: overview/contributors.md
    - Citing RuNNer: overview/citation.md
  - Getting Started:
    - Compiling RuNNer: getting_started/compilation.md
    - A Note on Units: getting_started/units.md
[...]
```

As one can see, this section puts the Markdown files in their correct location
on the web page. All Markdown files are contained in the _docs/_ folder. The
structure of this folder is completely arbitrary as long as the correct path is
given within the _nav_ section. The sub-folders are just meant for increased
readability.

### Contributing

In principle, contributing to the new manual is as easy as editing the
existing Markdown files or adding new ones to the docs/ folder. When you push
the documentation to the master branch of the git repository, the CI pipeline
will automatically build the new documentation and serve it under the GWDG web
address. 

#### Inspecting local modifications before pushing

Before committing a change, it is often beneficial to inspect your changes
locally. For that purpose, Mkdocs offers a local development server which you
can easily start on your computer. 

  * Clone the Gitlab project of the RuNNer manual.
  
    ```bash
    git clone git@gitlab.gwdg.de:runner/runner.git
    ```
  
  * Navigate into the project's folder. As described above, the documentation is
    built with _mkdocs_, a Python package. Therefore, let us install mkdocs to
    the system. It is good practice in Python to install all packages of one
    project to a fresh virtual environment: 
    
    ```bash 
    python3 -m venv venv
    source venv/bin/activate
    ```
    
  * Install the mkdocs-material package:
  
    ```bash 
    [venv] pip3 install mkdocs-material
    ```
    
  * Start the server: 
  
    ```bash 
    [venv] mkdocs serve
    ```
  
  * Navigate to the displayed address (usually 127.0.0.1:8000) to see the
    documentation. Changes on the Markdown files automatically trigger a page
    update.

#### Making a change to an existing file

This is the easiest thing you can do. Simply open up the Markdown file in your
preferred text editor and edit, and save it. Next, upload your changes to the
master branch of the Gitlab project:

  * Inspect your changes locally as described in the previous section.
  * Execute the following steps:
    ```bash
    git add path_to_modified_file
    git commit -m "YOUR COMMIT MESSAGE HERE"
    git push
    ```

#### Adding a new chapter/section

Adding a new chapter simply means creating a new Markdown file in the _docs/_ 
folder and making it visible to mkdocs. The latter is achieved by adding an
entry to the _nav/_ section in the mkdocs.yml file.

For example, if one wanted to create a new chapter in the "Theory" section
called "Gradients", one would modify the mkdocs.yml file like this:

```yaml
nav: 
  [...Some parts left out for brevity...]
  - Tutorials:
    - Silicium Bulk: tutorials/si.md
  - Theoretical Background:
    - Energy: theory/energy.md
    - Forces: theory/forces.md
    - Stress: theory/stress.md
    - Gradients: theory/gradients.md    <-- YOUR NEW LINE HERE
    - 4G-HDNNPs: theory/4G.md
    - Calculation of the RMSE: theory/rmse.md
    - Atom-centered Symmetry Functions: theory/symmetryfunctions.md
  [...]
```

Please be aware that .yml-files are JSON-formatted, thus, indentation is
important!

#### Adding LaTeX equations

LaTeX equations can be inserted at any place in a Markdown file and will be
automatically compiled at build time. Nevertheless, you should be aware of these
pitfalls:

  * Inline equations start and end with a dollar sign:
    
    ```latex
    $x=2$
    ```
  
  * Equations that should be centered in a separate line need to be separated
    from surrounding text by a blank line and start and end with two dollar
    signs in a separate line. If the equation is supposed to be numbered, you
    should use `\begin{align}...\end{align}` (and not the _aligned_ or 
    _equation_ environments)
    
    ```latex
    Lorem ipsum

    $$
    \begin{align}
    x=2
    \end{align}
    $$

    Lorem ipsum
    ```

#### Adding new keywords

Keywords are an exceptional case that requires sticking to some additional
formatting scheme so that they are displayed on the webpage correctly. The
general template looks like this:

```markdown

### `some_new_keyword` 

:material-chart-bell-curve-cumulative: **Mode 1:** :material-close:{ .no } **•** 
:custom-neural-network:                **Mode 2:** :material-check:{ .yes } **•** 
:material-head-cog-outline:            **Mode 3:** :material-check:{ .yes }

Give an explanation of the keyword, possible linking to another keyword like this:
[`#!runner-config some_other_keyword`](/runner/reference/keywords/#some_other_keyword).

!!! Abstract "**Format:** `#!runner-config some_new_keyword argument1 argument2`"

    **`#!runner-config argument1`: (`type`)**
    :   Explain what the argument does. Integer arguments are usually referred to as i0...in, real type arguments are referred to as a0...an, and some keywords are recognized by the lexer like element, group, or node.     
      
    **`#!runner-config argument2`: (`type`)**
    :   Explain the next argument.

    [:material-code-tags: **Source variable** source_variable](https://gitlab.com/TheochemGoettingen/RuNNer/-/blob/master/src-devel/nnshort_pair.f90#L36)

#### Example

Give some examples.

!!! warning "Hint"
    This keyword only takes effect if 
    [`#!runner-config some_other_keyword`](/runner/reference/keywords/#some_other_keyword) 
    is set to 2.

---

```

Let us go through this line by line:

  * Every keywords is a level-three subsection on the page. In Markdown, this is
    indicated by three number signs. As the keyword is a syntactic phrase, mark
    it accordingly by putting it in apostrophes. 
    
    ```markdown
    ### `some_new_keyword` 
    ```
    
  * Directly below the keyword little emoticons help the reader identify the
    corresponding RuNNer mode. For each mode, the line starts with the symbol
    (surrounded by ::) and the Mode in bold font. There are two possible states:
    a green check sign (`:material-check:{ .yes }`) and a red cross 
    (`:material-close:{ .no }`). The additional tags in curly braces append a
    CSS class to the emoticon which simply changes the color. In other words,
    leaving out the { .no/.yes } statement would result in a black emoticon. 
    
    ```markdown
    :material-chart-bell-curve-cumulative: **Mode 1:** :material-close:{ .no } **•** 
    :custom-neural-network:                **Mode 2:** :material-check:{ .yes } **•** 
    :material-head-cog-outline:            **Mode 3:** :material-check:{ .yes }
    ```
    
  * A concise explanation of the keyword.
  * The keyword formatting section. These make use of the //Abstract//
    admonition to achieve nice formatting. The easiest way to create this
    section is to simply copy it from an existing keyword and modify the relevant
    bits.
  * An example section. Examples start with level-four headings.

#### Syntax highlighting

As one can see on all manual pages, the various keywords of RuNNer and their
options are highlighted by different colors. This is done by the
pymdown-lexer-runner package I (AK) am providing in my Git repository.
In Markdown, it is simply invoked like this:

```

For example, if you want to mention the keyword `#!runner-config use_vdw` do it
like this. If you want to give an example from an input.data file, you might do
something like this:

```runner-data

begin
comment ABC
[...]
end

```